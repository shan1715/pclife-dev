import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { DragulaModule } from 'ng2-dragula';
import { DemoComponent } from './app.component';
import { LoginComponent } from './login.component';
import {routing} from './app.routing';
import {HttpModule} from '@angular/http';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboarddataComponent} from './dashboarddata/dashboarddata.component';
import { ContentbarComponent } from './product-config/middlebar/contentbar/contentbar.component';
import { TopbarComponent } from './product-config/topbar/topbar.component';
import { FooterbarComponent } from './product-config/footerbar/footerbar.component';
import { MiddlebarComponent } from './product-config/middlebar/middlebar.component';
import { SidebarComponent } from './product-config/middlebar/sidebar/sidebar.component';
import { ProductConfigComponent } from "./product-config/product-config.component";
import { BootstrapModalComponent } from "./bootstrap-modal/bootstrap-modal.component";
import { PrompComponent } from "./bootstrap-modal/Modaldialog/promp.component";
import { ProductDialogueComponent } from "./bootstrap-modal/Productdialogue/productdialogue.component";
import { FielddialogueComponent } from "./bootstrap-modal/Fielddialogue/fielddialogue.component";
import { AlertComponent } from "./bootstrap-modal/alert/alert.component";
import { MasterFieldDialogComponent } from "./bootstrap-modal/master-field-dialog/master-field-dialog.component";
import { MasterdialogComponent } from "./bootstrap-modal/masterdialog/masterdialog.component";
import { BootstrapModalModule } from "ng2-bootstrap-modal";
import {UserComponent} from "./bootstrap-modal/user/user.component";
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
// import {ToolTipModule} from 'angular2-tooltip';
import {ReactiveFormsModule} from '@angular/forms';
import {HashLocationStrategy,LocationStrategy} from '@angular/common';
import {AppServices} from './app.service';
import {MasterComponent} from './master/master.component';
import { EditPenComponent } from "./bootstrap-modal/edit-pen/edit-pen.component";
import { ReviewComponent } from "./bootstrap-modal/Review/review.component";
import { ChangePasswordComponent } from "./bootstrap-modal/change-password/change-password.component";
import {PageProductCommentComponent} from './page-product-comment/page-product-comment.component';

import { SweetAlertService } from "./SweetAlertService/sweet-alert-service";

// import { Ng4LoadingSpinnerModule, Ng4LoadingSpinnerService  } from 'ng4-loading-spinner';
import { DatePipe } from '@angular/common';
import {AdministratorGroupComponent} from './administrator-group/administrator-group.component';
import { OperationalAnnuityComponent } from './operational-annuity/operational-annuity.component';
import { OperationalGroupComponent } from './operational-group/operational-group.component';
import { OperationalUnitlinksComponent } from './operational-unitlinks/operational-unitlinks.component';
import { RiskcoverageComponent } from './riskcoverage/riskcoverage.component';
import { OperationalUnderwritingComponent } from './operational-underwriting/operational-underwriting.component';
import { OperationalPolicyservicingComponent } from './operational-policyservicing/operational-policyservicing.component';
import { ProductFeesRatesComponent } from './product-fees-rates/product-fees-rates.component';
import { ProductFeesChargesComponent } from './product-fees-charges/product-fees-charges.component';
import { ProductFeesFinanceComponent } from './product-fees-finance/product-fees-finance.component';
import { AgencyComponent } from './agency/agency.component';
import { OperationsNewbusinessComponent } from './operations-newbusiness/operations-newbusiness.component';
import { ProgressbarModule } from 'ngx-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ProductcloneComponent } from './productclone/productclone.component';
import { TooltipModule } from 'ngx-bootstrap';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { DataentryCompletionComponent } from './dataentry-completion/dataentry-completion.component';
import { LookupComponent } from './lookup/lookup.component';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import {BusyModule, BusyConfig} from 'angular2-busy';
import { DataDownloadComponent } from "./bootstrap-modal/dataDownload/dataDownload.component";
import { DataDownloadEmailComponent } from "./bootstrap-modal/dataDownloadEmail/dataDownloadEmail.component";
import { AlertdataComponent } from "./bootstrap-modal/alertdata/alertdata.component";
import {FileUploadModule} from 'ng2-file-upload';
import { DataMonitorComponent } from './data-monitor/data-monitor.component';

const busyConfig: BusyConfig = {
  message: 'Don\'t panic!',
  backdrop: true,
  template: '<div><img src="assets/img/loader.gif" /></div>',
  delay: 200,
  minDuration: 600,
  wrapperClass: 'loadingClass'
};


@NgModule({
  declarations: [
    OperationsNewbusinessComponent,
    AgencyComponent,
    LookupComponent,
    ProductFeesFinanceComponent,
    ProductFeesChargesComponent,
    ProductFeesRatesComponent,
    OperationalPolicyservicingComponent,
    OperationalUnderwritingComponent,
    RiskcoverageComponent,
    OperationalUnitlinksComponent,
    OperationalGroupComponent,
    OperationalAnnuityComponent,  
    UserComponent,
    AlertComponent,
    AdministratorGroupComponent,
    ReviewComponent,
    DemoComponent,
    LoginComponent,
    PageProductCommentComponent,
     BootstrapModalComponent,
    PrompComponent,
        ProductConfigComponent,
    FielddialogueComponent,
    MasterFieldDialogComponent,
    MasterdialogComponent,
    ProductDialogueComponent,
     TopbarComponent,
    FooterbarComponent,
    MiddlebarComponent,    
    SidebarComponent,
    ContentbarComponent,
    DashboardComponent,
    DashboarddataComponent,
    MasterComponent,
   
    EditPenComponent,
    ChangePasswordComponent,
    ProductcloneComponent,
    DataentryCompletionComponent,
    DataDownloadComponent,
    AlertdataComponent,
    DataDownloadEmailComponent,
    DataMonitorComponent


    
  ],
  imports: [
    //    BusyModule.forRoot(
    //     	new BusyConfig({
    //         	message: 'Don\'t panic!',
    //             backdrop: false,
    //         template: '<div><img src="assets/img/loader.gif" /></div>',
    //             delay: 200,
    //             minDuration: 600,
    //             wrapperClass: 'my-class'
    //         })
    //     ),
    AngularMultiSelectModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    DragulaModule,
    CommonModule,
    FileUploadModule,

        ReactiveFormsModule,
        BootstrapModalModule.forRoot({container:document.body}),
        ProgressbarModule.forRoot(),  
    NgxSpinnerModule,
    TooltipModule.forRoot(),
    AutocompleteModule.forRoot(),
    BusyModule.forRoot(busyConfig),

    FilterPipeModule,
        routing   
  ],
  entryComponents: [UserComponent,AlertComponent,ReviewComponent,EditPenComponent,ChangePasswordComponent,PrompComponent,FielddialogueComponent,MasterdialogComponent,MasterFieldDialogComponent,ProductDialogueComponent,DataDownloadComponent,AlertdataComponent,DataDownloadEmailComponent],
  providers: [DatePipe,AppServices,{provide:LocationStrategy,useClass:HashLocationStrategy},SweetAlertService],
  bootstrap: [DemoComponent]
})

export class DragulaDemoModule {
}
