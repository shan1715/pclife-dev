import { Injectable } from '@angular/core';
import { Router,CanActivate } from '@angular/router';
import {AppServices} from './app.service';
import { ActivatedRouteSnapshot } from "@angular/router";
import { RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/internal/Observable";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(protected router: Router, protected appService: AppServices)
    {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

        if (state.url !== '/' && !this.appService.isAuthenticated) {
            this.router.navigate(['/']);
            return false;
        }

        return true;
    }
}
