import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { PrompComponent } from "../bootstrap-modal/Modaldialog/promp.component";
import { Http, Headers } from '@angular/http';
import { AppServices } from '../app.service';
import { Router } from '@angular/router';
import { MasterdialogComponent } from "../bootstrap-modal/masterdialog/masterdialog.component";
import { ReviewComponent } from "../bootstrap-modal/Review/review.component";
import { AlertComponent } from "../bootstrap-modal/alert/alert.component";
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import {FooterbarComponent} from '../product-config/footerbar/footerbar.component';
@Component({
  selector: 'page-product-comment',
  templateUrl: './page-product-comment.component.html',
  styleUrls: ['./page-product-comment.component.css']
})
export class PageProductCommentComponent implements OnInit {
  bdCreate:any;
  bdModify:any;
  templateApproval:any;
  templateSaved: boolean = false;
  constructor(private router: Router, public loc: Location, private appService: AppServices, private dialogService: DialogService, private http: Http) {
 }
 
  ngOnInit() {
    this.templateApproval = this.appService.templateApproval;
    this.templateSaved=this.appService.templateSavedStaus;
    this.bdCreate = this.appService.bdCreate;
    this.bdModify = this.appService.bdModify;
  }
  review(eve) {
    
        this.dialogService.addDialog(ReviewComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '
    
        }).subscribe((isConfirmed) => {
        });
    
      }
}
