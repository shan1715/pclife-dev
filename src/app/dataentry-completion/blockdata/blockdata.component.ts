
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Http, Headers } from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AppServices } from '../../app.service';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'app-blockdata', 
  templateUrl: './blockdata.component.html',
  styleUrls: ['./blockdata.component.css']
})
export class BlockdataComponent implements OnInit {
  sub: any;
  page: any;
  dataMonitor: any = [];
  dataBlockMonitor: any = [];
  pageData: any = [];
  totalStatistic: boolean = false;
  blockStatistic: boolean = false;
  totalPageList:any=[];
  public barChartLegend:boolean = true;
  public barChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scaleShowLabels: false,
    legend: {
      display: false,
      labels: {
          display: false
      }
  }
  };
    public barChart2Labels:string[] = [];
   public barChart2Data:any[] = [];
  public barChartType:string = 'bar';


  constructor(private location: LocationStrategy,private appServices: AppServices,private router: Router,private spinner: NgxSpinnerService, private route: ActivatedRoute, private http: Http) {

    
    // location.onPopState(() => {
      
    //   this.router.navigate(['./productConfig/dataEntryCompletion']);


    //  });
   }

  ngOnInit() {
    this.pageData =  this.appServices.pageData;
   
    this.totalPageList=this.appServices.totalPageList;
    this.totalStatistic = true;
    this.blockStatistic = false;
    this.totalStatistic = false;
    this.blockStatistic = true;

   var chartdata=this.appServices.dataMonitor;
   var name=localStorage.getItem('dataEntryBlock');
   for(var k=0;k<chartdata.length;k++)
  {
    if(name==chartdata[k].name)
    {
     var fieldData=chartdata[k].data;
    }

  }
  this.dataBlockMonitor = fieldData.BlockDetails;
  var validcount1=[];
  var totalcount1=[];
  for(var j=0;j<this.dataBlockMonitor.length;j++)
  { 
    
    totalcount1.push(this.dataBlockMonitor[j].totalField)
    validcount1.push(this.dataBlockMonitor[j].validField);
    this.barChart2Labels.push(this.dataBlockMonitor[j].blockName);
    if(j==this.dataBlockMonitor.length-1){
      this.barChart2Data.push({'data':validcount1,'label':'ValidFieldCount'},{'data':totalcount1,'label':'TotalFieldCount'});
    }
  }



}

}