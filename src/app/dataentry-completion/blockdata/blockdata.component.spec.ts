import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockdataComponent } from './blockdata.component';

describe('BlockdataComponent', () => {
  let component: BlockdataComponent;
  let fixture: ComponentFixture<BlockdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
