import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Http, Headers } from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AppServices } from '../app.service';

@Component({
  selector: 'app-dataentry-completion',
  templateUrl: './dataentry-completion.component.html',
  styleUrls: ['./dataentry-completion.component.css']
})
export class DataentryCompletionComponent implements OnInit {
  sub: any;
  page: any;
  dataMonitor: any = [];
  dataBlockMonitor: any = [];
  pageData: any = [];
  totalStatistic: boolean = false;
  blockStatistic: boolean = false;
  totalPageList:any=[];
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scaleShowLabels: false

    
  };
    public barChartLabels:string[] = [];
    public barChart2Labels:string[] = [];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  
  public barChartData:any[] = [];
  public barChart2Data:any[] = [];

   public chartClicked(e:any):void {
    var name=e.active[0]._model.label;
    localStorage.setItem('dataEntryBlock',name);
    this.router.navigate(['./productConfig/blockDataCompletion']);
    

  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
 
  constructor(private appServices: AppServices,private router: Router,private spinner: NgxSpinnerService, private route: ActivatedRoute, private http: Http) { }

  ngOnInit() {
    this.pageData =  this.appServices.pageData;
    this.totalStatistic = true;
 this.blockStatistic = false;
 this.dataMonitor=this.appServices.dataMonitor;
 this.totalPageList=this.appServices.totalPageList;
 
 
 var validcount=[];
 var totalcount=[];
 for(var j=0;j<this.dataMonitor.length;j++)
 { 
   
   totalcount.push(this.dataMonitor[j].data.TotalFieldCount)
   validcount.push(this.dataMonitor[j].data.ValidFieldCount);
   this.barChartLabels.push(this.dataMonitor[j].name);
 }
this.barChartData.push({'data':validcount,'label':'ValidFieldCount'},{'data':totalcount,'label':'TotalFieldCount'});
 

//     this.sub = this.route
//       .queryParams
//       .subscribe(params => {

//         this.spinner.show();
//         var headers = new Headers();
//         headers.append('Content-Type', 'application/json');
//         this.http.post('/api/dataEntryMonitorStatus', { "folderId": params.productId, "publishStatus": JSON.parse(params.publishStatus) }, { headers: headers })
//           .subscribe(res => {
//             if(res.json()[0].DataMonitor.length>0){
//             this.totalStatistic = true;
//             this.blockStatistic = false;
//             //DataMonitor
//             var b = JSON.stringify(res.json());
//             var c = JSON.parse(b);
//             for (var i = 0; i < c[0].DataMonitor.length; i++) {
//               c[0].DataMonitor[i].data.PageName = c[0].DataMonitor[i].data.PageName.replace("_", " ")

//             }
//             this.dataMonitor = c[0].DataMonitor;
//             //PageList
//             for (var j = 0; j < c[0].PageList.length; j++) {
//               c[0].PageList[j].name = c[0].PageList[j].name.replace("_", " ")
//             }
//             //Reviews
//             for (var k = 0; k < c[0].Reviews.length; k++) {
//               c[0].Reviews[k].PageName = c[0].Reviews[k].PageName.replace("_", " ")
//             }


//           this.totalPageList.push({ "pname": "AGENCY", "count": 0,"reviews":[] }, { "pname": "NEW BUSINESS", "count": 0,"reviews":[] }, { "pname": "OPERATION ANNUITY", "count": 0,"reviews":[] }, { "pname": "OPERATION GROUP", "count": 0,"reviews":[] }, { "pname": "OPERATION UNDERWRITING", "count": 0,"reviews":[] }, { "pname": "OPERATION UNITLINKED", "count": 0,"reviews":[] }, { "pname": "POLICYSERVICING", "count": 0,"reviews":[] }, { "pname": "PROD BENEFITS", "count": 0,"reviews":[] }, { "pname": "PROD INFO", "count": 0,"reviews":[] }, { "pname": "PRODUCT CHARGES", "count": 0,"reviews":[] }, { "pname": "PRODUCT FINANCE", "count": 0,"reviews":[] }, { "pname": "OPERATION RATES", "count": 0,"reviews":[] });
//             for (var n = 0; n < c[0].Reviews.length; n++) {

//               for (var p = 0; p < this.totalPageList.length; p++) {
//                 if (c[0].Reviews[n].PageName == this.totalPageList[p].pname) {
//                     this.totalPageList[p].count++;
//                     this.totalPageList[p].reviews.push(c[0].Reviews[n].Review)                          


//                 }
//               }
//             }

//             this.pageData = c;
//             this.spinner.hide();
//             }else{
// 	  swal({
//               type: 'info',
//               title: 'Status',
//               text: 'No pages saved'
//             }).then(name => {
//                           this.router.navigate(['/productConfig/dashboarddata']);

// })
//             }


//           })

//       },err=>{
//         console.log(err);
//       });
  }

  blockGraph(data) {
    this.totalStatistic = false;
    this.blockStatistic = true;

    this.dataBlockMonitor = data.BlockDetails;
  }

}
