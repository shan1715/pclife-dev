import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataentryCompletionComponent } from './dataentry-completion.component';

describe('DataentryCompletionComponent', () => {
  let component: DataentryCompletionComponent;
  let fixture: ComponentFixture<DataentryCompletionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataentryCompletionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataentryCompletionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
