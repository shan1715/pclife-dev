import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AppServices} from '../../app.service';
@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  username:string;

  constructor(public appServices:AppServices,public router:Router) { }

  ngOnInit() {
    this.username=localStorage.getItem("user");
  }

   logout() {
         this.appServices.isAuthenticated=false;
    this.router.navigate(['']);

    window.location.reload();

  }

  toggleClick(){
  
    var id= document.getElementById('wrapper');
    if(id.className != 'toggled-2')
      id.className='toggled-2';
    else
      id.className='';

  }

  themeSwitch(eve){
    if(eve.target.parentElement.parentElement.style.right=="-283px"){
      eve.target.parentElement.parentElement.style.right="-27px";
      
    }else if(eve.target.parentElement.parentElement.style.right=="-27px"){
      eve.target.parentElement.parentElement.style.right="-283px";
    } 
   
   //eve.target.parentElement.parentElement.setAttribute("style","right:-20px");
  }

  theme1(eve){
  document.getElementById('theme').setAttribute('href','assets/productcss/custom-theme-4.css');
       document.getElementById('sidebar').setAttribute('href','assets/productcss/sidebar-theme-4.css');
         this.appServices.themeSelection="custom-theme-4";
         this.appServices.sidebartheme="sidebar-theme-4";
          eve.target.parentElement.parentElement.style.right="-283px";

  }

  theme2(eve){
        document.getElementById('theme').setAttribute('href','assets/productcss/custom-theme-2.css');
         document.getElementById('sidebar').setAttribute('href','assets/productcss/sidebar-theme-2.css');
         this.appServices.themeSelection="custom-theme-2";
         this.appServices.sidebartheme="sidebar-theme-2";
          eve.target.parentElement.parentElement.style.right="-283px";
  }
  theme3(eve){
      document.getElementById('theme').setAttribute('href','assets/productcss/custom.css');
         document.getElementById('sidebar').setAttribute('href','assets/productcss/sidebar.css');
         this.appServices.themeSelection="custom";
         this.appServices.sidebartheme="sidebar";
          eve.target.parentElement.parentElement.style.right="-283px";
  }
  theme4(eve){
    document.getElementById('theme').setAttribute('href','assets/productcss/custom-theme-4.css');
       document.getElementById('sidebar').setAttribute('href','assets/productcss/sidebar-theme-4.css');
         this.appServices.themeSelection="custom-theme-4";
         this.appServices.sidebartheme="sidebar-theme-4";
          eve.target.parentElement.parentElement.style.right="-283px";
  }
  theme5(eve){
    document.getElementById('theme').setAttribute('href','assets/productcss/custom-theme-5.css');
       document.getElementById('sidebar').setAttribute('href','assets/productcss/sidebar-theme-5.css');
         this.appServices.themeSelection="custom-theme-5";
         this.appServices.sidebartheme="sidebar-theme-5";
          eve.target.parentElement.parentElement.style.right="-283px";
  }
  theme6(eve){  
      document.getElementById('theme').setAttribute('href','assets/productcss/custom-theme-6.css');
         document.getElementById('sidebar').setAttribute('href','assets/productcss/sidebar-theme-6.css');
         this.appServices.themeSelection="custom-theme-6";
         this.appServices.sidebartheme="sidebar-theme-6";
          eve.target.parentElement.parentElement.style.right="-283px";
  }
}
 