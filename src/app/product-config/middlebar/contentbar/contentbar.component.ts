import { Component, OnInit, OnDestroy, OnChanges, ElementRef, ViewChild, Input, Renderer, AfterContentInit, ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DragulaService } from 'ng2-dragula';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DialogService } from "ng2-bootstrap-modal";
import { PrompComponent } from "../../../bootstrap-modal/Modaldialog/promp.component";
import { FielddialogueComponent } from "../../../bootstrap-modal/Fielddialogue/fielddialogue.component";
import { AppServices } from '../../../app.service';
import { EditPenComponent } from "../../../bootstrap-modal/edit-pen/edit-pen.component";
import { NgForm } from '@angular/forms';
import { AddRowComponent } from "../../../bootstrap-modal/add-row/add-row.component";
import { RetriveDelComponent } from "../../../bootstrap-modal/retrive-del/retrive-del.component";

import { DOCUMENT } from "@angular/platform-browser";
import { AlertComponent } from "../../../bootstrap-modal/alert/alert.component";
import { FormControl, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { tabAccGenProdInfo } from '../../../tabAccordformat';
import { DataDownloadComponent } from '../../../bootstrap-modal/dataDownload/dataDownload.component';
import { DataSearchComponent } from '../../../bootstrap-modal/dataSearch/dataSearch.component';
import swal from 'sweetalert2';
import { ignoreElements } from 'rxjs-compat/operator/ignoreElements';
import { TabHeadingDirective } from 'ngx-bootstrap';

declare var JsSearch: any;

@Component({
  selector: 'app-contentbar',
  templateUrl: './contentbar.component.html',
  styleUrls: ['./contentbar.component.css'],
  providers: [DatePipe,DragulaService]


})


export class ContentbarComponent implements OnInit, AfterContentInit {
  backLinkBool: boolean = false;
  display_data: any = [];
  parentName: any;
  backlinkField: any;
  navId: any;
  templateview: boolean = false;
  templateApproval: boolean = false;
  productApproval: boolean = false;
  templateSaved: boolean = false;
  productSaved: boolean = false;
  bdCreate: boolean = false;
  bdModify: boolean = false;
  bdView: boolean = false;
  pbdCreate: boolean = false;
  pbdUpdate: boolean = false;
  pbdModify: boolean = false;
  pbdView: boolean = false;
  productUpdated: boolean = false;
  draggedItem: any = [];
  showAddBtn: boolean = false;
  showRow: boolean = false;
  updatehide:boolean=false;
  count = 1;
  date: any;
  pageMode: any = 0;
  alertMsg: any = [];
  snumber:any=[{"sna":0},{"snb":0},{"snc":0},{"snd":0}];
  blockName: any;
  accName = [];
  expand:any;
  dropExpand:any;
  @ViewChild('headName')headName:ElementRef;
  draggingItem:any;
  currenttemp:any;
  dragField:any;
  dragI:any;
  

  constructor(private changedet:ChangeDetectorRef, private spinner: NgxSpinnerService,private router: Router, private datePipe: DatePipe, private http: Http, private dragulaService: DragulaService, private appService: AppServices, private dialogService: DialogService) {
    // dragulaService.setOptions('second-bag', {
    //   copy: (el, source) => {
    //     return source.id === 'left';
    //   },
    //   accepts: (el, target, source, sibling) => {
    //     // To avoid dragging from right to left container
    //     return target.id !== 'left';
    //   }
    // });

    

    dragulaService.setOptions('second-bag', {
      copy: (el, source) => {
        // alert(source.id)
        this.dragField=source.id;
        //return source.id === 'ok';
      },
      accepts: (el, target, source, sibling) => {
        // To avoid dragging from right to left container
        // alert(target.id)
        if(target.id === this.dragField){
          return true;
        }else{
          return false;
        }
        
      }
    });
  


    this.dragulaService.drag.subscribe((value: any) => {
      console.log("drag");

      this.onDrag(value.slice(1));

    });

    this.dragulaService.drop.subscribe((value: any) => {
      if(value[4]!=null){
      this.onDrop(value.slice(1));
      console.log("drop");
      }
    });
    this.dragulaService.over.subscribe((value: any) => {
      this.onOver(value.slice(1));
      console.log("over");
    });
    this.dragulaService.out.subscribe((value: any) => {
      this.onOut(value.slice(1));
      console.log("out");
    });


  }


  dud()
  {

  }

  async ngOnInit() {
    this.expand=0;
    this.currenttemp=this.appService.currentTemplate;
    this.appService.pageName = 'PROD_INFO';
    this.appService.snArr = this.snumber;

    if (localStorage.getItem("pageMode") == "1") {
      this.pageMode = 1;
      // this.headName.nativeElement.innerText="Template "+this.appService.templateName;
      
      if (this.appService.tempProdInfo.length > 0) {
        this.display_data = this.appService.tempProdInfo;
        this.blockName =this.appService.getAllBlockName(this.appService.tempProdInfo);
      }else{
      this.http.get('assets/data/productConfig.json')
        .subscribe((res) => {

          this.appService.tempProdInfo = res.json();
          this.display_data = this.appService.tempProdInfo;
          this.blockName =this.appService.getAllBlockName(this.appService.tempProdInfo);
          
        }, err => {
          console.log(err);
        })
      }
      
      if(this.appService.templateApproval){
        var approve = [];
          approve.push({
          "TemplateId": this.appService.templateId,
          "TemplateName": this.appService.templateName,
          "PageName": "PROD_INFO" 
          })
  
         // this.spinner.show();
          var headers = new Headers();
          headers.append("Content-Type", "application/json");
          this.http.post('/api/readTempApproveStatus', JSON.stringify(approve[0]), { headers: headers })
            .subscribe(res => {  

              this.spinner.hide();
              if (res.json().review == false) {
               
                this.appService.templateSavedStaus=true;
                this.templateSaved=true;
              }else{
                this.appService.templateSavedStaus=false;
                this.templateSaved=false;
              }
      
            }, err => {
              this.spinner.hide();
              console.log("err " + err);
            })
          }
    } else if (localStorage.getItem("pageMode") == "2") {
      this.pageMode = 2;
      if (this.appService.tempProdInfo.length > 0) {

        this.appService.lovWS(this.appService.tempProdInfo);
        this.display_data = this.appService.tempProdInfo;

        if(this.appService.productApproval){
          var approve = [];
          approve.push({
            "ProductId": this.appService.productId,
            "ProductName": this.appService.productName,
            "PageName": "PROD_INFO"
          })

          var headers = new Headers();
          headers.append("Content-Type", "application/json");

          const readProdApproveStatus = await this.http.post('/api/readProdApproveStatus',JSON.stringify(approve[0]),{headers:headers}).toPromise()
          console.log("xxxxxxxxxxxx")
          console.log(readProdApproveStatus)
          if(readProdApproveStatus.json().review == false){
            this.productSaved = true
          }else{
            this.productSaved = false
          }

          //<<comment by darong
          // this.http.post('/api/readProdApproveStatus', JSON.stringify(approve[0]), { headers: headers })
          //   .subscribe(res => {  
          //     if (res.json().review == false) {
          //       this.productSaved = true;
          //     }else{
          //       this.productSaved = false;

          //     }
      
          //   }, err => {
          //     console.log("err " + err);
          //   })
          //>>

        }
      } else {

        localStorage.setItem("alertContent", "Template Not Created");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.router.navigate(['/productConfig/dashboarddata']);

        });
      }


    }
  }

  ngAfterContentInit() {
    if (localStorage.getItem("pageMode") == "1") {
      if (document.getElementById("sValues")) {
        document.getElementById("sValues").style.display = "none";
      }
      setTimeout(() => {
        this.showAddBtn = true;
        this.showRow = false;
        this.bdCreate = this.appService.bdCreate;
        this.bdModify = this.appService.bdModify;
        this.bdView = this.appService.bdView;
        this.templateApproval = this.appService.templateApproval;
        if (this.appService.publishedTemplate) {
          this.bdCreate = false;
          this.bdModify = false;
          this.templateApproval = false;
        }
      }, 500)

    } else if (localStorage.getItem("pageMode") == "2") {
      if (document.getElementById("sFormat")) {
        document.getElementById("sFormat").style.display = "none";
      }

      setTimeout(() => {
        this.showRow = true;
        this.showAddBtn = false;

        this.pbdCreate = this.appService.pbdCreate;
        this.pbdModify = this.appService.pbdModify;
        this.pbdView = this.appService.pbdView;
        this.productApproval = this.appService.productApproval;
        if (this.appService.publishedProduct) {
          this.pbdCreate = false;
          this.pbdModify = false;
          this.pbdUpdate=true;
          this.productApproval = false;
        }

        this.dragulaService.find("second-bag").drake.destroy();
        //var output = this.appService.validPage(this.appService.tempProdInfo);

      }, 500);


    }
  }

  nav(i,nfield,id,tD) {
    this.expand=i;
    if(tD.sna != undefined){

      this.snumber=[{"sna":0},{"snb":0},{"snc":0},{"snd":0}];
      this.snumber[0].sna = tD.sna;
    }else if(tD.snb != undefined){
      this.snumber[1].snb = tD.snb;
      
    }else if(tD.snc != undefined){
      this.snumber[2].snc = tD.snc;
    }else if(tD.snd != undefined){
      this.snumber[3].snd = tD.snd;
    }
    this.appService.snArr = this.snumber;
    this.appService.selectedNav = id;
    this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "nav", "", this.appService.subPagesNavProdInfo);

    for (var d = 0; d < this.appService.subPagesNavProdInfo.length; d++) {
      if (this.appService.subPagesNavProdInfo[d].isActive) {

        this.navId = this.appService.subPagesNavProdInfo[d].id;
      }
    }
    this.parentName = nfield.parentName;
    this.backLinkBool = true;
    this.backlinkField = nfield;
    if (localStorage.getItem("pageMode") == "1") {
      this.accName=[];
      for (var a = 0; a < this.display_data.length; a++) {
        if(this.display_data[a].deleteAcc != undefined){
          this.accName.push({ "id": this.display_data[a].accordId, "itemName": this.display_data[a].accordionName });
        }
      }
    }

  }

  backLink() {

    var mpageNavigation = false;
    for (var b = 0; b < this.appService.subPagesNavProdInfo.length; b++) {
      if (this.appService.subPagesNavProdInfo[b].id == this.navId) {
        mpageNavigation = true;
        this.appService.subPagesNavProdInfo[b].isActive = false;
        this.display_data = this.appService.changeField(this.appService.tempProdInfo, { "id": this.navId }, "back", "", "");
        this.parentName = this.appService.subPagesNavProdInfo[b].grantparentName;
        this.navId = this.appService.subPagesNavProdInfo[b].parentId;
        if (this.navId == "") {
          this.backLinkBool = false;
        }
      }
    }
    if (!mpageNavigation) {
      this.display_data = this.appService.changeField(this.appService.tempProdInfo, { "id": this.backlinkField }, "back", "", "");

    }

    if (localStorage.getItem("pageMode") == "1") {
      this.accName=[];
      for (var a = 0; a < this.display_data.length; a++) {
        if(this.display_data[a].deleteAcc != undefined){
          this.accName.push({ "id": this.display_data[a].accordId, "itemName": this.display_data[a].accordionName });
        }
      }
    }

  }

  delfield(dfield) {
    this.display_data = this.appService.changeField(this.appService.tempProdInfo, dfield, "del", "", "");
  }

  incSize(incfield, eve) {
   // var sy=JSON.stringify(this.appService.tempProdInfo);

    this.display_data = this.appService.changeField(this.appService.tempProdInfo, incfield, "inc", eve.target.parentElement.parentElement.parentElement.style.width, "");

  }

  decSize(decfield, eve) {
   // var sy=JSON.stringify(this.appService.tempProdInfo);
    this.display_data = this.appService.changeField(this.appService.tempProdInfo, decfield, "dec", eve.target.parentElement.parentElement.parentElement.style.width, "");

  }

  editpenAccodion(eve) {

    eve.target.previousElementSibling.firstElementChild.removeAttribute("readonly");
    eve.target.previousElementSibling.firstElementChild.focus();
    eve.target.previousElementSibling.firstElementChild.style.backgroundColor = "#9ACD32";
  }

  disableEditAccordion(data, eve) {
    this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, data, eve.target.value,"", "editAcc");
    eve.target.setAttribute("readonly", true);
    eve.target.style.backgroundColor = "#FFFFFF";
  }

  delAccordion(data) {
    if (data.accordId != "pi") {
      this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, data,"", "", "delAcc");
      this.accName.push({ "id": data.accordId, "itemName": data.accordionName });
    } else {
      localStorage.setItem("alertContent", "You can't delete this Block");

      this.dialogService.addDialog(AlertComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '

      }).subscribe((isConfirmed) => {
      });

    }
  }

  editField(efield) {
    localStorage.setItem("editName", efield.fieldName);
    localStorage.setItem("editId", efield.id);
    localStorage.setItem("editBlockId", efield.blockId);
    localStorage.setItem("mandatory", efield.mandatory);
    localStorage.setItem("mandatoryMsg", efield.mandatoryMsg);
    localStorage.setItem("editType", efield.tagName);
    localStorage.setItem("editDisplay", efield.display);
    localStorage.setItem("editPageName", "PROD_INFO");
    var xx = [];
    if(efield.mappingId){
      var fieldDetailList = [];
      for (var j = 0; j < efield.mappingId.length; j++) {
       var fieldDetail = this.appService.changeField(this.appService.tempProdInfo, efield.mappingId[j], "mappedFieldDetails", "", "");
       fieldDetailList.push(fieldDetail);
      }
      localStorage.setItem("editMapping", JSON.stringify(fieldDetailList));
      localStorage.setItem("editBlockNameList", JSON.stringify(xx));
    }else{
      localStorage.setItem("editBlockNameList", JSON.stringify(this.blockName));
      localStorage.setItem("editMapping", JSON.stringify(xx));
    }

    if(efield.read){
      localStorage.setItem("editReadOnly", efield.read);
    }else{
      localStorage.setItem("editReadOnly", "false");

    }
    if(efield.tagValue != ''){
      localStorage.setItem("editDefaultValue", efield.tagValue);
    }else{
      localStorage.setItem("editDefaultValue", "");
    }

    if(efield.maxValue != ''){
      localStorage.setItem("editMaxValue", efield.maxValue);
    }else{
      localStorage.setItem("editMaxValue", "");
    }

    if(efield.minValue != ''){
      localStorage.setItem("editMinValue", efield.minValue);
    }else{
      localStorage.setItem("editMinValue", "");
    }
    var f = [];
    if (efield.tagName == 'select-one') {


      for (var i = 0; i < efield.optionObj.length; i++) {
        if (efield.optionObj[i].optionValue != "") {
          f.push({ "optionValue": efield.optionObj[i].optionValue, "optionLabel": efield.optionObj[i].optionLabel })
        }
      }
      localStorage.setItem("editSelectedItem", JSON.stringify(f));

    }


    this.dialogService.addDialog(EditPenComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {

        var edited = JSON.parse(localStorage.getItem("output"));
        this.display_data = this.appService.changeField(this.appService.tempProdInfo, edited[0], "editField", "", "");


      } else {

      }
    })

  }

  editTableField(tData) {
    localStorage.setItem("editName", tData.theadName);
    localStorage.setItem("editId", tData.id);
    localStorage.setItem("editBlockId", tData.blockId);
    localStorage.setItem("mandatory", tData.mandatory);
    localStorage.setItem("mandatoryMsg", tData.mandatoryMsg);
    localStorage.setItem("editDisplay", tData.display);
    localStorage.setItem("editPageName", "PROD_INFO");

    if(tData.maxValue != ''){
      localStorage.setItem("editMaxValue", tData.maxValue);
    }else{
      localStorage.setItem("editMaxValue", "");
    }

    if(tData.minValue != ''){
      localStorage.setItem("editMinValue", tData.minValue);
    }else{
      localStorage.setItem("editMinValue", "");
    }

    var xx = [];
    if(tData.mappingId){
      var fieldDetailList = [];
      for (var j = 0; j < tData.mappingId.length; j++) {
       var fieldDetail = this.appService.changeField(this.appService.tempProdInfo, tData.mappingId[j], "mappedFieldDetails", "", "");
       fieldDetailList.push(fieldDetail);
      }
      localStorage.setItem("editMapping", JSON.stringify(fieldDetailList));
      localStorage.setItem("editBlockNameList", JSON.stringify(xx));
    }else{
      localStorage.setItem("editBlockNameList", JSON.stringify(this.blockName));
      localStorage.setItem("editMapping", JSON.stringify(xx));
    }

    if(tData.tdObj){
      if(tData.tdObj[0].tdValue != ''){
        localStorage.setItem("editDefaultValue", tData.tdObj[0].tdValue);
    }else{
      localStorage.setItem("editDefaultValue", "");
    }
  }
  var f = [];
  localStorage.setItem("editSelectedItem", JSON.stringify(f));

    if (tData.optionObj) {
      localStorage.setItem("editType", 'select-one');
      
      for (var i = 0; i < tData.optionObj.length; i++) {

        f.push({ "optionValue": tData.optionObj[i].optionValue, "optionLabel": tData.optionObj[i].optionLabel })

        localStorage.setItem("editSelectedItem", JSON.stringify(f));

      }

    } else {
      localStorage.setItem("editType", tData.tdObj[0].tagName);

    }

    this.dialogService.addDialog(EditPenComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {


        var edited = JSON.parse(localStorage.getItem("output"));
        this.display_data = this.appService.changeField(this.appService.tempProdInfo, edited[0], "editTableField", "", "");



      } else {

      }
    })

  }



  private onDrag(args: any): void {
    let [e, target, parent, moves] = args;

    if(e.className!=''){
    //this.appService.dragMaintain=e.querySelector(".inputlabel").id;
    this.dragI=e.id.split("@");
    }else{
      if(document.getElementsByClassName("panel-collapse collapse in")[0]){
       // document.getElementsByClassName("panel-collapse collapse in")[0].className="panel-collapse collapse";
      }
      
    }
   

  }
  selectIndex(i){
    this.dropExpand=i;
  }



  private onDrop(args: any): void {  
    let [e, target, parent, moves] = args;
     if((e.className=="tc1 gu-transit" && target.className=="moTable") || (e.className=="first-box gu-transit" && target.className=="board")){
      this.appService.draggedItem=this.display_data[this.dragI[0]].field.splice(this.dragI[1],1);

      //this.display_data= this.appService.changeField(this.appService.tempProdInfo, { "id": this.appService.dragMaintain }, "drag", "", "");
   if( args[3].querySelector!=null){
  //  var indexi=e.id.split("@");
   // this.display_data[indexi[0]].field.splice(indexi[1],0,dragData[0]);
   this.display_data =this.appService.changeField(this.appService.tempProdInfo, { "id": args[3].querySelector(".inputlabel").id }, "drop", "", "");

   }
  }
  //  this.appService.tempProdInfo= this.appService.changeField(this.appService.tempProdInfo, { "id": this.appService.dragMaintain }, "drag", "", "");
   
   
  //   setTimeout(()=>{

    
  //   this.display_data =this.appService.changeField(this.appService.templateInfo, { "id": args[3].querySelector(".inputlabel").id }, "drop", "", "");
  //   },500)  
  
    //  if((e.className=="tc1 gu-transit" && target.className=="moTable") || (e.className=="first-box gu-transit" && target.className=="board")){
  //  this.display_data= this.appService.changeField(this.appService.tempProdInfo, { "id": this.appService.dragMaintain }, "drag", "", "");
  //  if( args[3].querySelector!=null){
  //  this.display_data =this.appService.changeField(this.appService.tempProdInfo, { "id": args[3].querySelector(".inputlabel").id }, "drop", "", "");

  //  }
   

// }else{

//   if(e.className=="gu-transit" && target.className=="panel-group"){
//   }else{
//   // this.dragulaService.find('second-bag').drake.cancel(true);

  

//   }

}
  
// }else{
//  // this.appService.dragIndex=-1;

//   this.dragulaService.find('second-bag').drake.cancel(true);
//    // this.appService.changeField(this.appService.tempProdInfo, { "id": e.querySelector(".inputlabel").id }, "drop", "", "");

// }
  


  private onOver(args: any): void {

    let [el] = args;

  }

  foutc;

  private onOut(args: any): void {
    let [e, target, parent, moves] = args;

    // if((target.nextElementSibling==null && target.previousElementSibling.className=="row") || (target.nextElementSibling.className=="moTable" && target.previousElementSibling.className=="moTable")){
    //  // this.dragulaService.find('second-bag').drake.cancel(true);  
    // }


//  if((target.previousElementSibling.className=="row" && target.nextElementSibling==null)){
//  this.dragulaService.find('second-bag').drake.cancel(true);
// }
  }

  saveFormat() {

    swal({
      title: 'Are you sure?',
      text: "You want to save!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then(async (resultSel) => {
      if (resultSel.value) {
    this.display_data[0].TemplateId = this.appService.templateId;
    this.display_data[0].PageName = this.appService.pageName;
    var data = JSON.stringify(this.display_data);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    await this.http.post('/api/tempPage',data,{headers:headers}).toPromise()

    //<<<comment by darong
    // this.http.post('/api/tempPage', data, { headers: headers })
    //   .subscribe(res => {
    //     setTimeout(() => {
    //       console.log(res.json());
    //     }, 1500);

    //   }, err => {
    //     console.log("err " + err);
    //   })
    //>>>>>>
    var pageName = [];
    pageName.push({
      "TemplateId": this.appService.templateId,
      "TemplateName": this.appService.templateName,
      "PageName": "PROD_INFO"
    });
    
    this.http.post('/api/tempPageNameInsert', pageName, { headers: headers })
      .subscribe(res => {
        if (res.json().review == false && this.templateApproval){
          this.templateSaved = true;
          this.appService.templateSavedStaus = true;
        }

      }, err => {
        console.log("err " + err);
      })



    localStorage.setItem("alertContent", "Saved Successfully");

    this.dialogService.addDialog(AlertComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
    });
  }
})
  }

  addBlock() {
       localStorage.setItem("dboard", "0");
    this.dialogService.addDialog(PrompComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.display_data = this.appService.addNewBlock(this.appService.tempProdInfo, "PROD_INFO", "PRODINFO" + this.count++, "PRODINFO" + this.count++);
        console.log(1);
      } else {

      }
    })
  }

  addField(field) {
    localStorage.setItem("id", field.accordId);
    localStorage.setItem("blockNameList", JSON.stringify(this.blockName));
    localStorage.setItem("pageName", "PROD_INFO");

    this.dialogService.addDialog(FielddialogueComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("output"));
        this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, field, s,"", "newField");

      } else {

      }
    })





  }

  addTableField(field) {
    localStorage.setItem("id", field.accordId);
    localStorage.setItem("blockNameList", JSON.stringify(this.blockName));
    localStorage.setItem("pageName", "PROD_INFO");
    
    this.dialogService.addDialog(FielddialogueComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("output"));
        this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, field, s,"", "newTableField");

      } else {

      }
    })





  }

  fieldInsert() {

    var s = JSON.parse(localStorage.getItem("output"));
    for (var i = 0; i < this.display_data.length; i++) {
      if (this.display_data[i].accordId == localStorage.getItem("id")) {
        if (localStorage.getItem("className") == "panel-body") {

          if (s[0].item_type == "text") {
            if (s[0].field_type == "Number") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "placeholder": "Enter " + s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "number" })

            } else if (s[0].field_type == "String") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "placeholder": "Enter " + s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "text" })

            } else if (s[0].field_type == "Date") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "placeholder": "Enter " + s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "date" })

            }

          } else if (s[0].item_type == "select") {
            this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "placeholder": "Enter " + s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "select-one", "tagvalue": "--Select--", "optionObj": s[0].optionObj })

          } else if (s[0].item_type == "date") {
            this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "placeholder": "Enter " + s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "date" })

          }
        } else if (localStorage.getItem("className") == "panel-body tle") {
          if (s[0].item_type == "text") {
            if (s[0].field_type == "Number") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "number", "tdValue": "" }] })

            } else if (s[0].field_type == "Date") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "date", "tdValue": "" }] })

            } else if (s[0].field_type == "String") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "text", "tdValue": "" }] })

            }

          } else if (s[0].item_type == "select") {
            this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "optionObj": s[0].optionObj })

          } else if (s[0].item_type == "date") {
            this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "date", "tdValue": "" }] })

          }
        }
      }

    }
  }


  approval() {
    var approve = [];
    approve.push({
      "TemplateId": this.appService.templateId,
      "TemplateName": this.appService.templateName,
      "PageName": "PROD_INFO"
    })

    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    this.http.post('/api/tempApprove', JSON.stringify(approve[0]), { headers: headers })
      .subscribe(res => {
        localStorage.setItem("alertContent", "Approved");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.templateSaved = false;

        });

      }, err => {
        console.log("err " + err);
      })



  }

  approvalProduct() {
    var approve = [];
    approve.push({
      "ProductId": this.appService.productId,
      "ProductName": this.appService.productName,
      "PageName": "PROD_INFO"
    })


    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    this.http.post('/api/prodApprove', JSON.stringify(approve[0]), { headers: headers })
      .subscribe(res => {
        localStorage.setItem("alertContent", "Approved");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.productSaved = false;
        });

      }, err => {
        console.log("err " + err);
      })
  }

  
  genRow(data) {
    //var countR = data.tableObj.length;
    var s;
    for(var i=0;i<Object.keys(data.tableObj[data.tableObj.length-1]).length;i++){
      if(Object.keys(data.tableObj[data.tableObj.length-1])[i].includes("sn")){
        s=i;
    }
    
    }
    var countR=data.tableObj[data.tableObj.length-1][Object.keys(data.tableObj[data.tableObj.length-1])[s]];



    localStorage.setItem("addRowOutpt", "");
    this.dialogService.addDialog(AddRowComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("addRowOutpt"));
        for(var i=0; i < s; i++){
          this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, data, ++countR,"", "addRow");
      }
      } else {

      }
    })

  }

  delRow(data, eve) {

    if (eve.target.parentElement.previousElementSibling != null) {
      this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, data, eve.target.parentElement.id,"", "delRow");

    } else {
      localStorage.setItem("alertContent", "You can't delete this Row");

      this.dialogService.addDialog(AlertComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '

      }).subscribe((isConfirmed) => {
      });
    }

  }

  textValid(eve, fieldData,nfield,ti) {
    this.expand=ti;
    if (localStorage.getItem("pageMode") == "2") {
      var resultVal = true;
      if((nfield.maxLength !='')){
        var dValue=eve.target.value.length;
        var max=Number(nfield.maxLength);
    
        if(dValue>max){
          eve.target.value="";
                         swal(
                           'Error!',
                           'Value Should not exceeds maximum length of '+nfield.maxLength,
                           'error'
                         )
                   
                        }
                      }
                    
      if (nfield.tagName == 'date') {
        var regx = new RegExp(/^\d{4}-\d{2}-\d{2}$/);
        if (regx.test(eve.target.value)) {
          if (eve.target.value != '') {
            this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", eve.target.value, "");

            for(var i=0;i<fieldData.length;i++){

              if(nfield.toId){
                
                if(fieldData[i].id==nfield.toId){
                var nDate=nfield;
                var fromDate = new Date(nDate.tagValue);	
                var toDate = new Date(fieldData[i].tagValue);
                if(fromDate > toDate){
                 //alert("invalid");
                 nDate.tagValue="";
                 nfield.mandatory=true;
                 this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
                  var tChange=fieldData[i];
                  tChange.tagValue="";
                  var output = this.appService.changeField(this.appService.tempProdInfo, tChange, "setVal", "", "");
                  var resultCheck=JSON.stringify(output);
                  
                  this.display_data=JSON.parse(resultCheck);
                  swal(
                    'Error!',
                    'Please Enter valid Date',
                    'error'
                  )
                   break;
                }
                }
                }
                else if(nfield.fromId){
                  if(fieldData[i].id==nfield.fromId){
                  var nDate=nfield.tagValue;
                  var toDate = new Date(nDate);	
                  var fromDate = new Date(fieldData[i].tagValue);
                  if(fromDate > toDate){
                   // alert("invalid");
                   nfield.tagValue="";
                   this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
                    var tChange=fieldData[i];
                    tChange.tagValue="";
                    
                    var output= this.appService.changeField(this.appService.tempProdInfo, tChange, "setVal", "", "");
                                    var resultCheck=JSON.stringify(output);
                    
                                    this.display_data=JSON.parse(resultCheck);
                                    swal(
                                      'Error!',
                                      'Please Enter valid Date',
                                      'error'
                                    )
                                     break;
                  }
                  }
                  
                  }
            }



            
          //   this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", eve.target.value, "");
          //   if(nfield.minValue != '' || nfield.maxValue != ''){
          //     resultVal = this.appService.validateNumDate(this.appService.tempProdInfo, nfield, "minMaxValidateDate", eve.target.value);
          //   }
          //   if(!resultVal){
          //     var output1 = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
          //     this.display_data = output1;
          //     // setTimeout(res => {
          //     //   this.display_data = output1;
          //     // }, 200);
          //     }else{
          //   if (nfield.toId) {
          //     var result = this.appService.validateField(this.appService.tempProdInfo, nfield, nfield.toId, "to", "date");
          //     if (result == "invalid") {
          //       var output = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
          //       this.display_data = output;
          //       // setTimeout(res => {
          //       //   this.display_data = output;
          //       // }, 200);
          //     }
          //   } else if (nfield.fromId) {
          //     var result = this.appService.validateField(this.appService.tempProdInfo, nfield, nfield.fromId, "from", "date");
          //     if (result == "invalid") {
          //       var output = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
          //       this.display_data = output;
          //       // setTimeout(res => {
          //       //   this.display_data = output;
          //       // }, 200);
          //     }
          //   }

          // }

        }
        } else {
          eve.target.value = "";

        }
      } else if (nfield.tagName == 'number') {
        var regexp = /^[0-9]+([,.][0-9]+)?$/g;
        if (regexp.test(eve.target.value)) {
          if (eve.target.value != '') {
            this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", Number(eve.target.value), "");

            for(var i=0;i<fieldData.length;i++){

              if(nfield.toId){
                
                if(fieldData[i].id==nfield.toId){
                var nDate=nfield;
                var fromDate = new Date(nDate.tagValue);	
                var toDate = new Date(fieldData[i].tagValue);
                if(fromDate > toDate){
                
                 nDate.tagValue="";
                 nfield.mandatory=true;
                 this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
                  var tChange=fieldData[i];
                  tChange.tagValue="";
                  var output = this.appService.changeField(this.appService.tempProdInfo, tChange, "setVal", "", "");
                  var resultCheck=JSON.stringify(output);
                  
                  this.display_data=JSON.parse(resultCheck);
                  swal(
                    'Error!',
                    'Please Enter valid Date',
                    'error'
                  )
                   break;
                }
                }
                }
                else if(nfield.fromId){
                  if(fieldData[i].id==nfield.fromId){
                  var nDate=nfield.tagValue;
                  var toDate = new Date(nDate);	
                  var fromDate = new Date(fieldData[i].tagValue);
                  if(fromDate > toDate){
                   // alert("invalid");
                   nfield.tagValue="";
                   this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
                    var tChange=fieldData[i];
                    tChange.tagValue="";
                    
                    var output= this.appService.changeField(this.appService.tempProdInfo, tChange, "setVal", "", "");
                                    var resultCheck=JSON.stringify(output);
                    
                                    this.display_data=JSON.parse(resultCheck);
                                    swal(
                                      'Error!',
                                      'Please Enter valid Date',
                                      'error'
                                    )
                                     break;
                  }
                  }
                  
                  }
            }

          //   this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", Number(eve.target.value), "");
          //   if(nfield.minValue != '' || nfield.maxValue != ''){
          //     resultVal = this.appService.validateNumDate(this.appService.tempProdInfo, nfield, "minMaxValidateNum", Number(eve.target.value));
          //   }
          //   if(!resultVal){
          //     var output1 = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
          //     this.display_data = output1;
          //     // setTimeout(res => {
          //     //   this.display_data = output1;
          //     // }, 200);
          //     }else{
          //   if (nfield.toId) {
          //     var result = this.appService.validateField(this.appService.tempProdInfo, nfield, nfield.toId, "to", "num");
          //     if (result == "invalid") {
          //       var output = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
          //       this.display_data = output;
          //       // setTimeout(res => {
          //       //   this.display_data = output;
          //       // }, 200);
          //     }
          //   } else if (nfield.fromId) {
          //     var result = this.appService.validateField(this.appService.tempProdInfo, nfield, nfield.fromId, "from", "num");
          //     if (result == "invalid") {
          //       var output = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", "", "");
          //       this.display_data = output;
          //       // setTimeout(res => {
          //       //   this.display_data = output;
          //       // }, 200);
          //     }
          //   }
          // }
        }
        } else {
          eve.target.value = "";
        }
      } else {
        if (nfield.mappingId) {
          for (var j = 0; j < nfield.mappingId.length; j++) {
            this.appService.changeField(this.appService.tempProdInfo, nfield.mappingId[j], "copyValues", eve.target.value, "");
          }
        }

        if( eve.target.value != ""){
        this.display_data = this.appService.changeField(this.appService.tempProdInfo, nfield, "setVal", eve.target.value, "");
        }
      
    }
    } else {
     // eve.target.value = "";
    }
  }

  

  textTabValid(eve,tableRow,tData,tD,expanIndex){
    this.expand=expanIndex;
    if (localStorage.getItem("pageMode") == "2") {

      var resultVal = true;
      if(tD.sna != undefined){
        this.snumber[0].sna = tD.sna;
      }else if(tD.snb != undefined){
        this.snumber[1].snb = tD.snb;
        
      }else if(tD.snc != undefined){
        this.snumber[2].snc = tD.snc;
      }else if(tD.snd != undefined){
        this.snumber[3].snd = tD.snd;
      }
      if((tData.maxLength !='')){
        var dValue=eve.target.value.length;
        var max=Number(tData.maxLength);
    
        if(dValue>max){
          eve.target.value="";
                         swal(
                           'Error!',
                           'Value Should not exceeds maximum length of '+tData.maxLength,
                           'error'
                         )
                   
                        }
                      }
      // this.appService.cIndex=Number(c);
      if (tData.tdObj[0].tagName == 'date') {
        var regx = new RegExp(/^\d{4}-\d{2}-\d{2}$/);
        if (regx.test(eve.target.value)) {
          
            if (eve.target.value != '') {
              this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", eve.target.value, "");
              for(var i=0;i<tableRow.length;i++){

                if(tData.toId){
                
                if(tableRow[i].id==tData.toId){
                
                var fromDate = new Date(tData.tdObj[0].tdValue);	
                var toDate = new Date(tableRow[i].tdObj[0].tdValue);
                if(fromDate > toDate){
                 //alert("invalid");
                 tData.tdObj[0].tdValue="";
                 tData.mandatory=true;
                 this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", "", "");
                  var tChange=tableRow[i];
                  tChange.tdObj[0].tdValue="";
                  var output = this.appService.changeField(this.appService.tempProdInfo, tChange, "setVal", "", "");
                  var resultCheck=JSON.stringify(output);
                  
                  this.display_data=JSON.parse(resultCheck);
                  swal(
                    'Error!',
                    'Please Enter valid Date',
                    'error'
                  )
                   break;
                }
                }
                }else if(tData.fromId){
                if(tableRow[i].id==tData.fromId){
                
                var toDate = new Date(tData.tdObj[0].tdValue);	
                var fromDate = new Date(tableRow[i].tdObj[0].tdValue);
                if(fromDate > toDate){
                 // alert("invalid");
                 tData.tdObj[0].tdValue="";
                 this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", "", "");
                  var tChange=tableRow[i];
                  tChange.tdObj[0].tdValue="";
                  
                  var output= this.appService.changeField(this.appService.tempProdInfo, tChange, "setVal", "", "");
                                  var resultCheck=JSON.stringify(output);
                  
                                  this.display_data=JSON.parse(resultCheck);
                                  swal(
                                    'Error!',
                                    'Please Enter valid Date',
                                    'error'
                                  )
                                   break;
                }
                }
                
                }
         
          }
        }
        } else {
          eve.target.value = "";

        }
      } else if (tData.tdObj[0].tagName == 'number') {
        var regexp = /^[0-9]+([,.][0-9]+)?$/g;
        if (regexp.test(eve.target.value)) {
          if (eve.target.value != '') {


            this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", Number(eve.target.value), "");
            for(var i=0;i<tableRow.length;i++){if(tData.toId){
                
              if(tableRow[i].id==tData.toId){
              
              var fromDate = new Date(tData.tdObj[0].tdValue);	
              var toDate = new Date(tableRow[i].tdObj[0].tdValue);
              if(fromDate > toDate){
              
               tData.tdObj[0].tdValue="";
               tData.mandatory=true;
               this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", "", "");
                var tChange=tableRow[i];
                tChange.tdObj[0].tdValue="";
                var output = this.appService.changeField(this.appService.tempProdInfo, tChange, "setVal", "", "");
                var resultCheck=JSON.stringify(output);
                
                this.display_data=JSON.parse(resultCheck);
                swal(
                  'Error!',
                  'Please Enter valid Date',
                  'error'
                )
                 break;
              }
              }
              }else if(tData.fromId){
                if(tableRow[i].id==tData.fromId){
                
                var toDate = new Date(tData.tdObj[0].tdValue);	
                var fromDate = new Date(tableRow[i].tdObj[0].tdValue);
                if(fromDate > toDate){
                 // alert("invalid");
                 tData.tdObj[0].tdValue="";
                 this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", "", "");
                  var tChange=tableRow[i];
                  tChange.tdObj[0].tdValue="";
                  
                  var output= this.appService.changeField(this.appService.tempProdInfo, tChange, "setVal", "", "");
                                  var resultCheck=JSON.stringify(output);
                  
                                  this.display_data=JSON.parse(resultCheck);
                                  swal(
                                    'Error!',
                                    'Please Enter valid Date',
                                    'error'
                                  )
                                   break;
                }
                }
                
                }
			  }


          //   this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", Number(eve.target.value), "");

          //   if(tData.minValue != '' || tData.maxValue != ''){
          //     resultVal = this.appService.validateNumDate(this.appService.tempProdInfo, tData, "minMaxValidateNum", Number(eve.target.value));
          //   }
          //   if(!resultVal){
          //     var output1 = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", "", "");
          //     this.display_data = output1
          //     // setTimeout(res => {
          //     //   this.display_data = output1;
          //     // }, 200);
          //     }else{
          //   if (tData.toId) {
          //     var result = this.appService.validateField(this.appService.tempProdInfo, tData, tData.toId, "to", "num");
          //     if (result == "invalid") {
          //       this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", "", "");
          //       // this.display_data = [];
          //       // setTimeout(res => {
          //       //   this.display_data = output;
                  
          //       // }, 200);
          //     }
          //   } else if (tData.fromId) {
          //     var result = this.appService.validateField(this.appService.tempProdInfo, tData, tData.fromId, "from", "num");
          //     if (result == "invalid") {
          //       this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", "", "");
          //       // this.display_data = [];
          //       // setTimeout(res => {
          //       //   this.display_data = output;
                  
          //       // }, 200);
          //     }
          //   }
          // }
        }
        } else {
          eve.target.value = "";
        }
      } else {
        if (tData.mappingId) {
          for (var j = 0; j < tData.mappingId.length; j++) {
            this.appService.changeField(this.appService.tempProdInfo, tData.mappingId[j], "copyValues", eve.target.value, "");
          }
        }
        if (eve.target.value != '') {
        this.display_data = this.appService.changeField(this.appService.tempProdInfo, tData, "setVal", eve.target.value, "");
        }
      }
    
    } else {
      //eve.target.value = "";
    }
  }



  setVal(field,eve,tD){
    if (localStorage.getItem("pageMode") == "2") {
      
      if(tD.sna != undefined){
        this.snumber[0].sna = tD.sna;
      }else if(tD.snb != undefined){
        this.snumber[1].snb = tD.snb;
        
      }else if(tD.snc != undefined){
        this.snumber[2].snc = tD.snc;
      }else if(tD.snd != undefined){
        this.snumber[3].snd = tD.snd;
      }

    if (field.displayAccId) {
      for (var l = 0; l < field.displayAccId.length; l++) {
        if (eve.target.value == "Y") {
          this.appService.changeAcc(this.appService.tempProdInfo, field.displayAccId[l], false,"", "displayAcc");
        } else {
          this.appService.changeAcc(this.appService.tempProdInfo, field.displayAccId[l], true, "","displayAcc");

        }
      }
    }

    if (field.navBtnId) {
      this.appService.cIndex=tD.snb;
      for (var k = 0; k < field.navBtnId.length; k++) {
        if (eve.target.value == "Y" || eve.target.value == "V") {
          this.appService.changeField1(this.appService.tempProdInfo, field.navBtnId[k], "BtnValidation", false, "");
        } else {
          this.appService.changeField1(this.appService.tempProdInfo, field.navBtnId[k], "BtnValidation", true, "");

        }
      }
    }
      if (field.mappingId) {
        for (var j = 0; j < field.mappingId.length; j++) {
          this.appService.changeField(this.appService.tempProdInfo, field.mappingId[j], "copyValues", eve.target.value, "");
        }
      }

      for (var i = 0; i < field.optionObj.length; i++) {
        if (field.optionObj[i].optionValue == eve.target.value) {
          field.optionObj[i].selected = true;
        } else {
          field.optionObj[i].selected = false;
        }
        if(i==field.optionObj.length-1){
          this.display_data = this.appService.changeField(this.appService.tempProdInfo, field, "setVal", "", "");
          if(field.id=='au_auwRule'){
           var setAUWInclude= {
              "id": "au_auwinclude",
              "blockId": "B0_5_PI_S",
              "theadName": "AUW Include",
              "mandatory": false,
              "maxLength": "1",
              "display": true,
              "mandatoryMsg": " Enter auw include",
              "tdObj": [
                {
                  "tagName": "select-one",
                  "tdValue": ""
                }
              ],
              "optionObj": [
                {
                  "optionLabel": "Yes",
                  "optionValue": "Y",
                  "selected": true
                },
                {
                  "optionLabel": "No",
                  "optionValue": "N",
                  "selected": false
                }
              ],
              "toolTip": [
                {
                  "tooltipDisplay": true,
                  "convTableName": "GNMM_PLAN_AUW_RULES",
                  "convColumnName": "V_AUW_INCLUDE",
                  "convToolTip": "Should The Auto Underwriting Rule Be Included ?"
                },
                {
                  "tooltipDisplay": false,
                  "takaTableName": "GNMM_PLAN_AUW_RULES",
                  "takaColumnName": "V_AUW_INCLUDE",
                  "takaToolTip": "Should The Auto Underwriting Rule Be Included ?"
                }
              ]
            }

            this.display_data = this.appService.changeField(this.appService.tempProdInfo, setAUWInclude, "setVal", "", "");

            var setAUWStatus={
              "id": "au_Status",
              "blockId": "B0_5_PI_S",
              "theadName": "Status",
              "mandatory": false,
              "maxLength": "1",
              "display": true,
              "mandatoryMsg": " Enter Status",
              "tdObj": [
                {
                  "tagName": "select-one",
                  "tdValue": ""
                }
              ],
              "optionObj": [
                {
                  "optionLabel": "Active",
                  "optionValue": "A",
                  "selected": true
                },
                {
                  "optionLabel": "Inactive",
                  "optionValue": "I",
                  "selected": false
                }
              ],
              "toolTip": [
                {
                  "tooltipDisplay": true,
                  "convTableName": "GNMM_PLAN_AUW_RULES",
                  "convColumnName": "V_STATUS",
                  "convToolTip": "Status"
                },
                {
                  "tooltipDisplay": false,
                  "takaTableName": "GNMM_PLAN_AUW_RULES",
                  "takaColumnName": "V_STATUS",
                  "takaToolTip": "Status"
                }
              ]
            }

            this.display_data = this.appService.changeField(this.appService.tempProdInfo, setAUWStatus, "setVal", "", "");

          }

        }
      }

    }
  }

  saveValues(pinfo: NgForm) {

    this.setCompCode();

    swal({
      title: 'Are you sure?',
      text: "You want to save!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then(async (resultSel) => {
      if (resultSel.value) {
    setTimeout(()=>{

      this.spinner.show();
    },100)
    this.appService.validFieldCount(this.appService.tempProdInfo);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var dataContent = [];
    dataContent.push({ "ProductId": this.appService.productId, "PageName": "PROD_INFO.json", "PageContent": this.appService.tempProdInfo });

    //<<comment by darong
    // this.http.post('/api/dataTmpPage', dataContent, { headers: headers })
    //   .subscribe(res => {
    //     setTimeout(() => {
    //       console.log(res.json());
    //     }, 1500);

    //   }, err => {
    //     console.log("err " + err);
    //   })
      //>>
    await this.http.post('/api/dataTmpPage', dataContent, { headers: headers }).toPromise()



    if (pinfo.valid) {
      var output = this.appService.validPage(this.appService.tempProdInfo);
      if (output.mandatory) {
        this.display_data=output.pageArray;
        this.alertMsg = [];
        for (var i = 0; i < output.mandatoryFields.length; i++) {
          this.alertMsg.push({ "msg": output.mandatoryFields[i].fieldName + ' (' + output.mandatoryFields[i].accordionName + ')' });

        }
        localStorage.setItem("alertContent", '');
        localStorage.setItem("prodAlertMsg", JSON.stringify(this.alertMsg));
        setTimeout(()=>{

          this.spinner.hide();
        },100)
        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {

          localStorage.setItem("prodAlertMsg", "");
        });
      } else {
        var result = this.storeValuesToDb();
        // var tabAccFormat = [{"accId": "pc" , "tableName" : "gnmmProdCompositions"},
        // {"accId": "rel" , "tableName" : "gnmmProdCompRelLinks"},
        // {"accId": "au" , "tableName" : "gnmmPlanAuwRulesSet"},
        // {"accId": "cop" , "tableName" : "gnmmPlanConversions"},
        // {"accId": "asvcalc" , "tableName" : "psmmPlanAsvMethods"},
        // {"accId": "asvrefund" , "tableName" : "psmmPlanAsvRefundSetups"},
        // {"accId": "mt" , "tableName" : "gnmmPlanMortLinks"}];
        this.spinner.show();
        var tabAccFormat=tabAccGenProdInfo;
      this.appService.saveValues(this.appService.tempProdInfo ,result ,tabAccFormat)
      .subscribe(res=>{
        setTimeout(()=>{
          this.spinner.hide();
        },200)
        
        if(res==true && this.productApproval){
          this.productSaved = true;
          this.appService.productSaved = false;
          localStorage.setItem("alertContent", "Saved Successfully");

          this.dialogService.addDialog(AlertComponent, { 
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '

          }).subscribe((isConfirmed) => {
            

          });
        }else{
          localStorage.setItem("alertContent", "Saved Successfully");

          this.dialogService.addDialog(AlertComponent, { 
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '

          }).subscribe((isConfirmed) => {
            

          });
        }
     
      })

       
     
      }
    }
  }
})
  }



  storeValuesToDb() {
    var format = {
      "planCode": null,
      "piName": null,
      "status":"A",
      "piNameInLocal": null,
      "piDescription": null,
      "piParentPlan": null,
      "productType": null,
      "piStartDate": null,
      "piEndDate": null,
      "piProductLine": null,
      "piProductGroup": null,
      "piProductCategory": null,
      "piGSTClassificationCode":null,
      "piLife": null,
      "pfComposite": null,
      "pfLoanProduct": null,
      "pfWopInterestRate": null,
      "pfChangeOfPlanOption": null,
      "pfCoInsurancePremium": null,
      "pfFireComposite": null,
      "pfAnnunityPlan": null,
      "pfArfPlan": null,
      "pfEpfPlan": null,
      "pfCancerProduct": null,
      "pfRopApplicable": null,
      "pfMedicalCardApplicable": null,
      "pfAsvApplicable": null,
      "pfHsCollectApplicable": null,
      "pfCiRiderApplicable": null,
      "pfStpAllowed": null,
      "pfDeclineFac": null,
      "pfBlockedInputTax": null,
      "pfUniversalLife": null,
      "pfHybridProductlLife":null,
      "gnmmPlanMortLinks": [
        {
          "mtPlanCode": null,
          "mtTableId": null,
          "mtDesc": null,
          "mtProcess": null,
          "mtValidFrom": null,
          "mtValidTo": null,
          "mtStatus": null, 
          "gnmmPlanMaster": null
        }
      ],

      //SB0_1_PI_S
      "psmmPlanAsvMethods": [
        { 
          "asvcalcPlanCode": null,
          "asvcalcProcess": null,
          "asvcalcMethod": null
        }
      ],

      //SB0_2_PI_S
      "psmmPlanAsvRefundSetups": [
        {
          "asvRefundPlanCode": null,
          "asvRefundPolicyStatusCode": null,
          "asvRefundProcessName": null,
          "asvRefundLapsedDuration": null,
          "asvrefundDaysfortriggeringLetter":null
        }
      ],

      //SB0_3_PI_S
      "gnmmProdCompositions": [
        {
          "pcPlanCode": null,
          "pcPlanRiderCode": null,
          "pcPlanRiderName": null,
          "pcRelationCriteria": null,
          "pcWop": null,
          "pcCSVToAddInPaidUp": null,
          "pcRiderStatusAfterPaidUp": null,
          "pcTermCriteria": null,
          "pcFixedTermCeaseAge": null,
          "pcSumAssuredCriteria": null,
          "pcSAfixedValue": null,
          "pcStatus": null,
          "pcIsPremiumZero": null,
          "pcReduceSA": null,
          "pcReducePremium": null,
          "pcFuturePremiumCollect": null,
          "pcPrimaryRider": null,
          "gnmmProdCompRelLinks": [
            {
              "relPlanCode": null,
              "relRiderCode": null,
              "relRelCode": null,
              "relStatus": null
            }
          ],
          "gnmmPlanAuwRulesSet": [
            {
              "auplancode": null,
              "auParentPlanCode": null,
              "auAuwRule": null,
              "auMonthIssue": null,
              "auMonthBefore": null,
              "auProductGroup": null,
              "auTableType": null,
              "auSafactor": null,
              "auMultiple": null,
              "auClaimRatio": null,
              "auPragnencyinMonths": null,
              "auNoofSticks": null,
              "auAlcohol": null,
              "auAuwInclude": null,
              "auProPentry": null,
              "auSumAtRisk": null,
              "auSumAtRiskPer": null,
              "auStatus": null,
              "auMonthsBefSurr": null,
              "auSurrIssuedMonth": null
            }
          ]
        }
      ],

      //SB0_4_PI_S
   

      ////SB0_6_PI_S
      "gnmmPlanConversions": [
        {
          "copPlanCode": null,
          "copMinYears": null,
          "copChangetoPlanCode": null,
          "copChangetoPlanName": null,
          "copStatus": null
        }
      ],

      //SB0_5_PI_S
     
    }

    return format;
  }
  dataUpload(data){
    data["PageName"]="PROD_INFO";
    this.appService.downloaddata=data;
    this.appService.uploadingPageContent=this.appService.tempProdInfo;
    this.appService.uploadingPageName="tempProdInfo";
    localStorage.setItem("upload", "true");
    this.dialogService.addDialog(DataDownloadComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '
 
    }).subscribe((isConfirmed) => { 
      // alert(1)
      // this.ngOnInit(); 

         if (isConfirmed=="done" ) {
          this.ngOnInit(); 
          for (var d = 0; d < this.appService.subPagesNavProdInfo.length; d++) {
            this.appService.subPagesNavProdInfo[d].isActive=false;
      
             
          }
   } else {
   // this.appService.uploadingPageContent=this.appService.tempProdInfo;
     }
    },err=>{
      // alert(0)
      // this.ngOnInit(); 

    })
   }

  dataDownload(data){
    data["PageName"]="PROD_INFO";
 this.appService.downloaddata=data;
   localStorage.setItem("upload", "false");
   this.dialogService.addDialog(DataDownloadComponent, {
     title: 'Name dialog',
     question1: 'Block Name ',
     question2: 'Block Type '

   }).subscribe((isConfirmed) => {
     if (isConfirmed) {
  } else {
     }
   })
  }

  copyRow(data, eve) { 
    // var countR = data.tableObj.length;
    var s;
for(var i=0;i<Object.keys(data.tableObj[data.tableObj.length-1]).length;i++){
	if(Object.keys(data.tableObj[data.tableObj.length-1])[i].includes("sn")){
		s=i;
}

}
    var countR=data.tableObj[data.tableObj.length-1][Object.keys(data.tableObj[data.tableObj.length-1])[s]];

    this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, data, ++countR,eve.target.parentElement.id, "copyRow");

   }

   retriveDel(data){
    var fieldName = [];
    localStorage.setItem("fieldNameList", "");
    for (var a = 0; a < data.deletedItems.length; a++) {
    fieldName.push({ "id": data.deletedItems[a].id, "itemName": data.deletedItems[a].fieldName });
    }
     localStorage.setItem("fieldNameList", JSON.stringify(fieldName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
        var selectedFields = [];
        for (var a = 0; a < data.deletedItems.length; a++) {
          for (var b = 0; b < seletedItems.length; b++) {
          if(seletedItems[b].id == data.deletedItems[a].id){
            selectedFields.push(data.deletedItems[a]);
          }
          }
        }
        this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, data, selectedFields,"", "addDelField");
      } else {

      }
    }) 
     
   }

   retriveTabDel(data){
    var fieldName = [];
    localStorage.setItem("fieldNameList", "");
    for (var a = 0; a < data.deletedItems.length; a++) {
    fieldName.push({ "id": data.deletedItems[a].id, "itemName": data.deletedItems[a].theadName });
    }
     localStorage.setItem("fieldNameList", JSON.stringify(fieldName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
        var selectedFields = [];
        for (var a = 0; a < data.deletedItems.length; a++) {
          for (var b = 0; b < seletedItems.length; b++) {
          if(seletedItems[b].id == data.deletedItems[a].id){
            selectedFields.push(data.deletedItems[a]);
          }
          }
        }
        this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, data, selectedFields,"", "addDelTabField");
      } else {

      }
    })
     
   }

    retriveDelAcc(){
    localStorage.setItem("fieldNameList", "");
     localStorage.setItem("fieldNameList", JSON.stringify(this.accName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
       if (isConfirmed) {
          var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
          for (var a = 0; a < this.accName.length; a++) {
            for (var b = 0; b < seletedItems.length; b++) {
              if(this.accName[a].id == seletedItems[b].id){
                this.accName.splice(a, 1);
              }
            var seletedAcc = { "accordId" :seletedItems[b].id };
            this.display_data = this.appService.changeAcc(this.appService.tempProdInfo, seletedAcc, "", "", "retriveAcc")
          }
        }
      } else {

      }
    })
    }
    searchLov(lovName,data,index,tData){
      localStorage.setItem('searchdata','table');
      this.appService.sIndex=index;
      var check=JSON.stringify(this.appService[lovName]);
      this.appService.lovListData=JSON.parse(check);
      this.appService.templateInfo=this.appService.tempProdInfo;
      this.appService.lovpiName=lovName;
      this.appService.dataInfo=data;
      this.appService.tData=tData;
      this.dialogService.addDialog(DataSearchComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '
   
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {
     } else {
       
        }
      })
  
    }
    searchLovfield(field)
    {
      localStorage.setItem('searchdata','field');
      var check=JSON.stringify(this.appService[field.lovUrl]);
      this.appService.lovListData=JSON.parse(check);
      // var ch1=JSON.stringify(field.optionObj);
      this.appService.dataInfo=field;
      this.appService.fieldName=field.fieldName;
      
      this.dialogService.addDialog(DataSearchComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '
   
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {
     } else {
       
        }
      })
    }
    xv(){

    }
    UpdateValues(pinfo: NgForm)
    {
      this.setCompCode();
      this.productUpdated=true;
      localStorage.setItem('ProductUpdated','true');
      // setTimeout(()=>{

      //   this.spinner.show();
      // },100)
      this.appService.validFieldCount(this.appService.tempProdInfo);
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      var dataContent = [];
      dataContent.push({ "ProductId": this.appService.productId, "PageName": "PROD_INFO.json", "PageContent": this.appService.tempProdInfo });
  
      this.http.post('/api/dataPublishPage', dataContent, { headers: headers })
        .subscribe(res => {
          setTimeout(() => {
            console.log(res.json());
          }, 1500);
  
        }, err => {
          console.log("err " + err);
        })
    if (pinfo.valid) {
        var output = this.appService.validPage(this.appService.tempProdInfo);
        if (output.mandatory) {
          this.alertMsg = [];
          for (var i = 0; i < output.mandatoryFields.length; i++) {
            this.alertMsg.push({ "msg": output.mandatoryFields[i].fieldName + ' (' + output.mandatoryFields[i].accordionName + ')' });
  
          }
          localStorage.setItem("alertContent", '');
          localStorage.setItem("prodAlertMsg", JSON.stringify(this.alertMsg));
          setTimeout(()=>{
  
            this.spinner.hide();
          },100)
          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '
  
          }).subscribe((isConfirmed) => {
  
            localStorage.setItem("prodAlertMsg", "");
          });
        } else {
          var result = this.storeValuesToDb();
          var tabAccFormat=tabAccGenProdInfo;
          this.appService.updateSaveValues(this.appService.tempProdInfo ,result ,tabAccFormat);
          setTimeout(() => {
            this.spinner.hide();
            if (this.appService.productSaved) {
              this.productSaved = true;
              this.appService.productSaved = false;
              localStorage.setItem("prodAlertMsg", "");
              localStorage.setItem("alertContent", "Updated Successfully");
  
              this.dialogService.addDialog(AlertComponent, { 
                title: 'Name dialog',
                question1: 'Block Name ',
                question2: 'Block Type '
  
              }).subscribe((isConfirmed) => {
                
  
              });
            }
          }, 1000)

        }
        
      }

    }

    
  validateField(pageArray, cField, FromTo, valFlag, flag) {
    for (var a = 0; a < pageArray.length; a++) {
      if (pageArray[a].field) {
        // main field
        for (var b = 0; b < pageArray[a].field.length; b++) {
          if (pageArray[a].field[b].id == FromTo) {
            if (pageArray[a].field[b].tagValue != '' && cField.tagValue != '') {
              if (flag == "date") {
                var toDate = new Date(cField.tagValue)
                var fromDate = new Date(pageArray[a].field[b].tagValue);
                if (valFlag == "to") {
                  if (toDate >= fromDate) {
                    pageArray[a].field[b].tagValue = '';
                    return "invalid";
                  } else {
                    return "valid";
                  }
                } else if (valFlag == "from") {
                  if (toDate <= fromDate) {
                    pageArray[a].field[b].tagValue = '';
                    return "invalid";
                  } else {
                    return "valid";
                  }
                }
              } else if (flag == "num") {
                var tonum = Number(cField.tagValue);
                var fromnum = Number(pageArray[a].field[b].tagValue);
                if (valFlag == "to") {
                  if (tonum > fromnum) {
                    pageArray[a].field[b].tagValue = '';
                    return "invalid";
                  } else {
                    return "valid";
                  }
                } else if (valFlag == "from") {
                  if (tonum < fromnum) {
                    pageArray[a].field[b].tagValue = '';
                    
                    return "invalid";
                  } else {
                    return "valid";
                  }
                }
              }

            }


          } else if (pageArray[a].field[b].subPage) {
            for (var zb = 0; zb < pageArray[a].field[b].subPage.length; zb++) {
              if (pageArray[a].field[b].subPage[zb].field) {
                // field - field
                for (var c = 0; c < pageArray[a].field[b].subPage[zb].field.length; c++) {
                  if (pageArray[a].field[b].subPage[zb].field[c].id == FromTo) {
                    if (pageArray[a].field[b].subPage[zb].field[c].tagValue != '' && cField.tagValue != '') {
                      if (flag == "date") {
                        var toDate = new Date(cField.tagValue)
                        var fromDate = new Date(pageArray[a].field[b].subPage[zb].field[c].tagValue);
                        if (valFlag == "to") {
                          if (toDate >= fromDate) {
                            pageArray[a].field[b].subPage[zb].field[c].tagValue = '';
                            return "invalid";
                          } else {
                            return "valid";
                          }
                        } else if (valFlag == "from") {
                          if (toDate <= fromDate) {
                            pageArray[a].field[b].subPage[zb].field[c].tagValue = '';
                            return "invalid";
                          } else {
                            return "valid";
                          }
                        }
                      } else if (flag == "num") {
                        var tonum = Number(cField.tagValue)
                        var fromnum = Number(pageArray[a].field[b].subPage[zb].field[c].tagValue);
                        if (valFlag == "to") {
                          if (tonum > fromnum) {
                            pageArray[a].field[b].subPage[zb].field[c].tagValue = '';
                            return "invalid";
                          } else {
                            return "valid";
                          }
                        } else if (valFlag == "from") {
                          if (tonum < fromnum) {
                            pageArray[a].field[b].subPage[zb].field[c].tagValue = '';
                            return "invalid";
                          } else {
                            return "valid";
                          }
                        }
                      }

                    }

                  } else if (pageArray[a].field[b].subPage[zb].field[c].subPage) {
                    // field - field - table
                    for (var zc = 0; zc < pageArray[a].field[b].subPage[zb].field[c].subPage.length; zc++) {
                      for (var f = 0; f < pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj.length; f++) {
                        if (pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].snc == this.appService.snArr[2].snc) {
                          for (var g = 0; g < pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].tableRow.length; g++) {

                            if (pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].tableRow[g].id == FromTo) {
                              if (pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].tableRow[g].tdObj[0].tdValue != '' && cField.tdObj[0].tdValue != '') {
                                if (flag == "date") {
                                  var toDate = new Date(cField.tdObj[0].tdValue)
                                  var fromDate = new Date(pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].tableRow[g].tdObj[0].tdValue);
                                  if (valFlag == "to") {
                                    if (toDate >= fromDate) {
                                      pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].tableRow[g].tdObj[0].tdValue = '';
                                      return "invalid";
                                    } else {
                                      return "valid";
                                    }
                                  } else if (valFlag == "from") {
                                    if (toDate <= fromDate) {
                                      pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].tableRow[g].tdObj[0].tdValue = '';
                                      return "invalid";
                                    } else {
                                      return "valid";
                                    }
                                  }
                                } else if (flag == "num") {
                                  var tonum = Number(cField.tdObj[0].tdValue)
                                  var fromnum = Number(pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].tableRow[g].tdObj[0].tdValue);
                                  if (valFlag == "to") {
                                    if (tonum > fromnum) {
                                      pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].tableRow[g].tdObj[0].tdValue = '';
                                      return "invalid";
                                    } else {
                                      return "valid";
                                    }
                                  } else if (valFlag == "from") {
                                    if (tonum < fromnum) {
                                      pageArray[a].field[b].subPage[zb].field[c].subPage[zc].tableObj[f].tableRow[g].tdObj[0].tdValue = '';
                                      return "invalid";
                                    } else {
                                      return "valid";
                                    }
                                  }
                                }

                              }
                            }
                          }
                        }
                      }
                    }
                  }


                }

              } else if (pageArray[a].field[b].subPage[zb].tableObj) {
                // field - table
                for (var d = 0; d < pageArray[a].field[b].subPage[zb].tableObj.length; d++) {
                  if (pageArray[a].field[b].subPage[zb].tableObj[d].snb == this.appService.snArr[1].snb) {
                    for (var e = 0; e < pageArray[a].field[b].subPage[zb].tableObj[d].tableRow.length; e++) {
                      if (pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].id == FromTo) {
                        if (pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].tdObj[0].tdValue != '' && cField.tdObj[0].tdValue != '') {
                          if (flag == "date") {
                            var toDate = new Date(cField.tdObj[0].tdValue)
                            var fromDate = new Date(pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].tdObj[0].tdValue);
                            if (valFlag == "to") {
                              if (toDate >= fromDate) {
                                pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].tdObj[0].tdValue = '';
                                return "invalid";
                              } else {
                                return "valid";
                              }
                            } else if (valFlag == "from") {
                              if (toDate <= fromDate) {
                                pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].tdObj[0].tdValue = '';
                                return "invalid";
                              } else {
                                return "valid";
                              }
                            }
                          } else if (flag == "num") {
                            var tonum = Number(cField.tdObj[0].tdValue)
                            var fromnum = Number(pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].tdObj[0].tdValue);
                            if (valFlag == "to") {
                              if (tonum > fromnum) {
                                pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].tdObj[0].tdValue = '';
                                return "invalid";
                              } else {
                                return "valid";
                              }
                            } else if (valFlag == "from") {
                              if (tonum < fromnum) {
                                pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].tdObj[0].tdValue = '';
                                return "invalid";
                              } else {
                                return "valid";
                              }
                            }
                          }

                        }
                      } else if (pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage) {
                        // field - table - table
                        for (var zd = 0; zd < pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage.length; zd++) {
                          for (var h = 0; h < pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj.length; h++) {
                            if (pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].snc == this.appService.snArr[2].snc) {
                              for (var i = 0; i < pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].tableRow.length; i++) {

                                if (pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].tableRow[i].id == FromTo) {
                                  if (pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].tableRow[i].tdObj[0].tdValue != '' && cField.tdObj[0].tdValue != '') {
                                    if (flag == "date") {
                                      var toDate = new Date(cField.tdObj[0].tdValue)
                                      var fromDate = new Date(pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].tableRow[i].tdObj[0].tdValue);
                                      if (valFlag == "to") {
                                        if (toDate >= fromDate) {
                                          pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].tableRow[i].tdObj[0].tdValue = '';
                                          return "invalid";
                                        } else {
                                          return "valid";
                                        }
                                      } else if (valFlag == "from") {
                                        if (toDate <= fromDate) {
                                          pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].tableRow[i].tdObj[0].tdValue = '';
                                          return "invalid";
                                        } else {
                                          return "valid";
                                        }
                                      }
                                    } else if (flag == "num") {
                                      var tonum = Number(cField.tdObj[0].tdValue)
                                      var fromnum = Number(pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].tableRow[i].tdObj[0].tdValue);
                                      if (valFlag == "to") {
                                        if (tonum > fromnum) {
                                          pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].tableRow[i].tdObj[0].tdValue = '';
                                          return "invalid";
                                        } else {
                                          return "valid";
                                        }
                                      } else if (valFlag == "from") {
                                        if (tonum < fromnum) {
                                          pageArray[a].field[b].subPage[zb].tableObj[d].tableRow[e].subPage[zd].tableObj[h].tableRow[i].tdObj[0].tdValue = '';
                                          return "invalid";
                                        } else {
                                          return "valid";
                                        }
                                      }
                                    }

                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }

        }


      } else if (pageArray[a].tableObj) {
        // main table
        for (var j = 0; j < pageArray[a].tableObj.length; j++) {
          if (pageArray[a].tableObj[j].sna == this.appService.snArr[0].sna) {
            for (var k = 0; k < pageArray[a].tableObj[j].tableRow.length; k++) {
              if (pageArray[a].tableObj[j].tableRow[k].id == FromTo) {
                if (pageArray[a].tableObj[j].tableRow[k].tdObj[0].tdValue != '' && cField.tdObj[0].tdValue != '') {
                  if (flag == "date") {
                  
                    if (valFlag == "to") {
                      var toDate = new Date(cField.tdObj[0].tdValue)
                      var fromDate = new Date(pageArray[a].tableObj[j].tableRow[k].tdObj[0].tdValue);
                      if (toDate >= fromDate) {
                        this.display_data[a].tableObj[j].tableRow[k].tdObj[0].tdValue = '';
                        this.display_data[a].tableObj[j].tableRow[k-1].tdObj[0].tdValue = '';
                      }
                    } else if (valFlag == "from") {
                      var fromDate = new Date(cField.tdObj[0].tdValue)
                      var  toDate= new Date(pageArray[a].tableObj[j].tableRow[k].tdObj[0].tdValue);
                      if (fromDate <= toDate) {
                        this.display_data[a].tableObj[j].tableRow[k].tdObj[0].tdValue = '';
                        this.display_data[a].tableObj[j].tableRow[k+1].tdObj[0].tdValue = '';
                      }
                    }
                    
                  } else if (flag == "num") {
                    var tonum = Number(cField.tdObj[0].tdValue)
                    var fromnum = Number(pageArray[a].tableObj[j].tableRow[k].tdObj[0].tdValue);
                    if (valFlag == "to") {
                      if (tonum > fromnum) {
                        pageArray[a].tableObj[j].tableRow[k].tdObj[0].tdValue = '';
                        return "invalid";
                      } else {
                        return "valid";
                      }
                    } else if (valFlag == "from") {
                      if (tonum < fromnum) {
                        pageArray[a].tableObj[j].tableRow[k].tdObj[0].tdValue = '';
                        return "invalid";
                      } else {
                        return "valid";
                      }
                    }
                  }

                }
              } else if (pageArray[a].tableObj[j].tableRow[k].subPage) {
                for (var za = 0; za < pageArray[a].tableObj[j].tableRow[k].subPage.length; za++) {

                  if (pageArray[a].tableObj[j].tableRow[k].subPage[za].field) {
                    // table - field
                    for (var l = 0; l < pageArray[a].tableObj[j].tableRow[k].subPage[za].field.length; l++) {

                      if (pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].id == FromTo) {
                        if (pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].tagValue != '' && cField.tagValue != '') {
                          if (flag == "date") {
                            var toDate = new Date(cField.tagValue)
                            var fromDate = new Date(pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].tagValue);
                            if (valFlag == "to") {
                              if (toDate >= fromDate) {
                                pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].tagValue = '';
                                return "invalid";
                              } else {
                                return "valid";
                              }
                            } else if (valFlag == "from") {
                              if (toDate <= fromDate) {
                                pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].tagValue = '';
                                return "invalid";
                              } else {
                                return "valid";
                              }
                            }
                          } else if (flag == "num") {
                            var tonum = Number(cField.tagValue)
                            var fromnum = Number(pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].tagValue);
                            if (valFlag == "to") {
                              if (tonum > fromnum) {
                                pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].tagValue = '';
                                return "invalid";
                              } else {
                                return "valid";
                              }
                            } else if (valFlag == "from") {
                              if (tonum < fromnum) {
                                pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].tagValue = '';
                                return "invalid";
                              } else {
                                return "valid";
                              }
                            }
                          }

                        }
                      } else if (pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage) {
                        // table - field - table
                        for (var ze = 0; ze < pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage.length; ze++) {
                          for (var o = 0; o < pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj.length; o++) {
                            if (pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].snc == this.appService.snArr[2].snc) {
                              for (var p = 0; p < pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].tableRow.length; p++) {
                                if (pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].tableRow[p].id == FromTo) {
                                  if (pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].tableRow[p].tdObj[0].tdValue != '' && cField.tdObj[0].tdValue != '') {
                                    if (flag == "date") {
                                      var toDate = new Date(cField.tdObj[0].tdValue)
                                      var fromDate = new Date(pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].tableRow[p].tdObj[0].tdValue);
                                      if (valFlag == "to") {
                                        if (toDate >= fromDate) {
                                          pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].tableRow[p].tdObj[0].tdValue = '';
                                          return "invalid";
                                        } else {
                                          return "valid";
                                        }
                                      } else if (valFlag == "from") {
                                        if (toDate <= fromDate) {
                                          pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].tableRow[p].tdObj[0].tdValue = '';
                                          return "invalid";
                                        } else {
                                          return "valid";
                                        }
                                      }
                                    } else if (flag == "num") {
                                      var tonum = Number(cField.tdObj[0].tdValue)
                                      var fromnum = Number(pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].tableRow[p].tdObj[0].tdValue);
                                      if (valFlag == "to") {
                                        if (tonum > fromnum) {
                                          pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].tableRow[p].tdObj[0].tdValue = '';
                                          return "invalid";
                                        } else {
                                          return "valid";
                                        }
                                      } else if (valFlag == "from") {
                                        if (tonum < fromnum) {
                                          pageArray[a].tableObj[j].tableRow[k].subPage[za].field[l].subPage[ze].tableObj[o].tableRow[p].tdObj[0].tdValue = '';
                                          return "invalid";
                                        } else {
                                          return "valid";
                                        }
                                      }
                                    }

                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }


                  } else if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj) {
                    // table - table
                    for (var m = 0; m < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj.length; m++) {
                      if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].snb == this.appService.snArr[1].snb) {
                        for (var n = 0; n < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow.length; n++) {
                          if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].id == FromTo) {
                            if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].tdObj[0].tdValue != '' && cField.tdObj[0].tdValue != '') {
                              if (flag == "date") {
                                var toDate = new Date(cField.tdObj[0].tdValue)
                                var fromDate = new Date(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].tdObj[0].tdValue);
                                if (valFlag == "to") {
                                  if (toDate >= fromDate) {
                                    pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].tdObj[0].tdValue = '';
                                    return "invalid";
                                  } else {
                                    return "valid";
                                  }
                                } else if (valFlag == "from") {
                                  if (toDate <= fromDate) {
                                    pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].tdObj[0].tdValue = '';
                                    return "invalid";
                                  } else {
                                    return "valid";
                                  }
                                }
                              } else if (flag == "num") {
                                var tonum = Number(cField.tdObj[0].tdValue)
                                var fromnum = Number(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].tdObj[0].tdValue);
                                if (valFlag == "to") {
                                  if (tonum > fromnum) {
                                    pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].tdObj[0].tdValue = '';
                                    return "invalid";
                                  } else {
                                    return "valid";
                                  }
                                } else if (valFlag == "from") {
                                  if (tonum < fromnum) {
                                    pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].tdObj[0].tdValue = '';
                                    return "invalid";
                                  } else {
                                    return "valid";
                                  }
                                }
                              }

                            }
                          } else if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage) {
                            for (var zf = 0; zf < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage.length; zf++) {
                              if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field) {
                                // table - table - field
                                for (var na = 0; na < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field.length; na++) {

                                  if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].id == FromTo) {
                                    if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].tagValue != '' && cField.tagValue != '') {
                                      if (flag == "date") {
                                        var toDate = new Date(cField.tagValue)
                                        var fromDate = new Date(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].tagValue);
                                        if (valFlag == "to") {
                                          if (toDate >= fromDate) {
                                            pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].tagValue = '';
                                            return "invalid";
                                          } else {
                                            return "valid";
                                          }
                                        } else if (valFlag == "from") {
                                          if (toDate <= fromDate) {
                                            pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].tagValue = '';
                                            return "invalid";
                                          } else {
                                            return "valid";
                                          }
                                        }
                                      } else if (flag == "num") {
                                        var tonum = Number(cField.tagValue)
                                        var fromnum = Number(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].tagValue);
                                        if (valFlag == "to") {
                                          if (tonum > fromnum) {
                                            pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].tagValue = '';
                                            return "invalid";
                                          } else {
                                            return "valid";
                                          }
                                        } else if (valFlag == "from") {
                                          if (tonum < fromnum) {
                                            pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].tagValue = '';
                                            return "invalid";
                                          } else {
                                            return "valid";
                                          }
                                        }
                                      }

                                    }
                                  } else if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage) {
                                    // table - table - field - table
                                    for (var nx = 0; nx < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage.length; nx++) {
                                      for (var ny = 0; ny < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj.length; ny++) {
                                        if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].snd == this.appService.snArr[3].snd) {
                                          for (var nz = 0; nz < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].tableRow.length; nz++) {
                                            if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].tableRow[nz].id == FromTo) {
                                              if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].tableRow[nz].tdObj[0].tdValue != '' && cField.tdObj[0].tdValue != '') {
                                                if (flag == "date") {
                                                  var toDate = new Date(cField.tdObj[0].tdValue)
                                                  var fromDate = new Date(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].tableRow[nz].tdObj[0].tdValue);
                                                  if (valFlag == "to") {
                                                    if (toDate >= fromDate) {
                                                      pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].tableRow[nz].tdObj[0].tdValue = '';
                                                      return "invalid";
                                                    } else {
                                                      return "valid";
                                                    }
                                                  } else if (valFlag == "from") {
                                                    if (toDate <= fromDate) {
                                                      pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].tableRow[nz].tdObj[0].tdValue = '';
                                                      return "invalid";
                                                    } else {
                                                      return "valid";
                                                    }
                                                  }
                                                } else if (flag == "num") {
                                                  var tonum = Number(cField.tdObj[0].tdValue)
                                                  var fromnum = Number(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].tableRow[nz].tdObj[0].tdValue);
                                                  if (valFlag == "to") {
                                                    if (tonum > fromnum) {
                                                      pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].tableRow[nz].tdObj[0].tdValue = '';
                                                      return "invalid";
                                                    } else {
                                                      return "valid";
                                                    }
                                                  } else if (valFlag == "from") {
                                                    if (tonum < fromnum) {
                                                      pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].field[na].subPage[nx].tableObj[ny].tableRow[nz].tdObj[0].tdValue = '';
                                                      return "invalid";
                                                    } else {
                                                      return "valid";
                                                    }
                                                  }
                                                }

                                              }
                                            }
                                          }
                                        }
                                      }

                                    }
                                  }
                                }
                              } else if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj) {
                                // table - table - table 
                                for (var q = 0; q < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj.length; q++) {
                                  if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].snc == this.appService.snArr[2].snc) {
                                    for (var r = 0; r < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow.length; r++) {
                                      if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].id == FromTo) {
                                        if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].tdObj[0].tdValue != '' && cField.tdObj[0].tdValue != '') {
                                          if (flag == "date") {
                                            var toDate = new Date(cField.tdObj[0].tdValue)
                                            var fromDate = new Date(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].tdObj[0].tdValue);
                                            if (valFlag == "to") {
                                              if (toDate >= fromDate) {
                                                pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].tdObj[0].tdValue = '';
                                                return "invalid";
                                              } else {
                                                return "valid";
                                              }
                                            } else if (valFlag == "from") {
                                              if (toDate <= fromDate) {
                                                pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].tdObj[0].tdValue = '';
                                                return "invalid";
                                              } else {
                                                return "valid";
                                              }
                                            }
                                          } else if (flag == "num") {
                                            var tonum = Number(cField.tdObj[0].tdValue)
                                            var fromnum = Number(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].tdObj[0].tdValue);
                                            if (valFlag == "to") {
                                              if (tonum > fromnum) {
                                                pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].tdObj[0].tdValue = '';
                                                return "invalid";
                                              } else {
                                                return "valid";
                                              }
                                            } else if (valFlag == "from") {
                                              if (tonum < fromnum) {
                                                pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].tdObj[0].tdValue = '';
                                                return "invalid";
                                              } else {
                                                return "valid";
                                              }
                                            }
                                          }

                                        }
                                      } else if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage) {
                                        // table - table - table - table
                                        for (var va = 0; va < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage.length; va++) {
                                          for (var vb = 0; vb < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj.length; vb++) {
                                            if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].snd == this.appService.snArr[3].snd) {
                                              for (var vc = 0; vc < pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].tableRow.length; vc++) {
                                                if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].tableRow[vc].id == FromTo) {
                                                  if (pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].tableRow[vc].tdObj[0].tdValue != '' && cField.tdObj[0].tdValue != '') {
                                                    if (flag == "date") {
                                                      var toDate = new Date(cField.tdObj[0].tdValue)
                                                      var fromDate = new Date(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].tableRow[vc].tdObj[0].tdValue);
                                                      if (valFlag == "to") {
                                                        if (toDate >= fromDate) {
                                                          pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].tableRow[vc].tdObj[0].tdValue = '';
                                                          return "invalid";
                                                        } else {
                                                          return "valid";
                                                        }
                                                      } else if (valFlag == "from") {
                                                        if (toDate <= fromDate) {
                                                          pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].tableRow[vc].tdObj[0].tdValue = '';
                                                          return "invalid";
                                                        } else {
                                                          return "valid";
                                                        }
                                                      }
                                                    } else if (flag == "num") {
                                                      var tonum = Number(cField.tdObj[0].tdValue)
                                                      var fromnum = Number(pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].tableRow[vc].tdObj[0].tdValue);
                                                      if (valFlag == "to") {
                                                        if (tonum > fromnum) {
                                                          pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].tableRow[vc].tdObj[0].tdValue = '';
                                                          return "invalid";
                                                        } else {
                                                          return "valid";
                                                        }
                                                      } else if (valFlag == "from") {
                                                        if (tonum < fromnum) {
                                                          pageArray[a].tableObj[j].tableRow[k].subPage[za].tableObj[m].tableRow[n].subPage[zf].tableObj[q].tableRow[r].subPage[va].tableObj[vb].tableRow[vc].tdObj[0].tdValue = '';
                                                          return "invalid";
                                                        } else {
                                                          return "valid";
                                                        }
                                                      }
                                                    }

                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }


  setCompCode(){
     console.log(1)
    //Setting Auw plan code from Composite
    for (var ia = 0; ia < this.appService.tempProdInfo.length; ia++) {
   
      
            if (this.appService.tempProdInfo[ia].accordId == "pf") {
      
              for (var ic = 0; ic < this.appService.tempProdInfo[ia].field.length; ic++) {
                if (this.appService.tempProdInfo[ia].field[ic].id == "btn_composite") {
      
                  for (var ib = 0; ib < this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj.length; ib++) {
                    var applyPlanCode = '';
                    for (var k = 0; k < this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow.length; k++) {
                      if (this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[k].id == "pc_PlanRiderCode") {
                        if (this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[k].optionObj) {
                          for (var id = 0; id < this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[k].optionObj.length; id++) {
                            if (this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[k].optionObj[id].selected) {
                              applyPlanCode = this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[k].optionObj[id].optionValue;
      
                            }
                          }
                        }
                      }
      
                      if (k == this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow.length - 1) {
      
                        for (var zb = 0; zb < this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow.length; zb++) {
                          if (this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[zb].id == "pc_auw") {
                            for (var zc = 0; zc < this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[zb].subPage[0].tableObj.length; zc++) {
                              for (var zd = 0; zd < this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[zb].subPage[0].tableObj[zc].tableRow.length; zd++) {
                                if (this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[zb].subPage[0].tableObj[zc].tableRow[zd].id == 'au_plancode') {
                                  this.appService.tempProdInfo[ia].field[ic].subPage[0].tableObj[ib].tableRow[zb].subPage[0].tableObj[zc].tableRow[zd].tdObj[0].tdValue = applyPlanCode;
      
                                }
                              }
                            }
                          }
                        }
      
                      }
                    }
                  }
                }
              }
            }
          }
  }

 
  
}  