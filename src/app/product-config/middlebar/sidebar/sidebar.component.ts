import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppServices } from '../../../app.service';
import { DialogService } from "ng2-bootstrap-modal";
import { AlertComponent } from "../../../bootstrap-modal/alert/alert.component";
import swal from 'sweetalert2';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  dashboard: any;

  bdCreate: boolean = false;
  bdModify: boolean = false;
  bdView: boolean = false;
  nbCreate: boolean = false;
  nbModify: boolean = false;
  nbView: boolean = false;
  uwCreate: boolean = false;
  uwModify: boolean = false;
  uwView: boolean = false;
  rcCreate: boolean = false;
  rcModify: boolean = false;
  rcView: boolean = false;


  ulCreate: boolean = false;
  ulModify: boolean = false;
  ulView: boolean = false;
  RateCreate: boolean = false;
  RateModify: boolean = false;
  RateView: boolean = false;
  FinanceCreate: boolean = false;
  FinanceModify: boolean = false;
  FinanceView: boolean = false;
  psCreate: boolean = false;
  psModify: boolean = false;
  psView: boolean = false;
  GroupCreate: boolean = false;
  GroupModify: boolean = false;
  GroupView: boolean = false;
  AnnuityCreate: boolean = false;
  AnnuityModify: boolean = false;
  AnnuityView: boolean = false;
  AgencyCreate: boolean = false;
  AgencyModify: boolean = false;
  AgencyView: boolean = false;
  chargesCreate: boolean = false;
  chargesModify: boolean = false;
  chargesView: boolean = false;
  callback: string;


  constructor(private dialogService: DialogService, private appServices: AppServices, private router: Router) { }

  ngOnInit() {
    setTimeout(() => {

      if (localStorage.getItem("pageMode") == "1") {

        this.appServices.pbdCreate = false;
        this.appServices.pbdModify = false;
        this.appServices.pbdView = false;
        this.appServices.pnbCreate = false;
        this.appServices.pnbModify = false;
        this.appServices.pnbView = false;
        this.appServices.puwCreate = false;
        this.appServices.puwModify = false;
        this.appServices.puwView = false;
        this.appServices.pulCreate = false;
        this.appServices.pulModify = false;
        this.appServices.pRateCreate = false;
        this.appServices.pRateModify = false;
        this.appServices.pRateView = false;
        this.appServices.pFinanceCreate = false;
        this.appServices.pFinanceModify = false;
        this.appServices.pFinanceView = false;
        this.appServices.ppsCreate = false;
        this.appServices.ppsModify = false;
        this.appServices.ppsView = false;
        this.appServices.pGroupCreate = false;
        this.appServices.pGroupModify = false;
        this.appServices.pGroupView = false;
        this.appServices.pAnnuityCreate = false;
        this.appServices.pAnnuityModify = false;
        this.appServices.pAnnuityView = false;
        this.appServices.pAgencyCreate = false;
        this.appServices.pAgencyModify = false;
        this.appServices.pAgencyView = false;
        this.appServices.pChargesView = false;
        this.appServices.pChargesCreate = false;
        this.appServices.pChargesModify = false;

        if (this.appServices.templateApproval) {
          this.bdView = true;
          this.rcView = true;
          this.nbView = true;
          this.uwView = true;
          this.ulView = true;
          this.RateView = true;
          this.FinanceView = true;
          this.psView = true;
          this.GroupView = true;
          this.AnnuityView = true;
          this.AgencyView = true;
          this.chargesView = true;
        } else {
          this.bdCreate = this.appServices.bdCreate;
          this.bdModify = this.appServices.bdModify;
          this.bdView = this.appServices.bdView;
          this.rcCreate = this.appServices.rcCreate;
          this.rcModify = this.appServices.rcModify;
          this.rcView = this.appServices.rcView;
          this.nbCreate = this.appServices.nbCreate;
          this.nbModify = this.appServices.nbModify;
          this.nbView = this.appServices.nbView;
          this.uwCreate = this.appServices.uwCreate;
          this.uwModify = this.appServices.uwModify;
          this.uwView = this.appServices.uwView;
          this.ulCreate = this.appServices.ulCreate;
          this.ulModify = this.appServices.ulModify;
          this.ulView = this.appServices.ulView;
          this.RateCreate = this.appServices.RateCreate;
          this.RateModify = this.appServices.RateModify;
          this.RateView = this.appServices.RateView;
          this.FinanceCreate = this.appServices.FinanceCreate;
          this.FinanceModify = this.appServices.FinanceModify;
          this.FinanceView = this.appServices.FinanceView;
          this.psCreate = this.appServices.psCreate;
          this.psModify = this.appServices.psModify;
          this.psView = this.appServices.psView;
          this.GroupCreate = this.appServices.GroupCreate;
          this.GroupModify = this.appServices.GroupModify;
          this.GroupView = this.appServices.GroupView;
          this.AnnuityCreate = this.appServices.AnnuityCreate;
          this.AnnuityModify = this.appServices.AnnuityModify;
          this.AnnuityView = this.appServices.AnnuityView;
          this.AgencyCreate = this.appServices.AgencyCreate;
          this.AgencyModify = this.appServices.AgencyModify;
          this.AgencyView = this.appServices.AgencyView;
          this.chargesCreate = this.appServices.ChargesCreate;
          this.chargesModify = this.appServices.ChargesModify;
          this.chargesView = this.appServices.ChargesView;
        }



      } else if (localStorage.getItem("pageMode") == "2") {

        this.appServices.bdCreate = false;
        this.appServices.bdModify = false;
        this.appServices.bdView = false;
        this.appServices.nbCreate = false;
        this.appServices.nbModify = false;
        this.appServices.nbView = false;
        this.appServices.uwCreate = false;
        this.appServices.uwModify = false;
        this.appServices.uwView = false;
        this.appServices.ulCreate = false;
        this.appServices.ulModify = false;
        this.appServices.ulView = false;
        this.appServices.RateCreate = false;
        this.appServices.RateModify = false;
        this.appServices.RateView = false;
        this.appServices.FinanceCreate = false;
        this.appServices.FinanceModify = false;
        this.appServices.FinanceView = false;
        this.appServices.psCreate = false;
        this.appServices.psModify = false;
        this.appServices.psView = false;
        this.appServices.GroupCreate = false;
        this.appServices.GroupModify = false;
        this.appServices.GroupView = false;
        this.appServices.AnnuityCreate = false;
        this.appServices.AnnuityModify = false;
        this.appServices.AnnuityView = false;
        this.appServices.AgencyCreate = false;
        this.appServices.AgencyModify = false;
        this.appServices.AgencyView = false;
        this.appServices.ChargesView = false;
        this.appServices.ChargesCreate = false;
        this.appServices.ChargesModify = false;

        if (this.appServices.productApproval) {
          this.bdView = true;
          this.rcView = true;
          this.nbView = true;
          this.uwView = true;
          this.ulView = true;
          this.RateView = true;
          this.FinanceView = true;
          this.psView = true;
          this.GroupView = true;
          this.AnnuityView = true;
          this.AgencyView = true;
          this.chargesView = true;
        } else {
          this.bdCreate = this.appServices.pbdCreate;
          this.bdModify = this.appServices.pbdModify;
          this.bdView = this.appServices.pbdView;
          this.rcCreate = this.appServices.prcCreate;
          this.rcModify = this.appServices.prcModify;
          this.rcView = this.appServices.prcView;
          this.nbCreate = this.appServices.pnbCreate;
          this.nbModify = this.appServices.pnbModify;
          this.nbView = this.appServices.pnbView;
          this.uwCreate = this.appServices.puwCreate;
          this.uwModify = this.appServices.puwModify;
          this.uwView = this.appServices.puwView;
          this.ulCreate = this.appServices.pulCreate;
          this.ulModify = this.appServices.pulModify;
          this.ulView = this.appServices.pulView;
          this.RateCreate = this.appServices.pRateCreate;
          this.RateModify = this.appServices.pRateModify;
          this.RateView = this.appServices.pRateView;
          this.FinanceCreate = this.appServices.pFinanceCreate;
          this.FinanceModify = this.appServices.pFinanceModify;
          this.FinanceView = this.appServices.pFinanceView;
          this.psCreate = this.appServices.ppsCreate;
          this.psModify = this.appServices.ppsModify;
          this.psView = this.appServices.ppsView;
          this.GroupCreate = this.appServices.pGroupCreate;
          this.GroupModify = this.appServices.pGroupModify;
          this.GroupView = this.appServices.pGroupView;
          this.AnnuityCreate = this.appServices.pAnnuityCreate;
          this.AnnuityModify = this.appServices.pAnnuityModify;
          this.AnnuityView = this.appServices.pAnnuityView;
          this.AgencyCreate = this.appServices.pAgencyCreate;
          this.AgencyModify = this.appServices.pAgencyModify;
          this.AgencyView = this.appServices.pAgencyView;
          this.chargesCreate = this.appServices.pChargesCreate;
          this.chargesModify = this.appServices.pChargesModify;
          this.chargesView = this.appServices.pChargesView;
        }



      }
    }, 1500)
  }

  ngAfterViewInit() {

  }

  navigationTargetActiveChange(targetId,pageName,navName,mode) {
    for (var k = 0; k < document.getElementsByClassName("mHover").length; k++) {
      document.getElementsByClassName("mHover")[k].classList.remove("on");
    }

    var navPage=true;
    if(mode=="1"){
      for(var i=0;i<this.appServices.templatePageList[0].PageList.length;i++){
        if(this.appServices.templatePageList[0].PageList[i].name==pageName){
          if(this.appServices.templatePageList[0].PageList[i].review=="Approved"){
            navPage=true;
          }
        }
      }
    }else if(mode=="2"){
      for(var i=0;i<this.appServices.productPageList[0].PageList.length;i++){
        if(this.appServices.productPageList[0].PageList[i].name==pageName){
          if(this.appServices.productPageList[0].PageList[i].review){
            navPage=true;
          }
        }
      }
    }
   
    if(navPage){
      document.getElementById(targetId).classList.add("on");
      this.router.navigate(['./productConfig/'+navName]);
    }else{
      swal({
        title: 'Status',
        text: "Page already approved!",
        type: 'warning',
        width: 400,
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ok'
      }).then((result) => {
        if (result.value) {
        }
      })
    }
     
    
  }

  navNB() {

    if (localStorage.getItem("pageMode") == "1") {
        this.navigationTargetActiveChange("newbusiness","NEW_BUSINESS","operation-newbusiness","1");
      
    } else if (localStorage.getItem("pageMode") == "2") {
   
          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            this.navigationTargetActiveChange("newbusiness","NEW_BUSINESS","operation-newbusiness","2");
            this.router.navigate(['./productConfig/operation-newbusiness']);
            
        
        setTimeout(() => {
          var approve = [];
          approve.push({
            "ProductId": this.appServices.productId,
            "ProductName": this.appServices.productName,
            "PageName": "NEW_BUSINESS"
          })
  
          this.appServices.firstBlockStatus(approve);
          this.appServices.fStatus.subscribe(status=>{
              if(!status){
                this.appServices.operation_nbusiness=this.appServices.firstBlockValues(this.appServices.operation_nbusiness);
              }
          })

     
        }, 2000)

          }
    }






  }

  navUW() {
    if (localStorage.getItem("pageMode") == "1") {
        this.navigationTargetActiveChange("underwriting","OPERATION_UNDERWRITING","opertional-underwriting","1");
   
    } else if (localStorage.getItem("pageMode") == "2") {

          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            
            //this.navigationTargetActiveChange("underwriting");
            this.navigationTargetActiveChange("underwriting","OPERATION_UNDERWRITING","opertional-underwriting","2");
            
        setTimeout(() => {
          var approve = [];
          approve.push({
            "ProductId": this.appServices.productId,
            "ProductName": this.appServices.productName,
            "PageName": "OPERATION_UNDERWRITING"
          })
  
          this.appServices.firstBlockStatus(approve);
          this.appServices.fStatus.subscribe(status=>{
              if(!status){
                this.appServices.operation_uwritting=this.appServices.firstBlockValues(this.appServices.operation_uwritting);
              }
          })

        }, 2000)

          }




      
    }


  }

  navUL() {
    if (localStorage.getItem("pageMode") == "1") {

        this.navigationTargetActiveChange("unitlink","OPERATION_UNITLINKED","operational-unitlinks","1");
  
    } else if (localStorage.getItem("pageMode") == "2") {
    
          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            //this.navigationTargetActiveChange("unitlink");
            this.navigationTargetActiveChange("unitlink","OPERATION_UNITLINKED","operational-unitlinks","2");
            setTimeout(() => {
              var approve = [];
              approve.push({
                "ProductId": this.appServices.productId,
                "ProductName": this.appServices.productName,
                "PageName": "OPERATION_UNITLINKED"
              })
      
              this.appServices.firstBlockStatus(approve);
              this.appServices.fStatus.subscribe(status=>{
                  if(!status){
                    this.appServices.operation_unitlink=this.appServices.firstBlockValues(this.appServices.operation_unitlink);
                  }
              })

          }, 2000)
          }




     
    }


  }

  navAnn() {
    if (localStorage.getItem("pageMode") == "1") {
        this.navigationTargetActiveChange("annuity","OPERATION_ANNUITY","operational-annuity","1");
        
      
    } else if (localStorage.getItem("pageMode") == "2") {
  
          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            //this.navigationTargetActiveChange("annuity");
            this.navigationTargetActiveChange("annuity","OPERATION_ANNUITY","operational-annuity","2");       
                 setTimeout(() => {
              var approve = [];
              approve.push({
                "ProductId": this.appServices.productId,
                "ProductName": this.appServices.productName,
                "PageName": "OPERATION_ANNUITY"
              })
      
              this.appServices.firstBlockStatus(approve);
              this.appServices.fStatus.subscribe(status=>{
                  if(!status){
                    this.appServices.operation_annuity=this.appServices.firstBlockValues(this.appServices.operation_annuity);
                  }
              })
        }, 2000)
          }


        

    
    }


  }

  navProdFeeRate() {

    if (localStorage.getItem("pageMode") == "1") {
 
        this.navigationTargetActiveChange("prodFeeRate","PRODUCT_RATES","productFee-rates","1");
        
      
    } else if (localStorage.getItem("pageMode") == "2") {

          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            //this.navigationTargetActiveChange("prodFeeRate");
            this.navigationTargetActiveChange("prodFeeRate","PRODUCT_RATES","productFee-rates","2");
            setTimeout(() => {
              var approve = [];
        approve.push({
          "ProductId": this.appServices.productId,
          "ProductName": this.appServices.productName,
          "PageName": "PRODUCT_RATES"
        })

        this.appServices.firstBlockStatus(approve);
        this.appServices.fStatus.subscribe(status=>{
            if(!status){
              this.appServices.product_rate=this.appServices.firstBlockValues(this.appServices.product_rate);
            }
        })
          }, 2000)
          }
    }

        


  }

  navAgency() {

    if (localStorage.getItem("pageMode") == "1") {

        this.navigationTargetActiveChange("agency","AGENCY","agency","1");
   
      
    } else if (localStorage.getItem("pageMode") == "2") {
 
          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            //this.navigationTargetActiveChange("agency");
            this.navigationTargetActiveChange("agency","AGENCY","agency","2");
            setTimeout(() => {
              var approve = [];
              approve.push({
                "ProductId": this.appServices.productId,
                "ProductName": this.appServices.productName,
                "PageName": "AGENCY"
              })
      
              this.appServices.firstBlockStatus(approve);
              this.appServices.fStatus.subscribe(status=>{
                  if(!status){
                    this.appServices.agency=this.appServices.firstBlockValues(this.appServices.agency);
                  }
              })
              }, 2000)
          }


        }

  }

  navGroup() {

    if (localStorage.getItem("pageMode") == "1") {
     
        this.navigationTargetActiveChange("group","OPERATION_GROUP","operational-group","1");
        
      
    } else if (localStorage.getItem("pageMode") == "2") {
    
          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            //this.navigationTargetActiveChange("group");
            this.navigationTargetActiveChange("group","OPERATION_GROUP","operational-group","2");
            setTimeout(() => {
              var approve = [];
              approve.push({
                "ProductId": this.appServices.productId,
                "ProductName": this.appServices.productName,
                "PageName": "OPERATION_GROUP"
              })
      
              this.appServices.firstBlockStatus(approve);
              this.appServices.fStatus.subscribe(status=>{
                  if(!status){
                    this.appServices.group=this.appServices.firstBlockValues(this.appServices.group);
                  }
              })
          }, 2000)
          }


    }


  }

  navPS() {

    if (localStorage.getItem("pageMode") == "1") {
    
        this.navigationTargetActiveChange("policyservicing","POLICYSERVICING","operational-policyservicing","1");

      
    } else if (localStorage.getItem("pageMode") == "2") {
      
          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            //this.navigationTargetActiveChange("policyservicing");
            this.navigationTargetActiveChange("policyservicing","POLICYSERVICING","operational-policyservicing","2");
            setTimeout(() => {
              var approve = [];
              approve.push({
                "ProductId": this.appServices.productId,
                "ProductName": this.appServices.productName,
                "PageName": "POLICYSERVICING"
              })
      
              this.appServices.firstBlockStatus(approve);
              this.appServices.fStatus.subscribe(status=>{
                  if(!status){
                    this.appServices.policyServicing=this.appServices.firstBlockValues(this.appServices.policyServicing);
                  }
              })
          }, 2000)
          }


      } 
     


  }

  navProdFeeFinance() {


    if (localStorage.getItem("pageMode") == "1") {
   
        this.navigationTargetActiveChange("finance","PRODUCT_FINANCE","productFee-finance","1");
      
    } else if (localStorage.getItem("pageMode") == "2") {
   
          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            //this.navigationTargetActiveChange("finance");
            this.navigationTargetActiveChange("finance","PRODUCT_FINANCE","productFee-finance","2");
            setTimeout(() => {
              var approve = [];
              approve.push({
                "ProductId": this.appServices.productId,
                "ProductName": this.appServices.productName,
                "PageName": "PRODUCT_FINANCE"
              })
      
              this.appServices.firstBlockStatus(approve);
              this.appServices.fStatus.subscribe(status=>{
                  if(!status){
                    this.appServices.finance=this.appServices.firstBlockValues(this.appServices.finance);
                  }
              })
      }, 2000)
          }


    
    }



  }

  navProdFeeCharges() {

    if (localStorage.getItem("pageMode") == "1") {
        this.navigationTargetActiveChange("charges","PRODUCT_CHARGES","productFee-charges","1");
      
    } else if (localStorage.getItem("pageMode") == "2") {
  
          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            //this.navigationTargetActiveChange("charges");
            this.navigationTargetActiveChange("charges","PRODUCT_CHARGES","productFee-charges","2");
            setTimeout(() => {
              var approve = [];
              approve.push({
                "ProductId": this.appServices.productId,
                "ProductName": this.appServices.productName,
                "PageName": "PRODUCT_CHARGES"
              })
      
              this.appServices.firstBlockStatus(approve);
              this.appServices.fStatus.subscribe(status=>{
                  if(!status){
                    this.appServices.charges=this.appServices.firstBlockValues(this.appServices.charges);
                  }
              })
        }, 2000)


        }


    }




  }

  navPB() {

    if (localStorage.getItem("pageMode") == "1") {
        this.navigationTargetActiveChange("prodbenefits","PROD_BENEFITS","riskcoverage","1");
     
    } else if (localStorage.getItem("pageMode") == "2") {
 
          this.callback = this.prodInfoValues();
          if (this.callback == "done") {
            //this.navigationTargetActiveChange("prodbenefits");
            this.navigationTargetActiveChange("prodbenefits","PROD_BENEFITS","riskcoverage","2");
            setTimeout(() => {
              var approve = [];
              approve.push({
                "ProductId": this.appServices.productId,
                "ProductName": this.appServices.productName,
                "PageName": "PROD_BENEFITS"
              })
      
              this.appServices.firstBlockStatus(approve);
              this.appServices.fStatus.subscribe(status=>{
                  if(!status){
                    this.appServices.risk_coverage=this.appServices.firstBlockValues(this.appServices.risk_coverage);
                  }
              })
            }, 2000)
          }


        }
      }      

  linkClick(eve) {
  }

  makeDashBoardActive(eve) {
    //this.navigationTargetActiveChange("dashboard");

    if (localStorage.getItem("modeSelection") == "template") {
      this.router.navigate(['./productConfig/dashboard']);
    } else if (localStorage.getItem("modeSelection") == "data") {
      this.router.navigate(['./productConfig/dashboarddata']);
    }


  }

  makeActive(eve) {
    if (localStorage.getItem("pageMode") == "1") {
      if (localStorage.getItem("selectedTemplate") != "") {
       
        this.navigationTargetActiveChange("productInfo","PROD_INFO","productInfo","1");
        
      } else {
        this.showAlert();
      }
    } else if (localStorage.getItem("pageMode") == "2") {
      if (localStorage.getItem("selectedProduct") != "") {
        //this.navigationTargetActiveChange("productInfo");
        this.navigationTargetActiveChange("productInfo","PROD_INFO","productInfo","2");
        setTimeout(() => {
          this.appServices.tempProdInfo=this.appServices.firstBlockValues(this.appServices.tempProdInfo);
          console.log(this.appServices.tempProdInfo)
        }, 2000)

      } else {

        this.showAlert();
      }

    }



    //   this.localstorage.set("riskCoverage",0);

    // for(var i=0;i<document.getElementsByClassName("mHover").length;i++){
    //   if (document.getElementsByClassName("mHover")[i].getAttribute("style")) {
    //    document.getElementsByClassName("mHover")[i].setAttribute("style", "background-color:'';");
    //   }

    // }  

    //   eve.target.setAttribute("style", "background-color:rgba(255,255,255,0.2);");
    //   this.localstorage.set("passStatus",true);
  }

  pConfigSelect() {
    //document.getElementById("productInfo").setAttribute("style","background-color:rgba(255,255,255,0.2);");
  }

  expandSmenu(eve) {
    if (eve.target.nextElementSibling.style.display == "none") {
      eve.target.nextElementSibling.style.display = "block";
    } else if (eve.target.nextElementSibling.style.display == "block") {
      eve.target.nextElementSibling.style.display = "none";
    }


  }

  showAlert() {
    localStorage.setItem("alertContent", "Please navigate from dashboard");

    this.dialogService.addDialog(AlertComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      if (localStorage.getItem("pageMode") == "1") {
        this.router.navigate(['/productConfig/dashboard']);
      } else if (localStorage.getItem("pageMode") == "2") {
        this.router.navigate(['/productConfig/dashboarddata']);
      }
    });
  }


  prodInfoValues() {

    localStorage.setItem("getInfoValues", "1");

    if (document.getElementById("piName") != null) {
      this.appServices.piname = (<HTMLInputElement>document.getElementById("piName").parentElement.getElementsByClassName("field_1")[0].children[0]).value;
    }
    if (document.getElementById("piNameInLocal") != null) {
      this.appServices.piNameInLocal = (<HTMLInputElement>document.getElementById("piNameInLocal").parentElement.getElementsByClassName("field_1")[0].children[0]).value;
    }
    if (document.getElementById("piDescription") != null) {
      this.appServices.piDescription = (<HTMLInputElement>document.getElementById("piDescription").parentElement.getElementsByClassName("field_1")[0].children[0]).value;
    }
    if (document.getElementById("productType") != null) {
      if ((<HTMLInputElement>document.getElementById("productType").parentElement.getElementsByClassName("field_1")[0].children[0]).value != "--Select--") {
        this.appServices.productType = (<HTMLInputElement>document.getElementById("productType").parentElement.getElementsByClassName("field_1")[0].children[0]).value;
      }
    }
    if (document.getElementById("piStartDate") != null) {
      this.appServices.piStartDate = (<HTMLInputElement>document.getElementById("piStartDate").parentElement.getElementsByClassName("field_1")[0].children[0]).value;
    }
    if (document.getElementById("piEndDate") != null) {
      this.appServices.piEndDate = (<HTMLInputElement>document.getElementById("piEndDate").parentElement.getElementsByClassName("field_1")[0].children[0]).value;
    }
 
    if (document.getElementById("piProductLine") != null) {
      if ((<HTMLInputElement>document.getElementById("piProductLine").parentElement.getElementsByClassName("field_1")[0].children[0]).value != "--Select--") {
        this.appServices.piProductLine = (<HTMLInputElement>document.getElementById("piProductLine").parentElement.getElementsByClassName("field_1")[0].children[0]).value;
      }
    }
    if (document.getElementById("piParentPlan") != null) {
      this.appServices.piParentPlan = (<HTMLInputElement>document.getElementById("piParentPlan").parentElement.getElementsByClassName("field_1")[0].children[0]).value;
    }

    return "done";
  }



}