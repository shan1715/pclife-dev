import { Component, OnInit,AfterViewInit } from '@angular/core';
import {AppServices} from '../../app.service';
import {Subscription} from 'rxjs';
@Component({
  selector: 'app-footerbar',
  templateUrl: './footerbar.component.html',
  styleUrls: ['./footerbar.component.css']
})
export class FooterbarComponent implements OnInit,AfterViewInit {
  isDboard:boolean=false;
  subscription:Subscription;
  constructor(private appServices:AppServices) { 
      this.subscription=this.appServices.isDboard.subscribe((data:boolean)=>{
      this.isDboard=data;
    })
  }

  ngOnInit() {
    

  }

  ngAfterViewInit(){
    

  }

}
