import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationsNewbusinessComponent } from './operations-newbusiness.component';

describe('OperationsNewbusinessComponent', () => {
  let component: OperationsNewbusinessComponent;
  let fixture: ComponentFixture<OperationsNewbusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationsNewbusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationsNewbusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
