import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef, AfterContentInit } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AppServices } from '../app.service';
import { PrompComponent } from "../bootstrap-modal/Modaldialog/promp.component";
import { DialogService } from "ng2-bootstrap-modal";
import { FielddialogueComponent } from "../bootstrap-modal/Fielddialogue/fielddialogue.component";
import { EditPenComponent } from "../bootstrap-modal/edit-pen/edit-pen.component";
import { AlertComponent } from "../bootstrap-modal/alert/alert.component";
import { PatternValidator } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Observable } from "rxjs";
import {NgForm} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataDownloadComponent } from '../bootstrap-modal/dataDownload/dataDownload.component';
import { AddRowComponent } from "../bootstrap-modal/add-row/add-row.component";
import { RetriveDelComponent } from "../bootstrap-modal/retrive-del/retrive-del.component";
  
import { DataSearchComponent } from '../bootstrap-modal/dataSearch/dataSearch.component';
import { default as data_json } from '../../assets/data/newbusiness.json';
import {tabAccopNewBusinessFormat } from '../tabAccordformat'
import swal from 'sweetalert2';


@Component({
  selector: 'operations-newbusiness',
  templateUrl: './operations-newbusiness.component.html',
  styleUrls: ['./operations-newbusiness.component.css'],
  providers: [DragulaService]

})

export class OperationsNewbusinessComponent implements OnInit, AfterContentInit {
  backLinkBool: boolean =  false;
  display_data: any = [];
  parentName: any;
  backlinkField:any;
  navId:any;
  templateview: boolean = false;
  templateApproval: boolean = false;
  productApproval: boolean = false;
  templateSaved: boolean = false;
  productSaved: boolean = false;
  draggedItem:any=[];
  showAddBtn:boolean=false;
  showRow:boolean=false;
  count = 1;
  pageMode:any=0;
  nbCreate: boolean = false;
  nbModify: boolean = false;
  nbView: boolean = false;
  pnbCreate: boolean = false;
  pnbModify: boolean = false;
  pnbView: boolean = false;
  alertMsg:any=[];
  snumber:any=[{"sna":0},{"snb":0},{"snc":0},{"snd":0}];
  blockName: any;
  accName = [];
  productUpdated: boolean = false;
  pbdUpdate: boolean = false;
  expand:any;
  dropIndex:any;
  currenttemp:any;
  dragField:any;
  maintainPlanConvTo:any=[];

  constructor(private spinner: NgxSpinnerService,private router:Router, private http: Http,private dragulaService: DragulaService, private appService: AppServices,private dialogService:DialogService) { 
    dragulaService.setOptions('second-bag', {
      copy: (el, source) => {
        // alert(source.id)
        this.dragField=source.id;
        //return source.id === 'ok';
      },
      accepts: (el, target, source, sibling) => {
        // To avoid dragging from right to left container
        // alert(target.id)
        if(target.id === this.dragField){
          return true;
        }else{
          return false;
        }
        
      }
    });
    
    this.dragulaService.drag.subscribe((value: any) => {
      console.log("drag");

      this.onDrag(value.slice(1));

    });

    this.dragulaService.drop.subscribe((value: any) => {
      this.onDrop(value.slice(1));
      console.log("drop");
    });
    this.dragulaService.over.subscribe((value: any) => {
      this.onOver(value.slice(1));
      console.log("over");
    });
    this.dragulaService.out.subscribe((value: any) => {
      this.onOut(value.slice(1));
      console.log("out");
    });


  }




  ngOnInit() {
    this.expand=0;
    this.currenttemp=this.appService.currentTemplate;
    this.appService.pageName='NEW_BUSINESS';
    this.appService.snArr = this.snumber;

    if (localStorage.getItem("pageMode") == "1") {
      this.pageMode=1;
      if(this.appService.operation_nbusiness.length>0){
        this.display_data=this.appService.operation_nbusiness;
        this.blockName =this.appService.getAllBlockName(this.appService.operation_nbusiness);
      }else{
              this.appService.operation_nbusiness = data_json;
        this.display_data = this.appService.operation_nbusiness;
        this.blockName =this.appService.getAllBlockName(this.appService.operation_nbusiness);

    // this.http.get('assets/data/newbusiness.json')
    //   .subscribe((res) => {
      
    //     this.appService.operation_nbusiness = res.json();
    //     this.display_data = this.appService.operation_nbusiness;
    //     this.blockName =this.appService.getAllBlockName(this.appService.operation_nbusiness);

    //   }, err => {
    //     console.log(err);
    //   })
    }

    if(this.appService.templateApproval){
      var approve = [];
        approve.push({
        "TemplateId": this.appService.templateId,
        "TemplateName": this.appService.templateName,
        "PageName": "NEW_BUSINESS" 
        })

        this.spinner.hide();
        var headers = new Headers();
        headers.append("Content-Type", "application/json");
        this.http.post('/api/readTempApproveStatus', JSON.stringify(approve[0]), { headers: headers })
          .subscribe(res => {  
            this.spinner.hide();
            if (res.json().review == false) {
               
              this.appService.templateSavedStaus=true;
              this.templateSaved=true;
            }else{
              this.appService.templateSavedStaus=false;
              this.templateSaved=false;
            }
    
          }, err => {
            this.spinner.hide();
            console.log("err " + err);
          })
        }
    }else if (localStorage.getItem("pageMode") == "2") {
      this.pageMode=2;
      if(this.appService.operation_nbusiness.length>0){

        this.appService.lovWS(this.appService.operation_nbusiness);
        this.display_data=this.appService.operation_nbusiness;

        if(this.appService.productApproval){
          var approve = [];
          approve.push({
            "ProductId": this.appService.productId,
            "ProductName": this.appService.productName,
            "PageName": "NEW_BUSINESS"
          })

          var headers = new Headers();
          headers.append("Content-Type", "application/json");
          this.http.post('/api/readProdApproveStatus', JSON.stringify(approve[0]), { headers: headers })
            .subscribe(res => {  
              if (res.json().review == false) {
                this.productSaved = true;
              }else{
                this.productSaved = false;

              }
            }, err => {
              console.log("err " + err);
            })
        }
         }else{
        
          localStorage.setItem("alertContent", "Template Not Created");

          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '

          }).subscribe((isConfirmed) => {
            this.router.navigate(['/productConfig/dashboarddata']);
            
          });
         }
    }
  }

  ngAfterContentInit(){
    if (localStorage.getItem("pageMode") == "1") {
      if (document.getElementById("sValues")) {
        document.getElementById("sValues").style.display = "none";
      }
      setTimeout(()=>{
        this.showAddBtn = true;
      this.showRow = false;
      this.nbCreate = this.appService.nbCreate;
        this.nbModify = this.appService.nbModify;
        this.nbView = this.appService.nbView;
      this.templateApproval = this.appService.templateApproval;
      if (this.appService.publishedTemplate) {
        this.nbCreate = false;
        this.nbModify = false;
        this.templateApproval = false;
      }
      },500)   
      
      
    } else if (localStorage.getItem("pageMode") == "2") {
      if (document.getElementById("sFormat")) {
        document.getElementById("sFormat").style.display = "none";
      }

      setTimeout(()=>{
      this.showRow = true;
      this.showAddBtn = false;

      this.pnbCreate = this.appService.pnbCreate;
      this.pnbModify = this.appService.pnbModify;
      this.pnbView = this.appService.pnbView;
      this.productApproval = this.appService.productApproval;
      if (this.appService.publishedProduct) {
        this.pnbCreate = false;
            this.pnbModify = false;
        this.productApproval = false;
        this.pbdUpdate=true;
      }
      this.dragulaService.find("second-bag").drake.destroy();
      //var output = this.appService.validPage(this.appService.operation_nbusiness);

    },500);
  }
}

nav(i,nfield,id,tD) {
  this.expand=i;
  if(tD.sna != undefined){
    this.snumber=[{"sna":0},{"snb":0},{"snc":0},{"snd":0}];
    this.snumber[0].sna = tD.sna;
  }else if(tD.snb != undefined){
    this.snumber[1].snb = tD.snb;
    
  }else if(tD.snc != undefined){
    this.snumber[2].snc = tD.snc;
  }else if(tD.snd != undefined){
    this.snumber[3].snd = tD.snd;
  }
  this.appService.snArr = this.snumber;
           this.appService.selectedNav=id;
           
           var planconv="";
           if(nfield.id=="btn_planConversion"){
            
              planconv=document.getElementById("sac_convertible").nextElementSibling.getElementsByTagName("select")[0].value;
            
          }
   
            this.display_data=this.appService.changeField(this.appService.operation_nbusiness,nfield,"nav","",this.appService.subPagesNavNB);

    if(nfield.id=="btn_planConversion"){
      this.spinner.show();
      if(planconv=="F"){
        
        this.http.get(this.appService.ws_url+"lovtable/ConvertibleTypeF/"+this.appService.planCodeDD)
        .subscribe((resF)=>{
          this.maintainPlanConvTo=[];
          this.maintainPlanConvTo=resF.json();
          for(var df=0;df<this.display_data.length;df++){
            if(this.display_data[df].accordId=='pc'){
              for(var da=0;da<this.display_data[df].tableObj.length;da++){
              for(var db=0;db<this.display_data[df].tableObj[da].tableRow.length;db++){
                if(this.display_data[df].tableObj[da].tableRow[db].id=="pc_plancodeto"){
                  var selectedData;
                  if(this.display_data[df].tableObj[da].tableRow[db].optionObj.length>0){
                  for(var dc=0;dc<this.display_data[df].tableObj[da].tableRow[db].optionObj.length;dc++){
                   if(this.display_data[df].tableObj[da].tableRow[db].optionObj[dc].selected){
                    selectedData=this.display_data[df].tableObj[da].tableRow[db].optionObj[dc].optionValue;
        
        }
        if(dc==this.display_data[df].tableObj[da].tableRow[db].optionObj.length-1){
         
          this.display_data[df].tableObj[da].tableRow[db].optionObj=[];
          
          for(var dt=0;dt<resF.json().length;dt++){
            this.display_data[df].tableObj[da].tableRow[db].optionObj.push({"optionValue":resF.json()[dt].code,"optionLabel":resF.json()[dt].desc});
            
          
          }
          
          for(var dd=0;dd<this.display_data[df].tableObj[da].tableRow[db].optionObj.length;dd++){
          if(selectedData!=undefined){
                   if(this.display_data[df].tableObj[da].tableRow[db].optionObj[dd].optionValue==selectedData){
                    this.display_data[df].tableObj[da].tableRow[db].optionObj[dd].selected=true;
                   }
        
        }
        

        }
        this.spinner.hide();
        }
        
        }
          
        
        }else{
          for(var dt=0;dt<resF.json().length;dt++){
            this.display_data[df].tableObj[da].tableRow[db].optionObj.push({"optionValue":resF.json()[dt].code,"optionLabel":resF.json()[dt].desc});
          
          }
          this.spinner.hide();
        }
        
                    }
        
        }
        
        }
        
        }
      }
        })

        setTimeout(()=>{
          if(document.getElementById("pcn")){
          document.getElementById("pcn").parentElement.style.display="none";
          }
        },1500)
           

      }else if(planconv=="T"){
        this.http.get(this.appService.ws_url+"lovtable/ConvertibleTypeT/"+this.appService.planCodeDD)
        .subscribe((resF)=>{
          this.maintainPlanConvTo=[];
          this.maintainPlanConvTo=resF.json();
          for(var df=0;df<this.display_data.length;df++){
            if(this.display_data[df].accordId=='pcn'){
              for(var da=0;da<this.display_data[df].tableObj.length;da++){
              for(var db=0;db<this.display_data[df].tableObj[da].tableRow.length;db++){
                if(this.display_data[df].tableObj[da].tableRow[db].id=="pcn_plancodefrom"){
                  var selectedData;
                  if(this.display_data[df].tableObj[da].tableRow[db].optionObj.length>0){
                  for(var dc=0;dc<this.display_data[df].tableObj[da].tableRow[db].optionObj.length;dc++){
                   if(this.display_data[df].tableObj[da].tableRow[db].optionObj[dc].selected){
                    selectedData=this.display_data[df].tableObj[da].tableRow[db].optionObj[dc].optionValue;
        
        }
        if(dc==this.display_data[df].tableObj[da].tableRow[db].optionObj.length-1){
        

          this.display_data[df].tableObj[da].tableRow[db].optionObj=[];
          
          for(var dt=0;dt<resF.json().length;dt++){
            this.display_data[df].tableObj[da].tableRow[db].optionObj.push({"optionValue":resF.json()[dt].code,"optionLabel":resF.json()[dt].desc});

          
          }
          
          for(var dd=0;dd<this.display_data[df].tableObj[da].tableRow[db].optionObj.length;dd++){
          if(selectedData!=undefined){
                   if(this.display_data[df].tableObj[da].tableRow[db].optionObj[dd].optionValue==selectedData){
                    this.display_data[df].tableObj[da].tableRow[db].optionObj[dd].selected=true;
                   }
        
        }
        }
        this.spinner.hide();

        }
        
        }
          
        
        }else{
          for(var dt=0;dt<resF.json().length;dt++){
            this.display_data[df].tableObj[da].tableRow[db].optionObj.push({"optionValue":resF.json()[dt].code,"optionLabel":resF.json()[dt].desc});
          
          }
          this.spinner.hide();

        }
        
                    }
        
        }
        
        }
        
        }
      }
        })
        setTimeout(()=>{
          if(document.getElementById("pc")){
          document.getElementById("pc").parentElement.style.display="none";
        }
        },1500)
        
      }

    }


    for(var d=0;d<this.appService.subPagesNavNB.length;d++){
      if(this.appService.subPagesNavNB[d].isActive){

        this.navId=this.appService.subPagesNavNB[d].id;
      }
    }
    this.parentName = nfield.parentName;
    this.backLinkBool =true;
    this.backlinkField=nfield;
    if (localStorage.getItem("pageMode") == "1") {
      this.accName=[];
      for (var a = 0; a < this.display_data.length; a++) {
        if(this.display_data[a].deleteAcc != undefined){
          this.accName.push({ "id": this.display_data[a].accordId, "itemName": this.display_data[a].accordionName });
        }
      }
    }
  }
 
  backLink() { 

    var mpageNavigation=false;
    for(var b=0;b<this.appService.subPagesNavNB.length;b++){
        if(this.appService.subPagesNavNB[b].id==this.navId){ 
          mpageNavigation=true;
          this.appService.subPagesNavNB[b].isActive=false;
          this.display_data = this.appService.changeField(this.appService.operation_nbusiness,{"id":this.navId},"back","","");
          this.parentName=this.appService.subPagesNavNB[b].grantparentName;
          this.navId=this.appService.subPagesNavNB[b].parentId;
          if(this.navId==""){
            this.backLinkBool=false;
          }
        }
    }
      if(!mpageNavigation){
        this.display_data = this.appService.changeField(this.appService.operation_nbusiness,{"id":this.backlinkField},"back","","");

      }
      if (localStorage.getItem("pageMode") == "1") {
        this.accName=[];
        for (var a = 0; a < this.display_data.length; a++) {
          if(this.display_data[a].deleteAcc != undefined){
            this.accName.push({ "id": this.display_data[a].accordId, "itemName": this.display_data[a].accordionName });
          }
        }
      }
    
   
  }

  delfield(dfield) {
    this.display_data=this.appService.changeField(this.appService.operation_nbusiness,dfield,"del","","");
    }

    incSize(incfield,eve) {
      this.display_data=this.appService.changeField(this.appService.operation_nbusiness,incfield,"inc",eve.target.parentElement.parentElement.parentElement.style.width,"");
      
    }

    decSize(decfield,eve){
      this.display_data=this.appService.changeField(this.appService.operation_nbusiness,decfield,"dec",eve.target.parentElement.parentElement.parentElement.style.width,"");
      
    }

    editpenAccodion(eve) {

      eve.target.previousElementSibling.firstElementChild.removeAttribute("readonly");
      eve.target.previousElementSibling.firstElementChild.focus();
      eve.target.previousElementSibling.firstElementChild.style.backgroundColor = "#9ACD32" ;
    }

    disableEditAccordion(data,eve) {
      this.display_data=this.appService.changeAcc(this.appService.operation_nbusiness,data,eve.target.value,"","editAcc");
      eve.target.setAttribute("readonly", true);
      eve.target.style.backgroundColor = "#FFFFFF";
    }

    delAccordion(data) {
      if (data.accordId != "bd") {
        this.display_data=this.appService.changeAcc(this.appService.operation_nbusiness,data,"","","delAcc");
        this.accName.push({ "id": data.accordId, "itemName": data.accordionName });
      } else {
        localStorage.setItem("alertContent", "You can't delete this Block");
  
        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '
  
        }).subscribe((isConfirmed) => {
        });
  
      }
    }

    editField(efield) {
      localStorage.setItem("editName", efield.fieldName);
      localStorage.setItem("editId", efield.id);
      localStorage.setItem("editBlockId", efield.blockId);
      localStorage.setItem("mandatory", efield.mandatory);
      localStorage.setItem("mandatoryMsg", efield.mandatoryMsg);
      localStorage.setItem("editType", efield.tagName);
      localStorage.setItem("editDisplay", efield.display);
      localStorage.setItem("editPageName", "NEW_BUSINESS");
      var xx = [];
      if(efield.mappingId){
        var fieldDetailList = [];
        for (var j = 0; j < efield.mappingId.length; j++) {
         var fieldDetail = this.appService.changeField(this.appService.operation_nbusiness, efield.mappingId[j], "mappedFieldDetails", "", "");
         fieldDetailList.push(fieldDetail);
        }
        localStorage.setItem("editMapping", JSON.stringify(fieldDetailList));
        localStorage.setItem("editBlockNameList", JSON.stringify(xx));
      }else{
        localStorage.setItem("editBlockNameList", JSON.stringify(this.blockName));
        localStorage.setItem("editMapping", JSON.stringify(xx));
      }
      if(efield.maxValue != ''){
        localStorage.setItem("editMaxValue", efield.maxValue);
      }else{
        localStorage.setItem("editMaxValue", "");
      }
  
      if(efield.minValue != ''){
        localStorage.setItem("editMinValue", efield.minValue);
      }else{
        localStorage.setItem("editMinValue", "");
      }
      if(efield.read){
        localStorage.setItem("editReadOnly", efield.read);
      }else{
        localStorage.setItem("editReadOnly", "false");
  
      }
      if(efield.tagValue != ''){
        localStorage.setItem("editDefaultValue", efield.tagValue);
      }else{
        localStorage.setItem("editDefaultValue", "");
      }
      var f = [];
      if (efield.tagName == 'select-one') {
  
  
        for (var i = 0; i < efield.optionObj.length; i++) {
          if (efield.optionObj[i].optionValue != "") {
            f.push({ "optionValue": efield.optionObj[i].optionValue, "optionLabel": efield.optionObj[i].optionLabel })
          }
        }
        localStorage.setItem("editSelectedItem", JSON.stringify(f));
  
      }
  
  
      this.dialogService.addDialog(EditPenComponent, {
        title: 'Name dialog',
        fquestion1: 'Item Name ',
        fquestion2: 'Item Type '
  
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {

          var edited = JSON.parse(localStorage.getItem("output"));
          this.display_data=this.appService.changeField(this.appService.operation_nbusiness,edited[0],"editField","","");

       
        } else {
  
        }
      })
  
    }

    editTableField(tData) {
      localStorage.setItem("editName", tData.theadName);
      localStorage.setItem("editId", tData.id);
      localStorage.setItem("editBlockId", tData.blockId);
      localStorage.setItem("mandatory", tData.mandatory);
      localStorage.setItem("mandatoryMsg", tData.mandatoryMsg);
      localStorage.setItem("editDisplay", tData.display);
      localStorage.setItem("editPageName", "NEW_BUSINESS");
      var xx = [];
      if(tData.mappingId){
        var fieldDetailList = [];
        for (var j = 0; j < tData.mappingId.length; j++) {
         var fieldDetail = this.appService.changeField(this.appService.operation_nbusiness, tData.mappingId[j], "mappedFieldDetails", "", "");
         fieldDetailList.push(fieldDetail);
        }
        localStorage.setItem("editMapping", JSON.stringify(fieldDetailList));
        localStorage.setItem("editBlockNameList", JSON.stringify(xx));
      }else{
        localStorage.setItem("editBlockNameList", JSON.stringify(this.blockName));
        localStorage.setItem("editMapping", JSON.stringify(xx));
      }
      if(tData.maxValue != ''){
        localStorage.setItem("editMaxValue", tData.maxValue);
      }else{
        localStorage.setItem("editMaxValue", "");
      }
  
      if(tData.minValue != ''){
        localStorage.setItem("editMinValue", tData.minValue);
      }else{
        localStorage.setItem("editMinValue", "");
      }
      if(tData.read){
        localStorage.setItem("editReadOnly", tData.read);
      }else{
        localStorage.setItem("editReadOnly", "false");
  
      }
      if(tData.tdObj){
        if(tData.tdObj[0].tdValue != ''){
          localStorage.setItem("editDefaultValue", tData.tdObj[0].tdValue);
      }else{
        localStorage.setItem("editDefaultValue", "");
      }
    }
    var f = [];
  localStorage.setItem("editSelectedItem", JSON.stringify(f));
      if(tData.optionObj){
        localStorage.setItem("editType", 'select-one');
        for (var i = 0; i < tData.optionObj.length; i++) {
          
            f.push({ "optionValue": tData.optionObj[i].optionValue, "optionLabel": tData.optionObj[i].optionLabel })
          
          localStorage.setItem("editSelectedItem", JSON.stringify(f));

        }

      }else{
        localStorage.setItem("editType", tData.tdObj[0].tagName);

      }

      this.dialogService.addDialog(EditPenComponent, {
        title: 'Name dialog',
        fquestion1: 'Item Name ',
        fquestion2: 'Item Type '
  
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {


          var edited = JSON.parse(localStorage.getItem("output"));
          this.display_data=this.appService.changeField(this.appService.operation_nbusiness,edited[0],"editTableField","","");

       
  
        } else {
  
        }
      })
  
    }


    
  private onDrag(args: any): void {
    let [e, target, parent, moves] = args;
    
    if(e.className!=''){
    this.appService.dragMaintain=e.querySelector(".inputlabel").id;
    }else{
      if(document.getElementsByClassName("panel-collapse collapse in")[0]){
        // document.getElementsByClassName("panel-collapse collapse in")[0].className="panel-collapse collapse";
      }
      
    }
   

   // this.appService.changeField(this.appService.operation_nbusiness,{"id":e.querySelector(".inputlabel").id},"drag","","");
    

  }



  private onDrop(args: any): void {
    let [e, target, parent, moves] = args;
    this.expand=this.dropIndex;

    if((e.className=="tc1 gu-transit" && target.className=="moTable") || (e.className=="first-box gu-transit" && target.className=="board")){
  this.display_data= this.appService.changeField(this.appService.operation_nbusiness, { "id": this.appService.dragMaintain }, "drag", "", "");
   if( args[3].querySelector!=null){
   this.display_data =this.appService.changeField(this.appService.operation_nbusiness, { "id": args[3].querySelector(".inputlabel").id }, "drop", "", "");

    }
   

 }

// //    if((e.className=="tc1 gu-transit" && target.className=="moTable") || (e.className=="first-box gu-transit" && target.className=="board")){
// //    this.appService.operation_nbusiness= this.appService.changeField(this.appService.operation_nbusiness, { "id": this.appService.dragMaintain }, "drag", "", "");
// //    if( args[3].querySelector!=null){
// //    this.display_data =this.appService.changeField(this.appService.operation_nbusiness, { "id": args[3].querySelector(".inputlabel").id }, "drop", "", "");

// //    }
   

// // }else{

// //   if(e.className=="gu-transit" && target.className=="panel-group"){
// //   }else{
// //    this.dragulaService.find('second-bag').drake.cancel(true);

  

// //   }

// }
  //   let [e, target, parent, moves] = args;

  //   this.appService.operation_nbusiness=this.appService.changeField(this.appService.operation_nbusiness,{"id":args[3].querySelector(".inputlabel").id},"drop","","");
  //   this.display_data=[];
    
  //   setTimeout(res=>{
  //    this.display_data=this.appService.operation_nbusiness;
  //    this.expand=this.dropIndex;
   
  //  },200);

  }


  private onOver(args: any): void {

    let [el] = args;
  
  }

  foutc;

  private onOut(args: any): void {
    let [e, target, parent, moves] = args;

  //   if((target.previousElementSibling.className=="row" && target.nextElementSibling==null)){
  //   this.dragulaService.find('second-bag').drake.cancel(true);
  //  } 
  }

  saveFormat(){
    swal({
      title: 'Are you sure?',
      text: "You want to save!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then(async (resultSel) => {
      if (resultSel.value) {
    this.display_data[0].TemplateId=this.appService.templateId;
    this.display_data[0].PageName=this.appService.pageName;
    var data = JSON.stringify(this.display_data);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    await this.http.post('/api/tempPage',data,{headers:headers}).toPromise()


    //comment by darong
    // this.http.post('/api/tempPage', data, { headers: headers })
    //   .subscribe(res => {
    //     setTimeout(() => {
    //       console.log(res.json());
    //     }, 1500);

    //   }, err => {
    //     console.log("err " + err);
    //   })

    var pageName = [];
    pageName.push({
      "TemplateId": this.appService.templateId,
      "TemplateName": this.appService.templateName,
      "PageName": "NEW_BUSINESS"
    });
    this.http.post('/api/tempPageNameInsert', pageName, { headers: headers })
      .subscribe(res => {
        if (res.json().review == false && this.templateApproval) {
          this.templateSaved = true;
          this.appService.templateSavedStaus=true;
        }

      }, err => {
        console.log("err " + err);
      })



    localStorage.setItem("alertContent", "Saved Successfully");

    this.dialogService.addDialog(AlertComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
    });
  }
})
  }

  addBlock() {
         localStorage.setItem("dboard", "0");
    this.dialogService.addDialog(PrompComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.display_data=this.appService.addNewBlock(this.appService.operation_nbusiness,"NEW_BUSINESS","NB" + this.count++,"NB" + this.count++);
      } else {

      }
    })
  }

  addField(field) {
    localStorage.setItem("id",field.accordId);
    localStorage.setItem("blockNameList", JSON.stringify(this.blockName));
    localStorage.setItem("pageName", "NEW_BUSINESS");

    this.dialogService.addDialog(FielddialogueComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("output"));
        this.display_data=this.appService.changeAcc(this.appService.operation_nbusiness,field,s,"","newField");

      } else {

      }
    })





  }

  addTableField(field) {
    localStorage.setItem("id",field.accordId);
    localStorage.setItem("blockNameList", JSON.stringify(this.blockName));
    localStorage.setItem("pageName", "NEW_BUSINESS");

    this.dialogService.addDialog(FielddialogueComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("output"));
        this.display_data=this.appService.changeAcc(this.appService.operation_nbusiness,field,s,"","newTableField");

      } else {

      }
    })





  }

  fieldInsert() {

    var s = JSON.parse(localStorage.getItem("output"));
    for (var i = 0; i < this.display_data.length; i++) {
      if (this.display_data[i].accordId == localStorage.getItem("id")) {
        if (localStorage.getItem("className") == "panel-body") {

          if (s[0].item_type == "text") {
            if (s[0].field_type == "Number") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "number" })

            } else if (s[0].field_type == "String") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "text" })

            } else if (s[0].field_type == "Date") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "date" })

            }

          } else if (s[0].item_type == "select") {
            this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "select-one", "tagvalue": "--Select--", "optionObj": s[0].optionObj })

          } else if (s[0].item_type == "date") {
            this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "date" })

          }
        } else if (localStorage.getItem("className") == "panel-body tle") {
          if (s[0].item_type == "text") {
            if (s[0].field_type == "Number") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "number", "tdValue": "" }] })

            } else if (s[0].field_type == "Date") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "date", "tdValue": "" }] })

            } else if (s[0].field_type == "String") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "text", "tdValue": "" }] })

            }

          } else if (s[0].item_type == "select") {
            this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "optionObj": s[0].optionObj })

          } else if (s[0].item_type == "date") {
            this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "date", "tdValue": "" }] })

          }
        }
      }

    }
  }


  approval() {

    var approve = [];
    approve.push({
      "TemplateId": this.appService.templateId,
      "TemplateName": this.appService.templateName,
      "PageName": "NEW_BUSINESS"
    })


    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    this.http.post('/api/tempApprove', JSON.stringify(approve[0]), { headers: headers })
      .subscribe(res => {
        localStorage.setItem("alertContent", "Approved");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.templateSaved = false;

        });

      }, err => {
        console.log("err " + err);
      })



  }

  approvalProduct() {
    var approve = [];
    approve.push({
      "ProductId": this.appService.productId,
      "ProductName": this.appService.productName,
      "PageName": "NEW_BUSINESS"
    })


    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    this.http.post('/api/prodApprove', JSON.stringify(approve[0]), { headers: headers })
      .subscribe(res => {
        localStorage.setItem("alertContent", "Approved");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.productSaved = false;
        });

      }, err => {
        console.log("err " + err);
      })
  }


  
  genRow(data) {
    // var countR = data.tableObj.length;
    var s;
    for(var i=0;i<Object.keys(data.tableObj[data.tableObj.length-1]).length;i++){
      if(Object.keys(data.tableObj[data.tableObj.length-1])[i].includes("sn")){
        s=i;
    }
    
    }
    var countR=data.tableObj[data.tableObj.length-1][Object.keys(data.tableObj[data.tableObj.length-1])[s]];


    localStorage.setItem("addRowOutpt", "");
    this.dialogService.addDialog(AddRowComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("addRowOutpt"));
        for(var i=0; i < s; i++){
          this.display_data = this.appService.changeAcc(this.appService.operation_nbusiness, data, ++countR,"", "addRow");
      }

      } else {

      }
    })

}

  delRow(data,eve) {

    if (eve.target.parentElement.previousElementSibling != null) {
      this.display_data=this.appService.changeAcc(this.appService.operation_nbusiness,data,eve.target.parentElement.id,"","delRow");

    } else {
      localStorage.setItem("alertContent", "You can't delete this Row");

      this.dialogService.addDialog(AlertComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '

      }).subscribe((isConfirmed) => {
      });
    }

  }

  textValid(eve,fieldData,nfield,ti){
    this.expand=ti;
    if (localStorage.getItem("pageMode") == "2") {
      var resultVal = true;

      if((nfield.maxLength !='')){
        var dValue=eve.target.value.length;
        var max=Number(nfield.maxLength);
    
        if(dValue>max){
          eve.target.value="";
                         swal(
                           'Error!',
                           'Value Should not exceeds maximum length of '+nfield.maxLength,
                           'error'
                         )
                   
                        }
                      }
    if(nfield.tagName=='date'){
       var regx=new RegExp(/^\d{4}-\d{2}-\d{2}$/);
        if(regx.test(eve.target.value)){
                if(eve.target.value!=''){
                  this.display_data = this.appService.changeField(this.appService.operation_nbusiness, nfield, "setVal", eve.target.value, "");

                  for(var i=0;i<fieldData.length;i++){
      
                    if(nfield.toId){
                      
                      if(fieldData[i].id==nfield.toId){
                      var nDate=nfield;
                      var fromDate = new Date(nDate.tagValue);	
                      var toDate = new Date(fieldData[i].tagValue);
                      if(fromDate > toDate){
                      
                       nDate.tagValue="";
                       nfield.mandatory=true;
                       this.display_data = this.appService.changeField(this.appService.operation_nbusiness, nfield, "setVal", "", "");
                        var tChange=fieldData[i];
                        tChange.tagValue="";
                        var output = this.appService.changeField(this.appService.operation_nbusiness, tChange, "setVal", "", "");
                        var resultCheck=JSON.stringify(output);
                        
                        this.display_data=JSON.parse(resultCheck);
                        swal(
                          'Error!',
                          'Please Enter valid Date',
                          'error'
                        )
                         break;
                      }
                      }
                      }
                      else if(nfield.fromId){
                        if(fieldData[i].id==nfield.fromId){
                        var nDate=nfield.tagValue;
                        var toDate = new Date(nDate);	
                        var fromDate = new Date(fieldData[i].tagValue);
                        if(fromDate > toDate){
                         // alert("invalid");
                         nfield.tagValue="";
                         this.display_data = this.appService.changeField(this.appService.operation_nbusiness, nfield, "setVal", "", "");
                          var tChange=fieldData[i];
                          tChange.tagValue="";
                          
                          var output= this.appService.changeField(this.appService.operation_nbusiness, tChange, "setVal", "", "");
                                          var resultCheck=JSON.stringify(output);
                          
                                          this.display_data=JSON.parse(resultCheck);
                                          swal(
                                            'Error!',
                                            'Please Enter valid Date',
                                            'error'
                                          )
                                           break;
                        }
                        }
                        
                        }
                  }
				  
				  

                //   this.display_data=this.appService.changeField(this.appService.operation_nbusiness,nfield,"setVal",eve.target.value,"");
                //   if(nfield.minValue != '' || nfield.maxValue != ''){
                //     resultVal = this.appService.validateNumDate(this.appService.operation_nbusiness, nfield, "minMaxValidateDate", Number(eve.target.value));
                //   }
                //   if(!resultVal){
                //     var output1 = this.appService.changeField(this.appService.operation_nbusiness, nfield, "setVal", "", "");
                //     this.display_data = output1;

                //     // setTimeout(res => {
                //     //   this.display_data = output1;
                //     // }, 200);
                //     }else{
                //   if(nfield.toId){
                //      var result =this.appService.validateField(this.appService.operation_nbusiness, nfield,nfield.toId,"to","date");
                //       if(result =="invalid" ){
                //         var output=this.appService.changeField(this.appService.operation_nbusiness,nfield,"setVal","","");
                //         this.display_data=output;
                //         // setTimeout(res=>{
                //         //   this.display_data=output;
                //         // },200);
                //       }
                //     }else if(nfield.fromId){
                //       var result =this.appService.validateField(this.appService.operation_nbusiness, nfield,nfield.fromId,"from","date");
                //       if(result =="invalid" ){
                //         var output=this.appService.changeField(this.appService.operation_nbusiness,nfield,"setVal","","");
                //         this.display_data=output;
                //         // setTimeout(res=>{
                //         //   this.display_data=output;
                //         // },200);
                //       }
                //     }
                // }
              }
        }else{
           eve.target.value="";
          
        }
      }else if(nfield.tagName=='number'){
        var regexp = /^[0-9]+([,.][0-9]+)?$/g;
        if(regexp.test(eve.target.value)){
          if(eve.target.value!=''){

            this.display_data = this.appService.changeField(this.appService.operation_nbusiness, nfield, "setVal", Number(eve.target.value), "");

            for(var i=0;i<fieldData.length;i++){

              if(nfield.toId){
                
                if(fieldData[i].id==nfield.toId){
                var nDate=nfield;
                var fromDate = new Date(nDate.tagValue);	
                var toDate = new Date(fieldData[i].tagValue);
                if(fromDate > toDate){
                
                 nDate.tagValue="";
                 nfield.mandatory=true;
                 this.display_data = this.appService.changeField(this.appService.operation_nbusiness, nfield, "setVal", "", "");
                  var tChange=fieldData[i];
                  tChange.tagValue="";
                  var output = this.appService.changeField(this.appService.operation_nbusiness, tChange, "setVal", "", "");
                  var resultCheck=JSON.stringify(output);
                  
                  this.display_data=JSON.parse(resultCheck);
                  swal(
                    'Error!',
                    'Please Enter valid number',
                    'error'
                  )
                   break;
                }
                }
                }
                else if(nfield.fromId){
                  if(fieldData[i].id==nfield.fromId){
                  var nDate=nfield.tagValue;
                  var toDate = new Date(nDate);	
                  var fromDate = new Date(fieldData[i].tagValue);
                  if(fromDate > toDate){
                   // alert("invalid");
                   nfield.tagValue="";
                   this.display_data = this.appService.changeField(this.appService.operation_nbusiness, nfield, "setVal", "", "");
                    var tChange=fieldData[i];
                    tChange.tagValue="";
                    
                    var output= this.appService.changeField(this.appService.operation_nbusiness, tChange, "setVal", "", "");
                                    var resultCheck=JSON.stringify(output);
                    
                                    this.display_data=JSON.parse(resultCheck);
                                    swal(
                                      'Error!',
                                      'Please Enter valid number',
                                      'error'
                                    )
                                     break;
                  }
                  }
                  
                  }
        
            }
          // this.display_data=this.appService.changeField(this.appService.operation_nbusiness,nfield,"setVal",Number(eve.target.value),"");
          // if(nfield.minValue != '' || nfield.maxValue != ''){
          //   resultVal = this.appService.validateNumDate(this.appService.operation_nbusiness, nfield, "minMaxValidateNum", Number(eve.target.value));
          // }
          // if(!resultVal){
          //   var output1 = this.appService.changeField(this.appService.operation_nbusiness, nfield, "setVal", "", "");
          //   this.display_data = output1;
          //   // setTimeout(res => {
          //   //   this.display_data = output1;
          //   // }, 200);
          //   }else{
          // if(nfield.toId){
          //   var result =this.appService.validateField(this.appService.operation_nbusiness, nfield,nfield.toId,"to","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.operation_nbusiness,nfield,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
          //     //  },200);
          //    }
          //  }else if(nfield.fromId){
          //    var result =this.appService.validateField(this.appService.operation_nbusiness, nfield,nfield.fromId,"from","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.operation_nbusiness,nfield,"setVal","","");
          //     // this.display_data=[];
              
          //        this.display_data=output;
             
          //    }
          //  }
          // }
        }
        }else{
          eve.target.value="";
        }
      }else{
        
        if(nfield.mappingId){
          for(var j=0;j<nfield.mappingId.length;j++){
            this.appService.changeField(this.appService.operation_nbusiness, nfield.mappingId[j], "copyValues", eve.target.value, "");
            }
          }
          if(eve.target.value!=''){
        this.display_data=this.appService.changeField(this.appService.operation_nbusiness,nfield,"setVal",eve.target.value,"");
          }
      }
    
    }else{
          // eve.target.value="";
     }
  }

  textTabValid(eve,tableRow,tData,tD,expanIndex){
    this.expand=expanIndex;
    if (localStorage.getItem("pageMode") == "2") {
      var resultVal = true;
      if(tD.sna != undefined){
        this.snumber[0].sna = tD.sna;
      }else if(tD.snb != undefined){
        this.snumber[1].snb = tD.snb;
        
      }else if(tD.snc != undefined){
        this.snumber[2].snc = tD.snc;
      }else if(tD.snd != undefined){
        this.snumber[3].snd = tD.snd;
      }
 	  
      if((tData.maxLength !='')){
        var dValue=eve.target.value.length;
        var max=Number(tData.maxLength);
    
        if(dValue>max){
          eve.target.value="";
                         swal(
                           'Error!',
                           'Value Should not exceeds maximum length of '+tData.maxLength,
                           'error'
                         )
                   
                        }
                      }
      
    if(tData.tdObj[0].tagName=='date'){
       var regx=new RegExp(/^\d{4}-\d{2}-\d{2}$/);
        if(regx.test(eve.target.value)){
          
                if(eve.target.value!=''){
                  this.display_data = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", eve.target.value, "");
                  for(var i=0;i<tableRow.length;i++){
    
                    if(tData.toId){
                    
                    if(tableRow[i].id==tData.toId){
                    
                    var fromDate = new Date(tData.tdObj[0].tdValue);	
                    var toDate = new Date(tableRow[i].tdObj[0].tdValue);
                    if(fromDate > toDate){
                     //alert("invalid");
                     tData.tdObj[0].tdValue="";
                     tData.mandatory=true;
                     this.display_data = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", "", "");
                      var tChange=tableRow[i];
                      tChange.tdObj[0].tdValue="";
                     this.display_data = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", "", "");
                     var output = this.appService.changeField(this.appService.operation_nbusiness, tChange, "setVal", "", "");
                      var resultCheck=JSON.stringify(output);
                      
                      this.display_data=JSON.parse(resultCheck);
                      swal(
                        'Error!',
                        'Please Enter valid Date',
                        'error'
                      )
                       break;
                    }
                    }
                    }else if(tData.fromId){
                    if(tableRow[i].id==tData.fromId){
                    
                    var toDate = new Date(tData.tdObj[0].tdValue);	
                    var fromDate = new Date(tableRow[i].tdObj[0].tdValue);
                    if(fromDate > toDate){
                     // alert("invalid");
                     tData.tdObj[0].tdValue="";
                     this.display_data = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", "", "");
                      var tChange=tableRow[i];
                      tChange.tdObj[0].tdValue="";
                      
                     this.display_data = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", "", "");
                     var output= this.appService.changeField(this.appService.operation_nbusiness, tChange, "setVal", "", "");
                                      var resultCheck=JSON.stringify(output);
                      
                                      this.display_data=JSON.parse(resultCheck);
                                      swal(
                                        'Error!',
                                        'Please Enter valid Date',
                                        'error'
                                      )
                                       break;
                    }
                    }
                    
                    }
                    }
					
					
                //   this.display_data=this.appService.changeField(this.appService.operation_nbusiness,tData,"setVal",eve.target.value,"");
                //   if(tData.minValue != '' || tData.maxValue != ''){
                //     resultVal = this.appService.validateNumDate(this.appService.operation_nbusiness, tData, "minMaxValidateDate", Number(eve.target.value));
                //   }
                //   if(!resultVal){
                //     var output1 = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", "", "");
                //     this.display_data = output1;
                //     // setTimeout(res => {
                //     //   this.display_data = output1;
                //     // }, 200);
                //     }else{
                //   if(tData.toId){
                //      var result =this.appService.validateField(this.appService.operation_nbusiness, tData,tData.toId,"to","date");
                //       if(result =="invalid" ){
                //         var output=this.appService.changeField(this.appService.operation_nbusiness,tData,"setVal","","");
                //         this.display_data=output;
                //         // setTimeout(res=>{
                //         //   this.display_data=output;
                //         // },200);
                //       }
                //     }else if(tData.fromId){
                //       var result =this.appService.validateField(this.appService.operation_nbusiness, tData,tData.fromId,"from","date");
                //       if(result =="invalid" ){
                //         var output=this.appService.changeField(this.appService.operation_nbusiness,tData,"setVal","","");
                //         this.display_data=output;
                //         // setTimeout(res=>{
                //         //   this.display_data=output;
                //         // },200);
                //       }
                //     }
                // }
              }
        }else{
           eve.target.value="";
          
        }
      }else if(tData.tdObj[0].tagName=='number'){
        var regexp = /^[0-9]+([,.][0-9]+)?$/g;
        if(regexp.test(eve.target.value)){
          if(eve.target.value!=''){
            		
					 this.display_data = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", Number(eve.target.value), "");
           for(var i=0;i<tableRow.length;i++){

             if(tData.toId){
             
             if(tableRow[i].id==tData.toId){
             
             var fromDate = new Date(tData.tdObj[0].tdValue);	
             var toDate = new Date(tableRow[i].tdObj[0].tdValue);
             if(fromDate > toDate){
              //alert("invalid");
              tData.tdObj[0].tdValue="";
              tData.mandatory=true;
              this.display_data = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", "", "");
               var tChange=tableRow[i];
               tChange.tdObj[0].tdValue="";
               var output = this.appService.changeField(this.appService.operation_nbusiness, tChange, "setVal", "", "");
               var resultCheck=JSON.stringify(output);
               
               this.display_data=JSON.parse(resultCheck);
               swal(
                 'Error!',
                 'Please Enter valid Number',
                 'error'
               )
                break;
             }
             }
             }else if(tData.fromId){
             if(tableRow[i].id==tData.fromId){
             
             var toDate = new Date(tData.tdObj[0].tdValue);	
             var fromDate = new Date(tableRow[i].tdObj[0].tdValue);
             if(fromDate > toDate){
              // alert("invalid");
              tData.tdObj[0].tdValue="";
              this.display_data = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", "", "");
               var tChange=tableRow[i];
               tChange.tdObj[0].tdValue="";
               
               var output= this.appService.changeField(this.appService.operation_nbusiness, tChange, "setVal", "", "");
                               var resultCheck=JSON.stringify(output);
               
                               this.display_data=JSON.parse(resultCheck);
                               swal(
                                 'Error!',
                                 'Please Enter valid Number',
                                 'error'
                               )
                                break;
             }
             }
             
             }
           
      
             }
     
          // this.display_data=this.appService.changeField(this.appService.operation_nbusiness,tData,"setVal",Number(eve.target.value),"");
          // if(tData.minValue != '' || tData.maxValue != ''){
          //   resultVal = this.appService.validateNumDate(this.appService.operation_nbusiness, tData, "minMaxValidateNum", Number(eve.target.value));
          // }
          // if(!resultVal){
          //   var output1 = this.appService.changeField(this.appService.operation_nbusiness, tData, "setVal", "", "");
          //   this.display_data = output1;
          //   // setTimeout(res => {
          //   //   this.display_data = output1;
          //   // }, 200);
          //   }else{
          // if(tData.toId){
          //   var result =this.appService.validateField(this.appService.operation_nbusiness, tData,tData.toId,"to","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.operation_nbusiness,tData,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
          //     //  },200);
          //    }
          //  }else if(tData.fromId){
          //    var result =this.appService.validateField(this.appService.operation_nbusiness, tData,tData.fromId,"from","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.operation_nbusiness,tData,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
          //     //  },200);
          //    }
          //  }
          // }
          }
        }else{
          eve.target.value="";
        }
      }else{

      
        //this.display_data=this.appService.changeField(this.appService.operation_nbusiness,tData,"setVal",eve.target.value,"");

        if(tData.mappingId){
          for(var j=0;j<tData.mappingId.length;j++){
            this.appService.changeField(this.appService.operation_nbusiness, tData.mappingId[j], "copyValues", eve.target.value, "");
            }
          }
          if(eve.target.value!=''){
        this.display_data=this.appService.changeField(this.appService.operation_nbusiness,tData,"setVal",eve.target.value,"");
          }
      }
    
    }else{
         //  eve.target.value="";
     }
  }
  
  
  setVal(field,eve,tD){
    if (localStorage.getItem("pageMode") == "2") {
      
      if(tD.sna != undefined){
        this.snumber[0].sna = tD.sna;
      }else if(tD.snb != undefined){
        this.snumber[1].snb = tD.snb;
        
      }else if(tD.snc != undefined){
        this.snumber[2].snc = tD.snc;
      }else if(tD.snd != undefined){
        this.snumber[3].snd = tD.snd;
      }

    if(field.fieldDisable){
      for(var m=0;m<field.fieldDisable.length;m++){
        if(eve.target.value == "N"){
          this.appService.changeField(this.appService.operation_nbusiness, field.fieldDisable[m] , "fieldDisable",false ,  "");
      }else{
        this.appService.changeField(this.appService.operation_nbusiness, field.fieldDisable[m] , "fieldDisable",true ,  "");
        
      }
    }
    }

    if(field.displayAccId){
      for(var l=0;l<field.displayAccId.length;l++){
        if(eve.target.value == "Y"){
          this.appService.changeAcc(this.appService.operation_nbusiness, field.displayAccId[l], false , "","displayAcc");
      }else{
        this.appService.changeAcc(this.appService.operation_nbusiness, field.displayAccId[l] , true ,"", "displayAcc");
        
      }
    } 
    }

    if(field.navBtnId){
      for(var k=0;k<field.navBtnId.length;k++){
        if(eve.target.value == "Y"  || eve.target.value == "V" || eve.target.value == "F"  || eve.target.value == "T" || eve.target.value == "IF"){
          this.appService.changeField(this.appService.operation_nbusiness, field.navBtnId[k] , "BtnValidation", false,  "");
          if(field.navBtnId[k].id=="btn_incSumA"){
            this.appService.changeField(this.appService.operation_nbusiness, {"id":"btn_decSumA"} , "BtnValidation", true,  "");   
            var dec={
              "id":"sacfp_decreasingSa",
              "fieldName":"Decreasing SA",
              "fieldParentwPercentage":"23",
              "fieldwPercentage":"93",
              "tagName":"select-one",
              "tagValue":"",
              "mandatory":false,
              "maxLength":"",
              "display":true,
              "mandatoryMsg":" Enter Decreasing SA",
              "navBtnId":[
                 {
                    "id":"btn_decSumA"
                 }
              ],
              "optionObj":[
                 {
                    "optionLabel":"Yes",
                    "optionValue":"Y",
                    "selected":false
                 },
                 {
                    "optionLabel":"No",
                    "optionValue":"N",
                    "selected":true
                 }
              ]
           };
            this.display_data=this.appService.changeField(this.appService.operation_nbusiness,dec,"setVal","N","");

          }else if(field.navBtnId[k].id=="btn_decSumA"){
            this.appService.changeField(this.appService.operation_nbusiness, {"id":"btn_incSumA"} , "BtnValidation", true,  "");    
            var inc={
              "id":"sacfp_increasingSa",
              "fieldName":"Increasing SA",
              "fieldParentwPercentage":"23",
              "fieldwPercentage":"93",
              "tagName":"select-one",
              "tagValue":"",
              "mandatory":false,
              "maxLength":"1",
              "display":true,
              "mandatoryMsg":" Enter Increasing SA",
              "navBtnId":[
                 {
                    "id":"btn_incSumA"
                 }
              ],
              "optionObj":[
                 {
                    "optionLabel":"Yes",
                    "optionValue":"Y",
                    "selected":false
                 },
                 {
                    "optionLabel":"No",
                    "optionValue":"N",
                    "selected":true
                 }
              ],
              "toolTip":[
                 {
                    "tooltipDisplay":true,
                    "convTableName":"GNMM_PLAN_MASTER",
                    "convColumnName":"V_INC_SA_FLAG",
                    "convToolTip":"Increasing sum Assured flag"
                 },
                 {
                    "tooltipDisplay":false,
                    "takaTableName":"GNMM_PLAN_MASTER",
                    "takaColumnName":"V_INC_SA_FLAG",
                    "takaToolTip":"Increasing sum Covered flag"
                 }
              ]
           };
            this.display_data=this.appService.changeField(this.appService.operation_nbusiness,inc,"setVal","N","");

          }
      }else{
        this.appService.changeField(this.appService.operation_nbusiness, field.navBtnId[k] , "BtnValidation", true,  "");
        
      }
    }
    }

      if(field.mappingId){
        for(var j=0;j<field.mappingId.length;j++){
          this.appService.changeField(this.appService.operation_nbusiness, field.mappingId[j], "copyValues", eve.target.value, "");
          }
        }

      for(var i=0;i<field.optionObj.length;i++){
        if(field.optionObj[i].optionValue==eve.target.value){
          field.optionObj[i].selected=true;
      }else{
        field.optionObj[i].selected=false;
      }
      if(i==field.optionObj.length-1){
        this.display_data=this.appService.changeField(this.appService.operation_nbusiness,field,"setVal","","");

      }
      }

      if(field.id=="jld_lcfix"){
        var selectedJointVar;
          for(var u=0;u<field.optionObj.length;u++){
            
        if(field.optionObj[u].selected){
          selectedJointVar=field.optionObj[u].optionValue;
        }

        if(u==field.optionObj.length-1){
           // Disable min/max lc in Joint Life
          for(var v=0;v<this.appService.operation_nbusiness.length;v++){
            if(this.appService.operation_nbusiness[v].accordId=='jld'){
          
            for(var w=0;w<this.appService.operation_nbusiness[v].field.length;w++){
                if(this.appService.operation_nbusiness[v].field[w].id=='jld_minlc'){
          if(selectedJointVar=='F'){
                    this.appService.operation_nbusiness[v].field[w]["read"]=true;
          }else if(selectedJointVar=='V'){
          
                    delete this.appService.operation_nbusiness[v].field[w].read;
          
          }
          
          
          }
                if(this.appService.operation_nbusiness[v].field[w].id=='jld_maxlc'){
          if(selectedJointVar=='F'){
                    this.appService.operation_nbusiness[v].field[w]["read"]=true;
          }else if(selectedJointVar=='V'){
            delete this.appService.operation_nbusiness[v].field[w].read;
                      
          
          }
          
          
          }

          if(this.appService.operation_nbusiness[v].field[w].id=='jld_nooflives'){
            if(selectedJointVar=='V'){
                      this.appService.operation_nbusiness[v].field[w]["read"]=true;
            }else if(selectedJointVar=='F'){
              delete this.appService.operation_nbusiness[v].field[w].read;
                        
            
            }
            
            
            }

          
          }
              }
          
        }
            }
        }
            

       


      //Check claims child data's filled or not
      var riskChild = false;
      var riskJLBenefits = false;
      var riskJLPolicy = false;
      for (var f = 0; f < this.appService.risk_coverage.length; f++) {
        if (this.appService.risk_coverage[f].accordId == 'pe') {
          if (this.appService.risk_coverage[f].tableObj) {
            for (var g = 0; g < this.appService.risk_coverage[f].tableObj.length; g++) {
              for (var h = 0; h < this.appService.risk_coverage[f].tableObj[g].tableRow.length; h++) {
                if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].id == "btn_ChildEvent") {
                  for (var e = 0; e < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj.length; e++) {
                    for (var ea = 0; ea < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow.length; ea++) {
                      if (ea > 1) {
                        if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj) {
                          for (var p = 0; p < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj.length; p++) {
                            if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj[p].selected) {
                              riskChild = true;


                            }

                          }

                        } else if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj) {
                          if(this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tagName!='button'){
                          if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tdValue != '') {
                            riskChild = true;

                          }
                        }}


                      }
                    }
                  }

                }
              }

            }
          }
        }

        // if (this.appService.risk_coverage[f].accordId == 'jld') {
        //   if (this.appService.risk_coverage[f].tableObj) {
        //     for (var g = 0; g < this.appService.risk_coverage[f].tableObj.length; g++) {
        //       for (var h = 0; h < this.appService.risk_coverage[f].tableObj[g].tableRow.length; h++) {
        //         if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].id == "btn_jointBene") {
        //           for (var e = 0; e < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj.length; e++) {
        //             for (var ea = 0; ea < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow.length; ea++) {
        //               if (ea > 2) {
        //                 if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj) {
        //                   for (var p = 0; p < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj.length; p++) {
        //                     if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj[p].selected) {
        //                       riskJLBenefits = true;


        //                     }

        //                   }

        //                 } else if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj) {
        //                   if(this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tagName!='button'){
        //                   if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tdValue != '') {
        //                     riskJLBenefits = true;

        //                   }
        //                 }}


        //               }
        //             }
        //           }

        //         }
        //       }

        //     }
        //   }

        // }

        // if (this.appService.risk_coverage[f].accordId == 'jld') {
        //   if (this.appService.risk_coverage[f].tableObj) {
        //     for (var g = 0; g < this.appService.risk_coverage[f].tableObj.length; g++) {
        //       for (var h = 0; h < this.appService.risk_coverage[f].tableObj[g].tableRow.length; h++) {
        //         if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].id == "btn_jointPoliStatus") {
        //           for (var e = 0; e < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj.length; e++) {
        //             for (var ea = 0; ea < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow.length; ea++) {
        //               if (ea > 2) {
        //                 if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj) {
        //                   for (var p = 0; p < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj.length; p++) {
        //                     if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj[p].selected) {
        //                       riskJLPolicy = true;


        //                     }

        //                   }

        //                 } else if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj) {
        //                   if(this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tagName!='button'){
        //                   if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tdValue != '') {
        //                     riskJLPolicy = true;

        //                   }
        //                 }
        //               }

        //               }
        //             }
        //           }

        //         }
        //       }

        //     }
        //   }

        // }

        if(f==this.appService.risk_coverage.length-1){
          if(riskChild){
            this.display_data=this.appService.changeField(this.appService.operation_nbusiness,field,"setVal","","");
            
            for (var i = 0; i < field.optionObj.length; i++) {
            
                field.optionObj[i].selected = false;
              
              if (i == field.optionObj.length - 1) {
                this.display_data = this.appService.changeField(this.appService.operation_nbusiness, field, "setVal", "", "");

              }
            }
            localStorage.setItem("alertContent", "Claims child events data filled");

            this.dialogService.addDialog(AlertComponent, { 
              title: 'Name dialog',
              question1: 'Block Name ',
              question2: 'Block Type '
        
            }).subscribe((isConfirmed) => {
              
        
            });
          }
        }
      }
        
        
        
    }

    if(field.id=="or_termplan"){
      if(eve.target.value=='Y'){
       var or_pay= {
          "id":"or_payterm",
          "blockId":"B71_OPN_M",
          "theadName":"Term Same as Plan Prem Pay Term",
          "mandatory":false,
          "maxLength":"1",
          "display":true,
          "mandatoryMsg":" Enter Term Same as Plan Prem Pay Term",
          "optionObj":[
             {
                "optionLabel":"Yes",
                "optionValue":"Y",
                "selected":false
             },
             { 
                "optionLabel":"No",
                "optionValue":"N",
                "selected":true
             } 
          ],
          "tdObj":[
             {
                "tagName":"select-one",
                "tdValue":""
             }
          ],
          "toolTip":[
             {
                "tooltipDisplay":true,
                "convTableName":"GNMM_PLAN_RIDER",
                "convColumnName":"V_TERM_SAMEAS_PLANPREM_PAY",
                "convToolTip":"Policy term same as Base plan Premium pay term"
             },
             {
                "tooltipDisplay":false,
                "takaTableName":"GNMM_PLAN_RIDER",
                "takaColumnName":"V_TERM_SAMEAS_PLANPREM_PAY",
                "takaToolTip":"Contract term same as Base plan contribution pay term"
             }
          ]
       }
       this.display_data = this.appService.changeField(this.appService.operation_nbusiness, or_pay, "setVal", "", "");

      }


    }

    if(field.id=="or_payterm"){
      if(eve.target.value=='Y'){
       var or_term= {
        "id":"or_termplan",
        "blockId":"B71_OPN_M",
        "theadName":"Term Same as Plan",
        "mandatory":false,
        "maxLength":"1",
        "display":true,
        "mandatoryMsg":" Enter Term Same as Plan",
        "optionObj":[
           {
              "optionLabel":"Yes",
              "optionValue":"Y",
              "selected":false
           },
           {
              "optionLabel":"No",
              "optionValue":"N",
              "selected":true
           }
        ],
        "tdObj":[
           {
              "tagName":"select-one",
              "tdValue":""
           }
        ],
        "toolTip":[
           {
              "tooltipDisplay":true,
              "convTableName":"GNMM_PLAN_RIDER",
              "convColumnName":"V_TERM_SAMEAS_PLAN",
              "convToolTip":"Policy term same as base plan term"
           },
           {
              "tooltipDisplay":false,
              "takaTableName":"GNMM_PLAN_RIDER",
              "takaColumnName":"V_TERM_SAMEAS_PLAN",
              "takaToolTip":"Contract term same as base plan term"
           }
        ]
     }
     this.display_data = this.appService.changeField(this.appService.operation_nbusiness, or_term, "setVal", "", "");

      }
    }

    // Hide Prem Pay Link
    if(field.id=="pppt_premimumpay"){
      var selectedJointVar;
      for(var u=0;u<field.optionObj.length;u++){
        
    if(field.optionObj[u].selected){
      selectedJointVar=field.optionObj[u].optionValue;
    }
    if(u==field.optionObj.length-1){
      if(selectedJointVar=='ST'){
        this.appService.changeAcc(this.appService.operation_nbusiness, {"accordId":"ppptl"}, true , "","displayAcc");
        for (var ia = 0; ia < this.appService.operation_nbusiness.length; ia++) {
          if (this.appService.operation_nbusiness[ia].accordId == 'ppptl') {
            if (this.appService.operation_nbusiness[ia].tableObj) {
              for (var ib = 0; ib < this.appService.operation_nbusiness[ia].tableObj.length; ib++) {
                for (var ic = 0; ic < this.appService.operation_nbusiness[ia].tableObj[ib].tableRow.length; ic++) {
                  if (this.appService.operation_nbusiness[ia].tableObj[ib].tableRow[ic].id == "ppptl_prepayterm") {
                    this.appService.operation_nbusiness[ia].tableObj[ib].tableRow[ic].theadName = "Premium Paying Term";




                  }



                }



              }

            }

          }

        }
        
      }else{
        
        this.appService.changeAcc(this.appService.operation_nbusiness, {"accordId":"ppptl"}, false , "","displayAcc");

        if(selectedJointVar=='CA'){
        for (var ia = 0; ia < this.appService.operation_nbusiness.length; ia++) {
          if (this.appService.operation_nbusiness[ia].accordId == 'ppptl') {
            if (this.appService.operation_nbusiness[ia].tableObj) {
              for (var ib = 0; ib < this.appService.operation_nbusiness[ia].tableObj.length; ib++) {
                for (var ic = 0; ic < this.appService.operation_nbusiness[ia].tableObj[ib].tableRow.length; ic++) {
                  if (this.appService.operation_nbusiness[ia].tableObj[ib].tableRow[ic].id == "ppptl_prepayterm") {
                    this.appService.operation_nbusiness[ia].tableObj[ib].tableRow[ic].theadName = "Premium Paying Cease Age";




                  }



                }



              }

            }

          }

        }
      }else{
        for (var ia = 0; ia < this.appService.operation_nbusiness.length; ia++) {
          if (this.appService.operation_nbusiness[ia].accordId == 'ppptl') {
            if (this.appService.operation_nbusiness[ia].tableObj) {
              for (var ib = 0; ib < this.appService.operation_nbusiness[ia].tableObj.length; ib++) {
                for (var ic = 0; ic < this.appService.operation_nbusiness[ia].tableObj[ib].tableRow.length; ic++) {
                  if (this.appService.operation_nbusiness[ia].tableObj[ib].tableRow[ic].id == "ppptl_prepayterm") {
                    this.appService.operation_nbusiness[ia].tableObj[ib].tableRow[ic].theadName = "Premium Paying Term";




                  }



                }



              }

            }

          }

        }
      }
      }
    }
  }

    }

    }
  }
  
  saveValues(pinfo:NgForm){
    swal({
      title: 'Are you sure?',
      text: "You want to save!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((resultSel) => {
      if (resultSel.value) {
    setTimeout(()=>{

      this.spinner.show();
    },100)
    this.appService.validFieldCount(this.appService.operation_nbusiness);

       var headers = new Headers();
      headers.append('Content-Type', 'application/json');
        var dataContent = [];
      dataContent.push({ "ProductId": this.appService.productId, "PageName": "NEW_BUSINESS.json", "PageContent": this.appService.operation_nbusiness});

      this.http.post('/api/dataTmpPage', dataContent, { headers: headers })
        .subscribe(res => {
     
        }, err => {
          console.log("err " + err);
        })



    if(pinfo.valid){
      var output=this.appService.validPage(this.appService.operation_nbusiness);
      if(output.mandatory){
        this.alertMsg=[];
        for(var i=0;i<output.mandatoryFields.length;i++){
            this.alertMsg.push({ "msg":output.mandatoryFields[i].fieldName+' ('+output.mandatoryFields[i].accordionName+')'});

        }
          localStorage.setItem("alertContent",'');
          localStorage.setItem("prodAlertMsg", JSON.stringify(this.alertMsg));
          setTimeout(()=>{

            this.spinner.hide();
          },100)
          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '

          }).subscribe((isConfirmed) => {
            
            localStorage.setItem("prodAlertMsg", "");
          });
      }else{
          var result=this.storeValuesToDb();
          // var tabAccFormat = [{"accId": "wopcov" , "tableName" : "gnmmPlanWopCoverageLink"},
          // {"accId": "tvt" , "tableName" : "tlValuations"},
          // {"accId": "ac" , "tableName" : "annexureCodes"},
          // {"accId": "lad" , "tableName" : "ageLALinks"},
          // {"accId": "pc" , "tableName" : "policyConversions"},
          // {"accId": "pcn" , "tableName" : "policyToPlans"},
          // {"accId": "ppptl" , "tableName" : "premPayTermLinks"},
          // {"accId": "inc_sas" , "tableName" : "incrDecrSas"},
          // {"accId": "dec_sas" , "tableName" : "incrDecrSas"},
          // {"accId": "mf" , "tableName" : "modalFactorLinks"},
          // {"accId": "osal" , "tableName" : "gnmmPlanOccupSaLinks"},
          // {"accId": "bmi" , "tableName" : "bmiRates"},
          // {"accId": "ppl" , "tableName" : "gnmmPlanPremPatternLinks"},
          // {"accId": "er" , "tableName" : "escalatingRates"},
          // {"accId": "sap" , "tableName" : "gndtSumCoverPatterns"},
          // {"accId": "sair" , "tableName" : "indexScRates"},
          // {"accId": "ir" , "tableName" : "indexationRates"},
          // {"accId": "or" , "tableName" : "gnmmPlanRiders"},
          // {"accId": "prd" , "tableName" : "primaryRiderLinks"},
          // {"accId": "la" , "tableName" : "optRiderLas"},
          // {"accId": "rn" , "tableName" : "optRiderRels"},
          // {"accId": "pm" , "tableName" : "gnmmPlanPaymodeLinks"},
          // {"accId": "pms" , "tableName" : "payMethodLinks"},
          // {"accId": "ped" , "tableName" : "methodModeExclusions"},
          // {"accId": "larl" , "tableName" : "saLinks"},
          // {"accId": "prl" , "tableName" : "proposerRelLinks"},
          // {"accId": "cl" , "tableName" : "currencyLinks"},
          // {"accId": "gtd" , "tableName" : "jlGrpLink"},
          // {"accId": "gead" , "tableName" : "jlAgeLink"},
          // {"accId": "patl" , "tableName" : "gnmmPlanTermAgeLink"},
          // {"accId": "nml" , "tableName" : "nomineeRelLinks"}];
          var tabAccFormat= tabAccopNewBusinessFormat

this.appService.saveValuesRisk(this.appService.operation_nbusiness,result,tabAccFormat)
.subscribe(res=>{
  setTimeout(()=>{
    this.spinner.hide();
  },200)
  
  if(res==true && this.productApproval){
    this.productSaved = true;
    this.appService.productSaved = false;
    localStorage.setItem("alertContent", "Saved Successfully");

    this.dialogService.addDialog(AlertComponent, { 
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      

    });
  }else{
    localStorage.setItem("alertContent", "Saved Successfully");

    this.dialogService.addDialog(AlertComponent, { 
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      

    });
  }
})


      }
    }
  }
})
  }

  

  storeValuesToDb(){
        
        var format =
        {
          "planCode": null,
          "piName": null,
          "piNameInLocal": null,
          "piDescription": null,
          "productType": null,
          "piStartDate": null,
          "piEndDate": null,
          "piProductLine": null,
          "podWopRider": null,
          "podWopType": null,
          "podMaxAnnPremium": null,
          "podStappCode": null,
          "podAmtType": null,
          "podSaCalType": null,
          "podPolicy": null,
          "podMaxPolPerPerson": null,
          "podDoc": null,
          "podPolToProposal": null,
          "podDaysAutoCancelStd": null,
          "podDaysAutoCancelSub": null,
          "podFreeCoverMonths": null,
          "atlMaturityAge":null,
          "atlMaxmaturityage":null,
          "sacAgedefination":null,
          "sacApplicabletogender":null,
          "sacConvertible":null,
          "ppptPremimumpay":null,
          "sacfpScheduletype":null,
          "sacfpSchedulefrequency":null,
          "sacfpIncreasingSa":null,
          "sacfpDecreasingSa":null,
          "jlJoinLifePlan":null,

          "gnmmPlanWopCoverageLink": [{
            "wopCovPlanCode": null,
            "wopCovCovCode": null,
            "wopCovStatus": null
          }],
          "atlTermNominee": null,
          "atlMatAge": null,
          "atlMaxMatAge": null,
          "sacAgeDef": null,
          "sacAppGender": null,
          "sacConOption": null,
          "ppptPrePayType": null,
          "premPayTermLinks": [
            {
              "premPayTermLinkId": null,
              "ppptlPlanCode": null,
              "ppptlPptt": null,
              "ppptlAgeTermfrom": null,
              "ppptlAgeTermTo": null,
              "ppptlPrePayTerm": null,
              "ppptlStatus": null
            }
          ],
          "sofpBackDating": null,
          "sofpMaxDatePeriod": null,
          "sofpGpLevelPremium": null,
          "sofpAutoCancelDays": null,
          "sofpRegTopUpPremium": null,
          "sacfpSchType": null,
          "sacfpSchFreq": null,
          "sacfpIncSa": null,
          "sacfpDecSa": null,
          "sacfpRoundRule": null,
          "proRePre": null,
          "proReAge": null,
          "proRePolYear": null,
          "proMinDefPer": null,
          "proMaxDefPer": null,
          "proRidTermType": null,
          "proCommWithHold": null,
          "proFolBasePlan": null,
          "gnmmPlanOccupSaLinks": [
            {
              "osalPlanCode": null,
              "osalOccupClass": null,
              "osalMinSa": null,
              "osalMaxSa": null,
              "osalStatus":null,
              "gnmmPlanMasterForSaLink": null
            }
          ],
          "bmiRates": [
            {
              "bmiRateID": null,
              "bmiPlanCode": null,
              "bmiBmiFrom": null,
              "bmiBmiTo": null,
              "bmiAgeFrom": null,
              "bmiAgeTo": null,
              "bmiGender": null,
              "bmiAction": null,
              "bmiLoadFactor": null
            }
          ],
          "gnmmPlanPremPatternLinks": [
            {
              "pplPlanCode": null,
              "pplPatternCode": null,
              "pplPatternDesc": null,
              "pplStatus": null
            }
          ],
          "escalatingRates": [
            {
              "escalatingRateId": null,
              "erPlanCode": null,
              "erEsCrate": null,
              "erFromTerm": null,
              "erToTerm": null,
              "erFromYear": null,
              "erToYear": null,
              "erMinEscRate": null,
              "erMaxEscRate": null
            }
          ],
          "gndtSumCoverPatterns": [
            {
              "sapPlanCode": null,
              "sapSaPattern": null,
              "sapSaPatternDesc": null,
              "sapStatus": null
            }
          ],
          "indexationRates": [
            {
              "indexationRateId": null,
              "irPlanCode": null,
              "irIndexrate": null
            }
          ],
          "gnmmPlanRiders": [
            {
              "orPlanCode": null,
              "orRiderCode": null,
              "orRiderName": null,
              "orParentRiderCode": null,
              "orParentRiderDesc": null,
              "orExclOptional": null,
              "orExlGroupId": null,
              "orInClorOption": null,
              "orInGroupId": null,
              "orTermPlan": null,
              "orPayTerm": null,
              "orTermMethod": null,
              "orSaPlan": null,
              "orMedrIdGrp": null,
              "orMinTimesBasicSa": null,
              "orMaxTimesBasicSa": null,
              "orMinTimesIncome": null,
              "orMaxTimesIncome": null,
              "orMinTimesBasicPre": null,
              "orMaxTimesBasicPre": null,
              "orNomAllowed": null,
              "orAutoPopulate": null,
              "orWopApplicable": null,
              "orComPlan": null,
              "orPlanPremPay": null,
              "orComPolicyTerm": null,
              "orcSvAddPaidUp": null,
              "orRiderStatus": null,
              "orLaApplicable": null,
              "orRelApplicable": null,
              "orPrimRider": null,
              "orStatus": null,
              "orLa": null
            } 
          ],
          "gnmmPlanPaymodeLinks": [
            {
              "pmPlanCode": null,
              "pmPaymentMode": null,
              "pmApplicableTo": null,
              "pmPatternCode": "L",
              "pmPatternDesc": null,
              "pmMinPre": null,
              "pmMaxPre": null,
              "pmMinRiderTerm": null,
              "pmMaxRiderTerm": null,
              "pmDefCriteria": null,
              "pmMinContdueCh": null,
              "pmMaxContdueCh": null,
              "pmMaxchreq": null,
              "pmMinYrsPaidCont": null,
              "pmMinFundWithCh": null,
              "pmStatus": null,
              "pmEffDateFrom": null,
              "pmEffDateTo": null
            }
          ],
          "payMethodLinks": [
            {
              "pmsPlanCode": null,
              "pmsPlanDesc": null,
              "pmsPayMethod": null,
              "pmsStatus": null
            }
          ],
          "methodModeExclusions": [
            {
              "pedPlanCode": null,
              "pedPlanDesc": null,
              "pedPayMode": null,
              "pedPaymentMethod": null,
              "pedStatus": null
            }
          ],
          "saLinks": [
            {
              "saLinkId": null,
              "larlPlanCode": null,
              "larlRelationCode": null,
              "larlRelationDesc": null,
              "larlMinsa": null,
              "larlMaxsa": null,
              "larlMinage": null,
              "larlMaxage": null,
              "larlStatus": null
            }
          ],
          "proposerRelLinks": [
            {
              "prlPlanCode": null,
              "prlRelationCode": null,
              "prlRelationDesc": null,
              "prlMinage": null,
              "prlMaxage": null,
              "prlStatus": null
            }
          ],
          "nomineeRelLinks": [
            {
              "nmlPlanCode": null,
              "nmlRelationCode": null,
              "nmlRelationsDesc": null,
              "nmlMinAge": null,
              "nmlMaxAge": null,
              "nmlStatus": null
            }
          ],
          "currencyLinks": [
            {
              "clPlanCode": null,
              "clCurrencyCode": null,
              "clCurrencyDesc": null,
              "clStatus": null
            }
          ],
          "gnmmPlanTermAgeLink":[
            {
            "patlPlancode":null,
            "patlEntryagemin":null,
            "patlEntryagemax":null,
            "patlStartterm":null,
            "patlEndterm":null,
            "patlStatus":null
            
            
            }
            ],
          "clJointLife": null,
          "jldLcFix": null,
          "jldNoOfLives": null,
          "jldAgeGroup": null,
          "jldMaxLc": null,
          "jldMinLc": null,
          "jldPremCalMtd": null,

          "jlGrpLink": [{
            "gtdPlanCode": null,
            "gtdGroupId": null,
            "gtdDefaultAge": null,
            "gtdStatus": null,
            "jlAgeLink": [{
              "geadPlanCode": null,
              "geadLcNo": null,
              "geadMinAge": null,
              "geadMaxAge": null,
              "geadStatus": null
            }]
          }],

         
          "tlValuations": [
            {
              "tLValuationId": null,
              "tvtPlanCode": null,
              "tvtCsvBase": null,
              "tvtPaidBase": null
            }
          ],
          "annexureCodes": [
            {
              "annexureCodeId": null,
              "acPlanCode": null,
              "acAeCode": null,
              "acAnnDesc": null,
              "acStartDate": null,
              "acEndDate": null,
              "acseqOrder": null,
              "acSource": null,
              "acStartAge": null,
              "acEndAge": null,
              "acPayFreq": null,
              "acFreqDesc": null
            }
          ],
          "policyConversions": [
            {
              "pcPlanCode": null,
              "pcPlanCodeTo": null,
              "pcEffDateFrom": null,
              "pcEffDateTo": null
            }
          ],
          "policyToPlans": [
            {
              "policyToPlanId": null,
              "pcnPlanCode": null,
              "pcnPlanCodeFrom": null,
              "pcnEffDateFrom": null,
              "pcnEffDateTo": null
            }
          ],
          "laasrclcno": null,
          "ageLALinks": [
            {
              "ageLALinkId": null,
              "ladPlanCode": null,
              "ladCurrLcNo": null,
              "ladTarLcNo": null,
              "ladcurrelationcode": null,
              "ladTargerrelcode": null,
              "ladTargetRelDesc": null
            }
          ],
          "incrDecrSas": [
            {
              "incrDecrSaId": null,
              "sasPlanCode": null,
              "sasIncDecflag": null,
              "sasGender": null,
              "sasOccupClass": null,
              "sasChNumber": null,
              "sasSmoker": null,
              "sasAlcohal": null,
              "sasPregnant": null,
              "sasAgeFrom": null,
              "sasAgeTo": null,
              "sasGrpSizeFrom": null,
              "sasGrpSizeTo": null,
              "sasTermFrom": null,
              "sasTermTo": null,
              "sasPolYearFrom": null,
              "sasPolyearTo": null,
              "sasPrePayTermFrom": null,
              "sasPrePayTermTo": null,
              "sasDefPeriodFrom": null,
              "sasDefPeriodTo": null,
              "sasIntRateFrom": null,
              "sasIntRateTo": null,
              "sasEffFrom": null,
              "sasEffTo": null,
              "sasRate": null,
              "sasUnit": null,
              "sasStatus": null
            }
          ],
          "modalFactorLinks": [
            {
              "mfPlanCode": null,
              "mfParentPlriCode": null,
              "mfPayFreq": null,
              "mfPaymtdCode": null,
              "mfModalFactor": null,
              "mfStatus": null
            }
          ],
          "indexScRates": [
            {
              "indexScRatesId": null,
              "sairPlanCode": null,
              "sairPolYearStart": null,
              "sairIndexRate":null,
              "sairPolYearEnd": null, 
              "sairAttAgeStart": null,
              "sairAttAgeEnd": null,
              "sairStatus": null
            }
          ],
          "optRiderLas": [
            {
              "optRiderLaId": null,
              "laPlanCode": null,
              "laRiderCode": null,
              "laLaNo": null
            }
          ],
          "optRiderRels": [
            {
              "optRiderRel": null,
              "rnPlanCode": null,
              "rnRiderCode": null,
              "rnRnship": null
            }
          ],
          "primaryRiderLinks": [
            {
              "prdPlanCode": null,
              "prdPrimaryRider": null,
              "prdRiderCode": null,
              "prdTsab": null,
              "prdCpts": null,
              "prdSsab": null,
              "prdCsab": null,
              "prdCTsaB": null,
              "prdBSAT": null,
              "prdNoTBSaMin": null,
              "prdNoTBSaMax": null,
              "prdNoTBCMin": null,
              "prdNoTBCMax": null,
              "prdStatus": null
            }
            
          ],
          "gnmmPlanIncrDTO": [
            {
              "incsasPlancode":null,
              "incsasIncdecflag":null,
              "incsasGender":null,
              "incsasOccupclass":null,
               "incsasChnumber":null,
               "incsasSmoker":null,
               "incsasAlcohal":null,
              "incsasPregnant":null,
               "incsasAgefrom":null,
             "incsasAgeto":null,
               "incsasGrpsizefrom":null,
               "incsasGrpsizeto":null,
               "incsasTermfrom1":null,
              "incsasTermto1":null,
               "incsasPolyearfrom":null,
              "incsasPolyearto":null,
               "incsasPrepaytermfrom":null,
              "incsasPrepaytermto":null,
               "incsasTermfrom2":null,
              "incsasTermto2":null,
              "incsasDefperiodfrom":null,
              "incsasDefperiodto":null,
              "incsasIntratefrom":null,
              "incsasIntrateto":null,
              "incsasEfffrom":null,
              "incsasEffto":null,
              "incsasRate":null,
              "incsasUnit":null,
              "incsasStatus":null,
              "incsassequenceNumber":null,
            }
            
          ],
          "gnmmPlanDecrDTO": [
            {
              "decsasPlancode":null,
              "decsasIncdecflag":null,
              "decsasGender":null,
              "decsasOccupclass":null,
               "decsasChnumber":null,
               "decsasSmoker":null,
               "decsasAlcohal":null,
              "decsasPregnant":null,
               "decsasAgefrom":null,
             "decsasAgeto":null,
               "decsasGrpsizefrom":null,
               "decsasGrpsizeto":null,
               "decsasTermfrom1":null,
              "decsasTermto1":null,
               "decsasPolyearfrom":null,
              "decsasPolyearto":null,
               "decsasPrepaytermfrom":null,
              "decasPrepaytermto":null,
               "decsasTermfrom2":null,
              "decsasTermto2":null,
              "decsasDefperiodfrom":null,
              "decsasDefperiodto":null,
              "decsasIntratefrom":null,
              "decsasIntrateto":null,
              "decsasEfffrom":null,
              "decsasEffto":null,
              "decsasRate":null,
              "decsasUnit":null,
              "decsasStatus":null,
              "decsassequenceNumber":null,
            }
            
          ]

        };
        return format;
  }
  dataUpload(data){
    data["PageName"]="NEW_BUSINESS";
    this.appService.downloaddata=data;
    localStorage.setItem("upload", "true");
    this.appService.uploadingPageContent=this.appService.operation_nbusiness;
    this.appService.uploadingPageName="operation_nbusiness";
    this.dialogService.addDialog(DataDownloadComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '
 
    }).subscribe((isConfirmed) => {
         if (isConfirmed=="done") {
              this.ngOnInit(); 
              for (var d = 0; d < this.appService.subPagesNavProdInfo.length; d++) {
                this.appService.subPagesNavProdInfo[d].isActive=false;
          
                 
              }
   } else {

      
      }
    })
   }

  dataDownload(data){
    data["PageName"]="NEW_BUSINESS"; 	
 this.appService.downloaddata=data;
   localStorage.setItem("upload", "false");
   this.dialogService.addDialog(DataDownloadComponent, {
     title: 'Name dialog',
     question1: 'Block Name ',
     question2: 'Block Type '

   }).subscribe((isConfirmed) => {
     if (isConfirmed) {
  } else {
     }
   })
  }

  copyRow(data, eve) {
    // var countR = data.tableObj.length;
    var s;
    for(var i=0;i<Object.keys(data.tableObj[data.tableObj.length-1]).length;i++){
      if(Object.keys(data.tableObj[data.tableObj.length-1])[i].includes("sn")){
        s=i;
    }
    
    }
    var countR=data.tableObj[data.tableObj.length-1][Object.keys(data.tableObj[data.tableObj.length-1])[s]];


    this.display_data = this.appService.changeAcc(this.appService.operation_nbusiness, data, ++countR,eve.target.parentElement.id, "copyRow");

   }
  
   retriveDel(data){
    var fieldName = [];
    localStorage.setItem("fieldNameList", "");
    for (var a = 0; a < data.deletedItems.length; a++) {
    fieldName.push({ "id": data.deletedItems[a].id, "itemName": data.deletedItems[a].fieldName });
    }
     localStorage.setItem("fieldNameList", JSON.stringify(fieldName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
        var selectedFields = [];
        for (var a = 0; a < data.deletedItems.length; a++) {
          for (var b = 0; b < seletedItems.length; b++) {
          if(seletedItems[b].id == data.deletedItems[a].id){
            selectedFields.push(data.deletedItems[a]);
          }
          }
        }
        this.display_data = this.appService.changeAcc(this.appService.operation_nbusiness, data, selectedFields,"", "addDelField");
      } else {

      }
    })
     
   }

   retriveTabDel(data){
    var fieldName = [];
    localStorage.setItem("fieldNameList", "");
    for (var a = 0; a < data.deletedItems.length; a++) {
    fieldName.push({ "id": data.deletedItems[a].id, "itemName": data.deletedItems[a].theadName });
    }
     localStorage.setItem("fieldNameList", JSON.stringify(fieldName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
        var selectedFields = [];
        for (var a = 0; a < data.deletedItems.length; a++) {
          for (var b = 0; b < seletedItems.length; b++) {
          if(seletedItems[b].id == data.deletedItems[a].id){
            selectedFields.push(data.deletedItems[a]);
          }
          }
        }
        this.display_data = this.appService.changeAcc(this.appService.operation_nbusiness, data, selectedFields,"", "addDelTabField");
      } else {

      }
    })
     
   }

   retriveDelAcc(){
    localStorage.setItem("fieldNameList", "");
     localStorage.setItem("fieldNameList", JSON.stringify(this.accName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
       if (isConfirmed) {
          var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
          for (var a = 0; a < this.accName.length; a++) {
            for (var b = 0; b < seletedItems.length; b++) {
              if(this.accName[a].id == seletedItems[b].id){
                this.accName.splice(a, 1);
              }
            var seletedAcc = { "accordId" :seletedItems[b].id };
            this.display_data = this.appService.changeAcc(this.appService.operation_nbusiness, seletedAcc, "", "", "retriveAcc")
          }
        }
      } else {

      }
    })
    }
    searchLov(lovName,data,index,tData){
      localStorage.setItem('searchdata','table');
      this.appService.sIndex=index;
      var check=JSON.stringify(this.appService[lovName]);
      this.appService.lovListData=JSON.parse(check);
      if(tData=='pc_plancodeto' || tData=='pcn_plancodefrom'){
        this.appService.lovListData=this.maintainPlanConvTo;
      }
      this.appService.templateInfo=this.appService.operation_nbusiness;
      this.appService.lovpiName=lovName;
      this.appService.dataInfo=data;
      this.appService.tData=tData;
      this.dialogService.addDialog(DataSearchComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '
   
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {
     } else {
       
        }
      })
  
    }
    searchLovfield(field)
    {
      localStorage.setItem('searchdata','field');
      var check=JSON.stringify(this.appService[field.lovUrl]);
      this.appService.lovListData=JSON.parse(check);
      // var ch1=JSON.stringify(field.optionObj);
      this.appService.dataInfo=field;
      this.appService.fieldName=field.fieldName;
      
      this.dialogService.addDialog(DataSearchComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '
   
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {
     } else {
       
        }
      })
    }
    
    UpdateValues(pinfo: NgForm)
    {
      
      this.productUpdated=true;
      localStorage.setItem('ProductUpdated','true');
      // setTimeout(()=>{

      //   this.spinner.show();
      // },100)
      this.appService.validFieldCount(this.appService.operation_nbusiness);
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      var dataContent = [];
      dataContent.push({ "ProductId": this.appService.productId, "PageName": "NEW_BUSINESS.json", "PageContent": this.appService.operation_nbusiness});
  
      this.http.post('/api/dataPublishPage', dataContent, { headers: headers })
        .subscribe(res => {
          setTimeout(() => {
            console.log(res.json());
          }, 1500);
  
        }, err => {
          console.log("err " + err);
        })
    if (pinfo.valid) {
        var output = this.appService.validPage(this.appService.operation_nbusiness);
        if (output.mandatory) {
          this.alertMsg = [];
          for (var i = 0; i < output.mandatoryFields.length; i++) {
            this.alertMsg.push({ "msg": output.mandatoryFields[i].fieldName + ' (' + output.mandatoryFields[i].accordionName + ')' });
  
          }
          localStorage.setItem("alertContent", '');
          localStorage.setItem("prodAlertMsg", JSON.stringify(this.alertMsg));
          setTimeout(()=>{
  
            this.spinner.hide();
          },100)
          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '
  
          }).subscribe((isConfirmed) => {
  
            localStorage.setItem("prodAlertMsg", "");
          });
        } else {
          var result = this.storeValuesToDb();
          var tabAccFormat=tabAccopNewBusinessFormat;
          this.appService.updateValuesRisk(this.appService.operation_nbusiness ,result ,tabAccFormat);
          
          setTimeout(() => {
            this.spinner.hide();
            if (this.appService.productSaved) {
              this.productSaved = true;
              this.appService.productSaved = false;
              localStorage.setItem("prodAlertMsg", "");
              localStorage.setItem("alertContent", "Updated Successfully");
  
              this.dialogService.addDialog(AlertComponent, { 
                title: 'Name dialog',
                question1: 'Block Name ',
                question2: 'Block Type '
  
              }).subscribe((isConfirmed) => {
                
  
              });
            }
          }, 1000)
        }
      }
    }
    
selectIndex(i)
{
  this.dropIndex=i;
}
}