import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { PrompComponent } from "../bootstrap-modal/Modaldialog/promp.component";
import { Http, Headers } from '@angular/http';
import { AppServices } from '../app.service';
import { Router } from '@angular/router';
import { MasterdialogComponent } from "../bootstrap-modal/masterdialog/masterdialog.component";
import { ReviewComponent } from "../bootstrap-modal/Review/review.component";
import { AlertComponent } from "../bootstrap-modal/alert/alert.component";
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import {FooterbarComponent} from '../product-config/footerbar/footerbar.component';
//import { Ng4LoadingSpinnerModule, Ng4LoadingSpinnerService  } from 'ng4-loading-spinner';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert2';
declare var jQuery: any;

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit,OnDestroy {
  @ViewChild('app-footerbar') footerBar:FooterbarComponent;
  busy: Subscription;
  wsCount: number;
  templateCreation: boolean = false;
  templateModification: boolean = false;
  templateView: boolean = false;
  templateApproval: boolean = false;
  templateDeletion: boolean = false;
  serviceList: any = [];
spinnerCount:any;
  constructor(private spinner: NgxSpinnerService,private router: Router, public loc: Location, private appServices: AppServices, private dialogService: DialogService, private http: Http) {
    //private spinnerService: Ng4LoadingSpinnerService,


  }
  dashBoard: any = [];
  tmpFolderList: any = [];
  publishFolderList: any = [];
  templatePush: any = [];
  masterPush: any = [];
  masterPushList: any = [];
  isOverlay: boolean = false;
  pageload: number = 0;
  publishStatus:boolean=false;

  ngOnInit() {
    this.appServices.isDboard.next(true);
    document.getElementById("dashboard").classList.add("on");



    // document.getElementById("dashboard").setAttribute("style","background-color:rgba(255,255,255,0.2);");
    //document.getElementById("productInfo").setAttribute("style","background-color:'';");
    this.wsCount = 0;

    if (localStorage.getItem("pageLoad") == "1") {
      this.accessLevel();

      this.displayPage();


    }


    if (localStorage.getItem("pageLoad") == "0") {

      this.accessLevel();

      this.displayPage();
    }

  }

  ngOnDestroy(){
    this.appServices.isDboard.next(false);
    // this.footerBar.ngAfterViewInit();
  }


  // stalert(){
  //       this.dialogService.addDialog(AlertComponent, {
  //     title: 'Name dialog',
  //     question1: 'Block Name ',
  //     question2: 'Block Type '

  //   }).subscribe((isConfirmed) => {
  //     alert(isConfirmed);
  //   });
  // }

  delete(eve, publishStatus) {

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(async (result) => {
      
      if (result.value) {

        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
    
        if (!publishStatus) {

          //add by darong
          console.log("zzzzzzzzzzzzzzzz")
          const templateId=eve.target.parentElement.parentElement.children[0].textContent
          let result = await this.http.delete("/api/deleteTmpTemplate/"+templateId).toPromise().catch(ex=>{})
          if(!result) {
            swal({
              title:"Template not exist.",
              width: 567,
              type: 'warning',
              confirmButtonColor: '#3085d6',
              confirmButtonText:"OK"
            })
          }else{
            swal(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            localStorage.setItem("selectedTemplate","");
            this.displayPage();
          }
        
          /////////

          // this.http.get('http://localhost:4777/api/getTmpFolders')
          //   .subscribe(res => {
   

           
          //     var folder = Object.keys(res.json());
      
          //     if (folder.length > 0) {
          //       for (var i = 0; i < folder.length; i++) {
          //         if (eve.target.parentElement.parentElement.children[0].textContent == folder[i]) {
          //           this.http.post('/api/delTmpFolder', { "foldername": eve.target.parentElement.parentElement.children[0].textContent }, { headers: headers })
          //             .subscribe((res) => {
          //               swal(
          //                 'Deleted!',
          //                 'Your file has been deleted.',
          //                 'success'
          //               )
          //               localStorage.setItem("selectedTemplate","");
          //               this.displayPage();
          //             })
          //         }
          //       }
    
          //     }
    
          //   })
            
        } else if (publishStatus) {
          this.http.get('/api/getPublishFolders')
            .subscribe(res => {
              var folder = Object.keys(res.json());
              if (folder.length > 0) {
                for (var j = 0; j < folder.length; j++) {
                  if (eve.target.parentElement.parentElement.children[0].textContent == folder[j]) {
                    this.http.post('/api/delPublishFolder', { "foldername": eve.target.parentElement.parentElement.children[0].textContent }, { headers: headers })
                      .subscribe((res) => {
                        swal(
                          'Deleted!',
                          'Your file has been deleted.',
                          'success'
                        )
                         localStorage.setItem("selectedTemplate","");
                        this.displayPage();
    
    
                      })
                  }
                }
    
              }
            })
        }



      
      }
    })
   
  }
  displayPage() {
    this.spinner.show();
    this.spinnerCount=0;
     this.isOverlay = true;

    this.serviceList = [];
    this.serviceList.push({"serviceName":"tempProdInfo","pageName":"PROD_INFO.json"},
{"serviceName":"operation_nbusiness","pageName":"NEW_BUSINESS.json"},
{"serviceName":"operation_uwritting","pageName":"OPERATION_UNDERWRITING.json"},
{"serviceName":"risk_coverage","pageName":"PROD_BENEFITS.json"},
{"serviceName":"operation_unitlink","pageName":"OPERATION_UNITLINKED.json"},
{"serviceName":"operation_annuity","pageName":"OPERATION_ANNUITY.json"},
{"serviceName":"product_rate","pageName":"PRODUCT_RATES.json"},
{"serviceName":"agency","pageName":"AGENCY.json"},
{"serviceName":"group","pageName":"OPERATION_GROUP.json"},
{"serviceName":"policyServicing","pageName":"POLICYSERVICING.json"},
{"serviceName":"finance","pageName":"PRODUCT_FINANCE.json"},
{"serviceName":"charges","pageName":"PRODUCT_CHARGES.json"});

    this.templatePush = [];
    this.tmpFolderList = []
    this.publishFolderList = [];
 
    this.busy = this.http.get('/api/getTmpFolders')
      .subscribe(res => {
      
      
        //this.busy = this.http.get('/api/getTmpFolders').toPromise();

        var fname = Object.keys(res.json());
        if (fname.length > 0) {
          for (var i = 0; i < fname.length; i++) {

            var pname = Object.keys(res.json()[fname[i]]);

            for (var j = 0; j < pname.length; j++) {

              if (JSON.parse(res.json()[fname[i]][pname[j]])) {


                if (JSON.parse(res.json()[fname[i]][pname[j]])[0].ProductType) {
                  var output = JSON.parse(res.json()[fname[i]][pname[j]])[0];
                                                                                                                                                                                                                                                                                            
                  this.tmpFolderList.push({ "TemplateId": output.TemplateId, "TemplateName": output.TemplateName, "ProductType": output.ProductType, "CreatedBy": output.CreatedBy, "CreatedDate": output.CreatedDate, "ModifiedDate": output.ModifiedDate, "Publish": false, "PageList": output.PageList })
                }


              }
            }

          }
          var statusarray =[];

          for (var k = 0; k < this.tmpFolderList.length; k++) {
      

            var review = false;
            if(this.tmpFolderList[k].PageList.length>0){
            for (var h = 0; h < this.tmpFolderList[k].PageList.length; h++) {
             
              statusarray[h]=this.tmpFolderList[k].PageList[h].review;
            
            }
            console.log(statusarray)
           
            var n = statusarray.indexOf(false);
            var m = statusarray.indexOf("Rejected");
            if(m<0 && n<0 && statusarray.length>0){
              review = true;
            }else{
              review = false;
            }
          }else{
            review = false;
          }
                                                                                                                                                                                                                                                                                                                                              
            this.templatePush.push({ "TemplateId": this.tmpFolderList[k].TemplateId, "ProductType": this.tmpFolderList[k].ProductType, "TemplateName": this.tmpFolderList[k].TemplateName, "CreatedBy": this.tmpFolderList[k].CreatedBy, "CreatedDate": this.tmpFolderList[k].CreatedDate, "ModifiedDate": this.tmpFolderList[k].ModifiedDate, "Publish": false, "PageList": this.tmpFolderList[k].PageList, "Review": review })

            if (this.tmpFolderList.length - 1 == k) {
              //    ++this.spinnerCount;
              //  this.spinner.hide();
              // this.spinnerHidden();
              ++this.spinnerCount;
              this.spinnerHidden();
              if (localStorage.getItem("selectedTemplate") != null) {
                  setTimeout(() => {
                  
                    for (var i = 0; i < document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr").length; i++) {
                      if (localStorage.getItem("selectedTemplate") == document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].children[0].textContent) {
                         document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].style.backgroundColor = "Bisque";
                         var templateId=document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].children[0].textContent;
                         var templateName=document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].children[1].textContent;
                         var eve=document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].children[0].parentElement;
                        // this.openTemplate(templateId,templateName,eve,this.publishStatus);
                      }
                    }
                  }, 1000);
              }
              
            }
          }

        } else {
           ++this.spinnerCount;
          //this.spinner.hide();
          this.spinnerHidden();
          this.isOverlay = false;

        }
      },err=>{
        ++this.spinnerCount;
        //this.spinner.hide();
        this.spinnerHidden();
      })

    //Get Publish Folders
    this.busy = this.http.get('/api/getPublishFolders')
      .subscribe(res => {
        var fname = Object.keys(res.json());
        if (fname.length > 0) {

          for (var i = 0; i < fname.length; i++) {

            var pname = Object.keys(res.json()[fname[i]]);

            for (var j = 0; j < pname.length; j++) {

              if (JSON.parse(res.json()[fname[i]][pname[j]])) {


                if (JSON.parse(res.json()[fname[i]][pname[j]])[0].ProductType) {

                  var output = JSON.parse(res.json()[fname[i]][pname[j]])[0];
                  this.publishFolderList.push({ "TemplateId": output.TemplateId, "TemplateName": output.TemplateName, "ProductType": output.ProductType, "CreatedBy": output.CreatedBy, "CreatedDate": output.CreatedDate, "ModifiedDate": output.ModifiedDate, "Publish": true, "PageList": output.PageList })

                }



              }
            }

          }
          for (var j = 0; j < this.publishFolderList.length; j++) {

           
            this.templatePush.push({ "TemplateId": this.publishFolderList[j].TemplateId, "ProductType": this.publishFolderList[j].ProductType, "TemplateName": this.publishFolderList[j].TemplateName, "CreatedBy": this.publishFolderList[j].CreatedBy, "CreatedDate": this.publishFolderList[j].CreatedDate, "ModifiedDate": this.publishFolderList[j].ModifiedDate, "Publish": true, "PageList": this.publishFolderList[j].PageList })
            if (this.publishFolderList.length - 1 == j) {
            //  this.spinner.hide();
                   ++this.spinnerCount;
              this.spinnerHidden();

              if (localStorage.getItem("selectedTemplate") != null) {
                for (var v = 0; v < this.templatePush.length; v++) {
                  setTimeout(() => {
                    for (var i = 0; i < document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr").length; i++) {
                      if (localStorage.getItem("selectedTemplate") == document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].children[0].textContent) {
                        document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].style.backgroundColor = "Bisque";
                          var templateId=document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].children[0].textContent;
                         var templateName=document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].children[1].textContent;
                         var eve=document.getElementsByClassName("gnrc-table")[0].getElementsByTagName("tr")[i].children[0].parentElement;
                        
                        //this.openTemplate(templateId,templateName,eve,this.publishStatus);
                         
                      }
                    }
                  }, 1000);


                }
              }
          
         

            }

          }
          
        } else {
           ++this.spinnerCount;
         // this.spinner.hide();
          this.spinnerHidden();
        }

      },err=>{
        ++this.spinnerCount;
        this.spinnerHidden();

      })

    // this.masterPush = [];
    // this.http.get('/api/getTempMaster')
    //   .subscribe(res => {
    //     console.log(res.json());
    //     console.log(1);

    //     //  this.busy = this.http.get('/api/getTempMaster').toPromise();

    //     var master = Object.keys(res.json());
    //     for (var i = 0; i < master.length; i++) {

    //       var output = JSON.parse(res.json()[master[i]])[0];



    //       this.masterPushList.push({ "MasterId": output.MasterId, "MasterName": output.MasterName, "dataEntryModal": output.dataEntryModal, "CreatedBy": output.CreatedBy, "CreatedDate": output.CreatedDate, "ModifiedDate": output.ModifiedDate });


    //     }
    //     for (var l = 0; l < this.masterPushList.length; l++) {


    //       console.log(1);
    //       this.masterPush.push({ "MasterId": this.masterPushList[l].MasterId, "MasterName": this.masterPushList[l].MasterName, "dataEntryModal": this.masterPushList[l].dataEntryModal, "CreatedBy": this.masterPushList[l].CreatedBy, "CreatedDate": this.masterPushList[l].CreatedDate, "ModifiedDate": output.ModifiedDate });


    //     }
    //     this.wsCount++;


    //   })
    // setTimeout(() => {
    //   this.isOverlay = false;
    // }, 1500)




  }

  spinnerHidden(){
    if(this.spinnerCount>=2){
      setTimeout(()=>{
        this.spinner.hide();
      
      },100)
    }
  }
  newTemplate() { 
      
    //unitLink
    this.appServices.operation_unitlink = [];

  

  //annuity
  this.appServices.operation_annuity = [];

  //rates
  this.appServices.product_rate = [];

  //agency
  this.appServices.agency = [];

  //group
  this.appServices.group = [];
  this.appServices.grp_ageLoading = [];
  this.appServices.grp_renewalPlan = [];

  //policy servicing
  this.appServices.policyServicing = [];


  //finance
  this.appServices.finance = [];
  
       //CHANRGES
       this.appServices.charges=[];
       this.appServices.chargesValues=[];
       this.appServices.contractStatus=[];
       this.appServices.loading=[];
       this.appServices.modelfactor=[];
       this.appServices.pcRate=[];
       this.appServices.specificLA=[];
	
	
  //risk
  this.appServices.risk_coverage = [];

  this.appServices.prod_info = [];
  this.appServices.operation_nbusiness = [];
  this.appServices.operation_uwritting = [];
 
  this.publishStatus = false;
  
    localStorage.setItem("dboard", "1");
    this.dialogService.addDialog(PrompComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        localStorage.setItem("dboard", "0");
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        if (localStorage.getItem("checkedPages") != "") {

          var checkedPages = JSON.parse(localStorage.getItem("checkedPages"));



          console.log("subpage check :"+checkedPages);



          if (checkedPages[0].ProductType) {


            this.http.post('/api/posts', checkedPages, { headers: headers })
              .subscribe(res => {


                this.templatePush.push({ "TemplateId": checkedPages[0].TemplateId, "TemplateName": checkedPages[0].TemplateName, "ProductType": checkedPages[0].ProductType, "CreatedBy": checkedPages[0].CreatedBy, "CreatedDate": checkedPages[0].CreatedDate, "PageList": [],"reviewComments":[] });
                localStorage.setItem("checkedPages", "");

              }, err => {
                console.log("err " + err);
              })

          }

        }

        if (localStorage.getItem("checkedPageNames") != "") {

          var checkedPageNames = JSON.parse(localStorage.getItem("checkedPageNames"));

          if (checkedPageNames[0].ProductType) {

            var templateSave = [];
            templateSave.push(checkedPageNames[0]);
            this.http.post('/api/createTemplate', templateSave, { headers: headers })
              .subscribe(res => {

                this.templatePush.push({ "TemplateId": checkedPageNames[0].TemplateId, "TemplateName": checkedPageNames[0].TemplateName, "ProductType": checkedPageNames[0].ProductType, "CreatedBy": checkedPageNames[0].CreatedBy, "CreatedDate": checkedPageNames[0].CreatedDate });
                localStorage.setItem("checkedPages", "");
 
              }, err => {
                console.log("err " + err);
              })

          }

          if (checkedPageNames[1].CopyExistTemp) {

            for (var h = 0; h < checkedPageNames[1].PagesSelected.length; h++) {


              var copyPages = [];
              copyPages.push({ "folderId": checkedPageNames[0].TemplateId, "PageName": checkedPageNames[1].PagesSelected[h].PageContent[0].PageName, "PageContent": checkedPageNames[1].PagesSelected[h].PageContent[0].PageContent });

              this.http.post('/api/movePublishFiles', copyPages, { headers: headers })
                .subscribe(res => {

                  console.log("Post:Move Publish files:PageInfo"+copyPages);


                }, err => {
                  console.log("err " + err);
                })

            }
            localStorage.setItem("checkedPageNames", "");
          }
        }
      } else {
        localStorage.setItem("dboard", "0");
      }
    })
  }

  // newTemplate() { 
  //   //unitLink
  //   this.appServices.operation_unitlink = [];

  

  // //annuity
  // this.appServices.operation_annuity = [];

  // //rates
  // this.appServices.product_rate = [];

  // //agency
  // this.appServices.agency = [];

  // //group
  // this.appServices.group = [];
  // this.appServices.grp_ageLoading = [];
  // this.appServices.grp_renewalPlan = [];

  // //policy servicing
  // this.appServices.policyServicing = [];


  // //finance
  // this.appServices.finance = [];
  
  //      //CHANRGES
  //      this.appServices.charges=[];
  //      this.appServices.chargesValues=[];
  //      this.appServices.contractStatus=[];
  //      this.appServices.loading=[];
  //      this.appServices.modelfactor=[];
  //      this.appServices.pcRate=[];
  //      this.appServices.specificLA=[];
	
	
  // //risk
  // this.appServices.risk_coverage = [];

  // this.appServices.prod_info = [];
  // this.appServices.operation_nbusiness = [];
  // this.appServices.operation_uwritting = [];
 
  
  //   localStorage.setItem("dboard", "1");
  //   this.dialogService.addDialog(PrompComponent, {
  //     title: 'Name dialog',
  //     question1: 'Block Name ',
  //     question2: 'Block Type '

  //   }).subscribe((isConfirmed) => {
  //     if (isConfirmed) {
  //       localStorage.setItem("dboard", "0");
  //       var headers = new Headers();
  //       headers.append('Content-Type', 'application/json');
  //       if (localStorage.getItem("checkedPages") != "") {

  //         var checkedPages = JSON.parse(localStorage.getItem("checkedPages"));


  //         console.log("subpage check");
  //         console.log(checkedPages);



  //         if (checkedPages[0].ProductType) {


  //           this.http.post('/api/posts', checkedPages, { headers: headers })
  //             .subscribe(res => {


  //               this.templatePush.push({ "TemplateId": checkedPages[0].TemplateId, "TemplateName": checkedPages[0].TemplateName, "ProductType": checkedPages[0].ProductType, "CreatedBy": checkedPages[0].CreatedBy, "CreatedDate": checkedPages[0].CreatedDate, "PageList": [],"reviewComments":[] });
  //               localStorage.setItem("checkedPages", "");

  //             }, err => {
  //               console.log("err " + err);
  //             })

  //         }

  //       }

  //       if (localStorage.getItem("checkedPageNames") != "") {

  //         var checkedPageNames = JSON.parse(localStorage.getItem("checkedPageNames"));

  //         if (checkedPageNames[0].ProductType) {

  //           var templateSave = [];
  //           templateSave.push(checkedPageNames[0]);
  //           this.http.post('/api/createTemplate', templateSave, { headers: headers })
  //             .subscribe(res => {

  //               this.templatePush.push({ "TemplateId": checkedPageNames[0].TemplateId, "TemplateName": checkedPageNames[0].TemplateName, "ProductType": checkedPageNames[0].ProductType, "CreatedBy": checkedPageNames[0].CreatedBy, "CreatedDate": checkedPageNames[0].CreatedDate });
  //               localStorage.setItem("checkedPages", "");

  //             }, err => {
  //               console.log("err " + err);
  //             })

  //         }

  //         if (checkedPageNames[1].CopyExistTemp) {

  //           for (var h = 0; h < checkedPageNames[1].PagesSelected.length; h++) {


  //             var copyPages = [];
  //             copyPages.push({ "folderId": checkedPageNames[0].TemplateId, "PageName": checkedPageNames[1].PagesSelected[h].PageContent[0].PageName, "PageContent": checkedPageNames[1].PagesSelected[h].PageContent[0].PageContent });

  //             this.http.post('/api/movePublishFiles', copyPages, { headers: headers })
  //               .subscribe(res => {

  //                 console.log(1);


  //               }, err => {
  //                 console.log("err " + err);
  //               })

  //           }
  //           localStorage.setItem("checkedPageNames", "");
  //         }




  //         // this.http.post('/api/posts', checkedPages, { headers: headers })
  //         //   .subscribe(res => {
  //         //     setTimeout(() => {
  //         //       console.log(res.json());
  //         //     }, 1500);

  //         //   }, err => {
  //         //     console.log("err " + err);
  //         //   })




  //         // this.templatePush.push({ "TemplateId": checkedPages[0].TemplateId, "TemplateName": checkedPages[0].TemplateName, "ProductType": checkedPages[0].ProductType, "CreatedBy": checkedPages[0].CreatedBy, "CopyExistTemp": checkedPages[0].CopyExistTemp, "PagesSelected": checkedPages[0].PagesSelected, "CreatedDate": checkedPages[0].CreatedDate });
  //         // console.log(this.templatePush[0]);

  //       }

  //       // setTimeout(()=>{
  //       //   localStorage.setItem("checkedPages", "");
  //       //   localStorage.setItem("checkedPageNames", "");
  //       // },2000)

 

  //     } else {
  //       localStorage.setItem("dboard", "0");
  //     }
  //   })
  // }

  publish(eve) {

    swal({
      title: 'Publish',
      text: "You want to publish the template!",
      type: 'info',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, publish it!'
    }).then((result) => {
      if (result.value) {
    // this.spinnerService.show();
    this.isOverlay = true;
    var folderId = eve.target.parentElement.parentElement.getElementsByTagName("td")[0].textContent;
    var folderName = eve.target.parentElement.parentElement.getElementsByTagName("td")[1].textContent;

    this.http.post("/api/publishTemplate",{templateId:folderId,templateName:folderName}).subscribe(res=>{
      console.log(res);
    })

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var check = [{ "folderId": eve.target.parentElement.parentElement.getElementsByTagName("td")[0].textContent }];


    var tempPageList = [];
    tempPageList.push({ "folderId": folderId });

    this.busy = this.http.post('/api/moveTmpFiles', tempPageList, { headers: headers })
      .subscribe(res => {
        localStorage.setItem("alertContent", "Published");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.displayPage();
        });


      }, err => {
         this.isOverlay = false;
        console.log("err " + err);
      })

    }
    })
    // setTimeout(() => {
    //         this.isOverlay = false;
    //         this.displayPage();

    //         this.router.navigate(['/dummy'], { skipLocationChange: true });
    //       }, 1000)

  }

  review(eve) {
    console.log( eve);
    localStorage.setItem("ReviewTID", eve.TemplateId);
    localStorage.setItem("ReviewTName", eve.TemplateName);
          localStorage.setItem("dboard", "2");
          this.dialogService.addDialog(PrompComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '
      
          }).subscribe((isConfirmed) => {
            localStorage.setItem("dboard", "0");
    });

  }

  openTemplate(templateId, templateName, eve,publishStatus) {
    this.appServices.tempProdInfo= [];
    this.appServices.agency= [];
    this.appServices.operation_annuity= []; 
    this.appServices.group= [];
    this.appServices.policyServicing= [];
    this.appServices.operation_uwritting= [];
    this.appServices.operation_unitlink= [];
    this.appServices.operation_nbusiness= [];
    this.appServices.charges= [];
    this.appServices.finance= [];
    this.appServices.product_rate= [];
    this.appServices.risk_coverage= [];
    this.appServices.currentTemplate=templateName;
    if(publishStatus){
      this.publishStatus=true;
    }else{
      this.publishStatus=false;
    }

    // if(eve.getElementsByTagName("td")[8].children){
    //   if(eve.getElementsByTagName("td")[8].children[0].textContent=="Published"){
    //     this.publishStatus=true;
    //   }
    // }else{
    //   this.publishStatus=false;
    // }
    // this.publishStatus=publishStatus;
    this.appServices.templatePublishStatus=publishStatus;
    localStorage.setItem("selectedTemplate", templateId);
    for (var i = 0; i < eve.parentElement.parentElement.getElementsByTagName("tr").length; i++) {
      eve.parentElement.parentElement.getElementsByTagName("tr")[i].style.backgroundColor = "";
    }
    eve.style.backgroundColor = "Bisque";
    this.appServices.templateId = templateId;
    this.appServices.templateName = templateName;
    //localStorage.setItem("templateId", templateId);
    localStorage.setItem("templateName", templateName);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    try{
    if (this.publishStatus) {
      this.http.post('/api/readPublishFiles', { "folderId": templateId }, { headers: headers })
        .subscribe(res => {
          this.appServices.templatePageList=JSON.parse(res.json()[templateName+".json"]);
          this.appServices.publishedTemplate = true;
          var fname = Object.keys(res.json());

          this.appServices.prod_info = [];
          this.appServices.risk_coverage = [];
          this.appServices.operation_uwritting = [];
          this.appServices.operation_nbusiness = [];

          for (var i = 0; i < fname.length; i++) {
            for (var j = 0; j < this.serviceList.length; j++) {
              //  this.serviceList[j].serviceName=[];
              if (fname[i] == this.serviceList[j].pageName) {
                this.appServices[this.serviceList[j].serviceName] = JSON.parse(res.json()[fname[i]])[0].PageContent;
              }
            }

          }

        })
    } else if(!this.publishStatus) {
      this.http.post('/api/readTemplateFiles', { "folderId": templateId }, { headers: headers })
        .subscribe(res => {
          this.appServices.templatePageList=JSON.parse(res.json()[templateName+".json"]);

          console.log(1);
          this.appServices.publishedTemplate = false;

          var fname = Object.keys(res.json());
          this.appServices.prod_info = [];
          this.appServices.risk_coverage = [];
          this.appServices.operation_uwritting = [];
          this.appServices.operation_nbusiness = [];

          for (var i = 0; i < fname.length; i++) {
            for (var j = 0; j < this.serviceList.length; j++) {
                 if (fname[i] == this.serviceList[j].pageName) {
                // this.appServices[this.serviceList[j].serviceName]=[];
                this.appServices[this.serviceList[j].serviceName] = JSON.parse(res.json()[fname[i]]);
              }
            }


          }

        },err=>{
          console.log(err);
        })
    }
    }catch(e){
      console.log(e);
    }


  }

  changeTheme() {
    document.getElementById('theme').setAttribute('href', 'assets/productcss/custom.css');

  }

  newMaster() {

    localStorage.setItem("dboard", "1");
    this.dialogService.addDialog(MasterdialogComponent, {
      title: 'Name dialog',
      question1: 'Master Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        localStorage.setItem("dboard", "0");
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');


        if (localStorage.getItem("masterDetails") != "") {

          var masterDetails = JSON.parse(localStorage.getItem("masterDetails"));



          this.http.post('/api/masterCreate', masterDetails, { headers: headers })
            .subscribe(res => {

              this.masterPush.push({ "MasterId": res.json()[0].MasterId, "MasterName": res.json()[0].MasterName, "MasterMappingCode": res.json()[0].MasterMappingCode, "dataEntryModal": res.json()[0].dataEntryModal, "CreatedBy": res.json()[0].CreatedBy, "CreatedDate": res.json()[0].CreatedDate });

            }, err => {
              console.log("err " + err);
            })






        }


      } else {
        localStorage.setItem("dboard", "0");
      }
    })
  }

  openMaster(id) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post('/api/getMaster', { "id": id }, { headers: headers })
      .subscribe(res => {
        //this.seletedMaster.push({ "MasterId": res.json()[0].MasterId, "MasterName": res.json()[0].MasterName, "dataEntryModal": res.json()[0].dataEntryModal, "CreatedBy": res.json()[0].CreatedBy,  "CreatedDate": res.json()[0].CreatedDate });
        localStorage.setItem("seletedMaster", JSON.stringify(res.json()));
        this.router.navigate(['./productConfig/master']);
      })



  }

  accessLevel() {
    if (localStorage.getItem("accessRights") != null) {

      var accessRights = JSON.parse(localStorage.getItem("accessRights"));
      console.log("AAAAAAAAAAA")
      console.log(accessRights)


      for (var j = 0; j < Object.keys(accessRights.gpa).length; j++) {
        if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])].length > 0) {
          for (var k = 0; k < accessRights.gpa[(Object.keys(accessRights.gpa)[j])].length; k++) {
            // 				console.log(Object.keys(accessRights.gpa)[j])
            // 				console.log(accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k])
            for (var i = 0; i < accessRights.selectedPermissions.length; i++) {
              if (accessRights.selectedPermissions[i].name == Object.keys(accessRights.gpa)[j]) {
                if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].parent == "Template") {

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Create Template") {
                  
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Create") {
                      this.templateCreation = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Copy") {
                      this.appServices.templateCopy = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Review Template") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Approval") {
                      this.appServices.templateApproval = true;
                      this.templateApproval = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Deletion") {
                      this.appServices.templateDeletion = true;
                      this.templateDeletion = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Basic Details") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.bdCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.bdModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.bdView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "New Business") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.nbCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.nbModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.nbView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Under Writting") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.uwCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.uwModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.uwView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Prod Benefits") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.rcCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.templateDeletion = true;
                      this.appServices.rcModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.templateDeletion = true;
                      this.appServices.rcView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Unit Link") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.ulCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.ulModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.ulView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Agency") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.AgencyCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.AgencyModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.AgencyView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Annuity") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.AnnuityCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.AnnuityModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.AnnuityView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Group") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.GroupCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.GroupModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.GroupView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Policy Servicing") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.psCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.psModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.psView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Finance") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.FinanceCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.FinanceModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.FinanceView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Rates") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.RateCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.RateModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.RateView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Charges") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.ChargesCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.ChargesModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.ChargesView = true;
                    }

                  }

                }

              }

            }
          }
        }
      }


    } else {
      alert("Rights not set");
    }



  }
 

}
