import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef, AfterViewInit, ChangeDetectionStrategy, ElementRef } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { ProductDialogueComponent } from "../bootstrap-modal/Productdialogue/productdialogue.component";
import { Http, Headers } from '@angular/http';
import { AppServices } from '../app.service';
import { Router } from '@angular/router';
import { ReviewComponent } from "../bootstrap-modal/Review/review.component";
import { AlertComponent } from "../bootstrap-modal/alert/alert.component";
import { SidebarComponent } from '../product-config/middlebar/sidebar/sidebar.component';
import { PrompComponent } from "../bootstrap-modal/Modaldialog/promp.component";
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from "@angular/router";
declare var jQuery: any;
import swal from 'sweetalert2';
import { ElementDef } from '@angular/core/src/view';

@Component({
  selector: 'dashboarddata',
  templateUrl: './dashboarddata.component.html',
  styleUrls: ['./dashboarddata.component.css']

})
export class DashboarddataComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(SidebarComponent) sideBar: SidebarComponent;

  progressBar: any = 1;
  productCreation: boolean = false;
  productModification: boolean = false;
  productView: boolean = false;
  productApproval: boolean = false;
  productDeletion: boolean = false;
  updatehide: boolean = false;


  isOverlay: boolean = false;
  pageData: any = [];
  ws_url: any;
  busy: Promise<any>;
  productPush: any = this.appServices.productPushCurrentPage;
  productTmpFolderList: any = [];
  productPublishFolderList: any = [];
  masterPush: any = [];
  masterPushList: any = [];
  serviceList: any = [];
  publishStatus: boolean = false;
  createdTempleatePages: any = [];

  outputPI: any;
  outputNB: any;
  outputUW: any;
  outputAY: any;
  outputGRP: any;
  outputUL: any;
  outputFE: any;
  outputRS: any;
  outputAgency: any;
  outputPRODUCT_CHARGES: any;
  loadingWebService: any;
  loaded: any;
  publishedStatus: false;
  spinnerCount = 0;
  TotalFieldCount: number = 0;
  ValidFieldCount: number = 0;
  InValidFieldCount: number = 0;
  CompletionPerc: number = 0;
  productId: any;
  planName: any;
  pieChart: boolean = false;
  totalPageList: any = [];




  output: any = { "outputPI": "", "outputNB": "", "outputUW": "", "outputAY": "", "outputGRP": "", "outputUL": "", "outputFE": "", "outputRS": "", "outputAgency": "" };

  constructor(private spinner: NgxSpinnerService, private ref: ChangeDetectorRef, private router: Router, private appServices: AppServices, private dialogService: DialogService, private http: Http) {


    //   this.appServices.loadingDone.subscribe((value) => {
    //   if(value==false){
    //      document.getElementsByClassName("container")[0].classList.add("wsLoading");
    //   }else{
    //       document.getElementsByClassName("container")[0].classList.remove("wsLoading");
    //   this.loaded=value;
    //   }
    // });





  }

  ngOnInit() {
    this.progressBar = this.appServices.progressBar;
    //this.productPush=[];
    this.loaded = true;
    if (!this.appServices.loadingDone) {
      this.loaded = false;
      var disBool = this.displayPage();
      //  this.spinner.show();
      console.log("BBBBBBBBBBBBBBBBBBb");
      console.log(this.appServices.lov_getGnluHSZone);// Added by Bhagavathy 06/08/2019  CallWS
      this.appServices.callWS();// Commented by Bhagavathy 06/08/2019  CallWS
      // this.spinner.hide();

    } else {
      if (localStorage.getItem("pageMode") == "2") {
        var selectProduct = false;
        var refreshData = [];
        var selectedProductIndex;

        for (var i = 0; i < this.appServices.productPushCurrentPage.length; i++) {
          if (this.appServices.productPushCurrentPage[i].selected) {
            selectProduct = true;
            refreshData.push(this.appServices.productPushCurrentPage[i]);
            selectedProductIndex = i;

          } else {
            this.appServices.productPushCurrentPage[i]["selected"] = false;
          }
        }

        if (!selectProduct) {
          var disBool = this.displayPage();
        } else {
          var headers = new Headers();
          headers.append('Content-Type', 'application/json');

          if (!refreshData[0].Publish) {

            this.http.post('/api/productRefreshTmp', { "ProductId": refreshData[0].ProductId, "ProductName": refreshData[0].ProductId }, { headers: headers })
              .subscribe((res) => {


                var statusarray = [];


                var review = false;
                if (res.json().latestData[0].PageList.length > 0) {
                  for (var h = 0; h < res.json().latestData[0].PageList.length; h++) {

                    statusarray[h] = res.json().latestData[0].PageList[h].review;

                  }
                  var n = statusarray.indexOf(false);
                  var m = statusarray.indexOf("Rejected");
                  if (m < 0 && n < 0 && statusarray.length > 0) {
                    review = true;
                  } else {
                    review = false;
                  }
                } else {
                  review = false;
                }

                var refreshedData = [{ "ProductId": res.json().latestData[0].ProductId, "ProductType": res.json().latestData[0].ProductType, "TemplateId": res.json().latestData[0].TemplateId, "ProductName": res.json().latestData[0].ProductName, "CreatedBy": res.json().latestData[0].CreatedBy, "CreatedDate": res.json().latestData[0].CreatedDate, "ModifiedDate": res.json().latestData[0].ModifiedDate, "Publish": res.json().latestData[0].Publish, "PageList": res.json().latestData[0].PageList, "Reviews": [], "review": review, "PlanName": res.json().latestData[0].PlanName, "PlanDesc": res.json().latestData[0].PlanDesc, "Updated": false }];
                this.appServices.productPushCurrentPage[selectedProductIndex] = refreshedData[0];
                this.appServices.productPushCurrentPage[selectedProductIndex]["selected"] = true;
                this.productPush[selectedProductIndex]["selected"] = true;

              })

          } else if (refreshData[0].Publish) {

            //     for(var i=0;i<this.appServices.productPushCurrentPage.length;i++){
            //     this.appServices.productPushCurrentPage[i]["selected"]=false;
            // }
            //     this.displayPage();

            this.http.post('/api/productRefreshPublish', { "ProductId": refreshData[0].ProductId, "ProductName": refreshData[0].ProductId }, { headers: headers })
              .subscribe((res) => {

                var updatearray = [];
                var update = false;
                if (res.json().latestData[0].PageList.length > 0) {
                  for (var h = 0; h < res.json().latestData[0].PageList.length; h++) {
                    if (res.json().latestData[0].PageList[h].Updated) {

                      updatearray[h] = res.json().latestData[0].PageList[h].review;
                    }

                  }
                  var n = updatearray.indexOf(false);
                  var m = updatearray.indexOf("Rejected");
                  if (m < 0 && n < 0 && updatearray.length > 0) {
                    update = true;
                  } else {
                    update = false;
                  }
                } else {
                  update = false;
                }
                console.log(1);
                //this.productPublishFolderList[j].PageList[0].Updated
                var refreshedData = [{ "ProductId": res.json().latestData[0].ProductId, "ProductType": res.json().latestData[0].ProductType, "TemplateId": res.json().latestData[0].TemplateId, "ProductName": res.json().latestData[0].ProductName, "CreatedBy": res.json().latestData[0].CreatedBy, "CreatedDate": res.json().latestData[0].CreatedDate, "ModifiedDate": res.json().latestData[0].ModifiedDate, "Publish": true, "PageList": res.json().latestData[0].PageList, "PlanName": res.json().latestData[0].PlanName, "PlanDesc": res.json().latestData[0].PlanDesc, "Updated": update }];

                this.appServices.productPushCurrentPage[selectedProductIndex] = refreshedData[0];
                this.appServices.productPushCurrentPage[selectedProductIndex]["selected"] = true;
                this.productPush[selectedProductIndex]["selected"] = true;

              })

          }
        }



      }


      // if (disBool) {
      //   this.spinner.hide();
      // }   
    }


    // this.loadingWebService=this.appServices.presentWebService;

    this.appServices.wsLoading.subscribe((value) => {
      this.loadingWebService = value.toString();

    });

    this.appServices.progressBar.subscribe((value) => {
      this.progressBar = value;
      if (this.progressBar > 100) {
        this.spinner.hide();

        this.loaded = true;
      }
    });




    this.appServices.isDboard.next(true);




    if (localStorage.getItem("pageMode") == "2") {
      this.accessLevel();

      document.getElementById("dashboard").classList.add("on");
console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");// Added by Bhagavathy 06/08/2019  CallWS
console.log(this.appServices.lovGnluFrequencyMasterFN);// Added by Bhagavathy 06/08/2019  CallWS

    }
    //  this.spinner.hide();  // Added by Bhagavathy 06/08/2019  CallWS
  }

  ngAfterViewInit() {

  }

  ngOnDestroy() {
    this.appServices.isDboard.next(false);
  }

  newProduct() {


    localStorage.setItem("dboard", "1");
    this.dialogService.addDialog(ProductDialogueComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {

        // this.spinner.show(); 
        this.displayPage();
        // this.productPush.push(JSON.parse(localStorage.getItem("productCreated"))[0]);

        //   this.http.get('assets/data/property.json')
        //   .subscribe(resURL => {
        //     this.ws_url = resURL.json().web.url;
        //      var dbPost={"planCode": JSON.parse(localStorage.getItem("productCreated"))[0].ProductId,"piName":JSON.parse(localStorage.getItem("productCreated"))[0].PlanName};

        //     var headers = new Headers();
        //   headers.append('Content-Type', 'application/json');

        //   this.http.post(this.ws_url + 'ProductBenefits/GnmmPlanMaster',dbPost, { headers: headers })
        //   .subscribe(resCheck => {
        //     this.http.get(this.ws_url + "ProductBenefits/GnmmPlanMaster")
        //     .subscribe((res) => {

        //      this.appServices.lovcom_tableoutput=res.json();
        //      this.spinner.hide();
        //     }, err => {
        //       this.spinner.hide();
        //     })


        //   },err=>{
        //     this.spinner.hide();
        //   })

        // })
        // var headers = new Headers();
        // headers.append('Content-Type', 'application/json');

        // if (localStorage.getItem("checkedPages") != "") {

        //   var checkedPages = JSON.parse(localStorage.getItem("checkedPages"));


        //   console.log("subpage check");
        //   console.log(checkedPages);



        //   if (checkedPages[0].TemplateId) {


        //     this.http.post('/api/dataPosts', checkedPages, { headers: headers })
        //       .subscribe(res => {


        //         this.productPush.push({ "ProductId": checkedPages[0].ProductId, "ProductType": checkedPages[0].ProductType, "TemplateId": checkedPages[0].TemplateId, "CreatedBy": checkedPages[0].CreatedBy, "CreatedDate": checkedPages[0].CreatedDate, "CopyExistProduct": false, "PageList": [] });
        //         localStorage.setItem("checkedPages", "");
        //         this.http.post('/api/readPublishFiles', { "folderId": checkedPages[0].TemplateId }, { headers: headers })
        //           .subscribe(res => {

        //             for (var i = 0; i < Object.keys(res.json()).length; i++) {
        //               if (JSON.parse(res.json()[Object.keys(res.json())[i]])[0].PageName) {
        //                 // var ot={}
        //                 // ot=JSON.parse(res.json()[Object.keys(res.json())[i]])[0];
        //                 this.http.post('/api/producttoproducttemp', { "ProductId": checkedPages[0].ProductId, "PageName": JSON.parse(res.json()[Object.keys(res.json())[i]])[0].PageName, "PageContent": JSON.parse(res.json()[Object.keys(res.json())[i]])[0].PageContent }, { headers: headers })
        //                   .subscribe(res => {
        //                   }, err => {
        //                     console.log("err " + err);
        //                   })

        //               }


        //             }



        //           }, err => {
        //             console.log("err " + err);
        //           })

        //       }, err => {
        //         console.log("err " + err);
        //       })

        //   }

        // }

        // if (localStorage.getItem("checkedPageNames") != "") {

        //   var checkedPageNames = JSON.parse(localStorage.getItem("checkedPageNames"));

        //   if (checkedPageNames[0].PageContent[0].ProductId) {


        //     this.http.post('/api/createPublishTemplate', checkedPageNames[0].PageContent, { headers: headers })
        //       .subscribe(res => {

        //         this.productPush.push({ "ProductId": checkedPageNames[0].PageContent[0].ProductId, "ProductType": checkedPageNames[0].PageContent[0].ProductType, "TemplateId": checkedPageNames[0].PageContent[0].TemplateId, "CreatedBy": checkedPageNames[0].PageContent[0].CreatedBy, "CreatedDate": checkedPageNames[0].PageContent[0].CreatedDate, "PageList": [] });
        //         localStorage.setItem("checkedPageNames", "");

        //       }, err => {
        //         console.log("err " + err);
        //       })

        //   }

        //   if (checkedPageNames[0].ProductContent) {

        //     for (var h = 0; h < checkedPageNames[0].ProductContent.PageName.length; h++) {


        //       var copyPages = [];
        //       // copyPages.push({ "folderId":checkedPageNames[0].ProductId, "PageName": checkedPageNames[1].PagesSelected.PageName[h].PageName, "PageContent":checkedPageNames[1].PagesSelected.PageName[h].PageContent});
        //       copyPages.push({ "folderId": checkedPageNames[0].ProductId, "PageName": checkedPageNames[1].PagesSelected.PageName[h].PageName, "PageContent": checkedPageNames[1].PagesSelected.PageName[h].PageContent });

        //       this.http.post('/api/moveProductPublishFiles', copyPages, { headers: headers })
        //         .subscribe(res => {

        //           console.log(1);


        //         }, err => {
        //           console.log("err " + err);
        //         })

        //     }
        //     localStorage.setItem("checkedPageNames", "");
        //   }

        // }
      } else {

      }
    })
  }

  openProduct(ProductId, ProductName, TemplateId, Publish, eve, ppush) {
    for (var i = 0; i < this.appServices.productPushCurrentPage.length; i++) {
      if (this.appServices.productPushCurrentPage[i].ProductId == ProductId) {
        this.appServices.productPushCurrentPage[i]["selected"] = true;
        this.productPush[i]["selected"] = true;

      } else {
        this.appServices.productPushCurrentPage[i]["selected"] = false;
        this.productPush[i]["selected"] = false;
      }

    }
    // this.appServices.maintainProduct=[];
    // this.appServices.maintainProduct.push(ProductId,ProductName,TemplateId,Publish,eve,ppush);
    this.spinner.show();
    this.appServices.tempProdInfo = [];
    this.appServices.agency = [];
    this.appServices.operation_annuity = [];
    this.appServices.group = [];
    this.appServices.policyServicing = [];
    this.appServices.operation_uwritting = [];
    this.appServices.operation_unitlink = [];
    this.appServices.operation_nbusiness = [];
    this.appServices.charges = [];
    this.appServices.finance = [];
    this.appServices.product_rate = [];
    this.appServices.risk_coverage = [];
    this.appServices.currentTemplate = ProductId;


    // this.isOverlay=true;
    localStorage.setItem("selectedProduct", ProductId);
    this.publishStatus = Publish;
    // for (var i = 0; i < eve.parentElement.parentElement.getElementsByTagName("tr").length; i++) {
    //   eve.parentElement.parentElement.getElementsByTagName("tr")[i].style.backgroundColor = "";
    // }
    // eve.style.backgroundColor = "Bisque";
    localStorage.setItem("templateId", TemplateId);
    localStorage.setItem("productId", ProductId);
    this.appServices.templateId = TemplateId;

    this.appServices.productId = ProductId;
    this.appServices.planCodeDD = ProductId;
    this.appServices.productName = ProductId;
    this.appServices.piname = ppush.PlanName;
    this.appServices.piDescription = ppush.PlanDesc;
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    if (Publish) {
      console.log("AAAAAAAAAAAAAAA")
      // if (eve.children[8].children[0].id == "published" || eve.children[8].children[0].id =="publishedupdate") {
      // this.isOverlay = true;
      this.callList();
      this.appServices.publishedStatus = true;
      this.http.post('/api/readProductPublishFiles', { "folderId": ProductId }, { headers: headers })
        .subscribe(res => {
          this.appServices.productPageList=JSON.parse(res.json()[ProductId+".json"]);

                console.log(1);
                this.appServices.publishedProduct = true;
                var fname = Object.keys(res.json());
                this.appServices.prod_info = [];
                this.appServices.risk_coverage = [];
                this.appServices.operation_uwritting = [];
                this.appServices.operation_nbusiness = [];


                for (var i = 0; i < fname.length; i++) {
                  for (var j = 0; j < this.serviceList.length; j++) {
                    if (fname[i] == this.serviceList[j].pageName) {
                      // this.appServices[this.serviceList[j].serviceName]=[];
                      this.appServices[this.serviceList[j].serviceName] = JSON.parse(res.json()[fname[i]])[0].PageContent;
                    }
                    if (j == this.serviceList.length - 1) {
                      //    setTimeout(()=>{
                      //   this.spinner.hide();
                      // },2000)
                    }
                  }
                  if (i == fname.length - 1) {
                    // setTimeout(()=>{
                    this.spinner.hide();
                    // },2000)
                  }
                }

        })
    } else {
      console.log("BBBBBBBBB")
      this.callList();
      this.http.post('/api/readProductTemplateFiles', { "folderId": ProductId }, { headers: headers })
        .subscribe(res => {
          this.appServices.productPageList = JSON.parse(res.json()[ProductId + ".json"]);

          // this.isOverlay = true;
          this.appServices.publishedProduct = false;
          var fname = Object.keys(res.json());
          this.appServices.pageList = JSON.parse(res.json()[ProductId + '.json'])[0].PageList;

          this.appServices.prod_info = [];
          this.appServices.risk_coverage = [];
          this.appServices.operation_uwritting = [];
          this.appServices.operation_nbusiness = [];

          for (var i = 0; i < fname.length; i++) {
            for (var j = 0; j < this.serviceList.length; j++) {
              if (fname[i] == this.serviceList[j].pageName) {
                this.appServices[this.serviceList[j].serviceName] = JSON.parse(res.json()[fname[i]])[0].PageContent;
              }

              // if(j==this.serviceList.length-1){
              //       setTimeout(()=>{
              //     this.spinner.hide();
              //   },2000)

              // }
            }
            if (i == fname.length - 1) {
              // setTimeout(()=>{
              this.spinner.hide();
              // },2000)

            }
          }

          // for (var i = 0; i < fname.length; i++) {
          //      for(var k=0;k<this.createdTempleatePages.length;k++){
          //     if(fname[i]!=this.createdTempleatePages[k].pageName){
          //       for(var h=0;h<this.createdTempleatePages[k].appservicePageAccess.length;h++){
          //         this.appServices[this.createdTempleatePages[k].appservicePageAccess[h]]=false;
          //       }
          //     }

          //   }
          //   if(fname.length-1==i){
          //     alert("trigger");
          //     this.sideBar.ngOnInit();

          //   }
          // }


        })


    }


  }


  release(eve, productId, type) {

    swal({
      title: 'Release',
      text: "You want to release the product!",
      type: 'info',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, release it!'
    }).then((result) => {
      if (result.value) {
        setTimeout(() => {

          this.spinner.show();
        }, 100)

        var folderId = productId;

        var pageReleased = [];

        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // var check = [{ "folderId": eve.target.parentElement.parentElement.getElementsByTagName("td")[0].textContent.trim() }];
        var apilink = "";
        if (type == 'save') {
          apilink = "readProductTemplateFiles";
        } else {
          apilink = "readProductPublishTemplateFiles";
        }

        this.http.post('/api/' + apilink, { "folderId": folderId }, { headers: headers })
          .subscribe(resProd => {
            console.log(resProd.json());
            var output = [];
            for (var i = 0; i < JSON.parse(resProd.json()[folderId + ".json"])[0].PageList.length; i++) {
              output.push(JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].SaveValue);

            }


            var prodBenefits = [];
            for (var i = 0; i < JSON.parse(resProd.json()[folderId + ".json"])[0].PageList.length; i++) {
              // if(JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].name!="PRODUCT_CHARGES"){
              //           output.push(JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].SaveValue);
              // }
              if (JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].name == "PROD_BENEFITS") {
                prodBenefits.push(JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].SaveValue);

              }

            }

            console.log(prodBenefits)
            if (prodBenefits.length > 0) {

              for (var i = 0; i < JSON.parse(resProd.json()[folderId + ".json"])[0].PageList.length; i++) {

                if (JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].name == "PRODUCT_CHARGES") {

                  if (JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].SaveValue["gnmmPlanEventLinkChargesDTO"]) {
                    for (var k = 0; k < JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].SaveValue["gnmmPlanEventLinkChargesDTO"].length; k++) {
                      if (JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].SaveValue["gnmmPlanEventLinkChargesDTO"][k].gnmmPlanChargesMethods) {
                        prodBenefits[0].gpmForEventLinkForParent[k]["gnmmPlanChargesMethods"] = JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].SaveValue["gnmmPlanEventLinkChargesDTO"][k].gnmmPlanChargesMethods;
                      }

                    }

                  }
                }

              }
            }

            output.push(prodBenefits[0]);

            var obj = Object.assign({}, ...output)
            if (obj.gnmmPlanEventLinkChargesDTO) {
              delete obj.gnmmPlanEventLinkChargesDTO;

            }
            // var UpdInftim=new Date(eve.target.parentElement.parentElement.getElementsByTagName("td")[4].textContent.trim());
            var UpdInftim = new Date();
            obj.userInfo = [];
            // obj.userInfo.push({"lastupdUser": eve.target.parentElement.parentElement.getElementsByTagName("td")[3].textContent.trim(),
            // "lastupdProg":"Product Configurator",
            // "lastupdInftim": UpdInftim.toISOString().substring(0,10)});
            obj.userInfo.push({
              "lastupdUser": eve.target.parentElement.parentElement.getElementsByTagName("td")[3].textContent.trim(),
              "lastupdProg": "Product Configurator",
              "lastupdInftim": UpdInftim.toISOString().substring(0, 10)
            });
            obj["status"] = "A";

            this.http.get('assets/data/property.json')
              .subscribe(resURL => {
                this.ws_url = resURL.json().web.url;
                ///this.ws_url='http://10.200.104.35:8080/productConfig/';

                if (type == 'save') {
                  // Updated by Pisith 02/12/2019
                  // this.http.get('assets/lov/' + fileName)
                  // How can you call this post , cuz we don't have this api in node server
                  this.http.post(this.ws_url + 'ProductBenefits/GnmmPlanMaster', obj, { headers: headers })
                    .subscribe(resCheck => {
                      var returnObj = resCheck.json();

                      if (returnObj != null && (!returnObj.piErrStatus) && (returnObj.piSuccessErrMsg == null) && (!returnObj.errorCode)) {
                        // if ((<any>resCheck)._body == " Product Benefit Added Successfully") {
                        this.spinner.hide();


                  localStorage.setItem("alertContent", "Released");


                        this.dialogService.addDialog(AlertComponent, {
                          title: 'Name dialog',
                          question1: 'Block Name ',
                          question2: 'Block Type '

                        }).subscribe((isConfirmed) => {
                          this.displayPage();
                        });

                        var tempPageList = [];
                        for (var rd = 0; rd < Object.keys(resProd.json()).length; rd++) {
                          if (Object.keys(resProd.json())[rd] != folderId + '.json') {
                            pageReleased.push({ "name": Object.keys(resProd.json())[rd] });
                          }
                        }
                        tempPageList.push({ "folderId": folderId, "pageRelease": pageReleased });

                        this.http.post('/api/moveProductTmpFiles', tempPageList, { headers: headers })
                          .subscribe(resFiles => {
                            this.appServices.fnSaveJsonFromValues(returnObj, folderId);
                            this.appServices.loadingDone = false;
                            setTimeout(() => {
                              // window.location.reload();
                              this.fnCallWSAfterCrud();
                            }, 2000);


                          }, err => {
                            // this.isOverlay = false;
                            console.log("err " + err);
                          })

                      } else if (JSON.parse((<any>resCheck)._body).errorMessage == eve.target.parentElement.parentElement.getElementsByTagName("td")[0].textContent.trim() + " PlanCode Already Exists.") {
                        // Updated by Pisith 02/12/2019
                        // this.http.get('assets/lov/' + fileName)
                        // How can you call this post , cuz we don't have this api in node server
                        this.http.put(this.ws_url + 'ProductBenefits/GnmmPlanMaster', obj, { headers: headers })
                          .subscribe(res => {
                            this.spinner.hide();
                            var returnObj = res.json();
                            if (returnObj != null && (!returnObj.piErrStatus) && (returnObj.piSuccessErrMsg == null) && (!returnObj.errorCode)) {

                              localStorage.setItem("alertContent", "Updated");

                              this.dialogService.addDialog(AlertComponent, {
                                title: 'Name dialog',
                                question1: 'Block Name ',
                                question2: 'Block Type '

                              }).subscribe((isConfirmed) => {
                                this.appServices.fnSaveJsonFromValues(returnObj, folderId);
                                this.appServices.loadingDone = false;
                                setTimeout(() => {
                                  // window.location.reload();
                                  this.fnCallWSAfterCrud();
                                  this.displayPage();
                                }, 2000);


                              });
                            }
                          }, err => {
                            localStorage.setItem("alertContent", "Update error" + err);
                            this.spinner.hide();
                            this.dialogService.addDialog(AlertComponent, {
                              title: 'Name dialog',
                              question1: 'Block Name ',
                              question2: 'Block Type '

                            }).subscribe((isConfirmed) => {
                              // this.isOverlay = false;
                            });

                          })
                      } else {
                        this.spinner.hide();

                        localStorage.setItem("alertContent", "DB Issue: " + JSON.parse((<any>resCheck)._body).errorMessage);

                        this.dialogService.addDialog(AlertComponent, {
                          title: 'Name dialog',
                          question1: 'Block Name ',
                          question2: 'Block Type '

                        }).subscribe((isConfirmed) => {
                          //this.isOverlay = false;
                        });

                      }






                    }, err => {
                      // this.isOverlay = false;
                      this.spinner.hide();
                      console.log("err " + err);
                    })

                } else if (type == "update") {
                  // Updated by Pisith 02/12/2019
                  // this.http.get('assets/lov/' + fileName)
                  // How can you call this post , cuz we don't have this api in node server
                  this.http.put(this.ws_url + 'ProductBenefits/GnmmPlanMaster', obj, { headers: headers })
                    .subscribe(res => {
                      this.spinner.hide();


                      this.updatehide = true;
                      var returnObj = res.json();
                      if (returnObj != null && (!returnObj.piErrStatus) && (returnObj.piSuccessErrMsg == null) && (!returnObj.errorCode)) {

                        localStorage.setItem("alertContent", "Updated");

                        this.dialogService.addDialog(AlertComponent, {
                          title: 'Name dialog',
                          question1: 'Block Name ',
                          question2: 'Block Type '

                        }).subscribe((isConfirmed) => {
                          this.appServices.fnSaveJsonFromValues(returnObj, folderId);
                          this.appServices.loadingDone = false;
                          setTimeout(() => {
                            // window.location.reload();
                            this.fnCallWSAfterCrud();
                            this.displayPage();
                          }, 2000);

                          (<HTMLInputElement>document.getElementById("publishedupdate")).innerHTML = 'Release';
                          (<HTMLInputElement>document.getElementById("publishedupdate")).setAttribute('style', 'background-color:forestgreen !important;border-color:forestgreen !important;')
                          //ppush.Updated=false;
                        });
                      } else {
                        localStorage.setItem("alertContent", "Update error" + returnObj.errorMessage);
                        this.spinner.hide();
                        this.dialogService.addDialog(AlertComponent, {
                          title: 'Name dialog',
                          question1: 'Block Name ',
                          question2: 'Block Type '

                        }).subscribe((isConfirmed) => {
                          // this.isOverlay = false;
                        });
                      }



                    }, err => {
                      localStorage.setItem("alertContent", "Update error" + err);
                      this.spinner.hide();
                      this.dialogService.addDialog(AlertComponent, {
                        title: 'Name dialog',
                        question1: 'Block Name ',
                        question2: 'Block Type '

                      }).subscribe((isConfirmed) => {
                        // this.isOverlay = false;
                      });
                    })

                }




              })
          })





      }

    })
  }
  /*

  release(eve,productId,type) {
    swal({
      title: 'Release',
      text: "You want to release the product!",
      type: 'info',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, release it!'
    }).then((result) => {
      if (result.value) {

    setTimeout(()=>{

      this.spinner.show();
    },100)
    
    var folderId = productId;

    var pageReleased = [];

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var check = [{ "folderId": eve.target.parentElement.parentElement.getElementsByTagName("td")[0].textContent.trim() }];
    var apilink="";
    if(type=='save')
    {
      apilink="readProductTemplateFiles";
    }else
    {
      apilink="readProductPublishTemplateFiles";
    }

    this.http.post('/api/'+apilink, { "folderId": folderId }, { headers: headers })
      .subscribe(resProd => {
        console.log(resProd.json());
        var output = [];
        for (var i = 0; i < JSON.parse(resProd.json()[folderId + ".json"])[0].PageList.length; i++) {
          output.push(JSON.parse(resProd.json()[folderId + ".json"])[0].PageList[i].SaveValue);

        }
        var obj = Object.assign({}, ...output)
        var UpdInftim=new Date(eve.target.parentElement.parentElement.getElementsByTagName("td")[4].textContent.trim());
        
        obj.userInfo = [];
        obj.userInfo.push({"lastupdUser": eve.target.parentElement.parentElement.getElementsByTagName("td")[3].textContent.trim(),
        "lastupdProg":"Product Configurator",
        "lastupdInftim": UpdInftim.toISOString().substring(0,10)});
        
        this.http.get('assets/data/property.json')
          .subscribe(resURL => {
            this.ws_url = resURL.json().web.url;
           ///this.ws_url='http://10.200.104.35:8080/productConfig/';
         

            this.http.post(this.ws_url + 'ProductBenefits/GnmmPlanMaster', obj, { headers: headers })
              .subscribe(resCheck => {
                var returnObj = resCheck.json();
                if(type=='save'){
                  if (JSON.parse((<any>resCheck)._body).errorMessage == folderId + " PlanCode Already Exists.") {
                  this.http.put(this.ws_url + 'ProductBenefits/GnmmPlanMaster', obj, { headers: headers })
                    .subscribe(resPut => {
                      this.spinner.hide();

                      if(!resPut.json().errorCode){

              
                        //  if(returnObj != null && (!returnObj.piErrStatus) && (returnObj.piSuccessErrMsg == null)){
                         // if ((<any>resCheck)._body == " Product Benefit Added Successfully") {
                            this.spinner.hide();
          
          
                            localStorage.setItem("alertContent", "Released");
          
                            this.dialogService.addDialog(AlertComponent, {
                              title: 'Name dialog',
                              question1: 'Block Name ',
                              question2: 'Block Type '
          
                            }).subscribe((isConfirmed) => {
                              this.displayPage();
                            });
                            
                            var tempPageList = [];
                            for (var rd = 0; rd < Object.keys(resProd.json()).length; rd++) {
                              if (Object.keys(resProd.json())[rd] != folderId + '.json') {
                                pageReleased.push({ "name": Object.keys(resProd.json())[rd] });
                              }
                            }
                            tempPageList.push({ "folderId": folderId, "pageRelease": pageReleased });
          
                            this.http.post('/api/moveProductTmpFiles', tempPageList, { headers: headers })
                              .subscribe(resFiles => {
                                this.appServices.fnSaveJsonFromValues(returnObj,folderId);
                                this.appServices.loadingDone = false;
                                setTimeout(() => {
                                  this.fnCallWSAfterCrud();
                                },10000);
                               
          
                              }, err => {
                               // this.isOverlay = false;
                                console.log("err " + err);
                              })
          
                          }else {
                            this.spinner.hide();
          
                            localStorage.setItem("alertContent", "DB Issue: " + JSON.parse((<any>resPut)._body).errorMessage);
          
                            this.dialogService.addDialog(AlertComponent, {
                              title: 'Name dialog',
                              question1: 'Block Name ',
                              question2: 'Block Type '
          
                            }).subscribe((isConfirmed) => {
                              //this.isOverlay = f alse;
                            });
          
                          }





                    }, err => {
                      localStorage.setItem("alertContent", "Update error" + err);
                      this.spinner.hide();
                      this.dialogService.addDialog(AlertComponent, {
                        title: 'Name dialog',
                        question1: 'Block Name ',
                        question2: 'Block Type '

                      }).subscribe((isConfirmed) => {
                       // this.isOverlay = false;
                      });

                    })
                } else if(!returnObj.errorCode){

              
                  //  if(returnObj != null && (!returnObj.piErrStatus) && (returnObj.piSuccessErrMsg == null)){
                   // if ((<any>resCheck)._body == " Product Benefit Added Successfully") {
                      this.spinner.hide();
    
    
                      localStorage.setItem("alertContent", "Released");
    
                      this.dialogService.addDialog(AlertComponent, {
                        title: 'Name dialog',
                        question1: 'Block Name ',
                        question2: 'Block Type '
    
                      }).subscribe((isConfirmed) => {
                        this.displayPage();
                      });
                      
                      var tempPageList = [];
                      for (var rd = 0; rd < Object.keys(resProd.json()).length; rd++) {
                        if (Object.keys(resProd.json())[rd] != folderId + '.json') {
                          pageReleased.push({ "name": Object.keys(resProd.json())[rd] });
                        }
                      }
                      tempPageList.push({ "folderId": folderId, "pageRelease": pageReleased });
    
                      this.http.post('/api/moveProductTmpFiles', tempPageList, { headers: headers })
                        .subscribe(resFiles => {
                          this.appServices.fnSaveJsonFromValues(returnObj,folderId);
                          this.appServices.loadingDone = false;
                          setTimeout(() => {
                            this.fnCallWSAfterCrud();
                          },10000);
                         
    
                        }, err => {
                         // this.isOverlay = false;
                          console.log("err " + err);
                        })
    
                    } else {
                  this.spinner.hide();

                  localStorage.setItem("alertContent", "DB Issue: " + JSON.parse((<any>resCheck)._body).errorMessage);

                  this.dialogService.addDialog(AlertComponent, {
                    title: 'Name dialog',
                    question1: 'Block Name ',
                    question2: 'Block Type '

                  }).subscribe((isConfirmed) => {
                    //this.isOverlay = false;
                  });

                }
              }
              else
              {
                //if (JSON.parse((<any>resCheck)._body).errorMessage == eve.target.parentElement.parentElement.getElementsByTagName("td")[0].textContent.trim() + " PlanCode Already Exists.") {
                  this.http.put(this.ws_url + 'ProductBenefits/GnmmPlanMaster', obj, { headers: headers })
                    .subscribe(res => {
                      this.spinner.hide();

                      localStorage.setItem("alertContent", "Updated");
                      this.updatehide=true;
                      (<HTMLInputElement> document.getElementById("publishedupdate")).innerHTML='Release'; 
                      (<HTMLInputElement> document.getElementById("publishedupdate")).setAttribute('style','background-color:forestgreen !important;border-color:forestgreen !important;')
                      //ppush.Updated=false;

                      this.dialogService.addDialog(AlertComponent, {
                        title: 'Name dialog',
                        question1: 'Block Name ',
                        question2: 'Block Type '

                      }).subscribe((isConfirmed) => {
                        var pname=localStorage.getItem('selectedProduct')
                       this.updateStatus();
                      
                        //this.displayPageupdate(productId,pname);
                      });
                    }, err => {
                      localStorage.setItem("alertContent", "Update error" + err);
                      this.spinner.hide();
                      this.dialogService.addDialog(AlertComponent, {
                        title: 'Name dialog',
                        question1: 'Block Name ',
                        question2: 'Block Type '

                      }).subscribe((isConfirmed) => {
                       // this.isOverlay = false;
                      });

                    })
               // }

              }


              }, err => {
                //this.isOverlay = false;
                this.spinner.hide();

                localStorage.setItem("alertContent", "DB Error" + JSON.parse(err._body).errorMessage);

                this.dialogService.addDialog(AlertComponent, {
                  title: 'Name dialog',
                  question1: 'Block Name ',
                  question2: 'Block Type '

                }).subscribe((isConfirmed) => {
                });
                console.log("err " + JSON.parse(err._body).errorMessage);
                
              }) 
            

          }, err => {
           // this.isOverlay = false;
           this.spinner.hide();
            console.log("err " + err);
          })
        
      })
    }
  })
  }
*/

  openMaster(id) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post('/api/getMaster', { "id": id }, { headers: headers })
      .subscribe(res => {
        //this.seletedMaster.push({ "MasterId": res.json()[0].MasterId, "MasterName": res.json()[0].MasterName, "dataEntryModal": res.json()[0].dataEntryModal, "CreatedBy": res.json()[0].CreatedBy,  "CreatedDate": res.json()[0].CreatedDate });
        localStorage.setItem("seletedMaster", JSON.stringify(res.json()));
        this.router.navigate(['./productConfig/master']);
      })



  }

  accessLevel() {


    if (localStorage.getItem("accessRights") != null) {

      var accessRights = JSON.parse(localStorage.getItem("accessRights"));

      for (var j = 0; j < Object.keys(accessRights.gpa).length; j++) {
        if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])].length > 0) {
          for (var k = 0; k < accessRights.gpa[(Object.keys(accessRights.gpa)[j])].length; k++) {
            // 				console.log(Object.keys(accessRights.gpa)[j])
            // 				console.log(accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k])
            for (var i = 0; i < accessRights.selectedPermissions.length; i++) {
              if (accessRights.selectedPermissions[i].name == Object.keys(accessRights.gpa)[j]) {
                if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].parent == "Product") {


                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Create Product") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Create") {
                      this.productCreation = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Copy") {
                      this.appServices.productCopy = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Review Product") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Approval") {
                      this.appServices.productApproval = true;
                      this.productApproval = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Deletion") {
                      this.appServices.productDeletion = true;
                      this.productDeletion = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Basic Details") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.pbdCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.pbdModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.pbdView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "New Business") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.pnbCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.pnbModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.pnbView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Under Writting") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.puwCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.puwModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.puwView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Prod Benefits") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.prcCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.templateDeletion = true;
                      this.appServices.prcModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.templateDeletion = true;
                      this.appServices.prcView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Unit Link") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.pulCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.pulModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.pulView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Agency") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.pAgencyCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.pAgencyModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.pAgencyView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Annuity") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.pAnnuityCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.pAnnuityModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.pAnnuityView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Group") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.pGroupCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.pGroupModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.pGroupView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Policy Servicing") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.ppsCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.ppsModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.ppsView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Finance") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.pFinanceCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.pFinanceModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.pFinanceView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Rates") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.pRateCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.pRateModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.pRateView = true;
                    }

                  }

                  if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].mode == "Charges") {
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Creation") {
                      this.appServices.pChargesCreate = true;

                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "Modification") {
                      this.appServices.pChargesModify = true;
                    }
                    if (accessRights.gpa[(Object.keys(accessRights.gpa)[j])][k].itemName == "View") {
                      this.appServices.pChargesView = true;
                    }

                  }

                }

              }

            }
          }
        }
      }
    } else {
      alert("Rights not set");
    }


  }

  review(eve) {

    localStorage.setItem("ReviewTID", eve.ProductId);
    localStorage.setItem("ReviewTName", eve.TemplateName);
    localStorage.setItem("dboard", "3");
    if (eve.Publish) {
      localStorage.setItem("publishStatus", "true");
    } else {
      localStorage.setItem("publishStatus", "false");
    }
    this.dialogService.addDialog(PrompComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      localStorage.setItem("dboard", "0");
    });

  }


  callList() {
    this.serviceList = [];
    this.serviceList.push({ "serviceName": "tempProdInfo", "pageName": "PROD_INFO.json" },
      { "serviceName": "operation_nbusiness", "pageName": "NEW_BUSINESS.json" },
      { "serviceName": "operation_uwritting", "pageName": "OPERATION_UNDERWRITING.json" },
      { "serviceName": "risk_coverage", "pageName": "PROD_BENEFITS.json" },
      { "serviceName": "operation_unitlink", "pageName": "OPERATION_UNITLINKED.json" },
      { "serviceName": "operation_annuity", "pageName": "OPERATION_ANNUITY.json" },
      { "serviceName": "product_rate", "pageName": "PRODUCT_RATES.json" },
      { "serviceName": "agency", "pageName": "AGENCY.json" },
      { "serviceName": "group", "pageName": "OPERATION_GROUP.json" },
      { "serviceName": "policyServicing", "pageName": "POLICYSERVICING.json" },
      { "serviceName": "finance", "pageName": "PRODUCT_FINANCE.json" },
      { "serviceName": "charges", "pageName": "PRODUCT_CHARGES.json" });


    //Enable,Disable sidedmenu based on created template

    this.createdTempleatePages.push({ "appservicePageAccess": ["pbdCreate", "pbdModify", "pbdView"], "pageName": "PROD_INFO.json" },
      { "appservicePageAccess": ["pnbCreate", "pnbModify", "pnbView"], "pageName": "NEW_BUSINESS.json" },
      { "appservicePageAccess": ["puwCreate", "puwModify", "puwView"], "pageName": "OPERATION_UNDERWRITING.json" },
      { "appservicePageAccess": ["prcCreate", "prcModify", "prcView"], "pageName": "PROD_BENEFITS.json" },
      { "appservicePageAccess": ["pulCreate", "pulModify", "pulView"], "pageName": "OPERATION_UNITLINKED.json" },
      { "appservicePageAccess": ["pAnnuityCreate", "pAnnuityModify", "pAnnuityView"], "pageName": "OPERATION_ANNUITY.json" },
      { "appservicePageAccess": ["pRateCreate", "pRateModify", "pRateView"], "pageName": "PRODUCT_RATES.json" },
      { "appservicePageAccess": ["pAgencyCreate", "pAgencyModify", "pAgencyView"], "pageName": "AGENCY.json" },
      { "appservicePageAccess": ["pGroupCreate", "pGroupModify", "pGroupView"], "pageName": "OPERATION_GROUP.json" },
      { "appservicePageAccess": ["ppsCreate", "ppsModify", "ppsView"], "pageName": "POLICYSERVICING.json" },
      { "appservicePageAccess": ["pFinanceCreate", "pFinanceModify", "pFinanceView"], "pageName": "PRODUCT_FINANCE.json" },
      { "appservicePageAccess": ["pChargesCreate", "pChargesModify", "pChargesView"], "pageName": "PRODUCT_CHARGES.json" });

  }
  displayPage() {
    //this.isOverlay = true;
    setTimeout(() => {
      this.spinner.show();

    }, 100)
    var result = false;
    this.callList();
    this.spinnerCount = 0;
    this.productTmpFolderList = [];
    this.productPublishFolderList = [];

    this.productPush = [];
    this.appServices.productPushCurrentPage = [];
    this.http.get('/api/getProductTmpJsonFiles')
      .subscribe(res => {

        var fname = res.json();

        if (fname.length > 0) {


          this.productTmpFolderList = fname;
          var statusarray = [];
          for (var k = 0; k < this.productTmpFolderList.length; k++) {

            var review = false;
            if (this.productTmpFolderList[k].PageList.length > 0) {
              for (var h = 0; h < this.productTmpFolderList[k].PageList.length; h++) {

                statusarray[h] = this.productTmpFolderList[k].PageList[h].review;

              }
              var n = statusarray.indexOf(false);
              var m = statusarray.indexOf("Rejected");
              if (m < 0 && n < 0 && statusarray.length > 0) {
                review = true;
              } else {
                review = false;
              }
            } else {
              review = false;
            }

            this.productPush.push({ "ProductId": this.productTmpFolderList[k].ProductId, "ProductType": this.productTmpFolderList[k].ProductType, "TemplateId": this.productTmpFolderList[k].TemplateId, "ProductName": this.productTmpFolderList[k].ProductName, "CreatedBy": this.productTmpFolderList[k].CreatedBy, "CreatedDate": this.productTmpFolderList[k].CreatedDate, "ModifiedDate": this.productTmpFolderList[k].ModifiedDate, "Publish": this.productTmpFolderList[k].Publish, "PageList": this.productTmpFolderList[k].PageList, "Reviews": [], "review": review, "PlanName": this.productTmpFolderList[k].PlanName, "PlanDesc": this.productTmpFolderList[k].PlanDesc, "Updated": false });
            this.appServices.productPushCurrentPage.push({ "ProductId": this.productTmpFolderList[k].ProductId, "ProductType": this.productTmpFolderList[k].ProductType, "TemplateId": this.productTmpFolderList[k].TemplateId, "ProductName": this.productTmpFolderList[k].ProductName, "CreatedBy": this.productTmpFolderList[k].CreatedBy, "CreatedDate": this.productTmpFolderList[k].CreatedDate, "ModifiedDate": this.productTmpFolderList[k].ModifiedDate, "Publish": this.productTmpFolderList[k].Publish, "PageList": this.productTmpFolderList[k].PageList, "Reviews": [], "review": review, "PlanName": this.productTmpFolderList[k].PlanName, "PlanDesc": this.productTmpFolderList[k].PlanDesc, "Updated": false });
            if (this.productTmpFolderList.length - 1 == k) {

              //this.isOverlay = false;
              ++this.spinnerCount;
              this.spinnerHidden();
            }

          }
        } else {
          ++this.spinnerCount;
          this.spinnerHidden();

        }
      }, err => {
        ++this.spinnerCount;
        this.spinnerHidden();


      })


    // Get Publish Folders
    this.http.get('/api/getProductPublishJsonFiles')
      .subscribe(resPublish => {
        var fname = resPublish.json();
        if (fname.length > 0) {
          var updatearray = [];
          this.productPublishFolderList = fname;
          // for (var j = 0; j < fname.length; j++) {

          //   var update = false;
          //   if (fname[j].PageList.length > 0) {
          //     for (var h = 0; h < fname[j].PageList.length; h++) {
          //        if( fname[j].PageList[h].Updated)
          //        {

          //       updatearray[h] = fname[j].PageList[h].review;
          //        }

          //     }
          //     var n = updatearray.indexOf(false);
          //     var m = updatearray.indexOf("Rejected");
          //     if (m < 0 && n < 0 && updatearray.length > 0) {
          //       update = true;
          //     } else {
          //       update = false;
          //     }
          //   } else {
          //     update = false;
          //   }
          for (var j = 0; j < fname.length; j++) {

            var update = false;
            var count = 0;
            if (fname[j].PageList.length > 0) {
              for (var h = 0; h < fname[j].PageList.length; h++) {
                if (fname[j].PageList[h].Updated && fname[j].PageList[h].review) {

                  ++count;
                }

              }
              if (count > 0) {
                update = true;
              }
            }

            console.log(1);
            //this.productPublishFolderList[j].PageList[0].Updated
            this.productPush.push({ "ProductId": this.productPublishFolderList[j].ProductId, "ProductType": this.productPublishFolderList[j].ProductType, "TemplateId": this.productPublishFolderList[j].TemplateId, "ProductName": this.productPublishFolderList[j].ProductName, "CreatedBy": this.productPublishFolderList[j].CreatedBy, "CreatedDate": this.productPublishFolderList[j].CreatedDate, "ModifiedDate": this.productPublishFolderList[j].ModifiedDate, "Publish": true, "PageList": this.productPublishFolderList[j].PageList, "PlanName": this.productPublishFolderList[j].PlanName, "PlanDesc": this.productPublishFolderList[j].PlanDesc, "Updated": update });
            this.appServices.productPushCurrentPage.push({ "ProductId": this.productPublishFolderList[j].ProductId, "ProductType": this.productPublishFolderList[j].ProductType, "TemplateId": this.productPublishFolderList[j].TemplateId, "ProductName": this.productPublishFolderList[j].ProductName, "CreatedBy": this.productPublishFolderList[j].CreatedBy, "CreatedDate": this.productPublishFolderList[j].CreatedDate, "ModifiedDate": this.productPublishFolderList[j].ModifiedDate, "Publish": true, "PageList": this.productPublishFolderList[j].PageList, "PlanName": this.productPublishFolderList[j].PlanName, "PlanDesc": this.productPublishFolderList[j].PlanDesc, "Updated": update });
            if (this.productPublishFolderList.length - 1 == j) {

              ++this.spinnerCount;
              this.spinnerHidden();
              //this.isOverlay = false;
            }
          }
        } else {
          ++this.spinnerCount;
          this.spinnerHidden();


        }
      }, err => {
        ++this.spinnerCount;
        this.spinnerHidden();


      })


    //return result;


    // this.masterPush = [];
    // this.http.get('/api/getTempMaster')
    //   .subscribe(res => {
    //     console.log(res.json());
    //     console.log(1);

    //     this.busy = this.http.get('/api/getTempMaster').toPromise();

    //     var master = Object.keys(res.json());
    //     for (var i = 0; i < master.length; i++) {

    //       var output = JSON.parse(res.json()[master[i]])[0];



    //       this.masterPushList.push({ "MasterId": output.MasterId, "MasterName": output.MasterName, "dataEntryModal": output.dataEntryModal, "CreatedBy": output.CreatedBy, "CreatedDate": output.CreatedDate, "ModifiedDate": output.ModifiedDate });


    //     }

    //     for (var l = 0; l < this.masterPushList.length; l++) {


    //       console.log(1);
    //       this.masterPush.push({ "MasterId": this.masterPushList[l].MasterId, "MasterName": this.masterPushList[l].MasterName, "dataEntryModal": this.masterPushList[l].dataEntryModal, "CreatedBy": this.masterPushList[l].CreatedBy, "CreatedDate": this.masterPushList[l].CreatedDate, "ModifiedDate": this.masterPushList[l].ModifiedDate });


    //     }

    //   })




    // setTimeout(() => {
    //   this.isOverlay = false;
    // }, 1500)






  }

  spinnerHidden() {
    if (this.spinnerCount > 1 && this.loaded) {
      setTimeout(() => {
        this.spinner.hide();

      }, 100)
    }
  }

  delete(eve, ppush, publish) {
    //this.isOverlay = true;

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        if (!publish) {
          this.http.get('http://localhost:4777/api/getProductTmpFolders')
            .subscribe(res => {

              var folder = Object.keys(res.json());
              if (folder.length > 0) {
                for (var i = 0; i < folder.length; i++) {
                  if (ppush.ProductId == folder[i]) {
                    this.http.post('/api/delProductTmpFolder', { "foldername": ppush.ProductId }, { headers: headers })
                      .subscribe((res) => {
                        swal(
                          'Deleted!',
                          'Your file has been deleted.',
                          'success'
                        )
                        localStorage.setItem("selectedProduct", "");
                        this.displayPage();
                      })
                  }
                }
                this.appServices.loadingDone = false;
              }

            }, err => {
              alert("DB Exception");
              console.log("err " + err);
            })
        } else if (publish) {
          // Updated by Pisith 02/12/2019
          // this.http.get('assets/lov/' + fileName)
          // How can you call this post , cuz we don't have this api in node server
          this.http.delete(this.ws_url + 'ProductBenefits/' + ppush.ProductId, { headers: headers })
            .subscribe(res => {

              localStorage.setItem("alertContent", "Deleted");

              this.dialogService.addDialog(AlertComponent, {
                title: 'Name dialog',
                question1: 'Block Name ',
                question2: 'Block Type '

              }).subscribe((isConfirmed) => {



                this.http.get('/api/getProductPublishFolders')
                  .subscribe(res => {
                    var folder = Object.keys(res.json());
                    if (folder.length > 0) {
                      for (var j = 0; j < folder.length; j++) {
                        if (ppush.ProductId == folder[j]) {
                          this.http.post('/api/delProductPublishFolder', { "foldername": ppush.ProductId }, { headers: headers })
                            .subscribe((res) => {
                              swal(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                              )
                              localStorage.setItem("selectedProduct", "");
                              this.displayPage();


                            })
                        }
                      }
                      this.appServices.loadingDone = false;
                    }
                  })
              });


            }, err => {
              alert("DB Exception");
              console.log("err " + err);
            })

        }
      }
    })
  }

  progBar() {
    this.progressBar = this.appServices.progressBar;
    return this.progressBar;
  }

  cloneProduct() {

    this.router.navigate(['./productConfig/productclone']);
  }

  dataEntryCompletion(productId, publishStatus) {
    this.router.navigate(['./productConfig/dataEntryCompletion'], { queryParams: { productId: productId, publishStatus: publishStatus } });
  }

  navLookup() {
    this.router.navigate(['./productConfig/lookup']);

  }
  loadDataMonitor(productName) {
    this.router.navigate(['./productConfig/data-monitor']);
    localStorage.setItem('downloadProduct', productName);
  }
  public pieChartLabels: string[] = ['Completed', 'Pending'];
  public pieChartData: number[] = [300, 500];
  public pieChartType: string = 'pie';

  chartPie(productId, publishStatus, product) {
    this.productId = productId;
    this.planName = product.PlanName;
    this.pieChart = true;

    this.spinner.show();
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('/api/dataEntryMonitorStatus', { "folderId": productId, "publishStatus": JSON.parse(publishStatus) }, { headers: headers })
      .subscribe(res => {
        if (res.json()[0].DataMonitor.length > 0) {
          // this.totalStatistic = true;
          // this.blockStatistic = false;
          //DataMonitor
          var b = JSON.stringify(res.json());
          var c = JSON.parse(b);
          for (var i = 0; i < c[0].DataMonitor.length; i++) {
            c[0].DataMonitor[i].data.PageName = c[0].DataMonitor[i].data.PageName.replace("_", " ")

          }
          this.appServices.dataMonitor = c[0].DataMonitor;
          //PageList
          for (var j = 0; j < c[0].PageList.length; j++) {
            c[0].PageList[j].name = c[0].PageList[j].name.replace("_", " ")
          }
          //Reviews
          for (var k = 0; k < c[0].Reviews.length; k++) {
            c[0].Reviews[k].PageName = c[0].Reviews[k].PageName.replace("_", " ")
          }

          this.appServices.totalPageList = [];
          this.appServices.totalPageList.push({ "pname": "AGENCY", "count": 0, "reviews": [] }, { "pname": "NEW BUSINESS", "count": 0, "reviews": [] }, { "pname": "OPERATION ANNUITY", "count": 0, "reviews": [] }, { "pname": "OPERATION GROUP", "count": 0, "reviews": [] }, { "pname": "OPERATION UNDERWRITING", "count": 0, "reviews": [] }, { "pname": "OPERATION UNITLINKED", "count": 0, "reviews": [] }, { "pname": "POLICYSERVICING", "count": 0, "reviews": [] }, { "pname": "PROD BENEFITS", "count": 0, "reviews": [] }, { "pname": "PROD INFO", "count": 0, "reviews": [] }, { "pname": "PRODUCT CHARGES", "count": 0, "reviews": [] }, { "pname": "PRODUCT FINANCE", "count": 0, "reviews": [] }, { "pname": "OPERATION RATES", "count": 0, "reviews": [] });
          for (var n = 0; n < c[0].Reviews.length; n++) {

            for (var p = 0; p < this.appServices.totalPageList.length; p++) {
              if (c[0].Reviews[n].PageName == this.appServices.totalPageList[p].pname) {
                this.appServices.totalPageList[p].count++;
                this.appServices.totalPageList[p].reviews.push(c[0].Reviews[n].Review)


              }
            }
          }

          this.pageData = c;

          var validfields = 0;
          for (var i = 0; i < c[0].DataMonitor.length; i++) {

            this.TotalFieldCount += c[0].DataMonitor[i].data.TotalFieldCount;
            this.ValidFieldCount += c[0].DataMonitor[i].data.ValidFieldCount;
            this.InValidFieldCount += this.TotalFieldCount - this.ValidFieldCount;
            var compPerc = 0;
            compPerc = (this.ValidFieldCount / this.TotalFieldCount) * 100;
            this.CompletionPerc = Math.round(compPerc);

            //    this.pieChartLabels=['Total FieldCount', 'Valid FieldCount'];
            //this.pieChartData=[ this.TotalFieldCount, this.ValidFieldCount];
            this.pieChartLabels = ['Completed', 'Pending'];
            this.pieChartData = [this.ValidFieldCount, this.InValidFieldCount];
            this.pieChartType = 'pie';


            if (i == c[0].DataMonitor.length - 1) {
              jQuery('#modalLookUp').modal('show');
            }
          }
          this.spinner.hide();

        } else {
          this.spinner.hide();
          swal({
            type: 'info',
            title: 'Status',
            text: 'No pages saved'
          }).then(name => {
            // this.router.navigate(['/productConfig/dashboarddata']);

          })
        }
      });
  }
  showDataDetails() {
    jQuery('#modalLookUp').modal('hide');
    this.appServices.pageData = this.pageData;
    this.router.navigate(['./productConfig/dataEntryCompletion']);
  }
  fnCallWSAfterCrud() {
    if (!this.appServices.loadingDone) {
      //this.loaded = false;
      this.appServices.callWS();// Commented by Bhagavathy 06/08/2019  CallWS
      this.appServices.progressBar.subscribe((value) => {
        var tempprogressBar = value;
        if (tempprogressBar > 80) {
          this.spinner.hide();
          this.loaded = true;
        }
      });
      // this.spinner.hide();  // Added by Bhagavathy 06/08/2019  CallWS
    }
  }
  updateStatus() {
    var pid = localStorage.getItem('selectedProduct');
    var pageInfo = [];
    pageInfo.push({ 'ProductId': pid, 'ProductName': pid })
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    this.http.post('/api/productpublishupdateStatus', JSON.stringify(pageInfo[0]), { headers: headers })
      .subscribe(res => {
        if (res.json().status == 'pass') {
          this.displayPage();
        }
      }, err => {

      })
  }
  displayPageupdate(pid, pname) {
    //this.isOverlay = true;
    setTimeout(() => {
      this.spinner.show();

    }, 100)
    var result = false;
    this.callList();
    this.spinnerCount = 0;
    this.productTmpFolderList = [];
    this.productPublishFolderList = [];
    this.productPush = [];


    //   var pageInfo=[];
    // pageInfo.push({'ProductId':pid,'ProductName':pname})
    // var headers = new Headers();
    // headers.append("Content-Type", "application/json");
    // headers.append('responseType', 'arraybuffer');
    //  this.http.post('/api/productpublishupdateStatus', JSON.stringify(pageInfo[0]), { headers: headers })
    //  .subscribe(res => {
    // if(res.json().status == 'pass')
    //    {
    //   }
    // }, err => {

    // })
    this.http.get('/api/getProductTmpJsonFiles')
      .subscribe(res => {

        var fname = res.json();

        if (fname.length > 0) {


          this.productTmpFolderList = fname;
          var statusarray = [];
          for (var k = 0; k < this.productTmpFolderList.length; k++) {

            var review = false;
            if (this.productTmpFolderList[k].PageList.length > 0) {
              for (var h = 0; h < this.productTmpFolderList[k].PageList.length; h++) {

                statusarray[h] = this.productTmpFolderList[k].PageList[h].review;

              }
              var n = statusarray.indexOf(false);
              var m = statusarray.indexOf("Rejected");
              if (m < 0 && n < 0 && statusarray.length > 0) {
                review = true;
              } else {
                review = false;
              }
            } else {
              review = false;
            }

            this.productPush.push({ "ProductId": this.productTmpFolderList[k].ProductId, "ProductType": this.productTmpFolderList[k].ProductType, "TemplateId": this.productTmpFolderList[k].TemplateId, "ProductName": this.productTmpFolderList[k].ProductName, "CreatedBy": this.productTmpFolderList[k].CreatedBy, "CreatedDate": this.productTmpFolderList[k].CreatedDate, "ModifiedDate": this.productTmpFolderList[k].ModifiedDate, "Publish": this.productTmpFolderList[k].Publish, "PageList": this.productTmpFolderList[k].PageList, "Reviews": [], "review": review, "PlanName": this.productTmpFolderList[k].PlanName, "PlanDesc": this.productTmpFolderList[k].PlanDesc, "Updated": false });
            this.appServices.productPushCurrentPage.push({ "ProductId": this.productTmpFolderList[k].ProductId, "ProductType": this.productTmpFolderList[k].ProductType, "TemplateId": this.productTmpFolderList[k].TemplateId, "ProductName": this.productTmpFolderList[k].ProductName, "CreatedBy": this.productTmpFolderList[k].CreatedBy, "CreatedDate": this.productTmpFolderList[k].CreatedDate, "ModifiedDate": this.productTmpFolderList[k].ModifiedDate, "Publish": this.productTmpFolderList[k].Publish, "PageList": this.productTmpFolderList[k].PageList, "Reviews": [], "review": review, "PlanName": this.productTmpFolderList[k].PlanName, "PlanDesc": this.productTmpFolderList[k].PlanDesc, "Updated": false });
            if (this.productTmpFolderList.length - 1 == k) {


              ++this.spinnerCount;
              this.spinnerHidden();
            }

          }
        }
        else {
          ++this.spinnerCount;
          this.spinnerHidden();

        }
      }, err => {
        ++this.spinnerCount;
        this.spinnerHidden();


      })
    //       var pageInfo=[];
    //       pageInfo.push({'ProductId':pid,'ProductName':pname})
    //       var headers = new Headers();
    //       headers.append("Content-Type", "application/json");
    // this.http.post('/api/productpublishupdateStatus', JSON.stringify(pageInfo[0]), { headers: headers })
    //        .subscribe(resstatus => {
    //       if(resstatus.json().status == 'pass')
    //          {
    this.http.get('/api/getProductPublishJsonFiles')
      .subscribe(resPublish => {
        var fname = resPublish.json();
        if (fname.length > 0) {

          this.productPublishFolderList = fname;
          for (var j = 0; j < this.productPublishFolderList.length; j++) {
            var updatedstatus = 'false';

            console.log(1);

            this.productPush.push({ "ProductId": this.productPublishFolderList[j].ProductId, "ProductType": this.productPublishFolderList[j].ProductType, "TemplateId": this.productPublishFolderList[j].TemplateId, "ProductName": this.productPublishFolderList[j].ProductName, "CreatedBy": this.productPublishFolderList[j].CreatedBy, "CreatedDate": this.productPublishFolderList[j].CreatedDate, "ModifiedDate": this.productPublishFolderList[j].ModifiedDate, "Publish": true, "PageList": this.productPublishFolderList[j].PageList, "PlanName": this.productPublishFolderList[j].PlanName, "PlanDesc": this.productPublishFolderList[j].PlanDesc, "Updated": this.productPublishFolderList[j].PageList[0].Updated });
            this.appServices.productPushCurrentPage.push({ "ProductId": this.productPublishFolderList[j].ProductId, "ProductType": this.productPublishFolderList[j].ProductType, "TemplateId": this.productPublishFolderList[j].TemplateId, "ProductName": this.productPublishFolderList[j].ProductName, "CreatedBy": this.productPublishFolderList[j].CreatedBy, "CreatedDate": this.productPublishFolderList[j].CreatedDate, "ModifiedDate": this.productPublishFolderList[j].ModifiedDate, "Publish": true, "PageList": this.productPublishFolderList[j].PageList, "PlanName": this.productPublishFolderList[j].PlanName, "PlanDesc": this.productPublishFolderList[j].PlanDesc, "Updated": this.productPublishFolderList[j].PageList[0].Updated });
            if (this.productPublishFolderList.length - 1 == j) {

              ++this.spinnerCount;
              this.spinnerHidden();

            }
          }
        } else {
          ++this.spinnerCount;
          this.spinnerHidden();


        }
      }, err => {
        ++this.spinnerCount;
        this.spinnerHidden();


      })
  }
  // }, err => {

  // })
  //  }






}
