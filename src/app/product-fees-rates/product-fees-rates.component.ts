import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef,AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService } from "ng2-bootstrap-modal";
import { DragulaService } from 'ng2-dragula';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AppServices } from '../app.service';
import { AlertComponent } from "../bootstrap-modal/alert/alert.component";
import { PrompComponent } from "../bootstrap-modal/Modaldialog/promp.component";
import { FielddialogueComponent } from "../bootstrap-modal/Fielddialogue/fielddialogue.component";
import { EditPenComponent } from "../bootstrap-modal/edit-pen/edit-pen.component";
import {NgForm} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataDownloadComponent } from '../bootstrap-modal/dataDownload/dataDownload.component';
import { AddRowComponent } from "../bootstrap-modal/add-row/add-row.component";
import { RetriveDelComponent } from "../bootstrap-modal/retrive-del/retrive-del.component";
  import { default as data_json } from '../../assets/data/product-fees-rates.json';

import { DataSearchComponent } from '../bootstrap-modal/dataSearch/dataSearch.component';
import {tabAccprodFeesRatesFormat} from '../tabAccordformat';
import swal from 'sweetalert2';

@Component({
  selector: 'product-fees-rates',
  templateUrl: './product-fees-rates.component.html',
  styleUrls: ['./product-fees-rates.component.css'],
  providers: [DragulaService]

})
export class ProductFeesRatesComponent implements OnInit,AfterContentInit {
  backLinkBool: boolean =  false;
  display_data: any = [];
  parentName: any;
  backlinkField:any;
  navId:any;
  templateview: boolean = false;
  templateApproval: boolean = false;
  productApproval: boolean = false;
  templateSaved: boolean = false;
  productSaved: boolean = false;
  draggedItem:any=[];
  showAddBtn:boolean=false;
  showRow:boolean=false;
  count = 1;
  pageMode:any=0;
  frCreate: boolean = false;
  frModify: boolean = false;
  frView: boolean = false;
  pfrCreate: boolean = false;
  pfrModify: boolean = false;
  pfrView: boolean = false;
  alertMsg:any=[];
  snumber:any=[{"sna":0},{"snb":0},{"snc":0},{"snd":0}];
  blockName: any;
  accName = [];
  productUpdated: boolean = false;
  pbdUpdate: boolean = false;
  expand:any;
  dropIndex:any;
  currenttemp:any;
  dragField:any;
  constructor(private spinner: NgxSpinnerService,private router:Router, private http: Http,private dragulaService: DragulaService, private appService: AppServices,private dialogService:DialogService) { 
    dragulaService.setOptions('second-bag', {
      copy: (el, source) => {
        // alert(source.id)
        this.dragField=source.id;
        //return source.id === 'ok';
      },
      accepts: (el, target, source, sibling) => {
        // To avoid dragging from right to left container
        // alert(target.id)
        if(target.id === this.dragField){
          return true;
        }else{
          return false;
        }
        
      }
    });
    
    this.dragulaService.drag.subscribe((value: any) => {
      console.log("drag");

      this.onDrag(value.slice(1));

    });

    this.dragulaService.drop.subscribe((value: any) => {
      this.onDrop(value.slice(1));
      console.log("drop");
    });
    this.dragulaService.over.subscribe((value: any) => {
      this.onOver(value.slice(1));
      console.log("over");
    });
    this.dragulaService.out.subscribe((value: any) => {
      this.onOut(value.slice(1));
      console.log("out");
    });


  }




  ngOnInit() {
    this.expand=0;
    this.currenttemp=this.appService.currentTemplate;
    this.appService.pageName='PRODUCT_RATES';
    this.appService.snArr = this.snumber;

    if (localStorage.getItem("pageMode") == "1") {
      this.pageMode=1;
      if(this.appService.product_rate.length>0){
        this.display_data=this.appService.product_rate;
        this.blockName =this.appService.getAllBlockName(this.appService.product_rate);
      }else{
             
        this.appService.product_rate = data_json;
        this.display_data = this.appService.product_rate;
        this.blockName =this.appService.getAllBlockName(this.appService.product_rate);
    // this.http.get('assets/data/product-fees-rates.json')
    //   .subscribe((res) => {
        
    //     this.appService.product_rate = res.json();
    //     this.display_data = this.appService.product_rate;
    //     this.blockName =this.appService.getAllBlockName(this.appService.product_rate);

    //   }, err => {
    //     console.log(err);
    //   })
    }

    if(this.appService.templateApproval){
      var approve = [];
        approve.push({
        "TemplateId": this.appService.templateId,
        "TemplateName": this.appService.templateName,
        "PageName": "PRODUCT_RATES" 
        })

        
        var headers = new Headers();
        headers.append("Content-Type", "application/json");
        this.http.post('/api/readTempApproveStatus', JSON.stringify(approve[0]), { headers: headers })
          .subscribe(res => {  
            if (res.json().review == false) {
               
              this.appService.templateSavedStaus=true;
              this.templateSaved=true;
            }else{
              this.appService.templateSavedStaus=false;
              this.templateSaved=false;
            }
    
          }, err => {
            console.log("err " + err);
          })
        }
    }else if (localStorage.getItem("pageMode") == "2") {
      this.pageMode=2;
      if(this.appService.product_rate.length>0){
        
        this.appService.lovWS(this.appService.product_rate);
        this.display_data=this.appService.product_rate;

        if(this.appService.productApproval){
          var approve = [];
          approve.push({
            "ProductId": this.appService.productId,
            "ProductName": this.appService.productName,
            "PageName": "PRODUCT_RATES"
          })

          var headers = new Headers();
          headers.append("Content-Type", "application/json");
          this.http.post('/api/readProdApproveStatus', JSON.stringify(approve[0]), { headers: headers })
            .subscribe(res => {  
              if (res.json().review == false) {
                this.productSaved = true;
              }else{
                this.productSaved = false;

              }
            }, err => {
              console.log("err " + err);
            })
        }
         }else{
        
          localStorage.setItem("alertContent", "Template Not Created");

          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '

          }).subscribe((isConfirmed) => {
            this.router.navigate(['/productConfig/dashboarddata']);
            
          });
         }
    }
  }

  ngAfterContentInit(){
    if (localStorage.getItem("pageMode") == "1") {
      if (document.getElementById("sValues")) {
        document.getElementById("sValues").style.display = "none";
      }
      setTimeout(()=>{
        this.showAddBtn = true;
      this.showRow = false;
      this.frCreate = this.appService.RateCreate;
        this.frModify = this.appService.RateModify;
        this.frView = this.appService.RateView;
      this.templateApproval = this.appService.templateApproval;
      if (this.appService.publishedTemplate) {
        this.frCreate = false;
          this.frModify = false;
        this.templateApproval = false;
      }
      },500)   
      
      
    } else if (localStorage.getItem("pageMode") == "2") {
      if (document.getElementById("sFormat")) {
        document.getElementById("sFormat").style.display = "none";
      }

      setTimeout(()=>{
      this.showRow = true;
      this.showAddBtn = false;

      this.pfrCreate = this.appService.pRateCreate;
        this.pfrModify = this.appService.pRateModify;
        this.pfrView = this.appService.pRateView;
      this.productApproval = this.appService.productApproval;
      if (this.appService.publishedProduct) {
        this.pfrCreate = false;
          this.pfrModify = false;
        this.productApproval = false;
        this.pbdUpdate=true;
      }

      this.dragulaService.find("second-bag").drake.destroy();
     // var output = this.appService.validPage(this.appService.product_rate);

    },500);
  }
}

nav(i,nfield,id,tD) {
  this.expand=i;
  if(tD.sna != undefined){
    this.snumber=[{"sna":0},{"snb":0},{"snc":0},{"snd":0}];
    this.snumber[0].sna = tD.sna;
  }else if(tD.snb != undefined){
    this.snumber[1].snb = tD.snb;
    
  }else if(tD.snc != undefined){
    this.snumber[2].snc = tD.snc;
  }else if(tD.snd != undefined){
    this.snumber[3].snd = tD.snd;
  }
  this.appService.snArr = this.snumber;
                   this.appService.selectedNav=id;

    this.display_data=this.appService.changeField(this.appService.product_rate,nfield,"nav","",this.appService.subPagesNavRate);
    
    for(var d=0;d<this.appService.subPagesNavRate.length;d++){
      if(this.appService.subPagesNavRate[d].isActive){

        this.navId=this.appService.subPagesNavRate[d].id;
        
      }
    }
    this.parentName = nfield.parentName;
    this.backLinkBool =true;
    this.backlinkField=nfield;
    if (localStorage.getItem("pageMode") == "1") {
      this.accName=[];
      for (var a = 0; a < this.display_data.length; a++) {
        if(this.display_data[a].deleteAcc != undefined){
          this.accName.push({ "id": this.display_data[a].accordId, "itemName": this.display_data[a].accordionName });
        }
      }
    }
  }
 
  backLink() { 

    var mpageNavigation=false;
    for(var b=0;b<this.appService.subPagesNavRate.length;b++){
        if(this.appService.subPagesNavRate[b].id==this.navId){ 
          mpageNavigation=true;
          this.appService.subPagesNavRate[b].isActive=false;
          this.display_data = this.appService.changeField(this.appService.product_rate,{"id":this.navId},"back","","");
          
          this.parentName=this.appService.subPagesNavRate[b].grantparentName;
          this.navId=this.appService.subPagesNavRate[b].parentId;
          if(this.navId==""){
            this.backLinkBool=false;
          }
        }
    }
      if(!mpageNavigation){
        this.display_data = this.appService.changeField(this.appService.product_rate,{"id":this.backlinkField},"back","","");
        

      }
    
      if (localStorage.getItem("pageMode") == "1") {
        this.accName=[];
        for (var a = 0; a < this.display_data.length; a++) {
          if(this.display_data[a].deleteAcc != undefined){
            this.accName.push({ "id": this.display_data[a].accordId, "itemName": this.display_data[a].accordionName });
          }
        }
      }
   
  }

  delfield(dfield) {
    this.display_data=this.appService.changeField(this.appService.product_rate,dfield,"del","","");
    }

    incSize(incfield,eve) {
      this.display_data=this.appService.changeField(this.appService.product_rate,incfield,"inc",eve.target.parentElement.parentElement.parentElement.style.width,"");
      
    }

    decSize(decfield,eve){
      this.display_data=this.appService.changeField(this.appService.product_rate,decfield,"dec",eve.target.parentElement.parentElement.parentElement.style.width,"");
      
    }

    editpenAccodion(eve) {

      eve.target.previousElementSibling.firstElementChild.removeAttribute("readonly");
      eve.target.previousElementSibling.firstElementChild.focus();
      eve.target.previousElementSibling.firstElementChild.style.backgroundColor = "#9ACD32" ;
    }

    disableEditAccordion(data,eve) {
      this.display_data=this.appService.changeAcc(this.appService.product_rate,data,eve.target.value,"","editAcc");
      eve.target.setAttribute("readonly", true);
      eve.target.style.backgroundColor = "#FFFFFF";
    }

    delAccordion(data) {
      if (data.accordId != "bd") {
        this.display_data=this.appService.changeAcc(this.appService.product_rate,data,"","","delAcc");
        this.accName.push({ "id": data.accordId, "itemName": data.accordionName });
      } else {
        localStorage.setItem("alertContent", "You can't delete this Block");
  
        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '
  
        }).subscribe((isConfirmed) => {
        });
  
      }
    }

    editField(efield) {
      localStorage.setItem("editName", efield.fieldName);
      localStorage.setItem("editId", efield.id);
      localStorage.setItem("editBlockId", efield.blockId);
      localStorage.setItem("mandatory", efield.mandatory);
      localStorage.setItem("mandatoryMsg", efield.mandatoryMsg);
      localStorage.setItem("editType", efield.tagName);
      localStorage.setItem("editDisplay", efield.display);
      localStorage.setItem("editPageName", "PRODUCT_RATES");
      var xx = [];
      if(efield.mappingId){
        var fieldDetailList = [];
        for (var j = 0; j < efield.mappingId.length; j++) {
         var fieldDetail = this.appService.changeField(this.appService.product_rate, efield.mappingId[j], "mappedFieldDetails", "", "");
         fieldDetailList.push(fieldDetail);
        }
        localStorage.setItem("editMapping", JSON.stringify(fieldDetailList));
        localStorage.setItem("editBlockNameList", JSON.stringify(xx));
      }else{
        localStorage.setItem("editBlockNameList", JSON.stringify(this.blockName));
        localStorage.setItem("editMapping", JSON.stringify(xx));
      }
      if(efield.maxValue != ''){
        localStorage.setItem("editMaxValue", efield.maxValue);
      }else{
        localStorage.setItem("editMaxValue", "");
      }
  
      if(efield.minValue != ''){
        localStorage.setItem("editMinValue", efield.minValue);
      }else{
        localStorage.setItem("editMinValue", "");
      }
      if(efield.read){
        localStorage.setItem("editReadOnly", efield.read);
      }else{
        localStorage.setItem("editReadOnly", "false");
  
      }
      if(efield.tagValue != ''){
        localStorage.setItem("editDefaultValue", efield.tagValue);
      }else{
        localStorage.setItem("editDefaultValue", "");
      }
      var f = [];
      if (efield.tagName == 'select-one') {
  
  
        for (var i = 0; i < efield.optionObj.length; i++) {
          if (efield.optionObj[i].optionValue != "") {
            f.push({ "optionValue": efield.optionObj[i].optionValue, "optionLabel": efield.optionObj[i].optionLabel })
          }
        }
        localStorage.setItem("editSelectedItem", JSON.stringify(f));
  
      }
  
  
      this.dialogService.addDialog(EditPenComponent, {
        title: 'Name dialog',
        fquestion1: 'Item Name ',
        fquestion2: 'Item Type '
  
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {

          var edited = JSON.parse(localStorage.getItem("output"));
          this.display_data=this.appService.changeField(this.appService.product_rate,edited[0],"editField","","");

       
        } else {
  
        }
      })
  
    }

    editTableField(tData) {
      localStorage.setItem("editName", tData.theadName);
      localStorage.setItem("editId", tData.id);
      localStorage.setItem("editBlockId", tData.blockId);
      localStorage.setItem("mandatory", tData.mandatory);
      localStorage.setItem("mandatoryMsg", tData.mandatoryMsg);
      localStorage.setItem("editDisplay", tData.display);
      localStorage.setItem("editPageName", "PRODUCT_RATES");
      var xx = [];
      if(tData.mappingId){
        var fieldDetailList = [];
        for (var j = 0; j < tData.mappingId.length; j++) {
         var fieldDetail = this.appService.changeField(this.appService.product_rate, tData.mappingId[j], "mappedFieldDetails", "", "");
         fieldDetailList.push(fieldDetail);
        }
        localStorage.setItem("editMapping", JSON.stringify(fieldDetailList));
        localStorage.setItem("editBlockNameList", JSON.stringify(xx));
      }else{
        localStorage.setItem("editBlockNameList", JSON.stringify(this.blockName));
        localStorage.setItem("editMapping", JSON.stringify(xx));
      }
      if(tData.maxValue != ''){
        localStorage.setItem("editMaxValue", tData.maxValue);
      }else{
        localStorage.setItem("editMaxValue", "");
      }
  
      if(tData.minValue != ''){
        localStorage.setItem("editMinValue", tData.minValue);
      }else{
        localStorage.setItem("editMinValue", "");
      }
      if(tData.read){
        localStorage.setItem("editReadOnly", tData.read);
      }else{
        localStorage.setItem("editReadOnly", "false");
  
      }
      if(tData.tdObj){
        if(tData.tdObj[0].tdValue != ''){
          localStorage.setItem("editDefaultValue", tData.tdObj[0].tdValue);
      }else{
        localStorage.setItem("editDefaultValue", "");
      }
    }
    var f = [];
  localStorage.setItem("editSelectedItem", JSON.stringify(f));
      if(tData.optionObj){
        localStorage.setItem("editType", 'select-one');
        for (var i = 0; i < tData.optionObj.length; i++) {
          
            f.push({ "optionValue": tData.optionObj[i].optionValue, "optionLabel": tData.optionObj[i].optionLabel })
          
          localStorage.setItem("editSelectedItem", JSON.stringify(f));

        }

      }else{
        localStorage.setItem("editType", tData.tdObj[0].tagName);

      }

      this.dialogService.addDialog(EditPenComponent, {
        title: 'Name dialog',
        fquestion1: 'Item Name ',
        fquestion2: 'Item Type '
  
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {


          var edited = JSON.parse(localStorage.getItem("output"));
          this.display_data=this.appService.changeField(this.appService.product_rate,edited[0],"editTableField","","");

       
  
        } else {
  
        }
      })
  
    }


    
  private onDrag(args: any): void {
    let [e, target, parent, moves] = args;
    
    if(e.className!=''){
    this.appService.dragMaintain=e.querySelector(".inputlabel").id;
    }else{
      if(document.getElementsByClassName("panel-collapse collapse in")[0]){
        // document.getElementsByClassName("panel-collapse collapse in")[0].className="panel-collapse collapse";
      }
      
    }
   

   // this.appService.changeField(this.appService.product_rate,{"id":e.querySelector(".inputlabel").id},"drag","","");
    

  }



  private onDrop(args: any): void {
    let [e, target, parent, moves] = args;
    this.expand=this.dropIndex;
       if((e.className=="tc1 gu-transit" && target.className=="moTable") || (e.className=="first-box gu-transit" && target.className=="board")){
   this.display_data= this.appService.changeField(this.appService.product_rate, { "id": this.appService.dragMaintain }, "drag", "", "");
   if( args[3].querySelector!=null){
   this.display_data =this.appService.changeField(this.appService.product_rate, { "id": args[3].querySelector(".inputlabel").id }, "drop", "", "");

   }
   

}


//    if((e.className=="tc1 gu-transit" && target.className=="moTable") || (e.className=="first-box gu-transit" && target.className=="board")){
//    this.appService.product_rate= this.appService.changeField(this.appService.product_rate, { "id": this.appService.dragMaintain }, "drag", "", "");
//    if( args[3].querySelector!=null){
//    this.display_data =this.appService.changeField(this.appService.product_rate, { "id": args[3].querySelector(".inputlabel").id }, "drop", "", "");

//    }
   

// }else{

//   if(e.className=="gu-transit" && target.className=="panel-group"){
//   }else{
//    this.dragulaService.find('second-bag').drake.cancel(true);

  

//   }

// }
  //   let [e, target, parent, moves] = args;

  //   this.appService.product_rate=this.appService.changeField(this.appService.product_rate,{"id":args[3].querySelector(".inputlabel").id},"drop","","");
  //   this.display_data=[];
    
  //   setTimeout(res=>{
  //    this.display_data=this.appService.product_rate;
  //    this.expand=this.dropIndex;
   
  //  },200);

  }


  private onOver(args: any): void {

    let [el] = args;
  
  }

  foutc;

  private onOut(args: any): void {
    let [e, target, parent, moves] = args;

//  if((target.previousElementSibling.className=="row" && target.nextElementSibling==null)){
//  this.dragulaService.find('second-bag').drake.cancel(true);
// }
 
  }

  saveFormat(){
    swal({
      title: 'Are you sure?',
      text: "You want to save!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then(async (resultSel) => {
      if (resultSel.value) {
    this.display_data[0].TemplateId=this.appService.templateId;
    this.display_data[0].PageName=this.appService.pageName;
    var data = JSON.stringify(this.display_data);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    await this.http.post('/api/tempPage',data,{headers:headers}).toPromise()


    //comment by darong
    // this.http.post('/api/tempPage', data, { headers: headers })
    //   .subscribe(res => {
    //     setTimeout(() => {
    //       console.log(res.json());
    //     }, 1500);

    //   }, err => {
    //     console.log("err " + err);
    //   })

    var pageName = [];
    pageName.push({
      "TemplateId": this.appService.templateId,
      "TemplateName": this.appService.templateName,
      "PageName": "PRODUCT_RATES"
    });
    this.http.post('/api/tempPageNameInsert', pageName, { headers: headers })
      .subscribe(res => {
        if (res.json().review == false && this.templateApproval) {
          this.templateSaved = true;
          this.appService.templateSavedStaus=true;
        }

      }, err => {
        console.log("err " + err);
      })



    localStorage.setItem("alertContent", "Saved Successfully");

    this.dialogService.addDialog(AlertComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
    });
  }
})
  }

  addBlock() {
         localStorage.setItem("dboard", "0");
    this.dialogService.addDialog(PrompComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.display_data=this.appService.addNewBlock(this.appService.product_rate,"PRODUCT_FEES_RATES","RATE" + this.count++,"RATE" + this.count++);
      } else {

      }
    })
  }

  addField(field) {
    localStorage.setItem("id",field.accordId);
    localStorage.setItem("blockNameList", JSON.stringify(this.blockName));
    localStorage.setItem("pageName", "PRODUCT_RATES");

    this.dialogService.addDialog(FielddialogueComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("output"));
        this.display_data=this.appService.changeAcc(this.appService.product_rate,field,s,"","newField");

      } else {

      }
    })





  }

  addTableField(field) {
    localStorage.setItem("id",field.accordId);
    localStorage.setItem("blockNameList", JSON.stringify(this.blockName));
    localStorage.setItem("pageName", "PRODUCT_RATES");

    this.dialogService.addDialog(FielddialogueComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("output"));
        this.display_data=this.appService.changeAcc(this.appService.product_rate,field,s,"","newTableField");

      } else {

      }
    })





  }

  fieldInsert() {

    var s = JSON.parse(localStorage.getItem("output"));
    for (var i = 0; i < this.display_data.length; i++) {
      if (this.display_data[i].accordId == localStorage.getItem("id")) {
        if (localStorage.getItem("className") == "panel-body") {

          if (s[0].item_type == "text") {
            if (s[0].field_type == "Number") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "number" })

            } else if (s[0].field_type == "String") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "text" })

            } else if (s[0].field_type == "Date") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "date" })

            }

          } else if (s[0].item_type == "select") {
            this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "select-one", "tagvalue": "--Select--", "optionObj": s[0].optionObj })

          } else if (s[0].item_type == "date") {
            this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "date" })

          }
        } else if (localStorage.getItem("className") == "panel-body tle") {
          if (s[0].item_type == "text") {
            if (s[0].field_type == "Number") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "number", "tdValue": "" }] })

            } else if (s[0].field_type == "Date") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "date", "tdValue": "" }] })

            } else if (s[0].field_type == "String") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "text", "tdValue": "" }] })

            }

          } else if (s[0].item_type == "select") {
            this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "optionObj": s[0].optionObj })

          } else if (s[0].item_type == "date") {
            this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "date", "tdValue": "" }] })

          }
        }
      }

    }
  }


  approval() {

    var approve = [];
    approve.push({
      "TemplateId": this.appService.templateId,
      "TemplateName": this.appService.templateName,
      "PageName": "PRODUCT_RATES"
    })


    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    this.http.post('/api/tempApprove', JSON.stringify(approve[0]), { headers: headers })
      .subscribe(res => {
        localStorage.setItem("alertContent", "Approved");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.templateSaved = false;

        });

      }, err => {
        console.log("err " + err);
      })



  }

  approvalProduct() {
    var approve = [];
    approve.push({
      "ProductId": this.appService.productId,
      "ProductName": this.appService.productName,
      "PageName": "PRODUCT_RATES"
    })


    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    this.http.post('/api/prodApprove', JSON.stringify(approve[0]), { headers: headers })
      .subscribe(res => {
        localStorage.setItem("alertContent", "Approved");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.productSaved = false;
        });

      }, err => {
        console.log("err " + err);
      })
  }


  
  genRow(data) {
    // var countR = data.tableObj.length;
    var s;
    for(var i=0;i<Object.keys(data.tableObj[data.tableObj.length-1]).length;i++){
      if(Object.keys(data.tableObj[data.tableObj.length-1])[i].includes("sn")){
        s=i;
    }
    
    }
    var countR=data.tableObj[data.tableObj.length-1][Object.keys(data.tableObj[data.tableObj.length-1])[s]];


    localStorage.setItem("addRowOutpt", "");
    this.dialogService.addDialog(AddRowComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("addRowOutpt"));
        for(var i=0; i < s; i++){
          this.display_data = this.appService.changeAcc(this.appService.product_rate, data, ++countR, "","addRow");
      }

      } else {

      }
    })
    

}

  delRow(data,eve) {

    if (eve.target.parentElement.previousElementSibling != null) {
      this.display_data=this.appService.changeAcc(this.appService.product_rate,data,eve.target.parentElement.id,"","delRow");

    } else {
      localStorage.setItem("alertContent", "You can't delete this Row");

      this.dialogService.addDialog(AlertComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '

      }).subscribe((isConfirmed) => {
      });
    }

  }

  textValid(eve,fieldData,nfield,ti){
    this.expand=ti;
	
    if (localStorage.getItem("pageMode") == "2") {
      var resultVal = true;
      if((nfield.maxLength !='')){
        var dValue=eve.target.value.length;
        var max=Number(nfield.maxLength);
    
        if(dValue>max){
          eve.target.value="";
                         swal(
                           'Error!',
                           'Value Should not exceeds maximum length of '+nfield.maxLength,
                           'error'
                         )
                   
                        }
                      }

					  
    if(nfield.tagName=='date'){
       var regx=new RegExp(/^\d{4}-\d{2}-\d{2}$/);
        if(regx.test(eve.target.value)){
          
                if(eve.target.value!=''){
                  this.display_data = this.appService.changeField(this.appService.product_rate, nfield, "setVal", eve.target.value, "");

                  for(var i=0;i<fieldData.length;i++){
      
                    if(nfield.toId){
                      
                      if(fieldData[i].id==nfield.toId){
                      var nDate=nfield;
                      var fromDate = new Date(nDate.tagValue);	
                      var toDate = new Date(fieldData[i].tagValue);
                      if(fromDate > toDate){
                      
                       nDate.tagValue="";
                       nfield.mandatory=true;
                       this.display_data = this.appService.changeField(this.appService.product_rate, nfield, "setVal", "", "");
                        var tChange=fieldData[i];
                        tChange.tagValue="";
                        var output = this.appService.changeField(this.appService.product_rate, tChange, "setVal", "", "");
                        var resultCheck=JSON.stringify(output);
                        
                        this.display_data=JSON.parse(resultCheck);
                        swal(
                          'Error!',
                          'Please Enter valid Date',
                          'error'
                        )
                         break;
                      }
                      }
                      }
                      else if(nfield.fromId){
                        if(fieldData[i].id==nfield.fromId){
                        var nDate=nfield.tagValue;
                        var toDate = new Date(nDate);	
                        var fromDate = new Date(fieldData[i].tagValue);
                        if(fromDate > toDate){
                         // alert("invalid");
                         nfield.tagValue="";
                         this.display_data = this.appService.changeField(this.appService.product_rate, nfield, "setVal", "", "");
                          var tChange=fieldData[i];
                          tChange.tagValue="";
                          
                          var output= this.appService.changeField(this.appService.product_rate, tChange, "setVal", "", "");
                                          var resultCheck=JSON.stringify(output);
                          
                                          this.display_data=JSON.parse(resultCheck);
                                          swal(
                                            'Error!',
                                            'Please Enter valid Date',
                                            'error'
                                          )
                                           break;
                        }
                        }
                        
                        }
                  }
				  
				  
                  // this.display_data=this.appService.changeField(this.appService.product_rate,nfield,"setVal",eve.target.value,"");
                  // if(nfield.minValue != '' || nfield.maxValue != ''){
                  //   resultVal = this.appService.validateNumDate(this.appService.product_rate, nfield, "minMaxValidateDate", Number(eve.target.value));
                  // }
                  // if(!resultVal){
                  //   var output1 = this.appService.changeField(this.appService.product_rate, nfield, "setVal", "", "");
                  //   this.display_data=output1;
                  //   // setTimeout(res => {
                  //   //   this.display_data = output1;
                  //   // }, 200);
                  //   }else{
                  // if(nfield.toId){
                  //    var result =this.appService.validateField(this.appService.product_rate, nfield,nfield.toId,"to","date");
                  //     if(result =="invalid" ){
                  //       var output=this.appService.changeField(this.appService.product_rate,nfield,"setVal","","");
                  //       this.display_data=output;
                  //       // setTimeout(res=>{
                  //       //   this.display_data=output;
                          
                  //       // },200);
                  //     }
                  //   }else if(nfield.fromId){
                  //     var result =this.appService.validateField(this.appService.product_rate, nfield,nfield.fromId,"from","date");
                  //     if(result =="invalid" ){
                  //       var output=this.appService.changeField(this.appService.product_rate,nfield,"setVal","","");
                  //       this.display_data=output;
                  //       // setTimeout(res=>{
                  //       //   this.display_data=output;
                          
                  //       // },200);
                  //     }
                  //   }
                  // }
          }
        }else{
           eve.target.value="";
          
        }
      }else if(nfield.tagName=='number'){
        var regexp = /^[0-9]+([,.][0-9]+)?$/g;
        if(regexp.test(eve.target.value)){
          if(eve.target.value!=''){
            this.display_data = this.appService.changeField(this.appService.product_rate, nfield, "setVal", Number(eve.target.value), "");

            for(var i=0;i<fieldData.length;i++){

              if(nfield.toId){
                
                if(fieldData[i].id==nfield.toId){
                var nDate=nfield;
                var fromDate = new Date(nDate.tagValue);	
                var toDate = new Date(fieldData[i].tagValue);
                if(fromDate > toDate){
                
                 nDate.tagValue="";
                 nfield.mandatory=true;
                 this.display_data = this.appService.changeField(this.appService.product_rate, nfield, "setVal", "", "");
                  var tChange=fieldData[i];
                  tChange.tagValue="";
                  var output = this.appService.changeField(this.appService.product_rate, tChange, "setVal", "", "");
                  var resultCheck=JSON.stringify(output);
                  
                  this.display_data=JSON.parse(resultCheck);
                  swal(
                    'Error!',
                    'Please Enter valid number',
                    'error'
                  )
                   break;
                }
                }
                }
                else if(nfield.fromId){
                  if(fieldData[i].id==nfield.fromId){
                  var nDate=nfield.tagValue;
                  var toDate = new Date(nDate);	
                  var fromDate = new Date(fieldData[i].tagValue);
                  if(fromDate > toDate){
                   // alert("invalid");
                   nfield.tagValue="";
                   this.display_data = this.appService.changeField(this.appService.product_rate, nfield, "setVal", "", "");
                    var tChange=fieldData[i];
                    tChange.tagValue="";
                    
                    var output= this.appService.changeField(this.appService.product_rate, tChange, "setVal", "", "");
                                    var resultCheck=JSON.stringify(output);
                    
                                    this.display_data=JSON.parse(resultCheck);
                                    swal(
                                      'Error!',
                                      'Please Enter valid number',
                                      'error'
                                    )
                                     break;
                  }
                  }
                  
                  }
            }
          // this.display_data=this.appService.changeField(this.appService.product_rate,nfield,"setVal",Number(eve.target.value),"");
          // if(nfield.minValue != '' || nfield.maxValue != ''){
          //   resultVal = this.appService.validateNumDate(this.appService.product_rate, nfield, "minMaxValidateNum", Number(eve.target.value));
          // }
          // if(!resultVal){
          //   var output1 = this.appService.changeField(this.appService.product_rate, nfield, "setVal", "", "");
          //   this.display_data=output1;
          //   // setTimeout(res => {
          //   //   this.display_data = output1;
          //   // }, 200);
          //   }else{
          // if(nfield.toId){
          //   var result =this.appService.validateField(this.appService.product_rate, nfield,nfield.toId,"to","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.product_rate,nfield,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
                 
          //     //  },200);
          //    }
          //  }else if(nfield.fromId){
          //    var result =this.appService.validateField(this.appService.product_rate, nfield,nfield.fromId,"from","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.product_rate,nfield,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
                 
          //     //  },200);
          //    }
          //  }
          // }
        }
        }else{
          eve.target.value="";
        }
      }else{
        if(nfield.mappingId){
          for(var j=0;j<nfield.mappingId.length;j++){
            this.appService.changeField(this.appService.product_rate, nfield.mappingId[j], "copyValues", eve.target.value, "");
            }
          }
          if(eve.target.value!=''){
        this.display_data=this.appService.changeField(this.appService.product_rate,nfield,"setVal",eve.target.value,"");
          }
      }
    
    }else{
          // eve.target.value="";
     }
  }


  textTabValid(eve,tableRow,tData,tD,expanIndex){
    this.expand=expanIndex;
    if (localStorage.getItem("pageMode") == "2") {
      var resultVal = true;
      if(tD.sna != undefined){
        this.snumber[0].sna = tD.sna;
      }else if(tD.snb != undefined){
        this.snumber[1].snb = tD.snb;
        
      }else if(tD.snc != undefined){
        this.snumber[2].snc = tD.snc;
      }else if(tD.snd != undefined){
        this.snumber[3].snd = tD.snd;
      }
      if((tData.maxLength !='')){
        var dValue=eve.target.value.length;
        var max=Number(tData.maxLength);
    
        if(dValue>max){
          eve.target.value="";
                         swal(
                           'Error!',
                           'Value Should not exceeds maximum length of '+tData.maxLength,
                           'error'
                         )
                   
                        }
                      }
					  
    if(tData.tdObj[0].tagName=='date'){
       var regx=new RegExp(/^\d{4}-\d{2}-\d{2}$/);
        if(regx.test(eve.target.value)){
        
                if(eve.target.value!=''){
                  this.display_data = this.appService.changeField(this.appService.product_rate, tData, "setVal", eve.target.value, "");
                  for(var i=0;i<tableRow.length;i++){
    
                    if(tData.toId){
                    
                    if(tableRow[i].id==tData.toId){
                    
                    var fromDate = new Date(tData.tdObj[0].tdValue);	
                    var toDate = new Date(tableRow[i].tdObj[0].tdValue);
                    if(fromDate > toDate){
                     //alert("invalid");
                     tData.tdObj[0].tdValue="";
                     tData.mandatory=true;
                     this.display_data = this.appService.changeField(this.appService.product_rate, tData, "setVal", "", "");
                      var tChange=tableRow[i];
                      tChange.tdObj[0].tdValue="";
                      var output = this.appService.changeField(this.appService.product_rate, tChange, "setVal", "", "");
                      var resultCheck=JSON.stringify(output);
                      
                      this.display_data=JSON.parse(resultCheck);
                      swal(
                        'Error!',
                        'Please Enter valid Date',
                        'error'
                      )
                       break;
                    }
                    }
                    }else if(tData.fromId){
                    if(tableRow[i].id==tData.fromId){
                    
                    var toDate = new Date(tData.tdObj[0].tdValue);	
                    var fromDate = new Date(tableRow[i].tdObj[0].tdValue);
                    if(fromDate > toDate){
                     // alert("invalid");
                     tData.tdObj[0].tdValue="";
                     this.display_data = this.appService.changeField(this.appService.product_rate, tData, "setVal", "", "");
                      var tChange=tableRow[i];
                      tChange.tdObj[0].tdValue="";
                      
                      var output= this.appService.changeField(this.appService.product_rate, tChange, "setVal", "", "");
                                      var resultCheck=JSON.stringify(output);
                      
                                      this.display_data=JSON.parse(resultCheck);
                                      swal(
                                        'Error!',
                                        'Please Enter valid Date',
                                        'error'
                                      )
                                       break;
                    }
                    }
                    
                    }
                    }
					
					
                  // this.display_data=this.appService.changeField(this.appService.product_rate,tData,"setVal",eve.target.value,"");
                  // if(tData.minValue != '' || tData.maxValue != ''){
                  //   resultVal = this.appService.validateNumDate(this.appService.product_rate, tData, "minMaxValidateDate", Number(eve.target.value));
                  // }
                  // if(!resultVal){
                  //   var output1 = this.appService.changeField(this.appService.product_rate, tData, "setVal", "", "");
                  //   this.display_data=output1;
                  //   // setTimeout(res => {
                  //   //   this.display_data = output1;
                  //   // }, 200);
                  //   }else{
                  // if(tData.toId){
                  //    var result =this.appService.validateField(this.appService.product_rate, tData,tData.toId,"to","date");
                  //     if(result =="invalid" ){
                  //       var output=this.appService.changeField(this.appService.product_rate,tData,"setVal","","");
                  //       this.display_data=output;
                  //       // setTimeout(res=>{
                  //       //   this.display_data=output;
                          
                  //       // },200);
                  //     }
                  //   }else if(tData.fromId){
                  //     var result =this.appService.validateField(this.appService.product_rate, tData,tData.fromId,"from","date");
                  //     if(result =="invalid" ){
                  //       var output=this.appService.changeField(this.appService.product_rate,tData,"setVal","","");
                  //       this.display_data=output;
                  //       // setTimeout(res=>{
                  //       //   this.display_data=output;
                          
                  //       // },200);
                  //     }
                  //   }
                  // }
          }
        }else{
           eve.target.value="";
          
        }
      }else if(tData.tdObj[0].tagName=='number'){
        var regexp = /^[0-9]+([,.][0-9]+)?$/g;
        if(regexp.test(eve.target.value)){
          if(eve.target.value!=''){
            	
					 this.display_data = this.appService.changeField(this.appService.product_rate, tData, "setVal", Number(eve.target.value), "");
           for(var i=0;i<tableRow.length;i++){

             if(tData.toId){
             
             if(tableRow[i].id==tData.toId){
             
             var fromDate = new Date(tData.tdObj[0].tdValue);	
             var toDate = new Date(tableRow[i].tdObj[0].tdValue);
             if(fromDate > toDate){
              //alert("invalid");
              tData.tdObj[0].tdValue="";
              tData.mandatory=true;
              this.display_data = this.appService.changeField(this.appService.product_rate, tData, "setVal", "", "");
               var tChange=tableRow[i];
               tChange.tdObj[0].tdValue="";
               var output = this.appService.changeField(this.appService.product_rate, tChange, "setVal", "", "");
               var resultCheck=JSON.stringify(output);
               
               this.display_data=JSON.parse(resultCheck);
               swal(
                 'Error!',
                 'Please Enter valid Number',
                 'error'
               )
                break;
             }
             }
             }else if(tData.fromId){
             if(tableRow[i].id==tData.fromId){
             
             var toDate = new Date(tData.tdObj[0].tdValue);	
             var fromDate = new Date(tableRow[i].tdObj[0].tdValue);
             if(fromDate > toDate){
              // alert("invalid");
              tData.tdObj[0].tdValue="";
              this.display_data = this.appService.changeField(this.appService.product_rate, tData, "setVal", "", "");
               var tChange=tableRow[i];
               tChange.tdObj[0].tdValue="";
               
               var output= this.appService.changeField(this.appService.product_rate, tChange, "setVal", "", "");
                               var resultCheck=JSON.stringify(output);
               
                               this.display_data=JSON.parse(resultCheck);
                               swal(
                                 'Error!',
                                 'Please Enter valid Number',
                                 'error'
                               )
                                break;
             }
             }
             
             }
             }
     
            
          // this.display_data=this.appService.changeField(this.appService.product_rate,tData,"setVal",Number(eve.target.value),"");
          // if(tData.minValue != '' || tData.maxValue != ''){
          //   resultVal = this.appService.validateNumDate(this.appService.product_rate, tData, "minMaxValidateNum", Number(eve.target.value));
          // }
          // if(!resultVal){
          //   var output1 = this.appService.changeField(this.appService.product_rate, tData, "setVal", "", "");
          //   this.display_data=output1;
          //   // setTimeout(res => {
          //   //   this.display_data = output1;
          //   // }, 200);
          //   }else{
          // if(tData.toId){
          //   var result =this.appService.validateField(this.appService.product_rate, tData,tData.toId,"to","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.product_rate,tData,"setVal","","");
               
          //      this.display_data=output;
          //    }
          //  }else if(tData.fromId){
          //    var result =this.appService.validateField(this.appService.product_rate, tData,tData.fromId,"from","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.product_rate,tData,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
                 
          //     //  },200);
          //    }
          //  }
          // }
        }
        }else{
          eve.target.value="";
        }
      }else{
        if(tData.mappingId){
          for(var j=0;j<tData.mappingId.length;j++){
            this.appService.changeField(this.appService.product_rate, tData.mappingId[j], "copyValues", eve.target.value, "");
            }
          }
          if(eve.target.value!=''){
        this.display_data=this.appService.changeField(this.appService.product_rate,tData,"setVal",eve.target.value,"");
          }
      }
    
    }else{
         //  eve.target.value="";
     }
  }
  
  
  setVal(field,eve,tD){
    if (localStorage.getItem("pageMode") == "2") {
      
      if(tD.sna != undefined){
        this.snumber[0].sna = tD.sna;
      }else if(tD.snb != undefined){
        this.snumber[1].snb = tD.snb;
        
      }else if(tD.snc != undefined){
        this.snumber[2].snc = tD.snc;
      }else if(tD.snd != undefined){
        this.snumber[3].snd = tD.snd;
      }

    if(field.displayAccId){
      for(var l=0;l<field.displayAccId.length;l++){
        if(eve.target.value == "Y"){
          this.appService.changeAcc(this.appService.product_rate, field.displayAccId[l], false , "","displayAcc");
      }else{
        this.appService.changeAcc(this.appService.product_rate, field.displayAccId[l] , true ,"", "displayAcc");
        
      }
    }
    }
    
    if(field.navBtnId){
      for(var k=0;k<field.navBtnId.length;k++){
        if(eve.target.value == "Y"){
          this.appService.changeField(this.appService.product_rate, field.navBtnId[k] , "BtnValidation", false,  "");
      }else{
        this.appService.changeField(this.appService.product_rate, field.navBtnId[k] , "BtnValidation", true,  "");
        
      }
    }
    }

      if(field.mappingId){
        for(var j=0;j<field.mappingId.length;j++){
          this.appService.changeField(this.appService.product_rate, field.mappingId[j], "copyValues", eve.target.value, "");
          }
        }

      for(var i=0;i<field.optionObj.length;i++){
        if(field.optionObj[i].optionValue==eve.target.value){
          field.optionObj[i].selected=true;
        }else{
          field.optionObj[i].selected=false;
        }
        if(i==field.optionObj.length-1){
          this.display_data=this.appService.changeField(this.appService.product_rate,field,"setVal","","");
  
        }
      }

    }
  }
  


  saveValues(pinfo:NgForm){
    swal({
      title: 'Are you sure?',
      text: "You want to save!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((resultSel) => {
      if (resultSel.value) {
    setTimeout(()=>{

      this.spinner.show();
    },100)
    this.appService.validFieldCount(this.appService.product_rate);

       var headers = new Headers();
      headers.append('Content-Type', 'application/json');
        var dataContent = [];
      dataContent.push({ "ProductId": this.appService.productId, "PageName": "PRODUCT_RATES.json", "PageContent": this.appService.product_rate});

      this.http.post('/api/dataTmpPage', dataContent, { headers: headers })
        .subscribe(res => {
          setTimeout(() => {
            console.log(res.json());
          }, 1500);

        }, err => {
          console.log("err " + err);
        })



    if(pinfo.valid){
      var output=this.appService.validPage(this.appService.product_rate);
      if(output.mandatory){
        this.alertMsg=[];
        for(var i=0;i<output.mandatoryFields.length;i++){
            this.alertMsg.push({ "msg":output.mandatoryFields[i].fieldName+' ('+output.mandatoryFields[i].accordionName+')'});

        }
          localStorage.setItem("alertContent",'');
          localStorage.setItem("prodAlertMsg", JSON.stringify(this.alertMsg));
          setTimeout(()=>{

            this.spinner.hide();
          },100)
          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '

          }).subscribe((isConfirmed) => {
            
            localStorage.setItem("prodAlertMsg", "");
          });
      }else{
          var result=this.storeValuesToDb();
        //   var tabAccFormat = [{"accId": "ffs" , "tableName" : "gnmmGccSADTO"},
        // {"accId": "me" , "tableName" : "gnmmPlanMDTAExpense"},
        // {"accId": "zf" , "tableName" : "gnmmPlanMDTAFactor"},
        // {"accId": "rr" , "tableName" : "gnmmPlanRateOfReturn"},
        // {"accId": "pre" , "tableName" : "gnmmPlanModalFactorLink"},
        // {"accId": "pr" , "tableName" : "gnmmPremium"}];
        var tabAccFormat = tabAccprodFeesRatesFormat;
        this.appService.saveValuesRisk(this.appService.product_rate ,result ,tabAccFormat)
        .subscribe(res=>{
          setTimeout(()=>{
            this.spinner.hide();
          },200)
          
          if(res==true && this.productApproval){
            this.productSaved = true;
            this.appService.productSaved = false;
            localStorage.setItem("alertContent", "Saved Successfully");
      
            this.dialogService.addDialog(AlertComponent, { 
              title: 'Name dialog',
              question1: 'Block Name ',
              question2: 'Block Type '
      
            }).subscribe((isConfirmed) => {
              
      
            });
          }else{
            localStorage.setItem("alertContent", "Saved Successfully");
      
            this.dialogService.addDialog(AlertComponent, { 
              title: 'Name dialog',
              question1: 'Block Name ',
              question2: 'Block Type '
      
            }).subscribe((isConfirmed) => {
              
      
            });
          }
        })

      
      }
    }
  }
})
  }

  

  storeValuesToDb(){
        
       var format = {
   "planCode":null,
   "piName":null,
   "piDescription":null,
   "piNameInLocal":null,
   "piStartDate":null,
   "piEndDate":null,
   "productType":null,
   "bdcomputeextramortality":null,
   "bdindriskpremcompute":null,
   "bdmodalfactor":null,
   "bdsapremiuminput":null,
   "bdsacomputelevel":null,
   "bdpremiumcalculationtype":null,
   "bdwakalafee1":null,
   "bdwakalafee2":null,
   "bdformulaeused":null,
   "bdroundrule":null,
   "gnmmPlanMDTAExpense":[
      {
         "mePlanCode":null,
         "meCommencementdatefrom":null,
         "meCommencementdateto":null,
         "meInitialexpense":null,
         "meRenewalexpense":null,
         "meExpensepercentage":null,
         "meCommissionpercentage":null,
         "mePersA":null,
         "meAveragesa":null
      }
   ],
   "gnmmPlanMDTAFactor":[
      {
         "zfPlanCode":null,
         "zfEntryagefrom":null,
         "zfEntryageto":null,
         "zfPolicytermfrom":null,
         "zfPolicytermto":null,
         "zfCommencementdatefrom":null,
         "zfCommencementdateto":null,
         "zfZfactor":""
      }
   ],
   "gnmmPlanRateOfReturn":[
      {
         "rrPlanCode":null,
         "rrEffectivefrom":null,
         "rrEffectiveto":null,
         "rrRateofreturn":null
      }
   ],
   "gnmmPlanModalFactorLink":[
      {
         "prePlanCode":null,
         "preParentPlanCode":null,
         "prePaymentFrequency":null,
         "preFrequencyDescription":null,
         "prePaymentMethod":null,
         "ldblExpensePercentage":null,
         "preMethodDescription":null,
         "preMinSA":null,
         "preMaxSA":null,
         "preModalFactor":null,
         "preTableType":null,
         "preStatus":null 
      }
   ],
   "gnmmPremium":[
      {
         "prPlanCode":null,
         "prparentplancode":null,
         "prevent":null,
         "prgender":null,
         "proccupationclass":null,
         "prchannelno":null,
         "prsmoker":null,
         "pralcohol":null,
         "prpregnant":null,
         "pragefrom":null,
         "prageto":null,
         "prfrequency":null,
         "prgroupsizefrom":null,
         "prgroupsizeto":null,
         "prtermfrom":null,
         "prtermto":null,
         "prpolicyyearfrom":null,
         "prpolicyyearto":null,
         "prpremiumpayTermfrom":null,
         "prpremiumpayTermto":null,
         "prdefermentperiodfrom":null,
         "prdefermentperiodto":null,
         "prguaranteeperiodfrom":null,
         "prguaranteeperiodto":null,
         "printerestratefrom":null,
         "printerestrateto":null,
         "prmortalityratefrom":null,
         "prmortalityrateto":null,
         "prempagefrom":null,
         "prempageto":null, 
         "prsumcoveredfrom":null,
         "prsumcoveredto":null,
         "preffectivefrom":null,
         "preffectiveto":null,
         "premployeegender":null,
         "prmortalitytableiD":null,
         "prrate":null,
         "prunit":null,
         "prpolicyceaseage":null,
         "prstatus":null,
         "prnetworktype":null,
         "prrulecode":null,
         "przonecode":null,
         "prsubplancode":null,
         "prfamilytype":null,
         "prfamilysize":null,
         "prfixedpercent":null,
         "prriskgroupcode":null
      }
   ],
   "gnmmGccSADTO":[
      {
         "ffsPlanCode":null,
         "ffseventcode":null,
         "ffsPremium":null,
         "ffsSumassured":null,
         "ffsStatus":null
      }
   ]
}
        return format;
  }
  dataUpload(data){
    data["PageName"]="PRODUCT_RATES";
    this.appService.downloaddata=data;
    localStorage.setItem("upload", "true");
    this.appService.uploadingPageContent=this.appService.product_rate;
    this.appService.uploadingPageName="product_rate";
    this.dialogService.addDialog(DataDownloadComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '
 
    }).subscribe((isConfirmed) => {
        if (isConfirmed=="done") {
              this.ngOnInit(); 
              for (var d = 0; d < this.appService.subPagesNavProdInfo.length; d++) {
                this.appService.subPagesNavProdInfo[d].isActive=false;
          
                 
              }
   } else {

      
      }
    })
   }

  dataDownload(data){
    data["PageName"]="PRODUCT_RATES"; 	
 this.appService.downloaddata=data;
   localStorage.setItem("upload", "false");
   this.dialogService.addDialog(DataDownloadComponent, {
     title: 'Name dialog',
     question1: 'Block Name ',
     question2: 'Block Type '

   }).subscribe((isConfirmed) => {
     if (isConfirmed) {
  } else {
     }
   })
  }
  copyRow(data, eve) {
    // var countR = data.tableObj.length;
    var s;
    for(var i=0;i<Object.keys(data.tableObj[data.tableObj.length-1]).length;i++){
      if(Object.keys(data.tableObj[data.tableObj.length-1])[i].includes("sn")){
        s=i;
    }
    
    }
    var countR=data.tableObj[data.tableObj.length-1][Object.keys(data.tableObj[data.tableObj.length-1])[s]];


    this.display_data = this.appService.changeAcc(this.appService.product_rate, data, ++countR,eve.target.parentElement.id, "copyRow");

   }

   retriveDel(data){
    var fieldName = [];
    localStorage.setItem("fieldNameList", "");
    for (var a = 0; a < data.deletedItems.length; a++) {
    fieldName.push({ "id": data.deletedItems[a].id, "itemName": data.deletedItems[a].fieldName });
    }
     localStorage.setItem("fieldNameList", JSON.stringify(fieldName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
        var selectedFields = [];
        for (var a = 0; a < data.deletedItems.length; a++) {
          for (var b = 0; b < seletedItems.length; b++) {
          if(seletedItems[b].id == data.deletedItems[a].id){
            selectedFields.push(data.deletedItems[a]);
          }
          }
        }
        this.display_data = this.appService.changeAcc(this.appService.product_rate, data, selectedFields,"", "addDelField");
      } else {

      }
    })
     
   }

   retriveTabDel(data){
    var fieldName = [];
    localStorage.setItem("fieldNameList", "");
    for (var a = 0; a < data.deletedItems.length; a++) {
    fieldName.push({ "id": data.deletedItems[a].id, "itemName": data.deletedItems[a].theadName });
    }
     localStorage.setItem("fieldNameList", JSON.stringify(fieldName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
        var selectedFields = [];
        for (var a = 0; a < data.deletedItems.length; a++) {
          for (var b = 0; b < seletedItems.length; b++) {
          if(seletedItems[b].id == data.deletedItems[a].id){
            selectedFields.push(data.deletedItems[a]);
          }
          }
        }
        this.display_data = this.appService.changeAcc(this.appService.product_rate, data, selectedFields,"", "addDelTabField");
      } else {

      }
    })
     
   }

   retriveDelAcc(){
    localStorage.setItem("fieldNameList", "");
     localStorage.setItem("fieldNameList", JSON.stringify(this.accName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
       if (isConfirmed) {
          var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
          for (var a = 0; a < this.accName.length; a++) {
            for (var b = 0; b < seletedItems.length; b++) {
              if(this.accName[a].id == seletedItems[b].id){
                this.accName.splice(a, 1);
              }
            var seletedAcc = { "accordId" :seletedItems[b].id };
            this.display_data = this.appService.changeAcc(this.appService.product_rate, seletedAcc, "", "", "retriveAcc")
          }
        }
      } else {

      }
    })
    }
  

    searchLov(lovName,data,index,tData){
      localStorage.setItem('searchdata','table');
      this.appService.sIndex=index;
      var check=JSON.stringify(this.appService[lovName]);
      this.appService.lovListData=JSON.parse(check);
      this.appService.templateInfo=this.appService.product_rate;
      this.appService.lovpiName=lovName;
      this.appService.dataInfo=data;
      this.appService.tData=tData;
      this.dialogService.addDialog(DataSearchComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '
   
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {
     } else {
       
        }
      })
  
    }
    searchLovfield(field)
    {
      localStorage.setItem('searchdata','field');
      var check=JSON.stringify(this.appService[field.lovUrl]);
      this.appService.lovListData=JSON.parse(check);
      // var ch1=JSON.stringify(field.optionObj);
      this.appService.dataInfo=field;
      this.appService.fieldName=field.fieldName;
      
      this.dialogService.addDialog(DataSearchComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '
   
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {
     } else {
       
        }
      })
    }
    UpdateValues(pinfo: NgForm)
    {
      
      this.productUpdated=true;
      localStorage.setItem('ProductUpdated','true');
      // setTimeout(()=>{

      //   this.spinner.show();
      // },100)
      this.appService.validFieldCount(this.appService.product_rate);
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      var dataContent = [];
      dataContent.push({ "ProductId": this.appService.productId, "PageName": "PRODUCT_RATES.json", "PageContent": this.appService.product_rate});
  
      this.http.post('/api/dataPublishPage', dataContent, { headers: headers })
        .subscribe(res => {
          setTimeout(() => {
            console.log(res.json());
          }, 1500);
  
        }, err => {
          console.log("err " + err);
        })
    if (pinfo.valid) {
        var output = this.appService.validPage(this.appService.product_rate);
        if (output.mandatory) {
          this.alertMsg = [];
          for (var i = 0; i < output.mandatoryFields.length; i++) {
            this.alertMsg.push({ "msg": output.mandatoryFields[i].fieldName + ' (' + output.mandatoryFields[i].accordionName + ')' });
  
          }
          localStorage.setItem("alertContent", '');
          localStorage.setItem("prodAlertMsg", JSON.stringify(this.alertMsg));
          setTimeout(()=>{
  
            this.spinner.hide();
          },100)
          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '
  
          }).subscribe((isConfirmed) => {
  
            localStorage.setItem("prodAlertMsg", "");
          });
        } else {
          var result = this.storeValuesToDb();
          var tabAccFormat=tabAccprodFeesRatesFormat;
          this.appService.updateValuesRisk(this.appService.product_rate ,result ,tabAccFormat);
          
          setTimeout(() => {
            this.spinner.hide();
            if (this.appService.productSaved) {
              this.productSaved = true;
              this.appService.productSaved = false;
              localStorage.setItem("prodAlertMsg", "");
              localStorage.setItem("alertContent", "Updated Successfully");
  
              this.dialogService.addDialog(AlertComponent, { 
                title: 'Name dialog',
                question1: 'Block Name ',
                question2: 'Block Type '
  
              }).subscribe((isConfirmed) => {
                
  
              });
            }
          }, 1000)
        }
      }
    }
    
selectIndex(i)
{
  this.dropIndex=i;
}
}