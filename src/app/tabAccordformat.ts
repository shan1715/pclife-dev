// General Product Information
export var tabAccGenProdInfo = 
     [{"accId": "pc" , "tableName" : "gnmmProdCompositions"},
        {"accId": "rel" , "tableName" : "gnmmProdCompRelLinks"},
        {"accId": "au" , "tableName" : "gnmmPlanAuwRulesSet"},
        {"accId": "cop" , "tableName" : "gnmmPlanConversions"},
        {"accId": "asvcalc" , "tableName" : "psmmPlanAsvMethods"},
        {"accId": "asvrefund" , "tableName" : "psmmPlanAsvRefundSetups"},
        {"accId": "mt" , "tableName" : "gnmmPlanMortLinks"}];

      export  var tabAccAgencyFormat = [{"accId": "cd" , "tableName" : "gnmmPlanChannelLinkAgency"},
        {"accId": "boc" , "tableName" : "ammmPlanChannelCommLink"},
        {"accId": "poc" , "tableName" : "amdtProcComRates"},
        {"accId": "bbrs" , "tableName" : "anmmPlanChannelBscLink"}];

       export var tabAccOpAnnuityFormat = [{"accId": "dp" , "tableName" : "gnmmPlanAnnuityPeriodMethods"},
        {"accId": "gp" , "tableName" : "gnmmPlanAnnuityGuranteeLink"},
        {"accId": "pf" , "tableName" : "gnmmPlanPensionFreqLink"},
        {"accId": "mf" , "tableName" : "gnmmPlanModalFactLink"},
        {"accId": "ab" , "tableName" : "gnmmPlanAnnuityBenefiLink"}];
  

        export var tabAccopGroupFormat = [{"accId": "al" , "tableName" : "ageLoading"},
        {"accId": "rpc" , "tableName" : "renewalPlan"},
        {"accId": "fcl" , "tableName" : "gnmmPlanFclMethods"},
        {"accId": "rd" , "tableName" : "gnmmFamilyPremMethods"}]; 

       export var tabAccopPolicyServicingFormat = [{"accId": "tb" , "tableName" : "gnmmPlanPwLimitValidateDTO"},
        {"accId": "si" , "tableName" : "gnmtPlanSurrIndexDTO"},
        {"accId": "sc" , "tableName" : "psmmProcessChargesdto"},
        {"accId": "pd" , "tableName" : "gnmmPlanPayableMasterDTO"},
        {"accId": "cpts" , "tableName" : "psmmPlansurvivalBenefitDTO"},
        {"accId": "pdd" , "tableName" : "psmmPlanPredueSurvLinkDTO"},
        {"accId": "ppl" , "tableName" : "psmmPlanProcessLinkDTO"},
        {"accId": "bs" , "tableName" : "psmmBenefitSetupDTO"},
        {"accId": "dd" , "tableName" : "psmmPlanDividendOptionLinkDTO"},
        {"accId": "sod" , "tableName" : "psmmPlansurvivalOptionDTO"},
        {"accId": "ds" , "tableName" : "psmmPlanDividendSharingDTO"},
        {"accId": "csvfs" , "tableName" : "acmmCsvFormulaSetupDTO"},
        {"accId": "csvfactor" , "tableName" : "acmmCsvFactorsDTO"},
        {"accId": "puf" , "tableName" : "acmmPaidUpFactorsDTO"},
        {"accId": "npf" , "tableName" : "acmmNpFactorsDTO"},
        {"accId": "ir" , "tableName" : "gnmmInterestRateMasterDTO"},
        {"accId": "nfd" , "tableName" : "gnmmPlanNfLinkDTO"},
        {"accId": "fpur" , "tableName" : "psmmFullyPaidUpRidersDTO"},
        {"accId": "fpuf" , "tableName" : "psmtFullyPaidupFactorDTO"},
        {"accId": "fpuo" , "tableName" : "psmmFullyPaidupOptionDTO"},
        {"accId": "gp" , "tableName" : "psmmPolicyGraceDTO"},
        {"accId": "pay" , "tableName" : "psmtPlanSurvivalPayabaleDTOChild"},
        {"accId": "gitab" , "tableName" : "gnmmLifeEventsDTO"},
      {"accId": "bmed" , "tableName" : "gnluBenefitRelatedMasterDTO"},
      {"accId": "bmgd" , "tableName" : "gnluBenefitRelatedMasterDTOPS"},
    {"accId": "rd" , "tableName" : "psmmPlanRevivalDTO"},
    {"accId": "cp" , "tableName" : "psmtPlanSurvivalPayabaleDTO"},
    {"accId": "ld" , "tableName" : "psmloandetails"},
    {"accId": "ad" , "tableName" : "gnmmPlanAutoAlterLinkdto"}];

    export var tabAccopUnderwritingFormat = [{"accId": "au" , "tableName" : "gnmmPlanAuwSet"},
    {"accId": "pd" , "tableName" : "gnmmPlanDocumentsLinkSet"},
    {"accId": "fd" , "tableName" : "gnmmPlanFamilyDetailsSet"},
    {"accId": "cc" , "tableName" : "gnmPlanCauseCountSet"},
    {"accId": "uq" , "tableName" : "gnmmPlanUwQuesLinkSet"},
    {"accId": "oe" , "tableName" : "gnmmPlanOccExclLinkSet"},
    {"accId": "owd" , "tableName" : "gnmmPlanOccupDocLinkSet"},
    {"accId": "dc" , "tableName" : "gnmtChargeLoaddisMasterSet"},
    {"accId": "od" , "tableName" : "gnmmPlanOccupLinkSet"},
    {"accId": "usq" , "tableName" : "gnmPlanUwSubQuesLinkSet"}];

   export  var tabAccopunitLinksFormat = [{"accId": "buys" , "tableName" : "gnmmPlanIuYear"},
    {"accId": "sadb" , "tableName" : "gnmmPlanDeathAgeSALink"},
    {"accId": "ffs" , "tableName" : "unmmPlanFundSubFeeLink"},
    {"accId": "fd" , "tableName" : "unmmPlanFundLink"},
    {"accId": "sd" , "tableName" : "unmmPlanStrategyLink"},
    {"accId": "gr" , "tableName" : "gnmmGrowthRateDTO"},
    {"accId": "ultp" , "tableName" : "gnmmPlanSATgtpremLink"}];

   export  var tabAccopNewBusinessFormat = [{"accId": "wopcov" , "tableName" : "gnmmPlanWopCoverageLink"},
    {"accId": "tvt" , "tableName" : "tlValuations"},
    {"accId": "ac" , "tableName" : "annexureCodes"},
    {"accId": "lad" , "tableName" : "ageLALinks"},
    {"accId": "pc" , "tableName" : "policyConversions"},
    {"accId": "pcn" , "tableName" : "policyToPlans"},
    {"accId": "ppptl" , "tableName" : "premPayTermLinks"},
    {"accId": "inc_sas" , "tableName" : "gnmmPlanIncrDTO"}, 
    {"accId": "dec_sas" , "tableName" : "gnmmPlanDecrDTO"},
    {"accId": "mf" , "tableName" : "modalFactorLinks"},
    {"accId": "osal" , "tableName" : "gnmmPlanOccupSaLinks"},
    {"accId": "bmi" , "tableName" : "bmiRates"},
    {"accId": "ppl" , "tableName" : "gnmmPlanPremPatternLinks"},
    {"accId": "er" , "tableName" : "escalatingRates"},
    {"accId": "sap" , "tableName" : "gndtSumCoverPatterns"},
    {"accId": "sair" , "tableName" : "indexScRates"},
    {"accId": "ir" , "tableName" : "indexationRates"},
    {"accId": "or" , "tableName" : "gnmmPlanRiders"},
    {"accId": "prd" , "tableName" : "primaryRiderLinks"},
    {"accId": "la" , "tableName" : "optRiderLas"},
    {"accId": "rn" , "tableName" : "optRiderRels"},
    {"accId": "pm" , "tableName" : "gnmmPlanPaymodeLinks"},
    {"accId": "pms" , "tableName" : "payMethodLinks"},
    {"accId": "ped" , "tableName" : "methodModeExclusions"},
    {"accId": "larl" , "tableName" : "saLinks"},
    {"accId": "prl" , "tableName" : "proposerRelLinks"},
    {"accId": "cl" , "tableName" : "currencyLinks"},
    {"accId": "gtd" , "tableName" : "jlGrpLink"},
    {"accId": "gead" , "tableName" : "jlAgeLink"},
    {"accId": "patl" , "tableName" : "gnmmPlanTermAgeLink"},
    {"accId": "nml" , "tableName" : "nomineeRelLinks"}];

    export var tabAccProdFeesChargesFormat = [{"accId": "pc" , "tableName" : "gnmmPlanChargesMethods"},
    {"accId": "cv" , "tableName" : "chargesValues"},
    {"accId": "mf" , "tableName" : "modalFactor"},
    {"accId": "ld" , "tableName" : "loadingDiscount"},
    {"accId": "cs" , "tableName" : "contractStatus"},
    {"accId": "la" , "tableName" : "specificLa"},
    {"accId": "cr" , "tableName" : "rates"},
    {"accId": "e" , "tableName" : "gnmmPlanEventLinkChargesDTO"}];

    export var tabAccprodFessFinanceFormat = [{"accId": "poc" , "tableName" : "gnmmPlanOtherChargesMethods"},
    {"accId": "pepo" , "tableName" : "gnmmPlanExcessOptsLinks"},
    {"accId": "pd" , "tableName" : "premiumDepositsLinks"},
    {"accId": "fp" , "tableName" : "futurePremiumLinks"},
    {"accId": "tubu" , "tableName" : "topUpBasicLinks"},
    {"accId": "turu" , "tableName" : "topUpRiderLinks"}];

    export var tabAccprodFeesRatesFormat = [{"accId": "ffs" , "tableName" : "gnmmGccSADTO"},
    {"accId": "me" , "tableName" : "gnmmPlanMDTAExpense"},
    {"accId": "zf" , "tableName" : "gnmmPlanMDTAFactor"},
    {"accId": "rr" , "tableName" : "gnmmPlanRateOfReturn"},
    {"accId": "pre" , "tableName" : "gnmmPlanModalFactorLink"},
    {"accId": "pr" , "tableName" : "gnmmPremium"}];

    export var tabAccRiskFormat = [{"accId": "pugl" , "tableName" : "gnmmPlanUserGroupLinks"},
    {"accId": "puhl" , "tableName" : "gnmmPlanCountryHospLinks"},
    {"accId": "vl" , "tableName" : "gnmmProductLimits"},
    {"accId": "vertBene_bd" , "tableName" : "gnmmLimitBenefitLinksChild"},
    {"accId": "vertInc_il" , "tableName" : "gnmmProductIncrLimits"},
    {"accId": "cils" , "tableName" : "gnmmPlanCoinSetups"},
    {"accId": "jld" , "tableName" : "gnmmPlanJointLifes"},
    {"accId": "jointBene_bt" , "tableName" : "eventPayableLinksN"},
    {"accId": "jointLien_lt" , "tableName" : "gnmmPlanLienLinksN"}, 
    {"accId": "coInGrp_bd" , "tableName" : "gnmmPlanCoinGroups"},
    {"accId": "pe" , "tableName" : "gpmForEventLinkForParent"},
    {"accId": "parentProp_pode" , "tableName" : "gpmForEventLinkForParent"}, 
    {"accId": "parentProp_cp" , "tableName" : "gpmForEventLinkForParent"}, 
    {"accId": "parentProp_icpp" , "tableName" : "gpmForEventLinkForParent"}, 
    {"accId": "parentProp_bpc" , "tableName" : "gpmForEventLinkForParent"}, 
    {"accId": "parentProp_bt" , "tableName" : "eventPayableLinks"},
    {"accId": "parentProp_rf" , "tableName" : "gnmmRenewalFactors"},
    {"accId": "parentProp_psuc" , "tableName" : "gnmmStatusSetupLinks"},
    {"accId": "parentProp_hl" , "tableName" : "gnmmPlanEventLimits"},
    {"accId": "parentProp_ei" , "tableName" : "gnmmPlanEventInstLinks"},
    {"accId": "parentProp_ncb" , "tableName" : "gnmmPlanEventNcbs"},
    {"accId": "parentDoc_cdr" , "tableName" : "gnmmPlanEventDocuments"},
    {"accId": "parentEcce_cce" , "tableName" : "gnmmPlanCauseExcels"},
    {"accId": "ce" , "tableName" : "gpmForEventLinkForChild"},
    {"accId": "childProp_pode" , "tableName" : "gpmForEventLinkForChild"},
    {"accId": "childProp_cp" , "tableName" : "gpmForEventLinkForChild"},
    {"accId": "childProp_bpc" , "tableName" : "gpmForEventLinkForChild"},
    {"accId": "childProp_icpp" , "tableName" : "gpmForEventLinkForChild"},
    {"accId": "childProp_bt" , "tableName" : "eventPayableLinksCh"},
    {"accId": "childProp_rf" , "tableName" : "gnmmRenewalFactorsChild"},
    {"accId": "childProp_psuc" , "tableName" : "gnmmStatusSetupLinksCh"},
    {"accId": "childProp_hl" , "tableName" : "gnmmPlanEventLimitsCh"},
    {"accId": "childProp_ei" , "tableName" : "gnmmPlanEventInstLinksChild"},
    {"accId": "childProp_ncb" , "tableName" : "gnmmPlanEventNcbsChild"},
    {"accId": "childDoc_cdr" , "tableName" : "gnmmPlanEventDocumentsChild"},
    {"accId": "childEcce_cce" , "tableName" : "gnmmPlanCauseExcelsChild"},
    {"accId": "jointPol_psuc" , "tableName" : "gnmmStatusSetupLinksN"},
    {"accId": "death_se" , "tableName" : "gnmtPlanSubEventsLinks"},
    {"accId": "parentbenefit_se" , "tableName" : "gnmmPlanLienLinks"},
    {"accId": "childDeath_se" , "tableName" : "gnmtPlanSubEventsLinksCh"},
    {"accId": "parentProp_aed" , "tableName" : "gnmmEventLimitBenefitLink"},
    {"accId": "childProp_aed" , "tableName" : "gnmmEventLimitBenefitLinkChh"},
    {"accId": "jointLien_lt_child" , "tableName" : "gnmmPlanLienLinksCh"}];