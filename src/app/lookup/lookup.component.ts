import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild, NgModule,OnDestroy  } from '@angular/core';
import { NgForm, FormGroup, FormControl,FormsModule,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import swal from 'sweetalert2';
import { CustomFormsModule,CustomValidators } from 'ng2-validation';
//import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';
import { AppServices } from '../app.service';

import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import {DomSanitizer} from "@angular/platform-browser";  
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { BrowserModule } from '@angular/platform-browser';
import {Subscription} from 'rxjs';
import { TooltipModule } from 'ngx-bootstrap';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {DashboarddataComponent} from '../dashboarddata/dashboarddata.component';
declare var jQuery: any; 

 

@Component({
  selector: 'app-lookup',
  templateUrl: './lookup.component.html',
  styleUrls: ['./lookup.component.css']
})

export class LookupComponent implements OnInit,OnDestroy {

  @ViewChild('lookup') public lookup: NgForm;
  @ViewChild('cnm') public cnm:NgForm;
  @ViewChild('itemFields') public itemFields: NgForm;
  busy: Subscription;
  form: FormGroup;
  htmlContent:string;
  ws_url: any;
  data:any=[];
  lookupList:any=[];
  showlookupdata:any=[];
  newLookupData:any=[];
  lookupFields:any=[];
  items:any=[];
  dropList:any =[{"listCode":"","listValue":""}];
  checkItems:any=[];
  theader:any=[];
  trVal:any=[];
  tableRows:any=[];
  newRow:any=[];
  RefTablesList:any=[];
  selectedItem: any = '';
  inputChanged: any = '';
  useLookupData:any=[];
  enableFirstColumn:boolean=false;
  addNewRow:boolean=false;
  editRowCount:any=0;
  currentdate=new Date();
  sessionUser=localStorage.getItem("user");
  sessionId=localStorage.getItem("user");
  config2: any = {'placeholder': 'Database Table Name','sourceField': ['Table_Name'], 'style':'top:-6px !important;'};
  selectedItemType:any;
  searchLength:any;
  modalRef: BsModalRef;
  config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false,
    class: "my-modal"
  };
  maintainType:any;
  public searchText : string;
  public searched : boolean=false;

  constructor(private modalService:BsModalService,dialogService: DialogService, private http: Http, private appServ: AppServices,private datepipe: DatePipe) {

    this.http.get('assets/data/property.json')
    .subscribe(res => {
      this.ws_url = res.json().web.url;
    }, err => {
      //console.log(err);
    }) 
  }

  //AUTOCOMPLETE FUNCTIONS
  onSelect(item: any) {
    this.selectedItem = item;
   // alert('select:'+this.selectedItem.Table_Name);
    this.lookup.form.controls['dbName'].setValue(item.Table_Name);
    this.searchText=item.Table_Name;
    this.searchLength=item.Table_Name.toString().length;
    this.searched=true;
    this.lookup.form.controls['comments'].setValue(item.Notes);
   // this.checkDBtable();
  }
 
  onInputChangedEvent(val: string)  {
    this.searched=false;
    //this.inputChanged = val;
    // if(val.length<this.searchLength){
    //   this.searched=false;
    // }
    
   // alert('changed:'+this.inputChanged);
    //this.lookup.form.controls['dbName'].setValue(this.inputChanged);
    //this.checkDBtable();
    
  }

  ngOnDestroy(){
    this.appServ.callWS();
  }
 
  
  ngOnInit() {
    this.editRowCount=0;
    //btnNext
    this.getlookuplist();
    jQuery('[data-toggle="tooltip"]').tooltip(); 
    //reftable list
    this.appServ.getRefTables();

    setTimeout(()=>{    //<<<---    using ()=> syntax
      this.RefTablesList=this.appServ.RefTablesList.RefTables;
     // console.log('ref tables'+JSON.stringify(this.appServ.RefTablesList.RefTables));
    }, 2000);
   
    //show/hide field according to item type
    //this.itemTypeVal();
  }

  setValueForSelect(){
    this.lookup.form.reset();
    this.lookup.form.get('lookupName').enable();

    this.lookup.form.controls['sltLookupStatus'].setValue('active');
    (<HTMLInputElement>document.getElementById("dbName")).disabled=false; 
    (<HTMLInputElement>document.getElementById("next")).style.display="block"; 
    (<HTMLInputElement>document.getElementById("Show")).style.display="none"; 
  }

  getlookuplist(){
      this.http.get('assets/data/lookup.json')
    .subscribe(resLookup => {
     
      this.lookupList=resLookup.json();
      //console.log('lookup: '+Object.keys(this.lookupList.lookupList).length);
      let responseJSON = JSON.parse(JSON.stringify(resLookup.json()));
      //console.log('lookup res: '+ responseJSON.lookupList.length);
      for (var i = 0; i < this.lookupList.lookupList.length; i++) {
     //   console.log('here: '+ this.lookupList.lookupList[i].lookupTableColumn);
      }
    }, err => {
   // console.log(err);
    })
  }
  //check if lookup exist or not 
  checkLookup(){
    let lookup_name=[]; //all lookup name
       if(this.lookupList.lookupList.length > 0){
          for(let i=0; i< this.lookupList.lookupList.length;i++){
             // console.log('lookupName: '+ this.lookupList.lookupList[i].lookupName );
              lookup_name.push(this.lookupList.lookupList[i].lookupName.trim());
          }

          let enteredLookup = (<HTMLInputElement>document.getElementById("lookupName")).value.trim();
          enteredLookup = enteredLookup.trim();
          if(lookup_name.indexOf(enteredLookup) == -1){
            //alert(enteredLookup+'------'+lookup_name.indexOf(enteredLookup));
          }else{
               swal({
                        type: 'info',
                        title: 'Data',
                        text: 'Lookup already exist!! Please enter different Name!!',
                      });
            // alert('Lookup already exist!! Please enter different Name');
            this.lookup.form.controls['lookupName'].setValue('');
          }
      }
  }

  checkDBtable(){
    let lookup_TBname=[]; //all table name
      if(this.lookupList.lookupList.length > 0){
          for(let i=0; i< this.lookupList.lookupList.length;i++){
              //console.log( 'lookupTableName:'+ this.lookupList.lookupList[i].lookupTableName );
              lookup_TBname.push(this.lookupList.lookupList[i].lookupTableName);
          }

          let enteredLookup =(<HTMLInputElement>document.getElementById("dbName")).value.trim();
          if(lookup_TBname.indexOf(enteredLookup) == -1){
            //alert(enteredLookup+'------'+lookup_TBname.indexOf(enteredLookup));
          }else{
            swal({
                        type: 'info',
                        title: 'Data',
                        text: 'Lookup Table already exist!! Please enter different Table Name!!',
                      });
            // alert('Lookup Table already exist!! Please enter different Table Name');
            this.lookup.form.controls['dbName'].setValue('');
          }
      }
  }
  //create new lookup
  lookUpData(lookup : NgForm){
    //alert('herererer.....');
    this.showlookupdata=[];
    this.newLookupData=[]; 
    let id;
    
      if(this.lookupList.lookupList.length > 0){
          
          //alert(this.lookupList.lookupList.length);
          id=parseInt(this.lookupList.lookupList.length)+1;
      }
        //first element
        //alert("new lookupList id" + id);

      //  console.log((<HTMLInputElement>document.getElementById("lookupName")).value);
        this.data={
            'id' : id,
            'lookup_name' : (<HTMLInputElement>document.getElementById("lookupName")).value,
            'lookup_table_name': (<HTMLInputElement>document.getElementById("dbName")).value,
            'lookup_status': (<HTMLInputElement>document.getElementById("sltLookupStatus")).value,
            'lookup_comment': (<HTMLInputElement>document.getElementById("comments")).value
        };
      //  console.log('new lookup data: '+JSON.stringify(this.data));
            //call service for lookup data
            this.busy = this.appServ.lookUpData(this.data).subscribe(res=>{
              //this.lookUpData.lookupData=res.data;
              let response = JSON.parse(JSON.stringify(res))[0].result.data[0];
           //   console.log('look up data : '+ response.lookupTableName);
              
              this.newLookupData=response;
              if(response.lookupTableColumn.length >0 && response.lookupTableColumn != '' && typeof response.lookupTableColumn !='undefined'){
                this.getFieldProperties(response,response.lookupTableColumn);
                jQuery('#dataEnterMode').modal('show');
              }else{
                this.lookupFields=[];
                swal("No Data!!!", "Table Not exist.", "error");
              }
          })
      
   
 

  //reset feilds
  setTimeout(()=>{    //<<<---    using ()=> syntax
    this.lookup.form.reset();
    jQuery('#modalLookUp').modal('hide');
  }, 1000);
 
  
  }

  //new or existing lookup
  showLookup(data){
    this.showlookupdata=[];
    this.showlookupdata=data;
    this.useLookupData=data;
  //  console.log("old data ID: "+ data.lookupID);
    (<HTMLInputElement>document.getElementById("next")).style.display="none"; 
    (<HTMLInputElement>document.getElementById("Show")).style.display="block"; 
    this.lookup.form.controls['lookupName'].setValue(data.lookupName);
    this.lookup.form.get('lookupName').disable();
    this.lookup.form.controls['dbName'].setValue(data.lookupTableName);
    (<HTMLInputElement>document.getElementById("dbName")).disabled=true; 
    this.lookup.form.controls['sltLookupStatus'].setValue(data.lookUpStatus);
    this.lookup.form.controls['comments'].setValue(data.lookupComment);
  }

  // (click)="getFieldProperties(showlookupdata.lookupTableColumn)" on save in new lookup
  getFieldProperties(lookup,fields){
   // console.log('click---'+fields);
    (<HTMLInputElement>document.getElementById("currentLookup")).innerText=lookup.lookupName;
    (<HTMLInputElement>document.getElementById("currentDBtable")).innerText=lookup.lookupTableName;
    (<HTMLInputElement>document.getElementById("currentComments")).innerText=lookup.lookupComment;
    jQuery('#modalLookUp').modal('hide');
    
    this.lookupFields=[];
    if(fields != '' || fields.length > 0){
     // console.log(fields[0].COLUMN_NAME);
      this.lookupFields=fields;
    }
  }

  getItemType(fieldarr,index,item,checkedStatus){
    
    if(checkedStatus.checked){
      this.maintainType=fieldarr[index].DATA_TYPE;
   
          jQuery('#dataDetailMode').modal('show');
 
    this.items=[];
    this.items=item;
    // setvalues  for fields 
    //console.log(item);
    this.itemFields.form.controls['itemType'].setValue(item.item_type);
    this.itemTypeVal();

    if(item.item_type == "dropdown"){
     // console.log('length dp : ' + item.dropdownDetails);
      this.dropList = item.dropdownDetails;
    }else if(item.item_type == "checkbox"){
      //console.log('checkbox: '+item.checkboxDetails.checkboxValue);
      this.itemFields.form.controls["CheckValue"].setValue( item.checkboxDetails.checkboxValue);
      this.itemFields.form.controls["noCheckValue"].setValue( item.checkboxDetails.notCheckValue);
      this.itemFields.form.controls["defaultCheck"].setValue( item.checkboxDetails.defaultState );
    }else{
      this.dropList=[];
      this.dropList.push({"listCode":"","listValue":""});
    }
    
    if(this.dropList.length > 0 ){
      for(let n=0;n<this.dropList.length;n++){
        //console.log('itemFields '+ this.dropList[n].listCode )
        setTimeout(()=>{    //<<<---    using ()=> syntax
          this.itemFields.form.controls["dpCode"+n].setValue( this.dropList[n].listCode);
        //(<HTMLInputElement>document.getElementById("dpCode"+n)).value=item.dropdownDetails[n].listCode;
           this.itemFields.form.controls["dpValue"+n].setValue( this.dropList[n].listValue);
       // (<HTMLInputElement>document.getElementById("dpValue"+n)).value=item.dropdownDetails[n].listValue;
        }, 1000);
       
      }
    }

    if(item.item_type=='number'){
      this.itemFields.form.controls['min'].setValue(item.min_value);
      this.itemFields.form.controls['max'].setValue(item.max_value);

    }else if(item.item_type=='date'){
      this.itemFields.form.controls['minDate'].setValue(item.minDate_value);
      this.itemFields.form.controls['maxDate'].setValue(item.maxDate_value);

    }
    

    this.itemFields.form.controls['fieldLabel'].setValue(item.label);
    this.itemFields.form.controls['maxLength'].setValue(fieldarr[index].DATA_LENGTH);
    
    this.itemFields.form.controls['colWidth'].setValue(item.columnwidth);
    this.itemFields.form.controls['defalutValueType'].setValue(item.def_value_type);
    this.defVal();
    this.itemFields.form.controls['Manual'].setValue(item.manual_value);
    this.itemFields.form.controls['AutoValue'].setValue(item.auto_value);
    this.itemFields.form.controls['Tooltip'].setValue(item.tooltip);
    //if(fieldarr[index].NULLABLE == 'N' || item.mandatory == 'Y'){
      if(fieldarr[index].NULLABLE=='N' || item.mandatory == 'Y'){
        (<HTMLInputElement>document.getElementById("mandatory")).checked=true;
      (<HTMLInputElement>document.getElementById("mandatory")).disabled=true;
      }else{
        (<HTMLInputElement>document.getElementById("mandatory")).checked=false;
      (<HTMLInputElement>document.getElementById("mandatory")).disabled=false;
      }
    // if( (<HTMLInputElement>document.getElementById("nullable"+index)).checked==true){
      
    // }else{
      
    // }
    
    this.itemFields.form.controls['Mandmsg'].setValue(item.mandatory_msg);
   
    if(item.visible == 'N'){
      (<HTMLInputElement>document.getElementById("visibility")).checked=false;
    }else{
      (<HTMLInputElement>document.getElementById("visibility")).checked=true;
    }
    //for saving current value 
    (<HTMLInputElement>document.getElementById("colIndex")).value=index;
    
    (<HTMLInputElement>document.getElementById("selectedCol")).innerText=fieldarr[index].COLUMN_NAME;
    }else{
          swal({
      type: 'info',
      title: 'Selection',
      text: 'Row not selected!!',
    });
    }
  }


  saveItemFields(itemFields:NgForm){
    
    let index =  (<HTMLInputElement>document.getElementById("colIndex")).value;
    //alert(index);
    //alert('showarr'+this.showlookupdata);
   // alert('newarr'+this.newLookupData.lookupName) --- lookupFields can be use;
   //alert(this.lookupFields[index].COLUMN_NAME)
   //(this.lookupFields[index].COLUMN_NAME +'------' + (<HTMLInputElement>document.getElementById("itemType")).value);
   //mandatory flag
   let mand:boolean = (<HTMLInputElement>document.getElementById("mandatory")).checked;
      let mand_flag;
      if(mand == true){
        mand_flag='Y';
      }else{
        mand_flag='N';
      }
    //visible flag
    let visibleVal:boolean = (<HTMLInputElement>document.getElementById("visibility")).checked;
    let visibile_flag;
    if(visibleVal == true){
      visibile_flag='Y';
    }else{
      visibile_flag='N';
    }

    //save dropdown list
    let droplistdata=[];
    if( (<HTMLInputElement>document.getElementById("itemType")).value == 'dropdown' ){
      //alert('dropdown: '+ this.dropList.length);
      for(let i=0;i< this.dropList.length;i++){
        this.dropList[i].listCode = (<HTMLInputElement>document.getElementById("dpCode"+i)).value;
        this.dropList[i].listValue = (<HTMLInputElement>document.getElementById("dpValue"+i)).value;
      }
      droplistdata = this.dropList;
    }

    //checkbox
    let checkboxData={};
    if( (<HTMLInputElement>document.getElementById("itemType")).value == 'checkbox' ){
      checkboxData= {
                      "checkboxValue":(<HTMLInputElement>document.getElementById("CheckValue")).value,
                      "notCheckValue":(<HTMLInputElement>document.getElementById("noCheckValue")).value,
                      "defaultState":(<HTMLInputElement>document.getElementById("defaultCheck")).value
                   }
    }


   this.lookupFields[index].column_type= {     'item_type' : (<HTMLInputElement>document.getElementById("itemType")).value,
                                                'label' : (<HTMLInputElement>document.getElementById("fieldLabel")).value,
                                                'max_len' : (<HTMLInputElement>document.getElementById("maxLength")).value,
                                                
                                                'columnwidth' : (<HTMLInputElement>document.getElementById("colWidth")).value,
                                                'def_value_type' : (<HTMLInputElement>document.getElementById("defalutValueType")).value,
                                                'manual_value' : (<HTMLInputElement>document.getElementById("Manual")).value,
                                                'auto_value' : (<HTMLInputElement>document.getElementById("AutoValue")).value, 
                                                'tooltip' : (<HTMLInputElement>document.getElementById("Tooltip")).value,
                                                'mandatory' : mand_flag,
                                                'mandatory_msg' : (<HTMLInputElement>document.getElementById("Mandmsg")).value,
                                                'visible' : visibile_flag,
                                                'dropdownDetails' : droplistdata,
                                                'checkboxDetails' : checkboxData
                                          };
                                              if(this.selectedItemType=='number'){
                                                this.lookupFields[index].column_type['min_value']= (<HTMLInputElement>document.getElementById("min")).value;
                                                this.lookupFields[index].column_type['max_value']= (<HTMLInputElement>document.getElementById("max")).value;
                                              }else  if(this.selectedItemType=='date'){
                                                this.lookupFields[index].column_type['minDate_value']= (<HTMLInputElement>document.getElementById("minDate")).value;
                                                this.lookupFields[index].column_type['maxDate_value']= (<HTMLInputElement>document.getElementById("maxDate")).value;
                                              }


       
    //console.log('after adding: '+this.lookupFields[index].column_type.item_type);
    swal({
      type: 'success',
      title: 'Success',
      text: 'Data Saved Successfully!!',
    });
    jQuery('#dataDetailMode').modal('hide');
  }

    // show modal for last slide data entry
  showDBdata(looklist){
    this.editRowCount=0;
   this.theader=[];
   this.trVal=[];
    (<HTMLInputElement>document.getElementById("selectedLookup")).innerText= looklist.lookupName;
    (<HTMLInputElement>document.getElementById("EdittableName")).value= looklist.lookupTableName;
    (<HTMLInputElement>document.getElementById("DataDBtable")).innerText= looklist.lookupTableName;
    (<HTMLInputElement>document.getElementById("dataComments")).innerText= looklist.lookupComment;
   //get visible column looklist.lookupName
    let tharr=[];
    let showTH=[];
    let fieldDetails=[];
    let width=[];
    for(let i=0; i< looklist.lookupTableColumn.length; i++){
     
      if(looklist.lookupTableColumn[i].column_type.visible =='Y'){
        //alert(looklist.lookupTableColumn[i].column_type.visible);
        //alert("{'column_name': "+ looklist.lookupTableColumn[i].COLUMN_NAME+"}")
        tharr.push(looklist.lookupTableColumn[i].COLUMN_NAME);
        showTH.push(looklist.lookupTableColumn[i].column_type.label);
        fieldDetails.push(looklist.lookupTableColumn[i].column_type);
        width.push(looklist.lookupTableColumn[i].column_type.columnwidth);
      }
    }
    this.theader.push({'column_name': showTH,'width': width });
    this.trVal.push({'column_name': tharr,'width': width,'feildDetail' :fieldDetails });
    //console.log('theader:'+JSON.stringify(this.theader));
    // to check visible column
    this.getTRvales(looklist, this.trVal);

    
  }

  newRows(th){
    this.newRow=[];
    for(let i=0; i< th[0].column_name.length; i++ ){
          
      let showClmn = th[0].column_name[i];
      th[0].feildDetail[i]['Column']=showClmn;
      th[0].feildDetail[i]['ItemValue']='';
      this.newRow.push(th[0].feildDetail[i]);
      //this.newRow.unshift(th[0].feildDetail[i]);
    }
  }
  //fetch table rows data
  getTRvales(tableanme,visibleTH){
    
    let tabledetails ={ 'table' : tableanme.lookupTableName }
    this.tableRows=[];
    this.busy = this.appServ.fetchTableData(tabledetails).subscribe(res=>{
    //this.lookUpData.lookupData=res.data;
    let response = JSON.parse(JSON.stringify(res))[0].result.data;
    //console.log('new look up data : '+ response);
    //this.tableRows = response;   
    let trarr=[];
    let aniArgs=[];
    let field =[];
    for(let n=0; n< response.length;n++){
      //var fieldDetail=[];
      field =[];
      for(let i=0; i< visibleTH[0].column_name.length; i++ ){
        let showClmn = visibleTH[0].column_name[i];
        aniArgs.push(response[n].row[showClmn]);
        visibleTH[0].feildDetail[i]['Column']=showClmn;
        // if(visibleTH[0].feildDetail[i].item_type=="date"){

          // }
       	if(visibleTH[0].feildDetail[i]['item_type']=="date"){
          if(response[n].row.V_LASTUPD_INFTIM){
            visibleTH[0].feildDetail[i]['ItemValue']=response[n].row.V_LASTUPD_INFTIM.slice(0,10);
          }
          else if(response[n].row.D_LASTUPD_INFTIM){
            visibleTH[0].feildDetail[i]['ItemValue']=response[n].row.D_LASTUPD_INFTIM.slice(0,10);
          }
      }else{
        visibleTH[0].feildDetail[i]['ItemValue']=response[n].row[showClmn];
      }
          
      field.push(visibleTH[0].feildDetail[i]);
    }
        
    this.tableRows.push({  'rowData' :JSON.parse(JSON.stringify(field)) });
       
        //this.tableRows.push(tr);
  }   
  console.log(field.length + ' merged array: '+ this.tableRows.length);  
  //console.log('tr: '+tr);
      //append tr data in table tbody as html
      //this.htmlContent = '<tr><td></td></tr>';
      //got row data
  console.log(' in trarr:'+JSON.stringify(this.tableRows));
  this.newRows(visibleTH);
});

  }

  //search from lookup list
  searchLookup(list){
  
    let val = (<HTMLInputElement>document.getElementById("search")).value.trim();
    let filter = val.toUpperCase();
    
    for(let a=0; a<list.length; a++){
      
      if (list[a].lookupName.toUpperCase().indexOf(filter) > -1 || list[a].lookupTableName.toUpperCase().indexOf(filter) > -1 ) {
        (<HTMLInputElement>document.getElementById("rowInterface"+a)).style.display = "";
      } else {
        (<HTMLInputElement>document.getElementById("rowInterface"+a)).style.display = "none";
      }
    }
  }

  //addRow in dropdown
  addRow(tblId){
 
    this.dropList.push({"listCode":"","listValue":""});
    
  }

  RemoveRow(index){
    //this.dropList.push({"listCode":"","listValue":""});
   // console.log('dropdown row index: '+index);
    this.dropList.pop();
   // this.dropList=this.dropList.splice(-1,1)
   }
  
  itemTypeVal(){
     var IT = (<HTMLInputElement>document.getElementById("itemType")).value;
     if(IT == "dropdown"){
       this.selectedItemType="";
        jQuery('#dropdownDiv').show();
        jQuery('#MaxMinDate').hide();
        jQuery('#MaxMin').hide();
        jQuery('#checkboxDiv').hide();
     }else if(IT == 'checkbox'){
       this.selectedItemType="";
        jQuery('#checkboxDiv').show();
        jQuery('#MaxMin').hide();
        jQuery('#MaxMinDate').hide();
        jQuery('#dropdownDiv').hide();
     }else if(IT == 'number'){
       this.selectedItemType="number";
        jQuery('#MaxMin').show();
        jQuery('#MaxMinDate').hide();
        jQuery('#dropdownDiv').hide();
        jQuery('#checkboxDiv').hide();
     }else if(IT =='date'){
              this.selectedItemType="date";

               jQuery('#MaxMin').hide();
        jQuery('#MaxMinDate').show();
        jQuery('#dropdownDiv').hide();
        jQuery('#checkboxDiv').hide();
     }else if(IT =='hidden'){
         this.selectedItemType="";

      this.itemFields.form.controls["fieldLabel"].setValue('hidden');
      this.itemFields.form.controls["Tooltip"].setValue('hidden');
      (<HTMLInputElement>document.getElementById("visibility")).checked=false;
      jQuery('#MaxMinDate').hide();
      jQuery('#MaxMin').hide(); 
      jQuery('#dropdownDiv').hide();
      jQuery('#checkboxDiv').hide();
     }else{
          this.selectedItemType="";
       jQuery('#MaxMinDate').hide();
        jQuery('#MaxMin').hide();
        jQuery('#dropdownDiv').hide();
        jQuery('#checkboxDiv').hide();
     }

    

  }

  checkedField(i,field){
    //alert(i+'****--------'+field.NULLABLE);
    // if((<HTMLInputElement>document.getElementById('nullable'+i)).checked==true){
    //  //alert('arr--- '+  this.lookupFields[i].NULLABLE);
    //   field.NULLABLE = 'N';
    // }else{
    //   field.NULLABLE = 'Y';
    // }
    
  }

  //
  defVal(){
    
    let defType = (<HTMLInputElement>document.getElementById("defalutValueType")).value;
    //alert('function call  '+defType);
    if(defType == 'auto'){
      jQuery('#manualDiv').hide()
    }
    else if(defType=='manual'){
      jQuery('#manualDiv').show()
    }
    else{
      jQuery('#mannualDiv').hide()
    }
    
    this.getdefaultValueType();
  }

  //default value type as per item type
  getdefaultValueType(){
    // alert('manual fun') 
    let item = (<HTMLInputElement>document.getElementById("itemType")).value;
    let defaultVal = (<HTMLInputElement>document.getElementById("defalutValueType")).value;

    if( item == 'text' && defaultVal == "manual" ){
      (<HTMLInputElement>document.getElementById("Manual")).type = 'text';
    }
    else if( item == 'number' && defaultVal == "manual" ){
      (<HTMLInputElement>document.getElementById("Manual")).type  = 'number';
    }
    else if(  item == 'date' && defaultVal == "manual" ){
      (<HTMLInputElement>document.getElementById("Manual")).type  = 'date';
    }
    else{
      (<HTMLInputElement>document.getElementById("Manual")).type  = 'text';
    }
  }

  finish(){
    
   // console.log('finish'+this.lookupFields.length);
  //  console.log('finish'+this.lookupFields[0].column_type);
    this.checkItems=[];
    let checkFlag=[];

     let columnsTmp = JSON.stringify(this.lookupFields);
    let columns=JSON.parse(columnsTmp);
      for (var j =0; j< this.lookupFields.length ; j++ ){
       //   console.log('column :'+columns[j].NULLABLE);
          if((<HTMLInputElement>document.getElementById('nullable'+j)).checked==true){
            this.checkItems.push(columns[j]);
            columns[j].NULLABLE = 'N';
          }
      
      }

      console.log('manadatory:'+this.checkItems.length);
      for( let n=0; n < this.checkItems.length; n++){
         // console.log(n + 'check: '+this.checkItems[n].column_type.item_type)
        if(this.checkItems[n].column_type != '' && typeof this.checkItems[n].column_type.item_type != 'undefined'){
          checkFlag.push(true);
        //  console.log(n +' --- > '+ checkFlag)       
        }else{
          checkFlag.push(false);
       //   console.log(n +' < --- '+ checkFlag) 
        }
        
      }
     // console.log('flag: '+checkFlag.indexOf(false));
      if(checkFlag.indexOf(false) == -1){
       // alert('all filled');
          if(this.showlookupdata != '' && typeof this.showlookupdata.lookupName !== 'undefined'){
                 //alert('old data');
                 //edit data
                 this.showlookupdata.lookUpStatus=this.lookup.form.controls["sltLookupStatus"].value;
                 this.busy = this.appServ.EditlookUpData(this.showlookupdata).subscribe(res=>{
                      //this.lookUpData.lookupData=res.data;
                      let response = JSON.parse(JSON.stringify(res)).message;
                     // console.log('old look up data : '+ response);
                      swal({
                        type: 'success',
                        title: 'Success',
                        text: 'Data Saved Successfully!!',
                      });
                      this.getlookuplist();
                      jQuery('#dataEnterMode').modal('hide');
                      
                  });
           }else{
                //alert('new data');
                //save new lookup
                //console.log('new data: '+JSON.stringify(this.newLookupData));
                this.busy = this.appServ.NewlookUpData(this.newLookupData).subscribe(res=>{
                    //this.lookUpData.lookupData=res.data;
                    let response = JSON.parse(JSON.stringify(res)).message;
                  //  console.log('new look up data : '+ response);
                    swal({
                      title: "Do You Want to continue?",
                      text: "You can continue with lookup creation!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonClass: "btn-danger",
                      confirmButtonText: "Yes",
                      cancelButtonText: "No",
                    }).then(willDelete => {
                      //alert(willDelete.value);
                      if (willDelete.value) {
    //                      this.modalref = this.modalService.show("", this.config);
    // this.http.get("./assets/data/ProdType.json")
    //   .subscribe((res) => {
    //     this.pType = res.json().productType;

    //   }, err => {

    //     console.log(err);
    //   }) 
           jQuery("#modalLookUp").modal('show');
                        
                      } 
                    });
                    this.getlookuplist();
                    jQuery('#dataEnterMode').modal('hide');
                });
            }
        
      }else{
        //alert('not filled');
          swal({
                        type: 'info',
                        title: 'Data',
                        text: 'Please fill all Item Type for all mandatory Items!!',
                      });
        // alert('Please fill all Item Type for all mandatory Items.')
      }
  }


  //data entry modal
  editRow(id,row){
    this.editRowCount++;
    if(this.editRowCount>1){
        	  swal({
					type: 'info',
					title: 'Info',
					text: 'Save row edited previously!',
				  });
    }else{
     
   
    //alert(id);
     jQuery('#saveRow'+id).show();
     jQuery('#showBtn'+id).css({"float":"left"})
     //jQuery('#deleteRow'+id).hide();
     jQuery('#editRow'+id).css("display", "none");

    
 
    for(let i=0;i<row.length;i++){
      if(i==0) {
        if(!this.addNewRow){
          row[i]["readOnly"]=false;
        }else{
          row[i]["readOnly"]=true;
        }
      
      }else{
        row[i]["readOnly"]=true;
      }

      if(i==row.length-1){
        this.addNewRow=false;
      }
      }
      //this.tableRows[id].readOnly=true;
   
    }
  }

  deleteRow(id,row){
    swal({
      title: 'Are you sure?',
      text: "Do you want to Delete this row?",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      this.editRowCount=0;
      if (result.value) {
        
			//alert(id +'----'+JSON.stringify(row));
		//	console.log('before: '+JSON.stringify(this.tableRows))
			let invalid = [];
			let conCode;
			let conCol;

			var index = id;
			if (index > -1) {
        this.tableRows.splice(index, 1);
            var priKey=false;
      for(let i=0;i<row.length;i++){
        //alert(row[i].Column +'====='+ row[i].Column.indexOf('_CODE'));
        if( row[i].Column.indexOf('_CODE') > -1){
          priKey=true;
          conCol=row[i].Column;
          conCode=row[i].ItemValue;
          //alert(conCode);
        }else{
      //     conCol=row[0].Column;
      // conCode=row[0].ItemValue;
        }
        row[i]["readOnly"]=false;
      }
      if(!priKey){
                  conCol=row[0].Column;
      conCode=row[0].ItemValue;
      }
			  // for(let i=0;i<row.length;i++){
				// if( row[i].Column.indexOf('_CODE') > -1){
				//   conCol=row[i].Column;
				//   conCode=row[i].ItemValue;
				// }
				
			  // }

			  // delete row
			  //alert('deleting row.......');
			  
				let tableDetails = { 'tbName' : (<HTMLInputElement>document.getElementById('EdittableName')).value,
									 'tbData' : row,
									 'colCode' : conCode,
									 'conCol' : conCol
								  };
          this.busy = this.appServ.DeleteData(tableDetails).subscribe(res=>{
				  //this.lookUpData.lookupData=res.data;
				  let response = JSON.parse(JSON.stringify(res)).message;
				  swal({
					type: 'success',
					title: 'Success',
					text: 'Row Deleted!',
				  });
				  
			  });
			}
			//console.log(index + 'after: '+JSON.stringify(this.tableRows))
      }
    })
      
  }


  saveRow(id,row){
    let invalid = [];
    let conCode;
    let conCol;
    for(let i=0;i<row.length;i++){
      invalid.push(document.getElementById('row'+id).children[i].getElementsByClassName("td_value")[0].children[0].classList.contains('ng-invalid'));
    }
    //alert(invalid.indexOf(true));
    if(invalid.indexOf(true) > -1){
        swal({
                        type: 'info',
                        title: 'Data',
                        text: 'Fill all values Correctly!!',
                      });
      // alert('Fill all values Correctly.')
    }else{
      var priKey=false;
      for(let i=0;i<row.length;i++){
        //alert(row[i].Column +'====='+ row[i].Column.indexOf('_CODE'));
        if( row[i].Column.indexOf('_CODE') > -1){
          priKey=true;
          conCol=row[i].Column;
          conCode=row[i].ItemValue;
          //alert(conCode);
        }else{
      //     conCol=row[0].Column;
      // conCode=row[0].ItemValue;
        }
        row[i]["readOnly"]=false;
      }
      if(!priKey){
                  conCol=row[0].Column;
      conCode=row[0].ItemValue;
      }

      

      //alert('saving row.......');
      
        let tableDetails = { 'tbName' : (<HTMLInputElement>document.getElementById('EdittableName')).value,
                             'tbData' : row,
                             'colCode' : conCode,
                             'conCol' : conCol
                          };

       // console.log(JSON.stringify(tableDetails));
        this.busy = this.appServ.saveEditedData(tableDetails).subscribe(res=>{
          //this.lookUpData.lookupData=res.data;
          let response = JSON.parse(JSON.stringify(res)).message;
          //console.log('new look up data : '+ response);
          if(res[0].status=='Error'){
            this.editRowCount=0;
             swal({
            type: 'warning',
            title: 'Server Refused',
            text: JSON.stringify(res[0].result.message)
          });
          jQuery('#saveRow'+id).hide();
          //jQuery('.showBtn').css({'float': 'left','margin-left': '0px;'})
          jQuery('#deleteRow'+id).show();
          jQuery('#editRow'+id).show();
          }else{
             this.editRowCount=0;
             swal({
            type: 'success',
            title: 'Success',
            text: 'Data Added Successfully!!',
          });
          jQuery('#saveRow'+id).hide();
          //jQuery('.showBtn').css({'float': 'left','margin-left': '0px;'})
          jQuery('#deleteRow'+id).show();
          jQuery('#editRow'+id).show();
          }
          
         
      });
      
    }
    
    
  }

  
  checkDate(eve,min_value,max_value){

      // var regx=new RegExp(/^\d{2}-\d{2}-\d{4}$/);
      //   if(regx.test(eve.target.value)){

        
     let latest_date =this.datepipe.transform(eve.target.value,'yyyy-MM-dd');
     min_value = this.datepipe.transform(min_value,'yyyy-MM-dd');
     max_value = this.datepipe.transform(max_value,'yyyy-MM-dd');
    //alert('date: '+min_value+','+max_value+'----' + latest_date );  
    let selectedDate = Date.parse(latest_date);
    let maxDate = Date.parse(max_value);
    let minDate = Date.parse(min_value);
    //alert(selectedDate +'<' + minDate + '||' +  selectedDate + '>' + maxDate)
    if(selectedDate < minDate || selectedDate > maxDate){
      this.cnm.form.controls[eve.target.id].setValue('');  
    }
        // }else{
        //   eve.target.value="";
        // }
  }
   checkNumber(eve,min_value,max_value){
    //alert(min_value+','+max_value+'----'+ eve.target.value);  

    let val = eve.target.value;
   if(parseInt(val) < parseInt(min_value) || parseInt(val) > parseInt(max_value)){
     this.cnm.form.controls[eve.target.id].setValue('');  
   }
  }

  /*editText(event,i){
    alert(i.ItemValue+'::'+event.target.value )
    i.ItemValue = event.target.value;
    alert('edited:'+i.ItemValue);
  }*/
 
 
  AddNewRow(){
       this.addNewRow=true;
    this.tableRows.push({  'rowData' :JSON.parse(JSON.stringify(this.newRow)) });
   // console.log('with new row: '+ JSON.stringify(this.newRow));
    swal({
      type: 'success',
      title: 'Success',
      text: 'New Row has been Added at end of the Table!!',
    }).then((result) => {

      //modalBottom
      jQuery('#myModal .modal-body').scrollTop(0);
      var sectionOffset = jQuery('#modalBottom').offset();
      //scroll the container
      jQuery('#myModal .modal-body').animate({
        scrollTop: sectionOffset.top - 30
      }, "slow");
      // var rowpos = jQuery("#columnTbody tr:last-child").offset().top;
      // alert(rowpos);
      // jQuery('html, body').animate({
      //     scrollTop: jQuery("#columnTbody tr:last-child").offset().top
      // }, 2000);
    });
   
    //jQuery('#columnTbody').scrollTop(rowpos.top);
  }

  
  saveEditedata(){
    //alert('saving data.......');
   // console.log(this.tableRows[0].rowData[0].ItemValue);
      let tableDetails = { tbName : (<HTMLInputElement>document.getElementById('EdittableName')).value,
                           tbData : this.tableRows
                        };
        this.busy = this.appServ.saveEditedData(tableDetails).subscribe(res=>{
        //this.lookUpData.lookupData=res.data;
        let response = JSON.parse(JSON.stringify(res)).message;
       // console.log('new look up data : '+ response);
        swal({
          type: 'success',
          title: 'Success',
          text: 'Data Added Successfully!!',
        });
        this.getlookuplist();
        jQuery('#dataEnterMode').modal('hide');
    }); 
  }

  validDate(m,eve,ctrlDate){
    // if(ctrlDate.value==''){
    //     this.itemFields.controls["minDate"].setValue('');
    // }
      //  var regx=new RegExp(/^\d{4}-\d{2}-\d{2}$/);
      //   if(eve.target.value!='' && !regx.test(eve.target.value)){
      //     this.itemFields.controls["minDate"].setValue('');
      //   }

  }
}
