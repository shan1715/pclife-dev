import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskcoverageComponent } from './riskcoverage.component';

describe('RiskcoverageComponent', () => {
  let component: RiskcoverageComponent;
  let fixture: ComponentFixture<RiskcoverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskcoverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskcoverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
 
  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
