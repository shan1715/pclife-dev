import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit, AfterContentInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { DragulaService } from 'ng2-dragula';
import { Http, Headers } from "@angular/http";
import { AppServices } from '../app.service';
import { DialogService } from "ng2-bootstrap-modal";
import { EditPenComponent } from "../bootstrap-modal/edit-pen/edit-pen.component";
import { PrompComponent } from "../bootstrap-modal/Modaldialog/promp.component";
import { FielddialogueComponent } from "../bootstrap-modal/Fielddialogue/fielddialogue.component";
import { AlertComponent } from "../bootstrap-modal/alert/alert.component";
import {NgForm} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataDownloadComponent } from '../bootstrap-modal/dataDownload/dataDownload.component';
import { AddRowComponent } from "../bootstrap-modal/add-row/add-row.component";
import { RetriveDelComponent } from "../bootstrap-modal/retrive-del/retrive-del.component";
import { DataSearchComponent } from '../bootstrap-modal/dataSearch/dataSearch.component';
  import { default as data_json } from '../../assets/data/risk.json';
  import { tabAccRiskFormat } from '../tabAccordformat';
  import swal from 'sweetalert2';


@Component({
  selector: 'riskcoverage',
  templateUrl: './riskcoverage.component.html',
  styleUrls: ['./riskcoverage.component.css'],
  providers: [DragulaService]


})
export class RiskcoverageComponent implements OnInit,AfterContentInit {
  maintainParent:any;
  maintainParentId:any;
  maintainChildId:any;
  maintainChild:any;
  maintainParentChildId:any;
  selectedParentCode:any=[];
  jointLifeCode=[];
  backLinkBool: boolean =  false;
  display_data: any = [];
  parentName: any;
  backlinkField:any;
  navId:any;
  templateview: boolean = false;
  templateApproval: boolean = false;
  productApproval: boolean = false;
  templateSaved: boolean = false;
  productSaved: boolean = false;
  draggedItem:any=[];
  showAddBtn:boolean=false;
  showRow:boolean=false;
  count = 1;
  pageMode:any=0;
  rcCreate: boolean = false;
  rcModify: boolean = false;
  rcView: boolean = false;
  prcCreate: boolean = false;
  prcModify: boolean = false;
  prcView: boolean = false;
  alertMsg:any=[];
  snumber:any=[{"sna":0},{"snb":0},{"snc":0},{"snd":0}];
  blockName: any;
  accName = [];
	productUpdated: boolean = false;
  pbdUpdate: boolean = false;
  expand:any;
  dropIndex:any;
  currenttemp:any;
  dragField:any;
  constructor(private spinner: NgxSpinnerService,private router:Router, private http: Http,private dragulaService: DragulaService, private appService: AppServices,private dialogService:DialogService) { 
    dragulaService.setOptions('second-bag', {
      copy: (el, source) => {
        // alert(source.id)
        this.dragField=source.id;
        //return source.id === 'ok';
      },
      accepts: (el, target, source, sibling) => {
        // To avoid dragging from right to left container
        // alert(target.id)
        if(target.id === this.dragField){
          return true;
        }else{
          return false;
        }
        
      }
    });
    
    this.dragulaService.drag.subscribe((value: any) => {
      console.log("drag");

      this.onDrag(value.slice(1));

    });

    this.dragulaService.drop.subscribe((value: any) => {
      this.onDrop(value.slice(1));
      console.log("drop");
    });
    this.dragulaService.over.subscribe((value: any) => {

      this.onOver(value.slice(1));
      console.log("over");
    });
    this.dragulaService.out.subscribe((value: any) => {

      this.onOut(value.slice(1));
      console.log("out");
    });


  }

  ngOnInit() {
    this.expand=0;
    this.currenttemp=this.appService.currentTemplate;
    this.appService.pageName='PROD_BENEFITS';
    this.appService.snArr = this.snumber;

    if (localStorage.getItem("pageMode") == "1") {
      this.pageMode=1;
      if(this.appService.risk_coverage.length>0){
        this.display_data=this.appService.risk_coverage;
        this.blockName =this.appService.getAllBlockName(this.appService.risk_coverage);
      }else{
                this.appService.risk_coverage = data_json;
        this.display_data = this.appService.risk_coverage;
        this.blockName =this.appService.getAllBlockName(this.appService.risk_coverage);
    // this.http.get('assets/data/risk.json')
    //   .subscribe((res) => {
       
    //     this.appService.risk_coverage = res.json();
    //     this.display_data = this.appService.risk_coverage;
    //     this.blockName =this.appService.getAllBlockName(this.appService.risk_coverage);
        

    //   }, err => {
    //     console.log(err);
    //   })
    }
    if(this.appService.templateApproval){
      var approve = [];
        approve.push({
        "TemplateId": this.appService.templateId,
        "TemplateName": this.appService.templateName,
        "PageName": "PROD_BENEFITS" 
        })

        this.spinner.hide();
        var headers = new Headers();
        headers.append("Content-Type", "application/json");
        this.http.post('/api/readTempApproveStatus', JSON.stringify(approve[0]), { headers: headers })
          .subscribe(res => {  
            this.spinner.hide();
            if (res.json().review == false) {
               
              this.appService.templateSavedStaus=true;
              this.templateSaved=true;
            }else{
              this.appService.templateSavedStaus=false;
              this.templateSaved=false;
            }
    
          }, err => {
            this.spinner.hide();
            console.log("err " + err);
          })
        }
    }else if (localStorage.getItem("pageMode") == "2") {
      this.pageMode=2;
      if(this.appService.risk_coverage.length>0){
        
        this.appService.lovWS(this.appService.risk_coverage);
        this.display_data=this.appService.risk_coverage;
    

        if(this.appService.productApproval){
          var approve = [];
          approve.push({
            "ProductId": this.appService.productId,
            "ProductName": this.appService.productName,
            "PageName": "PROD_BENEFITS"
          })

          var headers = new Headers();
          headers.append("Content-Type", "application/json");
          this.http.post('/api/readProdApproveStatus', JSON.stringify(approve[0]), { headers: headers })
            .subscribe(res => {  
              if (res.json().review == false) {
                this.productSaved = true;
              }else{
                this.productSaved = false;
              }
      
            }, err => {
              console.log("err " + err);
            })
        }
         }else{
        
          localStorage.setItem("alertContent", "Template Not Created");

          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '

          }).subscribe((isConfirmed) => {
            this.router.navigate(['/productConfig/dashboarddata']);
            
          });
         }
         
         for(var dt=0;dt<this.appService.loveventmaster_tableoutput.length;dt++){
          this.jointLifeCode.push({"optionValue":this.appService.loveventmaster_tableoutput[dt].code,"optionLabel":this.appService.loveventmaster_tableoutput[dt].desc,"selected":false});
        
        } 
    }
  }

  ngAfterContentInit(){ 
    if (localStorage.getItem("pageMode") == "1") {
      if (document.getElementById("sValues")) {
        document.getElementById("sValues").style.display = "none";
      }
      setTimeout(()=>{
        this.showAddBtn = true;
      this.showRow = false;
      this.rcCreate = this.appService.rcCreate;
        this.rcModify = this.appService.rcModify;
        this.rcView = this.appService.rcView;
      this.templateApproval = this.appService.templateApproval;
      if (this.appService.publishedTemplate) {
        this.rcCreate = false;
        this.rcModify = false;
        this.templateApproval = false;
      }
      },500)   
      
      
    } else if (localStorage.getItem("pageMode") == "2") {
      if (document.getElementById("sFormat")) {
        document.getElementById("sFormat").style.display = "none";
      }

      setTimeout(()=>{
      this.showRow = true;
      this.showAddBtn = false;

      this.prcCreate = this.appService.prcCreate;
        this.prcModify = this.appService.prcModify;
        this.prcView = this.appService.prcView;
      this.productApproval = this.appService.productApproval;
      if (this.appService.publishedProduct) {
        this.prcCreate = false;
          this.prcModify = false;
        this.productApproval = false;
        this.pbdUpdate=true;
      }

      this.dragulaService.find("second-bag").drake.destroy();
     // var output = this.appService.validPage(this.appService.risk_coverage);
      //New Business Joint Life selection
        var childEvents = false;
        for (var ca = 0; ca < this.appService.operation_nbusiness.length; ca++) {
          if (this.appService.operation_nbusiness[ca].accordId == 'jl') {
            for (var cb = 0; cb < this.appService.operation_nbusiness[ca].field.length; cb++) {
              if (this.appService.operation_nbusiness[ca].field[cb].id == "jl_joinLifePlan") {
                for (var cc = 0; cc < this.appService.operation_nbusiness[ca].field[cb].optionObj.length; cc++) {
                  if (this.appService.operation_nbusiness[ca].field[cb].optionObj[cc].selected) {
                    if (this.appService.operation_nbusiness[ca].field[cb].optionObj[cc].optionValue == 'Y') {
                      childEvents = true;

                    }else if (this.appService.operation_nbusiness[ca].field[cb].optionObj[cc].optionValue == 'N') {
                      childEvents = false;

                    }


                  }


                }
              }

            }

          }
          if (ca == this.appService.operation_nbusiness.length - 1) {

            if (childEvents) {

              for (var f = 0; f < this.appService.risk_coverage.length; f++) {
                if (this.appService.risk_coverage[f].accordId == 'pe') {
                  if (this.appService.risk_coverage[f].tableObj) {
                    for (var g = 0; g < this.appService.risk_coverage[f].tableObj.length; g++) {
                      for (var h = 0; h < this.appService.risk_coverage[f].tableObj[g].tableRow.length; h++) {
                        if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].id == "btn_ChildEvent") {

                          this.appService.risk_coverage[f].tableObj[g].tableRow[h]["navDisable"] = true;
                        }
                      }

                    }
                  }
                }


this.appService.changeAcc(this.appService.risk_coverage,  {"accordId":"jld"}, false , "","displayAcc");

        //           if (this.appService.risk_coverage[f].accordId == 'jld') {
        //   if (this.appService.risk_coverage[f].tableObj) {
        //     for (var g = 0; g < this.appService.risk_coverage[f].tableObj.length; g++) {
        //       for (var h = 0; h < this.appService.risk_coverage[f].tableObj[g].tableRow.length; h++) {
        //         if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].id == "btn_jointBene") {
        //           for (var e = 0; e < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj.length; e++) {
        //             for (var ea = 0; ea < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow.length; ea++) {
        //               if (ea > 2) {
        //                 if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj) {
        //                   for (var p = 0; p < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj.length; p++) {
        //                     if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj[p].selected) {
        //                       riskJLBenefits = true;


        //                     }

        //                   }

        //                 } else if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj) {
        //                   if(this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tagName!='button'){
        //                   if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tdValue != '') {
        //                     riskJLBenefits = true;

        //                   }
        //                 }}


        //               }
        //             }
        //           }

        //         }
        //       }

        //     }
        //   }

        // }

        // if (this.appService.risk_coverage[f].accordId == 'jld') {
        //   if (this.appService.risk_coverage[f].tableObj) {
        //     for (var g = 0; g < this.appService.risk_coverage[f].tableObj.length; g++) {
        //       for (var h = 0; h < this.appService.risk_coverage[f].tableObj[g].tableRow.length; h++) {
        //         if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].id == "btn_jointPoliStatus") {
        //           for (var e = 0; e < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj.length; e++) {
        //             for (var ea = 0; ea < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow.length; ea++) {
        //               if (ea > 2) {
        //                 if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj) {
        //                   for (var p = 0; p < this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj.length; p++) {
        //                     if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].optionObj[p].selected) {
        //                       riskJLPolicy = true;


        //                     }

        //                   }

        //                 } else if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj) {
        //                   if(this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tagName!='button'){
        //                   if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].subPage[0].tableObj[e].tableRow[ea].tdObj[0].tdValue != '') {
        //                     riskJLPolicy = true;

        //                   }
        //                 }
        //               }

        //               }
        //             }
        //           }

        //         }
        //       }

        //     }
        //   }

        // }


              }

            }else{
              this.appService.changeAcc(this.appService.risk_coverage,  {"accordId":"jld"}, true , "","displayAcc");

              for (var f = 0; f < this.appService.risk_coverage.length; f++) {
                if (this.appService.risk_coverage[f].accordId == 'pe') {
                  if (this.appService.risk_coverage[f].tableObj) {
                    for (var g = 0; g < this.appService.risk_coverage[f].tableObj.length; g++) {
                      for (var h = 0; h < this.appService.risk_coverage[f].tableObj[g].tableRow.length; h++) {
                        if (this.appService.risk_coverage[f].tableObj[g].tableRow[h].id == "btn_ChildEvent") {

                          delete this.appService.risk_coverage[f].tableObj[g].tableRow[h].navDisable;
                        }
                      }

                    }
                  }
                }


              }
            }

          }

        }


    },500);
  }
}

nav(i,nfield,id,tD) {
  this.expand=i;
  if(tD.sna != undefined){
    this.snumber=[{"sna":0},{"snb":0},{"snc":0},{"snd":0}];
    this.snumber[0].sna = tD.sna;
  }else if(tD.snb != undefined){
    this.snumber[1].snb = tD.snb;
    
  }else if(tD.snc != undefined){
    this.snumber[2].snc = tD.snc;
  }else if(tD.snd != undefined){
    this.snumber[3].snd = tD.snd;
  }
  this.appService.snArr = this.snumber;
                       this.appService.selectedNav=id;

    this.display_data=this.appService.changeField(this.appService.risk_coverage,nfield,"nav","",this.appService.subPagesNavRisk);

   
    if(nfield.id=='btn_parentProp'){
      this.maintainParentId=id;
    }

    if(nfield.id=='btn_childProp'){
      this.maintainChildId=id;
    }
    
    if(nfield.id=='btn_ChildEvent'){
      this.maintainParentChildId=id;

    }

      // For getting Parent Event Code
    if (nfield.id == "btn_parentProp_hl_OtherEvent") {
      this.selectedParentCode=[];
          for (var m = 0; m < this.appService.risk_coverage.length; m++) {
            if (this.appService.risk_coverage[m].accordId == 'pe') {

              if (this.appService.risk_coverage[m].tableObj) {
                for (var p = 0; p < this.appService.risk_coverage[m].tableObj.length; p++) {
                  for (var q = 0; q < this.appService.risk_coverage[m].tableObj[p].tableRow.length; q++) {
                    if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].id == 'pe_parenteventcode') {
                      console.log(2)
                      for(var r=0;r<this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj.length;r++){
                        if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj[r].selected) {
                          this.selectedParentCode.push({ "optionValue": this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj[r].optionValue, "optionLabel": this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj[r].optionLabel, "selected": false });


                        }

                      }
                      }


                    }

                  }
                }
              }
            
            if (m == this.appService.risk_coverage.length - 1) {
              if (this.selectedParentCode.length > 0) {

                for (var ma = 0; ma < this.appService.risk_coverage.length; ma++) {

                  if (this.appService.risk_coverage[ma].accordId == 'jld') {

                    if (this.appService.risk_coverage[ma].tableObj) {
                      for (var pa = 0; pa < this.appService.risk_coverage[ma].tableObj.length; pa++) {
                        for (var qa = 0; qa < this.appService.risk_coverage[ma].tableObj[pa].tableRow.length; qa++) {
                          if (this.appService.risk_coverage[ma].tableObj[pa].tableRow[qa].id == 'jld_eventcode') {
                            // this.appService.risk_coverage[ma].tableObj[pa].tableRow[qa].optionObj=[];
                            
                            this.appService.risk_coverage[ma].tableObj[pa].tableRow[qa].optionObj = this.selectedParentCode;
                            


                          }

                        }
                      }
                    }
                  }
                  if(ma==this.appService.risk_coverage.length-1){
                    // var s=JSON.stringify(this.appService.risk_coverage);
                    // this.appService.risk_coverage=JSON.parse(s);
                    // this.display_data=this.appService.risk_coverage;
                  }
                }

                   

              }else{
                for (var ma = 0; ma < this.appService.risk_coverage.length; ma++) {

                  if (this.appService.risk_coverage[ma].accordId == 'jld') {

                    if (this.appService.risk_coverage[ma].tableObj) {
                      for (var pa = 0; pa < this.appService.risk_coverage[ma].tableObj.length; pa++) {
                        for (var qa = 0; qa < this.appService.risk_coverage[ma].tableObj[pa].tableRow.length; qa++) {
                          if (this.appService.risk_coverage[ma].tableObj[pa].tableRow[qa].id == 'jld_eventcode') {
                            // this.appService.risk_coverage[ma].tableObj[pa].tableRow[qa].optionObj=[];
                            
                            this.appService.risk_coverage[ma].tableObj[pa].tableRow[qa].optionObj = this.jointLifeCode;
                            


                          }

                        }
                      }
                    }
                  }
                  if(ma==this.appService.risk_coverage.length-1){
                    // var s=JSON.stringify(this.appService.risk_coverage);
                    // this.appService.risk_coverage=JSON.parse(s);
                    // this.display_data=this.appService.risk_coverage;
                  }
                }
              }
                
          }
          if(m==this.appService.risk_coverage.length-1){
            for (var gp = 0; gp < this.appService.risk_coverage.length; gp++) {
              if (this.appService.risk_coverage[gp].accordId == 'pe') {
         
                for (var gt = 0; gt < this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow.length; gt++) {
                  if (this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow[gt].id == 'pe_parenteventcode') {
                    for (var gu = 0; gu < this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow[gt].optionObj.length; gu++) {
                      if (this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow[gt].optionObj[gu].selected) {
                        this.maintainParent = this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow[gt].optionObj[gu].optionValue;
                        console.log(1)
      
      
                      }
      
      
      
                    }
      
      
      
                  }
      
      
                }
      
      
              }
            }
          }
        }

  
  }


  //For getting Child Event Code 
  if (nfield.id == "btn_childProp_hl_OtherEvent") { 
    this.selectedParentCode=[];
        for (var m = 0; m < this.appService.risk_coverage.length; m++) {
          if (this.appService.risk_coverage[m].accordId == 'pe') {

            if (this.appService.risk_coverage[m].tableObj) {
              for (var p = 0; p < this.appService.risk_coverage[m].tableObj.length; p++) {
                for (var q = 0; q < this.appService.risk_coverage[m].tableObj[p].tableRow.length; q++) {
                  if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].id == 'pe_parenteventcode') {
                
                    for(var r=0;r<this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj.length;r++){
                      if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj[r].selected) {
                        this.selectedParentCode.push({ "optionValue": this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj[r].optionValue, "optionLabel": this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj[r].optionLabel, "selected": false });


                      }

                    }
                    }


                  }

                }
              }
            }
          
         
        if(m==this.appService.risk_coverage.length-1){
          for (var gp = 0; gp < this.appService.risk_coverage.length; gp++) {
            if (this.appService.risk_coverage[gp].accordId == 'pe') {
       
              for (var gt = 0; gt < this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow.length; gt++) {
                if (this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow[gt].id == 'pe_parenteventcode') {
                  for (var gu = 0; gu < this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow[gt].optionObj.length; gu++) {
                    if (this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow[gt].optionObj[gu].selected) {
                      this.maintainParent = this.appService.risk_coverage[gp].tableObj[this.maintainParentId].tableRow[gt].optionObj[gu].optionValue;
                      console.log(1)
    
    
                    }
    
    
    
                  }
    
    
    
                }
    
    
              }
    
    
            }
          }
        }
      }


}


   
    // Assign Child Event Code to Co-Insurance Event Code
  for (var f = 0; f < this.display_data.length; f++) {
    if (this.display_data[f].accordId == "coInGrp_bd") {
      var selectedChild = false;
      var selectedChildObj = [];
      for (var m = 0; m < this.appService.risk_coverage.length; m++) {
        if (this.appService.risk_coverage[m].accordId == 'pe') {
          if (this.appService.risk_coverage[m].tableObj) {
            for (var p = 0; p < this.appService.risk_coverage[m].tableObj.length; p++) {
              for (var q = 0; q < this.appService.risk_coverage[m].tableObj[p].tableRow.length; q++) {
                if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage) {
                  for (var r = 0; r < this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage.length; r++) {
                    if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage[r].accordId == 'ce') {
                      for (var sa = 0; sa < this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage[r].tableObj.length; sa++) {
                        for (var t = 0; t < this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage[r].tableObj[sa].tableRow.length; t++) {
                          if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage[r].tableObj[sa].tableRow[t].id == "ce_eventcode") {

                            for (var v = 0; v < this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage[r].tableObj[sa].tableRow[t].optionObj.length; v++) {
                              if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage[r].tableObj[sa].tableRow[t].optionObj[v].selected) {
                                selectedChild = true;
                                selectedChildObj.push({ "optionValue": this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage[r].tableObj[sa].tableRow[t].optionObj[v].optionValue, "optionLabel": this.appService.risk_coverage[m].tableObj[p].tableRow[q].subPage[r].tableObj[sa].tableRow[t].optionObj[v].optionLabel,"selected":false })

                              }


                            }


                          }

                        }


                      }

                    }

                  }


                }



              }

            }

          }

        }

      }
      if(f==this.display_data.length-1){
      if (selectedChild && selectedChildObj.length > 0) {
        var selectedCoinCode;
        var selectedChildObj1;




         for (var az = 0; az < this.display_data.length; az++) {
          loop1: for (var ab = 0; ab < this.display_data[az].tableObj.length; ab++) {
          

                    for (var ac = 0; ac < this.display_data[az].tableObj[ab].tableRow.length; ac++) {

            if (this.display_data[az].tableObj[ab].tableRow[ac].id == "coInGrp_bd_eventcode") {
   for (var mm = 0; mm < selectedChildObj.length; mm++) {
                      selectedChildObj[mm].selected=false;
            
                    
                  }
              for (var ad = 0; ad < this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length; ad++) {
                if (this.display_data[az].tableObj[ab].tableRow[ac].optionObj[ad].selected) {
                  selectedCoinCode = this.display_data[az].tableObj[ab].tableRow[ac].optionObj[ad].optionValue;

                }

                if(ad==this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length-1){
              //    this.display_data[az].tableObj[ab].tableRow[ac].optionObj=selectedChildObj;
                }
              }
              for (var af = 0; af < selectedChildObj.length; af++) {
                if (selectedChildObj[af].optionValue == selectedCoinCode) {
                  selectedChildObj[af].selected=true;
                  //this.display_data[f].tableObj[ab].tableRow[ac].optionObj[af].selected = true;
                }
                if(af==selectedChildObj.length-1){
                  this.display_data[az].tableObj[ab].tableRow[ac].optionObj=selectedChildObj;
                  break loop1;
                } 
              }
            
              
        
            }


          }

       

        }
        if(az==this.display_data.length-1){

          var sa1=JSON.stringify(this.display_data);
           
         this.display_data=JSON.parse(sa1);
        }
      } 
      }else{
//           for (var fa = 0; fa < this.display_data.length; fa++) {
//             if (this.display_data[fa].accordId =="coInGrp_bd") {
//           for (var ab = 0; ab < this.display_data[fa].tableObj.length; ab++) {
//             for (var ac = 0; ac < this.display_data[fa].tableObj[ab].tableRow.length; ac++) {
  
//               if (this.display_data[fa].tableObj[ab].tableRow[ac].id == "coInGrp_bd_eventcode") {
//                 var eDisplay;
// for(var am=0;am<this.display_data[fa].tableObj[ab].tableRow[ac].optionObj.length;am++){
// 	if(this.display_data[fa].tableObj[ab].tableRow[ac].optionObj[am].selected){
// 		eDisplay=this.display_data[fa].tableObj[ab].tableRow[ac].optionObj[am].optionValue;

// }
//   if(am==this.display_data[fa].tableObj[ab].tableRow[ac].optionObj.length-1){


//                 this.display_data[fa].tableObj[ab].tableRow[ac].optionObj=[];
//                 for(var dt=0;dt<this.appService.loveventmaster_tableoutput.length;dt++){
//                   this.display_data[fa].tableObj[ab].tableRow[ac].optionObj.push({"optionValue":this.appService.loveventmaster_tableoutput[dt].code,"optionLabel":this.appService.loveventmaster_tableoutput[dt].desc});
                
//                 }
//               }
//             }}
//           }}
  
//             }
//             if(fa==this.display_data.length-1){
//                var sa1=JSON.stringify(this.display_data);
             
//                this.display_data=JSON.parse(sa1);
//             }
  
//           }

         
        
        
      }

    }
  }
  if (this.display_data[f].accordId == "parentProp_aed"){
    // if (this.selectedParentCode.length > 0) {
    //   var selectedCoinCode;
    //   var selectedChildObj1;
    //   for(var sp=0;sp<this.selectedParentCode.length;sp++){
    //     if(this.selectedParentCode[sp].optionValue==this.maintainParent){
    //       this.selectedParentCode.splice(sp,1);


    //     }
    //   }
    //    for (var az = 0; az < this.display_data.length; az++) {
    //     loop1: for (var ab = 0; ab < this.display_data[az].tableObj.length; ab++) {
        

    //               for (var ac = 0; ac < this.display_data[az].tableObj[ab].tableRow.length; ac++) {


    //       if(this.display_data[az].tableObj[ab].tableRow[ac].id =="parentProp_aed_eventcode"){
    //         if(this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length>0){
    //         for (var ad = 0; ad < this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length; ad++) {
    //           if (this.display_data[az].tableObj[ab].tableRow[ac].optionObj[ad].selected) {
    //             selectedCoinCode = this.display_data[az].tableObj[ab].tableRow[ac].optionObj[ad].optionValue;


    //           } 

    //           if(ad==this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length-1){
    //             this.display_data[az].tableObj[ab].tableRow[ac].optionObj=this.selectedParentCode;
    //             if(selectedCoinCode!=undefined){
    //                 for(var sw=0;sw<this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length;sw++){
    //                   if(this.display_data[az].tableObj[ab].tableRow[ac].optionObj[sw].optionValue==selectedCoinCode){
    //                     this.display_data[az].tableObj[ab].tableRow[ac].optionObj[sw].selected=true;




    //                     break loop1;
    //                   }
    //                 }
    //             }else{
    //               this.display_data[az].tableObj[ab].tableRow[ac].optionObj=this.selectedParentCode;
    //               break loop1;



    //             }
    //           }
    //         }
          
    //             //this.display_data[az].tableObj[id].tableRow[ac].optionObj=selectedChildObj1;
    //             //break loop1;
                

    //         }else{
    //           this.display_data[az].tableObj[ab].tableRow[ac].optionObj=this.selectedParentCode;
    //           break loop1;

    //         }}
          
          


    //     }

     


    //   }
    //   if(az==this.display_data.length-1){
    //     //var sa1=JSON.stringify(this.display_data);
         
    //    // this.display_data=JSON.parse(sa1);



    //   }
    // } 
    // }
  }

  }


    // // Assign Child Event Code to Co-Insurance Event Code
    // for (var f = 0; f < this.display_data.length; f++) {
    //   if (this.display_data[f].accordId =="childProp_aed") {
    //     var selectedChild = false;
    //     var selectedChildObj = [];
    //     for (var m = 0; m < this.appService.risk_coverage.length; m++) {
    //       if (this.appService.risk_coverage[m].accordId == 'pe') {
    //         if (this.appService.risk_coverage[m].tableObj) {
    //             for (var q = 0; q < this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow.length; q++) {
    //               if (this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage) {
    //                 for (var r = 0; r < this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage.length; r++) {
    //                   if (this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage[r].accordId == 'ce') {
    //                     for (var sa = 0; sa < this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage[r].tableObj.length; sa++) {

    //                       for (var t = 0; t < this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage[r].tableObj[sa].tableRow.length; t++) {
    //                         if (this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage[r].tableObj[sa].tableRow[t].id == "ce_eventcode") {
  
    //                           for (var v = 0; v < this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage[r].tableObj[sa].tableRow[t].optionObj.length; v++) {
    //                             if (this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage[r].tableObj[sa].tableRow[t].optionObj[v].selected) {
    //                               selectedChild = true;
    //                               selectedChildObj.push({ "optionValue": this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage[r].tableObj[sa].tableRow[t].optionObj[v].optionValue, "optionLabel": this.appService.risk_coverage[m].tableObj[this.maintainParentChildId].tableRow[q].subPage[r].tableObj[sa].tableRow[t].optionObj[v].optionLabel,"selected":false })
  

    //                             }
  
  

    //                           }
  
  

    //                         }
  

    //                       }
  
  
                        
  



    //                   }
    //                 }
    //                 }
  
  

    //               }
  
  
  

    //             }
  
              
  

    //         }
  

    //       }
  

    //     }
    //     if(f==this.display_data.length-1){
    //     if (selectedChild && selectedChildObj.length > 0) {
    //       var selectedCoinCode;
    //       var selectedChildObj1;
  
  
  
  
    //        for (var az = 0; az < this.display_data.length; az++) {
    //         loop2: for (var ab = 0; ab < this.display_data[az].tableObj.length; ab++) {
            
  
    //                   for (var ac = 0; ac < this.display_data[az].tableObj[ab].tableRow.length; ac++) {
  
    //           if(this.display_data[az].tableObj[ab].tableRow[ac].id =="childProp_aed_eventcode"){
  
    //             for (var gp = 0; gp < this.appService.risk_coverage.length; gp++) {
    //               if (this.appService.risk_coverage[gp].accordId == 'pe') {
  
    //                 for (var gt = 0; gt < this.appService.risk_coverage[gp].tableObj[this.maintainParentChildId].tableRow.length; gt++) {
    //                   if (this.appService.risk_coverage[gp].tableObj[this.maintainParentChildId].tableRow[gt].id == 'btn_ChildEvent') {
    //                     for (var gu = 0; gu < this.appService.risk_coverage[gp].tableObj[this.maintainParentChildId].tableRow[gt].subPage[0].tableObj[this.maintainChildId].tableRow.length; gu++) {
    //                       if (this.appService.risk_coverage[gp].tableObj[this.maintainParentChildId].tableRow[gt].subPage[0].tableObj[this.maintainChildId].tableRow[gu].id == "ce_eventcode") {
    //                         if(this.appService.risk_coverage[gp].tableObj[this.maintainParentChildId].tableRow[gt].subPage[0].tableObj[this.maintainChildId].tableRow[gu].optionObj.length>0){
    //                         for (var gv = 0; gv < this.appService.risk_coverage[gp].tableObj[this.maintainParentChildId].tableRow[gt].subPage[0].tableObj[this.maintainChildId].tableRow[gu].optionObj.length; gv++) {
    //                           if (this.appService.risk_coverage[gp].tableObj[this.maintainParentChildId].tableRow[gt].subPage[0].tableObj[this.maintainChildId].tableRow[gu].optionObj[gv].selected) {
  
    //                             this.maintainChild = this.appService.risk_coverage[gp].tableObj[this.maintainParentChildId].tableRow[gt].subPage[0].tableObj[this.maintainChildId].tableRow[gu].optionObj[gv].optionValue;

    //                             break;
  

    //                           }
  
  
  


    //                         }
    //                       }else{
    //                         this.appService.risk_coverage[gp].tableObj[this.maintainParentChildId].tableRow[gt].subPage[0].tableObj[this.maintainChildId].tableRow[gu].optionObj=selectedChildObj;

    //                       }}
  
  
  

    //                     }
  
  
  

    //                   }
  
  

    //                 }
  
  

    //               }
                  

    //             }
    //             for(var sp=0;sp<selectedChildObj.length;sp++){
    //               if(selectedChildObj[sp].optionValue==this.maintainChild){
    //                 selectedChildObj.splice(sp,1);


    //               }
    //             }
    //             for (var ad = 0; ad < this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length; ad++) {
    //               if (this.display_data[az].tableObj[ab].tableRow[ac].optionObj[ad].selected) {
    //                 selectedCoinCode = this.display_data[az].tableObj[ab].tableRow[ac].optionObj[ad].optionValue;
  

    //               }
  
    //               if(ad==this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length-1){
    //                 this.display_data[az].tableObj[ab].tableRow[ac].optionObj=[];;


    //               }
    //             }
         
    //               for (var af1 = 0; af1 < selectedChildObj.length; af1++) {
  
    //               if (selectedCoinCode == selectedChildObj[af1].optionValue) {
    //                 selectedChildObj[af1].selected=true;
    //                 //this.display_data[f].tableObj[ab].tableRow[ac].optionObj[af].selected = true;

    //               } 
                
    //               if(af1==selectedChildObj.length-1){
    //                 this.display_data[az].tableObj[ab].tableRow[ac].optionObj=selectedChildObj;
    //                 break loop2;


    //               } 
    //             }
              

    //           }
  

    //         }
  
         
  

    //       }
    //       if(az==this.display_data.length-1){
  
    //         var sa1=JSON.stringify(this.display_data);
             
    //        this.display_data=JSON.parse(sa1);


    //       }
    //     } 
    //     }else{
    //         for (var fa = 0; fa < this.display_data.length; fa++) {
    //           if (this.display_data[fa].accordId =="coInGrp_bd" || this.display_data[fa].accordId =="childProp_aed") {
    //         for (var ab = 0; ab < this.display_data[fa].tableObj.length; ab++) {
    //           for (var ac = 0; ac < this.display_data[fa].tableObj[ab].tableRow.length; ac++) {
    
    //             if (this.display_data[fa].tableObj[ab].tableRow[ac].id == "coInGrp_bd_eventcode" || this.display_data[fa].tableObj[ab].tableRow[ac].id =="childProp_aed_eventcode") {
    //               this.display_data[fa].tableObj[ab].tableRow[ac].optionObj=[];
    //               for(var dt=0;dt<this.appService.loveventmaster_tableoutput.length;dt++){
    //                 this.display_data[fa].tableObj[ab].tableRow[ac].optionObj.push({"optionValue":this.appService.loveventmaster_tableoutput[dt].code,"optionLabel":this.appService.loveventmaster_tableoutput[dt].desc});
                  



    //               }
    //             }
    //           }}
    
    

    //           }
    //           if(fa==this.display_data.length-1){
    //              var sa1=JSON.stringify(this.display_data);
               
    //              this.display_data=JSON.parse(sa1);

    //           }
    

    //         }
  
           
          
          

    //     }
  


    //   }
    // }
    // if (this.display_data[f].accordId == "parentProp_aed"){
    //   if (this.selectedParentCode.length > 0) {
    //     var selectedCoinCode;
    //     var selectedChildObj1;
    //     for(var sp=0;sp<this.selectedParentCode.length;sp++){
    //       if(this.selectedParentCode[sp].optionValue==this.maintainParent){
    //         this.selectedParentCode.splice(sp,1);


    //       }
    //     }
    //      for (var az = 0; az < this.display_data.length; az++) {
    //       loop1: for (var ab = 0; ab < this.display_data[az].tableObj.length; ab++) {
          
  
    //                 for (var ac = 0; ac < this.display_data[az].tableObj[ab].tableRow.length; ac++) {
  
  
    //         if(this.display_data[az].tableObj[ab].tableRow[ac].id =="parentProp_aed_eventcode"){
    //           if(this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length>0){
    //           for (var ad = 0; ad < this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length; ad++) {
    //             if (this.display_data[az].tableObj[ab].tableRow[ac].optionObj[ad].selected) {
    //               selectedCoinCode = this.display_data[az].tableObj[ab].tableRow[ac].optionObj[ad].optionValue;
    

    //             } 
  
    //             if(ad==this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length-1){
    //               this.display_data[az].tableObj[ab].tableRow[ac].optionObj=this.selectedParentCode;
    //               if(selectedCoinCode!=undefined){
    //                   for(var sw=0;sw<this.display_data[az].tableObj[ab].tableRow[ac].optionObj.length;sw++){
    //                     if(this.display_data[az].tableObj[ab].tableRow[ac].optionObj[sw].optionValue==selectedCoinCode){
    //                       this.display_data[az].tableObj[ab].tableRow[ac].optionObj[sw].selected=true;




    //                       break loop1;
    //                     }
    //                   }
    //               }else{
    //                 this.display_data[az].tableObj[ab].tableRow[ac].optionObj=this.selectedParentCode;
    //                 break loop1;



    //               }
    //             }
    //           }
            
    //               //this.display_data[az].tableObj[id].tableRow[ac].optionObj=selectedChildObj1;
    //               //break loop1;
                  

    //           }else{
    //             this.display_data[az].tableObj[ab].tableRow[ac].optionObj=this.selectedParentCode;
    //             break loop1;

    //           }}
            
            
  

    //       }
  
       
  

    //     }
    //     if(az==this.display_data.length-1){
    //       //var sa1=JSON.stringify(this.display_data);
           
    //      // this.display_data=JSON.parse(sa1);




    //     }
    //   } 
    //   }
    // }
  

    // }
 

    for(var d=0;d<this.appService.subPagesNavRisk.length;d++){
      if(this.appService.subPagesNavRisk[d].isActive){
        this.navId=this.appService.subPagesNavRisk[d].id;
        
      }
    }  
    this.parentName = nfield.parentName;
    this.backLinkBool =true;
    this.backlinkField=nfield;
    if (localStorage.getItem("pageMode") == "1") {
      this.accName=[];
      for (var a = 0; a < this.display_data.length; a++) {
        if(this.display_data[a].deleteAcc != undefined){
          this.accName.push({ "id": this.display_data[a].accordId, "itemName": this.display_data[a].accordionName });
        }
      }
    }
  }
 
  backLink() { 
    
    var mpageNavigation=false;
    for(var b=0;b<this.appService.subPagesNavRisk.length;b++){
        if(this.appService.subPagesNavRisk[b].id==this.navId){ 
          mpageNavigation=true;
          this.appService.subPagesNavRisk[b].isActive=false;
          this.display_data = this.appService.changeField(this.appService.risk_coverage,{"id":this.navId},"back","","");
          
          this.parentName=this.appService.subPagesNavRisk[b].grantparentName;
          this.navId=this.appService.subPagesNavRisk[b].parentId;
          if(this.navId==""){
            this.backLinkBool=false;
          }
        }
    }
      if(!mpageNavigation){ 
        this.display_data = this.appService.changeField(this.appService.risk_coverage,{"id":this.backlinkField},"back","","");
        

      }
      if (localStorage.getItem("pageMode") == "1") {
        this.accName=[];
        for (var a = 0; a < this.display_data.length; a++) {
          if(this.display_data[a].deleteAcc != undefined){
            this.accName.push({ "id": this.display_data[a].accordId, "itemName": this.display_data[a].accordionName });
          }
        }
      }
    
   
  }

  delfield(dfield) {
    this.display_data=this.appService.changeField(this.appService.risk_coverage,dfield,"del","","");
    }

    incSize(incfield,eve) {
      this.display_data=this.appService.changeField(this.appService.risk_coverage,incfield,"inc",eve.target.parentElement.parentElement.parentElement.style.width,"");
      
    }

    decSize(decfield,eve){
      this.display_data=this.appService.changeField(this.appService.risk_coverage,decfield,"dec",eve.target.parentElement.parentElement.parentElement.style.width,"");
      
    }

    editpenAccodion(eve) {

      eve.target.previousElementSibling.firstElementChild.removeAttribute("readonly");
      eve.target.previousElementSibling.firstElementChild.focus();
      eve.target.previousElementSibling.firstElementChild.style.backgroundColor = "#9ACD32" ;
    }

    disableEditAccordion(data,eve) {
      this.display_data=this.appService.changeAcc(this.appService.risk_coverage,data,eve.target.value,"","editAcc");
      eve.target.setAttribute("readonly", true);
      eve.target.style.backgroundColor = "#FFFFFF";
    }

    delAccordion(data) {
      if (data.accordId != "bd") {
        this.display_data=this.appService.changeAcc(this.appService.risk_coverage,data,"","","delAcc");
        this.accName.push({ "id": data.accordId, "itemName": data.accordionName });
      } else {
        localStorage.setItem("alertContent", "You can't delete this Block");
  
        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '
  
        }).subscribe((isConfirmed) => {
        });
  
      }
    }

    editField(efield) {
      localStorage.setItem("editName", efield.fieldName);
      localStorage.setItem("editId", efield.id);
      localStorage.setItem("editBlockId", efield.blockId);
      localStorage.setItem("mandatory", efield.mandatory);
      localStorage.setItem("mandatoryMsg", efield.mandatoryMsg);
      localStorage.setItem("editType", efield.tagName);
      localStorage.setItem("editDisplay", efield.display);
      localStorage.setItem("editPageName", "PROD_BENEFITS");
      var xx = [];
      if(efield.mappingId){
        var fieldDetailList = [];
        for (var j = 0; j < efield.mappingId.length; j++) {
         var fieldDetail = this.appService.changeField(this.appService.risk_coverage, efield.mappingId[j], "mappedFieldDetails", "", "");
         fieldDetailList.push(fieldDetail);
        }
        localStorage.setItem("editMapping", JSON.stringify(fieldDetailList));
        localStorage.setItem("editBlockNameList", JSON.stringify(xx));
      }else{
        localStorage.setItem("editBlockNameList", JSON.stringify(this.blockName));
        localStorage.setItem("editMapping", JSON.stringify(xx));
      }
      if(efield.maxValue != ''){
        localStorage.setItem("editMaxValue", efield.maxValue);
      }else{
        localStorage.setItem("editMaxValue", "");
      }
  
      if(efield.minValue != ''){
        localStorage.setItem("editMinValue", efield.minValue);
      }else{
        localStorage.setItem("editMinValue", "");
      }
      if(efield.read){
        localStorage.setItem("editReadOnly", efield.read);
      }else{
        localStorage.setItem("editReadOnly", "false");
  
      }
      if(efield.tagValue != ''){
        localStorage.setItem("editDefaultValue", efield.tagValue);
      }else{
        localStorage.setItem("editDefaultValue", "");
      }
      var f = [];
      if (efield.tagName == 'select-one') {
  
  
        for (var i = 0; i < efield.optionObj.length; i++) {
          if (efield.optionObj[i].optionValue != "") {
            f.push({ "optionValue": efield.optionObj[i].optionValue, "optionLabel": efield.optionObj[i].optionLabel })
          }
        }
        localStorage.setItem("editSelectedItem", JSON.stringify(f));
  
      }
  
  
      this.dialogService.addDialog(EditPenComponent, {
        title: 'Name dialog',
        fquestion1: 'Item Name ',
        fquestion2: 'Item Type '
  
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {

          var edited = JSON.parse(localStorage.getItem("output"));
          this.display_data=this.appService.changeField(this.appService.risk_coverage,edited[0],"editField","","");

       
        } else {
  
        }
      })
  
    }

    editTableField(tData) {
      localStorage.setItem("editName", tData.theadName);
      localStorage.setItem("editId", tData.id);
      localStorage.setItem("editBlockId", tData.blockId);
      localStorage.setItem("mandatory", tData.mandatory);
      localStorage.setItem("mandatoryMsg", tData.mandatoryMsg);
      localStorage.setItem("editDisplay", tData.display);
      localStorage.setItem("editPageName", "PROD_BENEFITS");
      var xx = [];

      if(tData.mappingId){
        var fieldDetailList = [];
        for (var j = 0; j < tData.mappingId.length; j++) {
         var fieldDetail = this.appService.changeField(this.appService.risk_coverage, tData.mappingId[j], "mappedFieldDetails", "", "");
         fieldDetailList.push(fieldDetail);
        }
        localStorage.setItem("editMapping", JSON.stringify(fieldDetailList));
        localStorage.setItem("editBlockNameList", JSON.stringify(xx));
      }else{
        localStorage.setItem("editBlockNameList", JSON.stringify(this.blockName));
        localStorage.setItem("editMapping", JSON.stringify(xx));
      }
      if(tData.maxValue != ''){
        localStorage.setItem("editMaxValue", tData.maxValue);
      }else{
        localStorage.setItem("editMaxValue", "");
      }
if(tData.minValue != ''){
        localStorage.setItem("editMinValue", tData.minValue);
      }else{
        localStorage.setItem("editMinValue", "");
      }
      if(tData.read){
        localStorage.setItem("editReadOnly", tData.read);
      }else{
        localStorage.setItem("editReadOnly", "false");
  
      }
      if(tData.tdObj){
        if(tData.tdObj[0].tdValue != ''){
          localStorage.setItem("editDefaultValue", tData.tdObj[0].tdValue);
      }else{
        localStorage.setItem("editDefaultValue", "");
      }
    }
    var f = [];
  localStorage.setItem("editSelectedItem", JSON.stringify(f));
      if(tData.optionObj){
        localStorage.setItem("editType", 'select-one');
        for (var i = 0; i < tData.optionObj.length; i++) {
          
            f.push({ "optionValue": tData.optionObj[i].optionValue, "optionLabel": tData.optionObj[i].optionLabel })
          
          localStorage.setItem("editSelectedItem", JSON.stringify(f));

        }

      }else{
        localStorage.setItem("editType", tData.tdObj[0].tagName);

      }

      this.dialogService.addDialog(EditPenComponent, {
        title: 'Name dialog',
        fquestion1: 'Item Name ',
        fquestion2: 'Item Type '
  
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {


          var edited = JSON.parse(localStorage.getItem("output"));
          this.display_data=this.appService.changeField(this.appService.risk_coverage,edited[0],"editTableField","","");

       
  
        } else {
  
        }
      })
  
    }


    
  private onDrag(args: any): void {
    let [e, target, parent, moves] = args;
    
    if(e.className!=''){
    this.appService.dragMaintain=e.querySelector(".inputlabel").id;
    }else{
      if(document.getElementsByClassName("panel-collapse collapse in")[0]){
        // document.getElementsByClassName("panel-collapse collapse in")[0].className="panel-collapse collapse";
      }
      
    }
   

  //  this.appService.changeField(this.appService.risk_coverage,{"id":e.querySelector(".inputlabel").id},"drag","","");
    

  }



  private onDrop(args: any): void {
    let [e, target, parent, moves] = args;
    this.expand=this.dropIndex;
       if((e.className=="tc1 gu-transit" && target.className=="moTable") || (e.className=="first-box gu-transit" && target.className=="board")){
   this.display_data= this.appService.changeField(this.appService.risk_coverage, { "id": this.appService.dragMaintain }, "drag", "", "");
   if( args[3].querySelector!=null){
   this.display_data =this.appService.changeField(this.appService.risk_coverage, { "id": args[3].querySelector(".inputlabel").id }, "drop", "", "");

   }
   

}
 
//    if((e.className=="tc1 gu-transit" && target.className=="moTable") || (e.className=="first-box gu-transit" && target.className=="board")){
//    this.appService.risk_coverage= this.appService.changeField(this.appService.risk_coverage, { "id": this.appService.dragMaintain }, "drag", "", "");
//    if( args[3].querySelector!=null){
//    this.display_data =this.appService.changeField(this.appService.risk_coverage, { "id": args[3].querySelector(".inputlabel").id }, "drop", "", "");

//    }
   

// }else{

//   if(e.className=="gu-transit" && target.className=="panel-group"){
//   }else{
//    this.dragulaService.find('second-bag').drake.cancel(true);

  

//   }

// }
  //   let [e, target, parent, moves] = args;

  //   this.appService.risk_coverage=this.appService.changeField(this.appService.risk_coverage,{"id":args[3].querySelector(".inputlabel").id},"drop","","");
  //   this.display_data=[];
    
  //   setTimeout(res=>{
  //    this.display_data=this.appService.risk_coverage;
  //    this.expand=this.dropIndex;
   
  //  },200);

  }


  private onOver(args: any): void {

    let [el] = args;
  
  }

  foutc;

  private onOut(args: any): void {
    let [e, target, parent, moves] = args;

//  if((target.previousElementSibling.className=="row" && target.nextElementSibling==null)){
//  this.dragulaService.find('second-bag').drake.cancel(true);
// }
 
  }

  saveFormat(){
    swal({
      title: 'Are you sure?',
      text: "You want to save!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then(async (resultSel) => {
      if (resultSel.value) {
    this.display_data[0].TemplateId=this.appService.templateId;
    this.display_data[0].PageName=this.appService.pageName;
    var data = JSON.stringify(this.display_data);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    await this.http.post('/api/tempPage',data,{headers:headers}).toPromise()

    //comment by darong
    // this.http.post('/api/tempPage', data, { headers: headers })
    //   .subscribe(res => {
    //     setTimeout(() => {
    //       console.log(res.json());
    //     }, 1500);

    //   }, err => {
    //     console.log("err " + err);
    //   })

    var pageName = [];
    pageName.push({
      "TemplateId": this.appService.templateId,
      "TemplateName": this.appService.templateName,
      "PageName": "PROD_BENEFITS"
    });
    this.http.post('/api/tempPageNameInsert', pageName, { headers: headers })
      .subscribe(res => {
        if (res.json().review == false && this.templateApproval) {
          this.templateSaved = true;
          this.appService.templateSavedStaus=true;
        }

      }, err => {
        console.log("err " + err);
      })



    localStorage.setItem("alertContent", "Saved Successfully");

    this.dialogService.addDialog(AlertComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
    });
  }
})
  }

  addBlock() {
         localStorage.setItem("dboard", "0");
    this.dialogService.addDialog(PrompComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.display_data=this.appService.addNewBlock(this.appService.risk_coverage,"PROD_BENEFITS","RISK" + this.count++,"RISK" + this.count++);
      } else {

      }
    })
  }

  addField(field) {
    localStorage.setItem("id",field.accordId);
    localStorage.setItem("blockNameList", JSON.stringify(this.blockName));
    localStorage.setItem("pageName", "PROD_BENEFITS");

    this.dialogService.addDialog(FielddialogueComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("output"));
        this.display_data=this.appService.changeAcc(this.appService.risk_coverage,field,s,"","newField");

      } else {

      }
    })





  }

  addTableField(field) {
    localStorage.setItem("id",field.accordId);
    localStorage.setItem("blockNameList", JSON.stringify(this.blockName));
    localStorage.setItem("pageName", "PROD_BENEFITS");

    this.dialogService.addDialog(FielddialogueComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("output"));
        this.display_data=this.appService.changeAcc(this.appService.risk_coverage,field,s,"","newTableField");

      } else {

      }
    })





  }

  fieldInsert() {

    var s = JSON.parse(localStorage.getItem("output"));
    for (var i = 0; i < this.display_data.length; i++) {
      if (this.display_data[i].accordId == localStorage.getItem("id")) {
        if (localStorage.getItem("className") == "panel-body") {

          if (s[0].item_type == "text") {
            if (s[0].field_type == "Number") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "number" })

            } else if (s[0].field_type == "String") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "text" })

            } else if (s[0].field_type == "Date") {
              this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "date" })

            }

          } else if (s[0].item_type == "select") {
            this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "select-one", "tagvalue": "--Select--", "optionObj": s[0].optionObj })

          } else if (s[0].item_type == "date") {
            this.display_data[i].field.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name,"placeholder" : "Enter "+s[0].item_name, "blockId": this.display_data[i].blockId, "field_name": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tagName": "date" })

          }
        } else if (localStorage.getItem("className") == "panel-body tle") {
          if (s[0].item_type == "text") {
            if (s[0].field_type == "Number") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "number", "tdValue": "" }] })

            } else if (s[0].field_type == "Date") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "date", "tdValue": "" }] })

            } else if (s[0].field_type == "String") {
              this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "text", "tdValue": "" }] })

            }

          } else if (s[0].item_type == "select") {
            this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "optionObj": s[0].optionObj })

          } else if (s[0].item_type == "date") {
            this.display_data[i].tableObj[0].tableRow.push({ "id": this.display_data[i].accordId + "_" + s[0].item_name, "blockId": this.display_data[i].blockId, "theadName": s[0].item_name, "mandatory": s[0].mandatory, "mandatoryMsg": s[0].mandatoryMsg, "field_parentwpercentage": "23", "field_wpercentage": "", "tdObj": [{ "tagName": "date", "tdValue": "" }] })

          }
        }
      }

    }
  }


  approval() {

    var approve = [];
    approve.push({
      "TemplateId": this.appService.templateId,
      "TemplateName": this.appService.templateName,
      "PageName": "PROD_BENEFITS"
    })


    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    this.http.post('/api/tempApprove', JSON.stringify(approve[0]), { headers: headers })
      .subscribe(res => {
        localStorage.setItem("alertContent", "Approved");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.templateSaved = false;

        });

      }, err => {
        console.log("err " + err);
      })



  }

  approvalProduct() {
    var approve = [];
    approve.push({
      "ProductId": this.appService.productId,
      "ProductName": this.appService.productName,
      "PageName": "PROD_BENEFITS"
    })


    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    this.http.post('/api/prodApprove', JSON.stringify(approve[0]), { headers: headers })
      .subscribe(res => {
        localStorage.setItem("alertContent", "Approved");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.productSaved = false;
        });

      }, err => {
        console.log("err " + err);
      })
  }


  
  genRow(data) {
    // var countR = data.tableObj.length;
    var s;
    for(var i=0;i<Object.keys(data.tableObj[data.tableObj.length-1]).length;i++){
      if(Object.keys(data.tableObj[data.tableObj.length-1])[i].includes("sn")){
        s=i;
    }
    
    }
    var countR=data.tableObj[data.tableObj.length-1][Object.keys(data.tableObj[data.tableObj.length-1])[s]];


    localStorage.setItem("addRowOutpt", "");
    this.dialogService.addDialog(AddRowComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var s = JSON.parse(localStorage.getItem("addRowOutpt"));
        for(var i=0; i < s; i++){
          this.display_data = this.appService.changeAcc(this.appService.risk_coverage, data, ++countR,"", "addRow");
        }

      } else {

      }
    })
    

}

  delRow(data,eve) {

    if (eve.target.parentElement.previousElementSibling != null) {
      this.display_data=this.appService.changeAcc(this.appService.risk_coverage,data,eve.target.parentElement.id,"","delRow");

    } else {
      localStorage.setItem("alertContent", "You can't delete this Row");

      this.dialogService.addDialog(AlertComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '

      }).subscribe((isConfirmed) => {
      });
    }

  }

  textValid(eve,fieldData,nfield,ti){
    this.expand=ti;
    if (localStorage.getItem("pageMode") == "2") {
      var resultVal = true;
      if((nfield.maxLength !='')){
        var dValue=eve.target.value.length;
        var max=Number(nfield.maxLength);
    
        if(dValue>max){
          eve.target.value="";
                         swal(
                           'Error!',
                           'Value Should not exceeds maximum length of '+nfield.maxLength,
                           'error'
                         )
                   
                        }
                      }

					  
    if(nfield.tagName=='date'){
       var regx=new RegExp(/^\d{4}-\d{2}-\d{2}$/);
        if(regx.test(eve.target.value)){
         
                if(eve.target.value!=''){

		
                  this.display_data = this.appService.changeField(this.appService.risk_coverage, nfield, "setVal", eve.target.value, "");

                  for(var i=0;i<fieldData.length;i++){
      
                    if(nfield.toId){
                      
                      if(fieldData[i].id==nfield.toId){
                      var nDate=nfield;
                      var fromDate = new Date(nDate.tagValue);	
                      var toDate = new Date(fieldData[i].tagValue);
                      if(fromDate > toDate){
                      
                       nDate.tagValue="";
                       nfield.mandatory=true;
                       this.display_data = this.appService.changeField(this.appService.risk_coverage, nfield, "setVal", "", "");
                        var tChange=fieldData[i];
                        tChange.tagValue="";
                        var output = this.appService.changeField(this.appService.risk_coverage, tChange, "setVal", "", "");
                        var resultCheck=JSON.stringify(output);
                        
                        this.display_data=JSON.parse(resultCheck);
                        swal(
                          'Error!',
                          'Please Enter valid Date',
                          'error'
                        )
                         break;
                      }
                      }
                      }
                      else if(nfield.fromId){
                        if(fieldData[i].id==nfield.fromId){
                        var nDate=nfield.tagValue;
                        var toDate = new Date(nDate);	
                        var fromDate = new Date(fieldData[i].tagValue);
                        if(fromDate > toDate){
                         // alert("invalid");
                         nfield.tagValue="";
                         this.display_data = this.appService.changeField(this.appService.risk_coverage, nfield, "setVal", "", "");
                          var tChange=fieldData[i];
                          tChange.tagValue="";
                          
                          var output= this.appService.changeField(this.appService.risk_coverage, tChange, "setVal", "", "");
                                          var resultCheck=JSON.stringify(output);
                          
                                          this.display_data=JSON.parse(resultCheck);
                                          swal(
                                            'Error!',
                                            'Please Enter valid Date',
                                            'error'
                                          )
                                           break;
                        }
                        }
                        
                        }
                        if(nfield.minValue != '' || nfield.maxValue != ''){
                             resultVal = this.appService.validateNumDate(this.appService.risk_coverage, nfield, "minMaxValidateDate", Number(eve.target.value));
                            }
                  }
                //   this.display_data=this.appService.changeField(this.appService.risk_coverage,nfield,"setVal",eve.target.value,"");
                //   if(nfield.minValue != '' || nfield.maxValue != ''){
                //     resultVal = this.appService.validateNumDate(this.appService.risk_coverage, nfield, "minMaxValidateDate", Number(eve.target.value));
                //   }
                //   if(!resultVal){
                //     var output1 = this.appService.changeField(this.appService.risk_coverage, nfield, "setVal", "", "");
                //     this.display_data=output1;
                //     // setTimeout(res => {
                //     //   this.display_data = output1;
                //     // }, 200);
                //     }else{
                //   if(nfield.toId){
                //      var result =this.appService.validateField(this.appService.risk_coverage, nfield,nfield.toId,"to","date");
                //       if(result =="invalid" ){
                //         var output=this.appService.changeField(this.appService.risk_coverage,nfield,"setVal","","");
                //         this.display_data=output;
                //         // setTimeout(res=>{
                //         //   this.display_data=output;
                          
                //         // },200);
                //       }
                //     }else if(nfield.fromId){
                //       var result =this.appService.validateField(this.appService.risk_coverage, nfield,nfield.fromId,"from","date");
                //       if(result =="invalid" ){
                //         var output=this.appService.changeField(this.appService.risk_coverage,nfield,"setVal","","");
                //         this.display_data=output;
                //         // setTimeout(res=>{
                //         //   this.display_data=output;
                          
                //         // },200);
                //       }
                //     }
                // }
              }
        }else{
           eve.target.value="";
          
        }
      }else if(nfield.tagName=='number'){
        var regexp = /^[0-9]+([,.][0-9]+)?$/g;
        if(regexp.test(eve.target.value)){
          if(eve.target.value!=''){

            this.display_data = this.appService.changeField(this.appService.risk_coverage, nfield, "setVal", Number(eve.target.value), "");

            for(var i=0;i<fieldData.length;i++){

              if(nfield.toId){
                
                if(fieldData[i].id==nfield.toId){
                var nDate=nfield;
                var fromDate = new Date(nDate.tagValue);	
                var toDate = new Date(fieldData[i].tagValue);
                if(fromDate > toDate){
                
                 nDate.tagValue="";
                 nfield.mandatory=true;
                 this.display_data = this.appService.changeField(this.appService.risk_coverage, nfield, "setVal", "", "");
                  var tChange=fieldData[i];
                  tChange.tagValue="";
                  var output = this.appService.changeField(this.appService.risk_coverage, tChange, "setVal", "", "");
                  var resultCheck=JSON.stringify(output);
                  
                  this.display_data=JSON.parse(resultCheck);
                  swal(
                    'Error!',
                    'Please Enter valid number',
                    'error'
                  )
                   break;
                }
                }
                }
                else if(nfield.fromId){
                  if(fieldData[i].id==nfield.fromId){
                  var nDate=nfield.tagValue;
                  var toDate = new Date(nDate);	
                  var fromDate = new Date(fieldData[i].tagValue);
                  if(fromDate > toDate){
                   // alert("invalid");
                   nfield.tagValue="";
                   this.display_data = this.appService.changeField(this.appService.risk_coverage, nfield, "setVal", "", "");
                    var tChange=fieldData[i];
                    tChange.tagValue="";
                    
                    var output= this.appService.changeField(this.appService.risk_coverage, tChange, "setVal", "", "");
                                    var resultCheck=JSON.stringify(output);
                    
                                    this.display_data=JSON.parse(resultCheck);
                                    swal(
                                      'Error!',
                                      'Please Enter valid number',
                                      'error'
                                    )
                                     break;
                  }
                  }
                  
                  }
            }
          // this.display_data=this.appService.changeField(this.appService.risk_coverage,nfield,"setVal",Number(eve.target.value),"");
          // if(nfield.minValue != '' || nfield.maxValue != ''){
          //   resultVal = this.appService.validateNumDate(this.appService.risk_coverage, nfield, "minMaxValidateNum", Number(eve.target.value));
          // }
          // if(!resultVal){
          //   var output1 = this.appService.changeField(this.appService.risk_coverage, nfield, "setVal", "", "");
          //   this.display_data=output1;
          //   // setTimeout(res => {
          //   //   this.display_data = output1;
          //   // }, 200);
          //   }else{
          // if(nfield.toId){
          //   var result =this.appService.validateField(this.appService.risk_coverage, nfield,nfield.toId,"to","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.risk_coverage,nfield,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
                 
          //     //  },200);
          //    }
          //  }else if(nfield.fromId){
          //    var result =this.appService.validateField(this.appService.risk_coverage, nfield,nfield.fromId,"from","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.risk_coverage,nfield,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
                 
          //     //  },200);
          //    }
          //  }
          // }
        }
        }else{
          eve.target.value="";
        }
      }else{
        if(nfield.mappingId){
          for(var j=0;j<nfield.mappingId.length;j++){
            this.appService.changeField(this.appService.risk_coverage, nfield.mappingId[j], "copyValues", eve.target.value, "");
            }
          }
          if(eve.target.value!=''){
        this.display_data=this.appService.changeField(this.appService.risk_coverage,nfield,"setVal",eve.target.value,"");
          }
      
    }
    }else{
          // eve.target.value="";
     }
  }


  textTabValid(eve,tableRow,tData,tD,expanIndex){
    this.expand=expanIndex;
    //alert(expanIndex)
    if (localStorage.getItem("pageMode") == "2") {
      var resultVal = true;
      if(tD.sna != undefined){
        this.snumber[0].sna = tD.sna;
      }else if(tD.snb != undefined){
        this.snumber[1].snb = tD.snb;
        
      }else if(tD.snc != undefined){
        this.snumber[2].snc = tD.snc;
      }else if(tD.snd != undefined){
        this.snumber[3].snd = tD.snd;
      }
  	  
					  
      if((tData.maxLength !='')){
        var dValue=eve.target.value.length;
        var max=Number(tData.maxLength);
    
        if(dValue>max){
          eve.target.value="";
                         swal(
                           'Error!',
                           'Value Should not exceeds maximum length of '+tData.maxLength,
                           'error'
                         )
                   
                        }
                      }

    if(tData.tdObj[0].tagName=='date'){
       var regx=new RegExp(/^\d{4}-\d{2}-\d{2}$/);
        if(regx.test(eve.target.value)){
         
                if(eve.target.value!=''){
                  this.display_data = this.appService.changeField(this.appService.risk_coverage, tData, "setVal", eve.target.value, "");
                  for(var i=0;i<tableRow.length;i++){
    
                    if(tData.toId){
                    
                    if(tableRow[i].id==tData.toId){
                    
                    var fromDate = new Date(tData.tdObj[0].tdValue);	
                    var toDate = new Date(tableRow[i].tdObj[0].tdValue);
                    if(fromDate > toDate){
                     //alert("invalid");
                     tData.tdObj[0].tdValue="";
                     tData.mandatory=true;
                     this.display_data = this.appService.changeField(this.appService.risk_coverage, tData, "setVal", "", "");
                      var tChange=tableRow[i];
                      tChange.tdObj[0].tdValue="";
                      var output = this.appService.changeField(this.appService.risk_coverage, tChange, "setVal", "", "");
                      var resultCheck=JSON.stringify(output);
                      
                      this.display_data=JSON.parse(resultCheck);
                      swal(
                        'Error!',
                        'Please Enter valid Date',
                        'error'
                      )
                       break;
                    }
                    }
                    }else if(tData.fromId){
                    if(tableRow[i].id==tData.fromId){
                    
                    var toDate = new Date(tData.tdObj[0].tdValue);	
                    var fromDate = new Date(tableRow[i].tdObj[0].tdValue);
                    if(fromDate > toDate){
                     // alert("invalid");
                     tData.tdObj[0].tdValue="";
                     this.display_data = this.appService.changeField(this.appService.risk_coverage, tData, "setVal", "", "");
                      var tChange=tableRow[i];
                      tChange.tdObj[0].tdValue="";
                      
                      var output= this.appService.changeField(this.appService.risk_coverage, tChange, "setVal", "", "");
                                      var resultCheck=JSON.stringify(output);
                      
                                      this.display_data=JSON.parse(resultCheck);
                                      swal(
                                        'Error!',
                                        'Please Enter valid Date',
                                        'error'
                                      )
                                       break;
                    }
                    }
                    
                    }
                    }

            
                  // this.display_data=this.appService.changeField(this.appService.risk_coverage,tData,"setVal",eve.target.value,"");
                  // if(tData.minValue != '' || tData.maxValue != ''){
                  //   resultVal = this.appService.validateNumDate(this.appService.risk_coverage, tData, "minMaxValidateDate", Number(eve.target.value));
                  // }
                  // if(!resultVal){
                  //   var output1 = this.appService.changeField(this.appService.risk_coverage, tData, "setVal", "", "");
                  //   this.display_data=output1;
                  //   // setTimeout(res => {
                  //   //   this.display_data = output1;
                  //   // }, 200);
                  //   }else{
                  // if(tData.toId){
                  //    var result =this.appService.validateField(this.appService.risk_coverage, tData,tData.toId,"to","date");
                  //     if(result =="invalid" ){
                  //       var output=this.appService.changeField(this.appService.risk_coverage,tData,"setVal","","");
                  //       this.display_data=output;
                  //       // setTimeout(res=>{
                  //       //   this.display_data=output;
                          
                  //       // },200);
                  //     }
                  //   }else if(tData.fromId){
                  //     var result =this.appService.validateField(this.appService.risk_coverage, tData,tData.fromId,"from","date");
                  //     if(result =="invalid" ){
                  //       var output=this.appService.changeField(this.appService.risk_coverage,tData,"setVal","","");
                  //       this.display_data=output;
                  //       // setTimeout(res=>{
                  //       //   this.display_data=output;
                          
                  //       // },200);
                  //     }
                  //   }
                  // }
          }
        }else{
           eve.target.value="";
          
        }
      }else if(tData.tdObj[0].tagName=='number'){
        var regexp = /^[0-9]+([,.][0-9]+)?$/g;
        if(regexp.test(eve.target.value)){
          if(eve.target.value!=''){
            this.display_data = this.appService.changeField(this.appService.risk_coverage, tData, "setVal", Number(eve.target.value), "");
            for(var i=0;i<tableRow.length;i++){

              if(tData.toId){
              
              if(tableRow[i].id==tData.toId){
              
              var fromDate = new Date(tData.tdObj[0].tdValue);	
              var toDate = new Date(tableRow[i].tdObj[0].tdValue);
              if(fromDate > toDate){
               //alert("invalid");
               tData.tdObj[0].tdValue="";
               tData.mandatory=true;
               this.display_data = this.appService.changeField(this.appService.risk_coverage, tData, "setVal", "", "");
                var tChange=tableRow[i];
                tChange.tdObj[0].tdValue="";
                var output = this.appService.changeField(this.appService.risk_coverage, tChange, "setVal", "", "");
                var resultCheck=JSON.stringify(output);
                
                this.display_data=JSON.parse(resultCheck);
                swal(
                  'Error!',
                  'Please Enter valid Number',
                  'error'
                )
                this.expand=expanIndex;
                break;
              }
              }
              }else if(tData.fromId){
              if(tableRow[i].id==tData.fromId){
              
              var toDate = new Date(tData.tdObj[0].tdValue);	
              var fromDate = new Date(tableRow[i].tdObj[0].tdValue);
              if(fromDate > toDate){
               // alert("invalid");
               tData.tdObj[0].tdValue="";
               this.display_data = this.appService.changeField(this.appService.risk_coverage, tData, "setVal", "", "");
                var tChange=tableRow[i];
                tChange.tdObj[0].tdValue="";
                
                var output= this.appService.changeField(this.appService.risk_coverage, tChange, "setVal", "", "");
                                var resultCheck=JSON.stringify(output);
                
                                this.display_data=JSON.parse(resultCheck);
                                swal(
                                  'Error!',
                                  'Please Enter valid Number',
                                  'error'
                                )
                                this.expand=expanIndex;

                                 break;
              }
              }
              
              }
        
              }
              
      
          // this.display_data=this.appService.changeField(this.appService.risk_coverage,tData,"setVal",Number(eve.target.value),"");
          // if(tData.minValue != '' || tData.maxValue != ''){
          //   resultVal = this.appService.validateNumDate(this.appService.risk_coverage, tData, "minMaxValidateNum", Number(eve.target.value));
          // }
          // if(!resultVal){
          //   var output1 = this.appService.changeField(this.appService.risk_coverage, tData, "setVal", "", "");
          //   this.display_data=output1;
          //   // setTimeout(res => {
          //   //   this.display_data = output1;
          //   // }, 200);
          //   }else{
          // if(tData.toId){
          //   var result =this.appService.validateField(this.appService.risk_coverage, tData,tData.toId,"to","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.risk_coverage,tData,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
                 
          //     //  },200);
          //    }
          //  }else if(tData.fromId){
          //    var result =this.appService.validateField(this.appService.risk_coverage, tData,tData.fromId,"from","num");
          //    if(result =="invalid" ){
          //      var output=this.appService.changeField(this.appService.risk_coverage,tData,"setVal","","");
          //      this.display_data=output;
          //     //  setTimeout(res=>{
          //     //    this.display_data=output;
                 
          //     //  },200);
          //    }
          //  }
          // }
        }
        }else{
          eve.target.value="";
        }
      }
    
      else{
        if(tData.mappingId){
          for(var j=0;j<tData.mappingId.length;j++){
            this.appService.changeField(this.appService.risk_coverage, tData.mappingId[j], "copyValues", eve.target.value, "");
            }
          }
          if(eve.target.value!=''){
        this.display_data=this.appService.changeField(this.appService.risk_coverage,tData,"setVal",eve.target.value,"");
          }
      }
    
    }else{
          // eve.target.value="";
     }
  }
  
  
  setVal(field,eve,tD){
    if (localStorage.getItem("pageMode") == "2") {
      
      if(tD.sna != undefined){
        this.snumber[0].sna = tD.sna;
      }else if(tD.snb != undefined){
        this.snumber[1].snb = tD.snb;
        
      }else if(tD.snc != undefined){
        this.snumber[2].snc = tD.snc;
      }else if(tD.snd != undefined){
        this.snumber[3].snd = tD.snd;
      }


    if(field.displayAccId){
      for(var l=0;l<field.displayAccId.length;l++){
        if(eve.target.value == "Y"){
          this.appService.changeAcc(this.appService.risk_coverage, field.displayAccId[l], false ,"", "displayAcc");
      }else{
        this.appService.changeAcc(this.appService.risk_coverage, field.displayAccId[l] , true , "","displayAcc");
        
      } 
    }
    }
    
    if(field.navBtnId){
      for(var k=0;k<field.navBtnId.length;k++){
        if(eve.target.value == "Y"){
          this.appService.changeField(this.appService.risk_coverage, field.navBtnId[k] , "BtnValidation", false,  "");
      }else{
        this.appService.changeField(this.appService.risk_coverage, field.navBtnId[k] , "BtnValidation", true,  "");
        
      }

      if(eve.target.value=="05"){
        this.appService.changeField(this.appService.risk_coverage, field.navBtnId[k] , "BtnValidation", false,  "");
      }
    }
  }
            
      if(field.mappingId){
        for(var j=0;j<field.mappingId.length;j++){ 
          this.appService.changeFieldMappingRisk(this.appService.risk_coverage, field.mappingId[j], "copyValues", eve.target.value, "",tD);
          }
        }

      

      for(var i=0;i<field.optionObj.length;i++){
        if(field.optionObj[i].optionValue==eve.target.value){
          field.optionObj[i].selected=true;
        }else{
          field.optionObj[i].selected=false;
        }
        if(i==field.optionObj.length-1){
          this.display_data=this.appService.changeField(this.appService.risk_coverage,field,"setVal","","");
          
       
      //     if(field.id=='parentProp_bpc_installmenttype'){
      //       if(eve.target.value == "N"){
      //         this.appService.changeAcc(this.appService.risk_coverage, { "accordId": "parentProp_ei"}, false ,"", "displayAcc");
              

      //     }else if(eve.target.value == "Y"){{
      //       this.appService.changeAcc(this.appService.risk_coverage, { "accordId": "parentProp_ei"} , true , "","displayAcc");
            
      //     }
           
      //     }
          
      //   }
      //   if(field.id=='childProp_bpc_installmenttype'){
      //     if(eve.target.value == "N"){
      //       this.appService.changeAcc(this.appService.risk_coverage, { "accordId": "childProp_ei"}, false ,"", "displayAcc");
      //   }else if(eve.target.value == "Y"){{
      //     this.appService.changeAcc(this.appService.risk_coverage, { "accordId": "childProp_ei"} , true , "","displayAcc");
          
      //   }
         
      //   }
        
      // }
      
      }
      }


      
     





  
  }
  
  }

  assPlanCode(field,eve,tD){
    for (var ma = 0; ma < this.appService.risk_coverage.length; ma++) {

      if (this.appService.risk_coverage[ma].accordId == 'jld') {

        if (this.appService.risk_coverage[ma].tableObj) {
          for (var pa = 0; pa < this.appService.risk_coverage[ma].tableObj.length; pa++) {
            for (var qa = 0; qa < this.appService.risk_coverage[ma].tableObj[pa].tableRow.length; qa++) {
              if (this.appService.risk_coverage[ma].tableObj[pa].tableRow[qa].id == 'jld_eventcode') {
                this.appService.risk_coverage[ma].tableObj[pa].tableRow[qa].optionObj=[];
                
                this.appService.risk_coverage[ma].tableObj[pa].tableRow[qa].optionObj = this.selectedParentCode;


              }

            }
          }
        }
      }
     
    }
  }


  assignParentCode(field,eve,tD){
    if (localStorage.getItem("pageMode") == "2") {
      
      if(tD.sna != undefined){
        this.snumber[0].sna = tD.sna;
      }else if(tD.snb != undefined){
        this.snumber[1].snb = tD.snb;
        
      }else if(tD.snc != undefined){
        this.snumber[2].snc = tD.snc;
      }else if(tD.snd != undefined){
        this.snumber[3].snd = tD.snd;
      }



      if (field.id == "jld_eventcode") {
        var selectedParentCode = [];
        var selectedJointLifeEventCode;
        for (var i = 0; i < field.optionObj.length; i++) {
          if (field.optionObj[i].selected) {
            selectedJointLifeEventCode = field.optionObj[i].optionValue;

          }
          if (i == field.optionObj.length - 1) {

         

            for (var m = 0; m < this.appService.risk_coverage.length; m++) {
              if (this.appService.risk_coverage[m].accordId == 'pe') {

                if (this.appService.risk_coverage[m].tableObj) {
                  for (var p = 0; p < this.appService.risk_coverage[m].tableObj.length; p++) {
                    for (var q = 0; q < this.appService.risk_coverage[m].tableObj[p].tableRow.length; q++) {
                      if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].id == 'pe_parenteventcode') {
                        console.log(2)
                        for(var r=0;r<this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj.length;r++){
                          if (this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj[r].selected) {
                            selectedParentCode.push({ "optionValue": this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj[r].optionValue, "optionLabel": this.appService.risk_coverage[m].tableObj[p].tableRow[q].optionObj[r].optionLabel, "selected": false });


                          }

                        }
                        }


                      }

                    }
                  }
                }
              
              if (m == this.appService.risk_coverage.length - 1) {
                if (selectedParentCode.length > 0) {

                  for (var ma = 0; ma < this.appService.risk_coverage.length; ma++) {
                    if (this.appService.risk_coverage[ma].accordId == 'jld') {

                      if (this.appService.risk_coverage[ma].tableObj) {
                        for (var p = 0; p < this.appService.risk_coverage[ma].tableObj.length; p++) {
                          for (var q = 0; q < this.appService.risk_coverage[ma].tableObj[p].tableRow.length; q++) {
                            if (this.appService.risk_coverage[ma].tableObj[p].tableRow[q].id == 'jld_eventcode') {
                              this.appService.risk_coverage[ma].tableObj[p].tableRow[q].optionObj = selectedParentCode;


                            }

                          }
                        }
                      }
                    }
                  }



                } else {
                  
                  for (var mb = 0; mb < this.appService.risk_coverage.length; mb++) {
                    if (this.appService.risk_coverage[mb].accordId == 'jld') {

                      if (this.appService.risk_coverage[mb].tableObj) {
                        for (var p = 0; p < this.appService.risk_coverage[mb].tableObj.length; p++) {
                          for (var q = 0; q < this.appService.risk_coverage[mb].tableObj[p].tableRow.length; q++) {
                            if (this.appService.risk_coverage[mb].tableObj[p].tableRow[q].id == 'jld_eventcode') {
                             
                                this.appService.risk_coverage[mb].tableObj[p].tableRow[q].optionObj=this.jointLifeCode;
                              
                              
                              


                            }

                          
                        }
                      }
                    }
                   
                  }
                }}
                  

                }


              }


            }

          }

    }
  }
}
  
  saveValues(pinfo:NgForm){
    swal({
      title: 'Are you sure?',
      text: "You want to save!",
      type: 'warning',
      width: 567,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((resultSel) => {
      if (resultSel.value) {
    setTimeout(()=>{

      this.spinner.show();
    },50)
        this.appService.validFieldCount(this.appService.risk_coverage);

       var headers = new Headers();
      headers.append('Content-Type', 'application/json');
        var dataContent = [];
      dataContent.push({ "ProductId": this.appService.productId, "PageName": "PROD_BENEFITS.json", "PageContent": this.appService.risk_coverage});

      this.http.post('/api/dataTmpPage', dataContent, { headers: headers })
        .subscribe(res => {
          setTimeout(() => {
            console.log(res.json());
          }, 1500);

        }, err => {
          console.log("err " + err);
        })



    if(pinfo.valid){
      var output=this.appService.validPage(this.appService.risk_coverage);
      if(output.mandatory){
        this.alertMsg=[];
        for(var i=0;i<output.mandatoryFields.length;i++){
            this.alertMsg.push({ "msg":output.mandatoryFields[i].fieldName+' ('+output.mandatoryFields[i].accordionName+')'});

        }
          localStorage.setItem("alertContent",'');
          localStorage.setItem("prodAlertMsg", JSON.stringify(this.alertMsg));
          setTimeout(()=>{

            this.spinner.hide();
          },100)
          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '

          }).subscribe((isConfirmed) => {
            
            localStorage.setItem("prodAlertMsg", "");
          });
      }else{
          var result=this.storeValuesToDb();
          // var tabAccFormat = [{"accId": "pugl" , "tableName" : "gnmmPlanUserGroupLinks"},
          // {"accId": "puhl" , "tableName" : "gnmmPlanCountryHospLinks"},
          // {"accId": "vl" , "tableName" : "gnmmProductLimits"},
          // {"accId": "vertBene_bd" , "tableName" : "gnmmLimitBenefitLinksChild"},
          // {"accId": "vertInc_il" , "tableName" : "gnmmProductIncrLimits"},
          // {"accId": "cils" , "tableName" : "gnmmPlanCoinSetups"},
          // {"accId": "jld" , "tableName" : "gnmmPlanJointLifes"},
          // {"accId": "jointBene_bt" , "tableName" : "eventPayableLinksN"},
          // {"accId": "jointLien_lt" , "tableName" : "gnmmPlanLienLinksN"},
          // {"accId": "coInGrp_bd" , "tableName" : "gnmmPlanCoinGroups"},
          // {"accId": "pe" , "tableName" : "gpmForEventLinkForParent"},
          // {"accId": "parentProp_pode" , "tableName" : "gpmForEventLinkForParent"}, 
          // {"accId": "parentProp_cp" , "tableName" : "gpmForEventLinkForParent"}, 
          // {"accId": "parentProp_icpp" , "tableName" : "gpmForEventLinkForParent"}, 
          // {"accId": "parentProp_bpc" , "tableName" : "gpmForEventLinkForParent"}, 
          // {"accId": "parentProp_bt" , "tableName" : "eventPayableLinks"},
          // {"accId": "parentProp_rf" , "tableName" : "gnmmRenewalFactors"},
          // {"accId": "parentProp_psuc" , "tableName" : "gnmmStatusSetupLinks"},
          // {"accId": "parentProp_hl" , "tableName" : "gnmmPlanEventLimits"},
          // {"accId": "parentProp_ei" , "tableName" : "gnmmPlanEventInstLinks"},
          // {"accId": "parentProp_ncb" , "tableName" : "gnmmPlanEventNcbs"},
          // {"accId": "parentDoc_cdr" , "tableName" : "gnmmPlanEventDocuments"},
          // {"accId": "parentEcce_cce" , "tableName" : "gnmmPlanCauseExcels"},
          // {"accId": "ce" , "tableName" : "gpmForEventLinkForChild"},
          // {"accId": "childProp_pode" , "tableName" : "gpmForEventLinkForChild"},
          // {"accId": "childProp_cp" , "tableName" : "gpmForEventLinkForChild"},
          // {"accId": "childProp_bpc" , "tableName" : "gpmForEventLinkForChild"},
          // {"accId": "childProp_icpp" , "tableName" : "gpmForEventLinkForChild"},
          // {"accId": "childProp_bt" , "tableName" : "eventPayableLinksCh"},
          // {"accId": "childProp_rf" , "tableName" : "gnmmRenewalFactorsChild"},
          // {"accId": "childProp_psuc" , "tableName" : "gnmmStatusSetupLinksCh"},
          // {"accId": "childProp_hl" , "tableName" : "gnmmPlanEventLimitsCh"},
          // {"accId": "childProp_ei" , "tableName" : "gnmmPlanEventInstLinksChild"},
          // {"accId": "childProp_ncb" , "tableName" : "gnmmPlanEventNcbsChild"},
          // {"accId": "childDoc_cdr" , "tableName" : "gnmmPlanEventDocumentsChild"},
          // {"accId": "childEcce_cce" , "tableName" : "gnmmPlanCauseExcelsChild"},
          // {"accId": "jointPol_psuc" , "tableName" : "gnmmStatusSetupLinksN"},
          // {"accId": "death_se" , "tableName" : "gnmtPlanSubEventsLinks"},
          // {"accId": "parentbenefit_se" , "tableName" : "gnmmPlanLienLinks"},
          // {"accId": "childDeath_se" , "tableName" : "gnmtPlanSubEventsLinksCh"},
          // {"accId": "parentProp_aed" , "tableName" : "gnmmEventLimitBenefitLink"},
          // {"accId": "childProp_aed" , "tableName" : "gnmmEventLimitBenefitLinkChh"},
          // {"accId": "jointLien_lt_child" , "tableName" : "gnmmPlanLienLinksCh"}];
          var tabAccFormat=tabAccRiskFormat;
          
          this.appService.saveValuesRIskCoverage(this.appService.risk_coverage,result,tabAccFormat)
          .subscribe(res=>{
            setTimeout(()=>{
              this.spinner.hide();
            },200)
            
            if(res==true && this.productApproval){
              this.productSaved = true;
              this.appService.productSaved = false;
              localStorage.setItem("alertContent", "Saved Successfully");
        
              this.dialogService.addDialog(AlertComponent, { 
                title: 'Name dialog',
                question1: 'Block Name ',
                question2: 'Block Type '
        
              }).subscribe((isConfirmed) => {
                
        
              });
            }else{
              localStorage.setItem("alertContent", "Saved Successfully");
        
              this.dialogService.addDialog(AlertComponent, { 
                title: 'Name dialog',
                question1: 'Block Name ',
                question2: 'Block Type '
        
              }).subscribe((isConfirmed) => {
                
        
              });
            }
          })

      }
    }
  }
})
  }

  

  storeValuesToDb(){
        
      
    var format = {
      "planCode":null,
      "piName":null,
      "piNameInLocal":null,
      "piDescription":null,
      "productType":null,
      "piStartDate":null,
      "piEndDate":null,
      "piProductLine":null,
      "pfTpaindicato":null,
      "pfCoinsurance":null,
      "pfWopprovisiontype":null,
      "pfClaimcash":null,
      "pfCopayment":null,
      "pfMaturityredemption":null,
      "pfSumassured":null,
      "pfCancerProduct":null,
      "pfNcdapplicable":null,
      "pfPaymentamount":null,
      "pfDuedays":null,
      "pfDeductableamount":null,
      "pfInsuranceterminate":null,
      "pfGuranteeletter":null,
     
     
    
     
      "gnmmPlanUserGroupLinks":[
         {
            "puglPlanCode":null,
            "puglUserGroup":null,
            "puglStatus":null
         }
      ],
      "gnmmPlanCountryHospLinks":[  
         {
            "puhlPlanCode":null,
            "puhlArea":null,
            "puhlCountry":null,
            "puhlStatus":null
         }
      ],
      "gnmmProductLimits":[
         {
            "vlPlanCode":null,
            "vlLimitCode":null,
            "vlLimitType":null,
            "vlLimitFrequency":null,
            "vlPriority":null,
            "vlFreq":null,
            "vlNoOfTimesFreq":null,
            "vlServiceTaxApplicable":null,
            "vlPayCriteria":null,
            "vlFactorAmount":null,
            "vlFixedAmt":null,
            "vlTermination":null,
            "vlLimitApplicable":null,
            "gnmmProductIncrLimits":[
              {
                 "vertIncilplancode":null,
                 "vertIncillimitcode":null,
                 "vertIncilincfreq":null,
                 "vertIncilclaimallowed":null,
                 "vertIncilrate":null,
                 "vertIncilmaxamt":null,
                 "vertIncilmaxnooftime":null,
                 "vertIncilstatus":null
              }
           ],
           "gnmmLimitBenefitLinksChild":[
            {
               "vertBenebdplancode":null,
               "vertBenebdlimitcode":null,
               "vertBenebdeventcode":null,
               "vertBenebdstatus":null
            }
         ]
         } 
      ],
     
      "gnmmPlanCoinGroups":[
         {
            "coInGrpbdplancode":null,
            "coInGrpbdgroupid":null,
            "coInGrpbdeventcode":null,
            "coInGrpbdstatus":null
         }
      ],
      "gnmmPlanCoinSetups":[
         {
            "cilsPlanCode":null,
            "cilsFrom":null,
            "cilsTo":null,
            "cilsBenefitsGroup":null,
            "cilsMinimumValue":null,
            "cilsMaximumValue":null,
            "cilsPercentage":null,
            "cilsStatus":null,
            "cilsDeductFlag":null
         }
      ],
      "gnmmPlanJointLifes":[
         {
            "jldPlanCode":null,
            "jldLifeNumber":null,
            "jldEventCode":null,
            "jldOutPreRecovery":null,
            
      "gnmmStatusSetupLinksN":[
        {
           "jointPolpsucplancode":null,
           "jointPolpsuceventcode":null,
           "jointPolpsucparenteventcode":null,
           "jointPolpsucstatusupon":null,
           "jointPolpsucstatusvalue":null,
           "jointPolpsuctermtype":null,
           "jointPolpsucstatus":null,
           "jointPolpsuctermflag":null,
           "jointPolpsuclifeno":null,
           "jointPolpsucjlind":null,
           "jointPolpsucmaxlimit":null
        }
     ],
            "eventPayableLinksN":[
              { 
                 "jointBenebtplancode":null,
                 "jointBenebteventcode":null,
                 "jointBenebtparenteventcode":null,
                 "jointBenebtpaytype":null,
                 "jointBenebtloading":null,
                 "jointBenebtdiscount":null,
                 "jointBenebtrider":null,
                 "jointBenebtlianapplicablity":null,
                 "jointBenebtstatus":null,
                 "jointBenebtcommisionreverse":null,
                 "gnmmPlanLienLinksN":[
                  {
                     "jointLienltplancode":null,
                     "jointLienlteventcode":null,
                     "jointLienltlientype":null,
                     "jointLienltageevfrom":null,
                     "jointLienltageevto":null,
                     "jointLienltentryFrm":null,
                     "jointLienltentryTo":null,
                     "jointLienltpolyrfrom":null,
                     "jointLienltpolyrto":null,
                     "jointLienltclaimyrfrom":null,
                     "jointLienltclaimyrto":null,
                     "jointLienlttermfrom":null,
                     "jointLienlttermto":null, 
                     "jointLienltrates":null,
                     "jointLienltunits":null,
                     "jointLienltpolicytype":null,
                     "jointLienltlifeno":null,
                     "jointLienltjli":null,
                     "jointLienltamtpay":null,
                     "jointLienltstatus":null
                  }
               ]
              }
           ]
         }
      ],
      "gpmForEventLinkForParent":[
         {
            "lastupdUser":null,
            "lastupdProg":null,
            "pePlancode":null,
            "peEventcode":null,
            "peStartdate":null,
            "peEnddate":null,
            "peParenteventcode":null,
            "lastupdInftim":null,
            "parentPropPodeMimimumage":null,
            "parentPropPodeMaximumage":null,
            "parentPropPodeCoverageagefrom":null,
            "parentPropPodeCoverageageto":null,
            "parentPropPodePaycriteria":null,
            "parentPropPodeFactoramount":null,
            "parentPropPodeCievent":null,
            "parentPropPodeFullreimbursement":null,
            "parentPropPodeFrequency":null,
            "parentPropPodeMaxtimesfrequency":null,
            "parentPropPodeUnits":null,
            "parentPropPodeFixedmaxamount":null,
            "parentPropPodeCashreimbursementtype":null,
            "parentPropPodeClaimsallowed":null,
            "parentPropPodeBeneficiariesallowed":null,
            "parentPropPodeSubeventsapplicablity":null,
            "parentProppodeclaimtype":null,
            "parentProppodeclaimspayto":null,
            "parentPropcpreducesa":null,
            "parentPropcpfuturepremium":null,
            "parentPropcpreducepremium":null,
            "parentPropcpterminateriders":null,
            "parentPropbpcreducesa":null,
            "parentPropbpcfuturepremium":null,
            "parentPropbpcreducepremium":null,
            "parentPropbpcwaitingperiod":null,
            "parentPropbpcseperiationperiodself":null,
            "parentPropbpcseperationperdependent":null,
            "parentPropbpcconstestableperiod":null,
            "parentPropbpcdisablityperiod":null,
            "parentPropbpcwarnstop":null,
            "parentPropbpcsurivalperiod":null,
            "parentPropbpcsucideperiod":null,
            "parentPropbpchospdetflag":null,
            "parentPropbpccopaymentflag":null,
            "parentPropbpcphabletterperiod":null,
            "parentPropbpcinstallmenttype":null,
            "parentPropbpcmaxinstallage":null,
            "parentPropbpcinstagedepon":null,
            "parentPropbpcmaxriage":null,
            "parentPropbpcinstpayage":null,
            "parentPropbpcinstpaycriteria":null,
            "parentPropbpcregclaim":null,
            "parentPropbpcmatprocess":null,
            "parentPropbpcetipolicy":null,
            "parentPropbpcpaiduppolicy":null,
            "parentPropbpcfullypaidup":null,
            "parentPropbpcregforce":null,
            "parentPropbpcintapplicable":null,
            "parentPropbpcuwcriteria":null,
            "parentPropbpcpremiumage":null,
            "parentPropbpcloadindicator":null,
            "parentPropbpcpreloadindicator":null,
            "parentPropbpcoutprerec":null,
            "parentPropbpcriapplicable":null,
            "parentPropbpcrepreapplicable":null,
            "parentPropbpcriskcat":null,
            "parentPropbpcpriority":null,
            "parentPropbpcaveragefundvalues":null,
            "parentPropbpcclaimrec":null,
            "parentPropbpcdeferrisk":null,
            "parentPropbpcdedclaimamt":null,
            "parentPropbpcclaimbentype":null,
            "parentPropbpcaddclaimben":null,
            "parentPropicppfreq":null,
            "parentPropicppnooftimesfreq":null,
            "parentPropicpppaycriteria":null,
            "parentPropicppfactor":null,
            "parentPropicppstatus":null,
            "parentPropicppncbapplicable":null,
            "parenticppCoinsPer":null,
            "parenticcpDedAmt":null,
            "parenticcpMinSarFactor":null,
            "parenticcpMaxSarFactor":null,
            "parenticcpAcctCode":null,
            "gnmmPlanEventInstLinks":[
              {
                 "parentPropeiplancode":null,
                 "parentPropeieventcode":null, 
                 "parentPropeiparenteventcode":null,
                 "parentPropeistartmonth":null,
                 "parentPropeiendmonth":null, 
                 "parentPropeifreq":null,
                 "parentPropeipaymenytamtrper":null,
                 "parentPropeiamtpercentage":null,
                 "parentPropeiinstamttype":null,
                 "parentPropeiinstendcriteria":null,
                 "parentPropeiannicriteria":null,
                 "parentPropeiamtfactper":null,
                 "parentPropeiperinmonths":null
              }
           ],
           "gnmmPlanEventNcbs":[
            {
               "parentPropncbplancode":null,
               "parentPropncbeventcode":null,
               "parentPropncbincfreq":null,
               "parentPropncbclaimallowed":null,
               "parentPropncbrateper":null,
               "parentPropncbmaxamt":null,
               "parentPropncbstatus":null
            }
         ],
         "gnmmRenewalFactors":[
          {
             "parentProprfplancode":null,
             "parentProprfeventcode":null,
             "parentProprfparenteventcode":null,
             "parentProprfentryageform":null,
             "parentProprfentryageto":null,
             "parentProprfrenewalagefrom":null,
             "parentProprfrenewalageto":null,
             "parentProprfprecalmethod":null,
             "parentProprfsafactor":null,
             "parentProprfminsumassured":null,
             "parentProprfmaxsumassured":null,
             "parentProprfstatus":null
          }
       ],
   "gpmForEventLinkForChild":[
         {
            "lastupdUser":null,
            "lastupdProg":null,
            "lastupdInftim":null,
            "cePlancode":null,
            "ceEventcode":null,
            "ceParenteventcode":null,
            "ceStartdate":null,
            "ceEnddate":null,
            "childPropPodeMimimumage":null,
            "childPropPodeMaximumage":null,
            "childPropPodeCoverageagefrom":null,
            "childPropPodeCoverageageto":null,
            "childPropPodePaycriteria":null,
            "childPropPodeFactoramount":null,
            "childPropPodeCievent":null,
            "childPropPodeFullreimbursement":null,
            "childPropPodeFrequency":null,
            "childPropPodeMaxtimesfrequency":null,
            "childPropPodeUnits":null,
            "childPropPodeFixedmaxamount":null,
            "childPropPodeCashreimbursementtype":null,
            "childPropPodeClaimsallowed":null,
            "childPropPodeBeneficiariesallowed":null,
            "childPropPodeSubeventsapplicablity":null,
            "childProppodeclaimspayto":null,
            "childProppodeclaimtype":null,
            "childPropcpreducesa":null,
            "childPropcpfuturepremium":null,
            "childPropcpreducepremium":null,
            "childPropcpterminateriders":null,
            "childPropbpcreducesa":null,
            "childPropbpcfuturepremium":null,
            "childPropbpcreducepremium":null,
            "childPropbpcwaitingperiod":null,
            "childPropbpcseperiationperiodself":null,
            "childPropbpcseperationperdependent":null,
            "childPropbpcconstestableperiod":null,
            "childPropbpcdisablityperiod":null,
            "childPropbpcwarnstop":null,
            "childPropbpcsurivalperiod":null,
            "childPropbpcsucideperiod":null,
            "childPropbpchospdetflag":null,
            "childPropbpccopaymentflag":null,
            "childPropbpcphabletterperiod":null,
            "childPropbpcinstallmenttype":null,
            "childPropbpcmaxinstallage":null,
            "childPropbpcinstagedepon":null,
            "childPropbpcmaxriage":null,
            "childPropbpcinstpayage":null,
            "childPropbpcinstpaycriteria":null,
            "childPropbpcregclaim":null,
            "childPropbpcmatprocess":null,
            "childPropbpcetipolicy":null,
            "childPropbpcpaiduppolicy":null,
            "childPropbpcfullypaidup":null,
            "childPropbpcregforce":null,
            "childPropbpcintapplicable":null,
            "childPropbpcuwcriteria":null,
            "childPropbpcpremiumage":null,
            "childPropbpcloadindicator":null,
            "childPropbpcpreloadindicator":null,
            "childPropbpcoutprerec":null,
            "childPropbpcriapplicable":null,
            "childPropbpcrepreapplicable":null,
            "childPropbpcriskcat":null,
            "childPropbpcpriority":null,
            "childPropbpcaveragefundvalues":null,
            "childPropbpcclaimrec":null,
            "childPropbpcdeferrisk":null,
            "childPropbpcdedclaimamt":null,
            "childPropbpcclaimbentype":null,
            "childPropbpcaddclaimben":null,
            "childPropicppfreq":null,
            "childPropicppnooftimesfreq":null,
            "childPropicpppaycriteria":null,
            "childPropicppfactor":null,
            "childPropicppstatus":null,
            "childPropicppncbapplicable":null,
            "childicppCoinsPer":null,
            "childiccpDedAmt":null,
            "childiccpMinSarFactor":null,
            "childiccpMaxSarFactor":null,
            "childiccpAcctCode":null,
            "gnmmRenewalFactorsChild":[
              {
                 "childProprfplancode":null,
                 "childProprfeventcode":null,
                 "childProprfparenteventcode":null,
                 "childProprfentryageform":null,
                 "childProprfentryageto":null,
                 "childProprfrenewalagefrom":null,
                 "childProprfrenewalageto":null,
                 "childProprfprecalmethod":null,
                 "childProprfsafactor":null,
                 "childProprfminsumassured":null,
                 "childProprfmaxsumassured":null,
                 "childProprfstatus":null
              }
           ],
           "gnmmPlanEventInstLinksChild":[
              {
                 "childPropeiplancode":null,
                 "childPropeieventcode":null,
                 "childPropeiparenteventcode":null,
                 "childPropeistartmonth":null,
                 "childPropeiendmonth":null,
                 "childPropeifreq":null,
                 "childPropeipaymenytamtrper":null,
                 "childPropeiamtpercentage":null,
                 "childPropeiinstamttype":null,
                 "childPropeiinstendcriteria":null,
                 "childPropeiannicriteria":null,
                 "childPropeiamtfactper":null,
                 "childPropeiperinmonths":null
              }
           ],
           "gnmmPlanEventNcbsChild":[
              {
                 "childPropncbplancode":null,
                 "childPropncbeventcode":null,
                 "childPropncbincfreq":null,
                 "childPropncbclaimallowed":null,
                 "childPropncbrateper":null,
                 "childPropncbmaxamt":null,
                 "childPropncbstatus":null
              }
           ],
            "gnmmPlanEventDocumentsChild":[
             {
                "childDocCdrPlancode":null,
                "childDocCdrEventcode":null,
                "childDocCdrParenteventcode":null,
                "childDocCdrPlanriderflag":null,
                "childDocCdrDocumentcode":null,
                "childDocCdrStatus":null
             }
          ],
          "gnmmPlanCauseExcelsChild":[
             {
                "childEcceCcePlancode":null,
                "childEcceCceEventcode":null,
                "childEcceCceParenteventcode":null,
                "childEcceCceCausecode":null,
                "childEcceCceStatus":null
             }
          ],
            "eventPayableLinksCh":[
               {
                  "childPropbtplancode":null,
                  "childPropbteventcode":null,
                  "childPropbtparenteventcode":null,
                  "childPropbtpaytype":null,
                  "childPropbtloading":null,
                  "childPropbtdiscount":null,
                  "childPropbtrider":null,
                  "childPropbtlienapplicable":null,
                  "childPropbtstatus":null,
                  "childPropbtcommreverse":null,
                  "gnmmPlanLienLinksCh":[
                   {
                      "jointLienltchildplancode":null,
                      "jointLienltchildeventcode":null,
                      "jointLienltchildlientype":null,
                      "jointLienltchildageevfrom":null,
                      "jointLienltchildageevto":null, 
                      "jointLienltageentryfrom":null,
                      "jointLienltageentryto":null,
                      "jointLienltchildpolyrfrom":null,
                      "jointLienltchildpolyrto":null,
                      "jointLienltchildclaimyrfrom":null,
                      "jointLienltchildclaimyrto":null,
                      "jointLienltchildtermfrom":null,
                      "jointLienltchildtermto":null,
                      "jointLienltchildrates":null,
                      "jointLienltchildunits":null,
                      "jointLienltchildpolicytype":null,
                      "jointLienltchildlifeno":null,
                      "jointLienltchildjli":null,
                      "jointLienltchildamtpay":null,
                      "jointLienltchildstatus":null
                   }
                ]
               }
            ],
            "gnmtPlanSubEventsLinksCh":[
               {
                  "childdeathSePlancode":null,
                  "childdeathSeParenteventcode":null,
                  "childdeathSeEventcode":null,
                  "childdeathSeSubeventcode":null,
                  "childdeathSePaycriteria":null,
                  "childdeathSeAmountpayable":null,
                  "childdeathSeStatus":null
               }
            ],
            "gnmmStatusSetupLinksCh":[
               {
                  "childProppsucplancode":null,
                  "childProppsuceventcode":null,
                  "childProppsucparenteventcode":null,
                  "childProppsucstatusupon":null,
                  "childProppsucstatusvalue":null,
                  "childProppsuctermtype":null,
                  "childProppsucstatus":null,
                  "childProppsuctermflag":null,
                  "childProppsuclifeno":null,
                  "childProppsucjlind":null,
                  "childProppsucmaxlimit":null
               }
            ],
            "gnmmPlanEventLimitsCh":[
               {
                  "childProphlplancode":null,
                  "childProphleventcode":null,
                  "childProphlparenteventcode":null,
                  "childProphllimitfreq":null,
                  "childProphlpriority":null,
                  "childProphlfreqtimes":null,
                  "childProphlpercentagefixed":null,
                  "childProphlmaxfixedamt":null,
                  "childProphlservicetaxapp":null,
                  "childProphlstatus":null,
                  "gnmmEventLimitBenefitLinkChh":[
                     {
                        "childPropaedplancode":null,
                        "childPropaedparenteventcode":null,
                        "childPropaedeventcode":null,
                        "childpropaedeventLink":null,
                        "childpropaedLimitFrequency":null,
                        "childPropaedPayCriteria":null,
                        "childPropaedPayableType":null,
                        "childPropaedAmount":null
                     }
                  ]
               }
            ]
         }
      ],
            "gnmmPlanEventDocuments":[
             {
                "parentDocCdrPlancode":null,
                "parentDocCdrEventcode":null,
                "parentDocCdrParenteventcode":null,
                "parentDocCdrPlanriderflag":null,
                "parentDocCdrDocumentcode":null,
                "parentDocCdrStatus":null
             }
          ],
          "gnmmPlanCauseExcels":[
             {
                "parentEcceCcePlancode":null,
                "parentEcceCceEventcode":null,
                "parentEcceCceParenteventcode":null,
                "parentEcceCceCausecode":null,
                "parentEcceCceStatus":null
             }
          ],
            "eventPayableLinks":[ 
               {
                  "parentPropbtplancode":null,
                  "parentPropbteventcode":null,
                  "parentPropbtparenteventcode":null,
                  "parentPropbtpaytype":null,
                  "parentPropbtloading":null,
                  "parentPropbtdiscount":null, 
                  "parentPropbtrider":null,
                  "parentPropbtlienapplicable":null,
                  "parentPropbtstatus":null,
                  "parentPropbtcommreverse":null,
                  "gnmmPlanLienLinks":[
                   {
                      "parentbenefitseplancode":null,
                      "parentbenefitseeventcode":null,
                      "parentbenefitselientype":null,
                      "parentbenefitseagefrom":null,
                      "parentbenefitseageto":null,
                      "parentbenefitseageentryfrom":null,
                      "parentbenefitseageentryto":null,
                      "parentbenefitsepolicyyearform":null,
                      "parentbenefitsepolicyyearto":null,
                      "parentbenefitseclaimyearfrom":null,
                      "parentbenefitseclaimyearto":null,
                      "parentbenefitsetermfrom":null,
                      "parentbenefitsetermto":null,
                      "parentbenefitserate":null,
                      "parentbenefitseunits":null,
                      "parentbenefitsepayabletype":null,
                      "parentbenefitselifeno":null,
                      "parentbenefitsejlind":null,
                      "parentbenefitseamountpayb":null,
                      "parentbenefitsestatus":null
                   }
                ]
               }
            ],
            "gnmtPlanSubEventsLinks":[
               {
                  "parentdeathSePlancode":null,
                  "parentdeathSeParenteventcode":null,
                  "parentdeathSeEventcode":null,
                  "parentdeathSeSubeventcode":null,
                  "parentdeathSePaycriteria":null,
                  "parentdeathSeAmountpayable":null,
                  "parentdeathSeStatus":null
               }
            ],
            "gnmmStatusSetupLinks":[
               {
                  "parentProppsucplancode":null,
                  "parentProppsuceventcode":null,
                  "parentProppsucparenteventcode":null,
                  "parentProppsucstatusupon":null,
                  "parentProppsucstatusvalue":null,
                  "parentProppsuctermtype":null,
                  "parentProppsucstatus":null,
                  "parentProppsuctermflag":null,
                  "parentProppsuclifeno":null,
                  "parentProppsucjlind":null,
                  "parentProppsucmaxlimit":null
               }
            ],
            "gnmmPlanEventLimits":[
               {
                  "parentProphlplancode":null,
                  "parentProphleventcode":null,
                  "parentProphlparenteventcode":null,
                  "parentProphllimitfreq":null,
                  "parentProphlpriority":null,
                  "parentProphlfreqtimes":null,
                  "parentProphlpercentagefixed":null,
                  "parentProphlmaxfixedamt":null,
                  "parentProphlservicetaxapp":null,
                  "parentProphlstatus":null,
                  "gnmmEventLimitBenefitLink":[
                     {
                        "parentPropaedplancode":null,
                        "parentPropaedparenteventcode":null,
                        "parentpropaedLimitFrequency":null,
                        "parentpropaedeventLink":null,
                        "parentPropaedeventcode":null,
                        "parentPropaedPayCriteria":null,
                        "parentPropaedPayableType":null,
                        "parentPropaedAmount":null
                     }
                  ]
               }
            ]
         }
      ]
      
      
    
    
   }
      
        return format;
  }
  dataUpload(data){
    data["PageName"]="PROD_BENEFITS";
    this.appService.downloaddata=data;
    localStorage.setItem("upload", "true");
    this.appService.uploadingPageContent=this.appService.risk_coverage;
    this.appService.uploadingPageName="risk_coverage";
    this.dialogService.addDialog(DataDownloadComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '
 
    }).subscribe((isConfirmed) => {
        if (isConfirmed=="done") {
              this.ngOnInit(); 
              for (var d = 0; d < this.appService.subPagesNavProdInfo.length; d++) {
                this.appService.subPagesNavProdInfo[d].isActive=false;
          
                 
              }
   } else {

      
      }
    })
   }

  dataDownload(data){
    data["PageName"]="PROD_BENEFITS"; 	
 this.appService.downloaddata=data;
   localStorage.setItem("upload", "false");
   this.dialogService.addDialog(DataDownloadComponent, {
     title: 'Name dialog',
     question1: 'Block Name ',
     question2: 'Block Type '

   }).subscribe((isConfirmed) => {
     if (isConfirmed) {
  } else {
     }
   })
  }

  copyRow(data, eve) {
    // var countR = data.tableObj.length;
    var s;
    for(var i=0;i<Object.keys(data.tableObj[data.tableObj.length-1]).length;i++){
      if(Object.keys(data.tableObj[data.tableObj.length-1])[i].includes("sn")){
        s=i;
    }
    
    }
    var countR=data.tableObj[data.tableObj.length-1][Object.keys(data.tableObj[data.tableObj.length-1])[s]];


    this.display_data = this.appService.changeAcc(this.appService.risk_coverage, data, ++countR,eve.target.parentElement.id, "copyRow");

   }

   retriveDel(data){
    var fieldName = [];
    localStorage.setItem("fieldNameList", "");
    for (var a = 0; a < data.deletedItems.length; a++) {
    fieldName.push({ "id": data.deletedItems[a].id, "itemName": data.deletedItems[a].fieldName });
    }
     localStorage.setItem("fieldNameList", JSON.stringify(fieldName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
        var selectedFields = [];
        for (var a = 0; a < data.deletedItems.length; a++) {
          for (var b = 0; b < seletedItems.length; b++) {
          if(seletedItems[b].id == data.deletedItems[a].id){
            selectedFields.push(data.deletedItems[a]);
          }
          }
        }
        this.display_data = this.appService.changeAcc(this.appService.risk_coverage, data, selectedFields,"", "addDelField");
      } else {

      }
    })
     
   }

   retriveTabDel(data){
    var fieldName = [];
    localStorage.setItem("fieldNameList", "");
    for (var a = 0; a < data.deletedItems.length; a++) {
    fieldName.push({ "id": data.deletedItems[a].id, "itemName": data.deletedItems[a].theadName });
    }
     localStorage.setItem("fieldNameList", JSON.stringify(fieldName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
        var selectedFields = [];
        for (var a = 0; a < data.deletedItems.length; a++) {
          for (var b = 0; b < seletedItems.length; b++) {
          if(seletedItems[b].id == data.deletedItems[a].id){
            selectedFields.push(data.deletedItems[a]);
          }
          }
        }
        this.display_data = this.appService.changeAcc(this.appService.risk_coverage, data, selectedFields,"", "addDelTabField");
      } else {

      }
    })
     
   }

   retriveDelAcc(){
    localStorage.setItem("fieldNameList", "");
     localStorage.setItem("fieldNameList", JSON.stringify(this.accName));
    this.dialogService.addDialog(RetriveDelComponent, {
      title: 'Name dialog'
    }).subscribe((isConfirmed) => {
       if (isConfirmed) {
          var seletedItems = JSON.parse(localStorage.getItem("fieldNameListOutpt"));
          for (var a = 0; a < this.accName.length; a++) {
            for (var b = 0; b < seletedItems.length; b++) {
              if(this.accName[a].id == seletedItems[b].id){
                this.accName.splice(a, 1);
              }
            var seletedAcc = { "accordId" :seletedItems[b].id };
            this.display_data = this.appService.changeAcc(this.appService.risk_coverage, seletedAcc, "", "", "retriveAcc")
          }
        }
      } else {

      }
    })
    }

    searchLov(lovName,data,index,tData){
      localStorage.setItem('searchdata','table');
      this.appService.sIndex=index;
      var check=JSON.stringify(this.appService[lovName]);
      this.appService.lovListData=JSON.parse(check);
      this.appService.templateInfo=this.appService.risk_coverage;
      this.appService.lovpiName=lovName;
      this.appService.dataInfo=data;
      this.appService.tData=tData;
      this.dialogService.addDialog(DataSearchComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '
   
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {
     } else {
       
        }
      })
  
    }
    searchLovfield(field)
    {
      localStorage.setItem('searchdata','field');
      var check=JSON.stringify(this.appService[field.lovUrl]);
      this.appService.lovListData=JSON.parse(check);
      // var ch1=JSON.stringify(field.optionObj);
      this.appService.dataInfo=field;
      this.appService.fieldName=field.fieldName;
      
      this.dialogService.addDialog(DataSearchComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '
   
      }).subscribe((isConfirmed) => {
        if (isConfirmed) {
     } else {
       
        }
      })
    }
    
    UpdateValues(pinfo: NgForm)
    {
      
      this.productUpdated=true;
      localStorage.setItem('ProductUpdated','true');
      // setTimeout(()=>{

      //   this.spinner.show();
      // },100)
      this.appService.validFieldCount(this.appService.risk_coverage);
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      var dataContent = [];
      dataContent.push({ "ProductId": this.appService.productId, "PageName": "PROD_BENEFITS.json", "PageContent": this.appService.risk_coverage});
  
      this.http.post('/api/dataPublishPage', dataContent, { headers: headers })
        .subscribe(res => {
          setTimeout(() => {
            console.log(res.json());
          }, 1500);
  
        }, err => {
          console.log("err " + err);
        })
    if (pinfo.valid) {
        var output = this.appService.validPage(this.appService.risk_coverage);
        if (output.mandatory) {
          this.alertMsg = [];
          for (var i = 0; i < output.mandatoryFields.length; i++) {
            this.alertMsg.push({ "msg": output.mandatoryFields[i].fieldName + ' (' + output.mandatoryFields[i].accordionName + ')' });
  
          }
          localStorage.setItem("alertContent", '');
          localStorage.setItem("prodAlertMsg", JSON.stringify(this.alertMsg));
          setTimeout(()=>{
  
            this.spinner.hide();
          },100)
          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '
  
          }).subscribe((isConfirmed) => {
  
            localStorage.setItem("prodAlertMsg", "");
          });
        } else { 
          var result = this.storeValuesToDb();
          var tabAccFormat=tabAccRiskFormat;
          this.appService.updateValuesRisk(this.appService.risk_coverage ,result ,tabAccFormat);
          
          setTimeout(() => {
            this.spinner.hide();
            if (this.appService.productSaved) {
              this.productSaved = true;
              this.appService.productSaved = false;
              localStorage.setItem("prodAlertMsg", "");
              localStorage.setItem("alertContent", "Updated Successfully");
  
              this.dialogService.addDialog(AlertComponent, { 
                title: 'Name dialog',
                question1: 'Block Name ',
                question2: 'Block Type '
  
              }).subscribe((isConfirmed) => {
                
  
              });
            }
          }, 1000)
        }
      }
    }
    
selectIndex(i)
{
  this.dropIndex=i;
}
  }