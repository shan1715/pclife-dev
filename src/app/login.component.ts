import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule, NgForm } from '@angular/forms';
import { Http, RequestOptions, Headers } from '@angular/http';
import { AppServices } from '../app/app.service';
import { Observable } from "rxjs";
import { AlertComponent } from "./bootstrap-modal/alert/alert.component";
import { ChangePasswordComponent } from "./bootstrap-modal/change-password/change-password.component";

import { DialogService } from "ng2-bootstrap-modal";
// import { Ng4LoadingSpinnerModule, Ng4LoadingSpinnerService  } from 'ng4-loading-spinner';
import {Subscription} from 'rxjs';
import swal from 'sweetalert2';


@Component({
    selector: 'login',
    templateUrl: './templates/login.html',
    styleUrls: ['./templates/login.css']


})



export class LoginComponent implements AfterViewInit {
    @ViewChild('f') form:NgForm;
    public loading = false;
    data;
    userdata;
    invalidLogin: boolean = false;
    datas: any;
    lovValues_planCode: any;
    modeSelect: any = [];
    selMode = true;
    template: string = "";
    busy:Subscription;
    isOverlay: boolean = false;
     myVar:string="0";
     user: any = [];
     adminUser:boolean=true;
    public constructor(private appServices: AppServices, private router: Router, public http: Http,private dialogService:DialogService) {
        this.modeSelect = ["--Select--", "Template Modify", "Data Entry"];
        localStorage.setItem("productId", "");
        localStorage.setItem("templateId", "");
        //this.template=`<img src="http://pa1.narvii.com/5722/2c617cd9674417d272084884b61e4bb7dd5f0b15_hq.gif" />`

        //     this.http.get('data/data.json')
        //     .subscribe(res=>this.data=res.json());
        //  console.log(this.data);

    }

    ngAfterViewInit(){
          setTimeout(()=>{
            if(localStorage.getItem("user")!=null){
                    this.form.controls["uname"].setValue(localStorage.getItem("user"));	    
                    this.form.controls["pwd"].setValue(this.appServices.userPassword);
                }
          },200);
        
                this.isOverlay = false;
                this.adminUser=localStorage.getItem("user") == "admin" ? false : true ;
    }

    showPS: boolean = false;
    chooseL: boolean = true;
    chooseP: boolean = false;

    selectMode(eve) {
      
        if (eve.target.value == "0") {
            this.selMode = false;

        } else {

            this.selMode = true;

        }
    }

    // load(){
    //       this.spinnerService.show();
    //     setTimeout(()=>{
    //         this.spinnerService.hide();
    //     },4000);
    // }

    login(form: NgForm) {
      if(form.valid){
        console.log(this.myVar);
       

        
         this.isOverlay = true;
         if (form.value.uname == "admin" && form.value.pwd == "admin") {
                             this.appServices.isAuthenticated=true;
                            // this.selMode=true;
            // && (<HTMLInputElement>document.getElementById("mode")).value == "0"
            this.router.navigate(['administrator-group']);

        }else{
            if(this.myVar!="0")
            {
        // }else{
        //     this.selMode = false;
        // }  
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.busy=this.http.post('http://localhost:4777/api/login', { "username": form.value.uname, "pwd": form.value.pwd }, { headers: headers })
            .subscribe(res => {
                
                this.appServices.isAuthenticated=true;

                if (res.json().output == "fail") {
                    localStorage.setItem("alertContent", "Invalid login, Please contact admin");

                    this.dialogService.addDialog(AlertComponent, {
                        title: 'Name dialog',
                        question1: 'Block Name ',
                        question2: 'Block Type '

                    }).subscribe((isConfirmed) => {
                    });

                }else if (res.json().output == "disabled") {
                    localStorage.setItem("alertContent", "Your account was disabled");

                    this.dialogService.addDialog(AlertComponent, {
                        title: 'Name dialog',
                        question1: 'Block Name ',
                        question2: 'Block Type '

                    }).subscribe((isConfirmed) => {
                    });

                }else{
                    localStorage.setItem("user",form.value.uname);
                    this.appServices.userPassword=form.value.pwd;
                    localStorage.setItem("pageLoad", "1");

                    //template
                    this.appServices.templateCopy=false;
                    this.appServices.templateApproval=false;
                    this.appServices.templateDeletion=false;
                    //Basic Details
                    this.appServices.bdCreate=false;
                    this.appServices.bdModify=false;
                    this.appServices.bdView=false;
                    //New Business
                    this.appServices.nbCreate=false;
                    this.appServices.nbModify=false;
                    this.appServices.nbView=false;
                    //Risk Coverage
                    this.appServices.rcCreate=false;
                    this.appServices.rcModify=false;
                    this.appServices.rcView=false;
                    //Underwriting
                    this.appServices.uwCreate=false;
                    this.appServices.uwModify=false;
                    this.appServices.uwView=false;
                    
                    //product
                    this.appServices.productCopy=false;
                    this.appServices.productApproval=false;
                    this.appServices.productDeletion=false;
                    //Basic Details
                    this.appServices.pbdCreate=false;
                    this.appServices.pbdModify=false;
                    this.appServices.pbdView=false;
                    //New Business
                    this.appServices.pnbCreate=false;
                    this.appServices.pnbModify=false;
                    this.appServices.pnbView=false;
                   
                    //Risk Coverage
                    this.appServices.prcCreate=false;
                    this.appServices.prcModify=false;
                    this.appServices.prcView=false;
                    //Underwriting
                    this.appServices.puwCreate=false;
                    this.appServices.puwModify=false;
                    this.appServices.puwView=false;

                    //UnitLink
                    this.appServices.ulCreate=false;
                    this.appServices.ulModify=false;
                    this.appServices.ulView=false;

                    //Rate
                    this.appServices.RateCreate=false;
                    this.appServices.RateModify=false;
                    this.appServices.RateView=false;

                    //charges
                    this.appServices.pChargesCreate=false;
                    this.appServices.pChargesModify=false;
                    this.appServices.pChargesView=false;

                     //charges
                    this.appServices.ChargesCreate=false;
                    this.appServices.ChargesModify=false;
                    this.appServices.ChargesView=false;

                    this.appServices.FinanceCreate=false;
                    this.appServices.FinanceModify=false;
                    this.appServices.FinanceView=false;

                    //Policy Servicing
                    this.appServices.psCreate=false;
                    this.appServices.psModify=false;
                    this.appServices.psView=false;

                    //Group
                    this.appServices.GroupCreate=false;
                    this.appServices.GroupModify=false;
                    this.appServices.GroupView=false;

                    //Annuity
                    this.appServices.AnnuityCreate=false;
                    this.appServices.AnnuityModify=false;
                    this.appServices.AnnuityView=false;

                    //Agency
                    this.appServices.AgencyCreate=false;
                    this.appServices.AgencyModify=false;
                    this.appServices.AgencyView=false;

                    //UnitLink
                    this.appServices.pulCreate=false;
                    this.appServices.pulModify=false;
                    this.appServices.pulView=false;

                    //Rate
                    this.appServices.pRateCreate=false;
                    this.appServices.pRateModify=false;
                    this.appServices.pRateView=false;

                    //Finance
                    this.appServices.pFinanceCreate=false;
                    this.appServices.pFinanceModify=false;
                    this.appServices.pFinanceView=false;

                    //Policy Servicing
                    this.appServices.ppsCreate=false;
                    this.appServices.ppsModify=false;
                    this.appServices.ppsView=false;

                    //Group
                    this.appServices.pGroupCreate=false;
                    this.appServices.pGroupModify=false;
                    this.appServices.pGroupView=false;
 
                    //Annuity
                    this.appServices.pAnnuityCreate=false; 
                    this.appServices.pAnnuityModify=false;
                    this.appServices.pAnnuityView=false;

                    //Agency
                    this.appServices.pAgencyCreate=false;
                    this.appServices.pAgencyModify=false;
                    this.appServices.pAgencyView=false;

                    
                    this.appServices.accessRights=res.json();
                    localStorage.setItem("accessRights",JSON.stringify(res.json()));
                    // Updated by Pisith 27/11/2019
                    // this.http.get('assets/data/user.json')
                    // Change from get data from file to get from db
                    this.http.get('http://localhost:4777/api/getUsers')
                    .subscribe(resuser => {
                        this.user = resuser.json();
                        for (var i = 0; i < this.user.length; i++) {
                            if (form.value.uname == this.user[i].username) {
                              if(this.user[i].statusFlag == 1){
                                this.appServices.changepswd = true;
                                            this.dialogService.addDialog(ChangePasswordComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '
    
        }).subscribe((isConfirmed) => {
          // Updated by Pisith 27/11/2019
          // This block should be update user data
        });
        break;
                              }else{
                                if (this.myVar == "1") {
                                    var permissionAvilable=false;
                                    for(var h=0;h<13;h++){
                                        if(res.json().gpa["skills"+h.toString()].length>0){
                                            permissionAvilable=true;
                                            
                                        }
                                    }
                                    if(permissionAvilable){
                                        localStorage.setItem("pageMode","1");  
                                            localStorage.setItem("modeSelection", "template");
                                            this.router.navigate(['/productConfig/dashboard']);
                                    }else{
                                        swal({
                                            title: 'Permission',
                                            text: "You don't have permission to access this feature!",
                                            type: 'warning',
                                            width: 567,
                                            showCancelButton: false,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Ok',
                                            cancelButtonText: 'No'
                                          }).then((resultSel) => {
                                            if (resultSel.value) {
                                            }
                                        })
                                    }
                          
              
                } else if (this.myVar == "2") {
                    var permissionAvilable=false;
                    for(var h=14;h<27;h++){
                        if(res.json().gpa["skills"+h.toString()].length>0){
                            permissionAvilable=true;
                            
                        }
                    }
                 

                    if(permissionAvilable){
                        localStorage.setItem("pageMode","2");  
                        localStorage.setItem("modeSelection", "data");
                        this.router.navigate(['/productConfig/dashboarddata']);
                    }else{
                        swal({
                            title: 'Permission',
                            text: "You don't have permission to access this feature!",
                            type: 'warning',
                            width: 567,
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok',
                            cancelButtonText: 'No'
                          }).then((resultSel) => {
                            if (resultSel.value) {
                            }
                        })                    }
                   
             
                }
                              }
                            }
                          }
                
                    })
                            
                }
        
            }, err => {
            })
        }else{
            this.selMode = false;
        }  
        }
                 this.isOverlay = false;

        // if (form.value.uname == "q" && form.value.pwd == "w" && (<HTMLInputElement>document.getElementById("mode")).value != "0") {

        //     localStorage.setItem("pageMode", (<HTMLInputElement>document.getElementById("mode")).value)
        //     this.selMode = true;
        //     this.invalidLogin = false;
        //     if ((<HTMLInputElement>document.getElementById("mode")).value == "1") {
        //         localStorage.setItem("modeSelection", "template");
        //         this.router.navigate(['/productConfig/dashboard']);
        //         // this.loading=false;
        //         this.isOverlay = false;

        //     } else {
        //         localStorage.setItem("modeSelection", "data");
        //         this.router.navigate(['/productConfig/dashboarddata']);
        //         //   this.loading=false;
        //         this.isOverlay = false;

        //     }


        //     localStorage.setItem("user", form.value.uname);
        //     localStorage.setItem("pageLoad", "1");
        //     localStorage.setItem("loadOneTime", "0");


            //  window.location.reload();
            // setTimeout(function(){
            //     window.location.reload();
            // });     
    // }else{
    //     this.selMode = false;
    // }     
    }
    else{
        
        this.selMode = false;
    }
        }



        // this.http.get('http://10.200.101.26:8283/ProductConfigServer/config/userList/'+uname)
        // .subscribe(res=>this.userdata=res.json());


        // console.log("user details");
        //  console.log(this.userdata);

        //  if(this.userdata.username==uname && this.userdata.password==pwd){
        //      this.localStorageService.set('data',this.data);
        //      this.router.navigate(['./policyservice']);
        // }else{
        //      alert("Login Failed, Please check username and password");
        //  }

        openForgetpswd(form: NgForm){
            if(form.value.uname == "" || form.value.uname == null){
                localStorage.setItem("alertContent", "Enter Username");
                
                this.dialogService.addDialog(AlertComponent, {
                    title: 'Name dialog',
                    question1: 'Block Name ',
                    question2: 'Block Type '

                }).subscribe((isConfirmed) => {
                });
            }else{
                localStorage.setItem("user",form.value.uname);
                this.appServices.forgetpswd = true;
                this.dialogService.addDialog(ChangePasswordComponent, {
                title: 'Name dialog',
                question1: 'Block Name ',
                question2: 'Block Type '
    
                }).subscribe((isConfirmed) => {
    
                });
            }
            
        }

    



    reg() {
        this.router.navigate(['./register']);
    }

    checkAdmin(eve)
    {
        this.adminUser=eve.target.value ==  "admin"? false : true;

    }


    hello(s) {
        if (s == "Product Configurator") {
            this.router.navigate(['./demo']);
        } else if (s == "Product Analyze") {
            alert("Page not created");
        } else if (s == "Product Sales") {
            alert("Page not created");

        }

    }
    /*
    policyservice(){
         this.router.navigate(['./policyservice']);
    }
    */
}