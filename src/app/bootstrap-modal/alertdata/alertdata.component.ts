import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../../app.service';
export interface PromptModel {
  title: string;
  question1: string;
  question2: string;

}
@Component({
  selector: 'app-promp',
  templateUrl: './alertdata.component.html',
  styleUrls: ['./alertdata.component.css']
})
export class AlertdataComponent extends DialogComponent<PromptModel, string> implements PromptModel {
  title: string;
  question1: string;
  question2: string;
  PageName:string;
  content:string;
  alertMsg: any = [];
  fmessage1: string = '';
  data:any;
  trecords:any;
  row:any;
  err:any;
  process:any;
  tot:any;
  date:any;
  refId:any;


  constructor(dialogService: DialogService,appService:AppServices) {
    super(dialogService);

    this.PageName="Notification";
    if(localStorage.getItem("alertContent")!=""){
   this.content=localStorage.getItem("alertContent");
    }
    if(localStorage.getItem("prodAlertMsg")!=""){
      this.alertMsg=JSON.parse(localStorage.getItem("prodAlertMsg"));
    }      
  }

  closeAlert(){
    //this.result=this.fmessage1;
  
    this.close();
  }

  ok(){
    this.result=this.fmessage1;
    this.close();
  }
  ngOnInit() {
    this.refId=localStorage.getItem('refId');
   
   this.data= JSON.parse(localStorage.getItem('uploaddata'));
   this.trecords=JSON.parse(localStorage.getItem('uploaddata')).count;
   this.row= JSON.parse(localStorage.getItem('uploaddata')).row-1 ;
   this.err=JSON.parse(localStorage.getItem('uploaddata')).err + " of "+this.row+" row";
   this.tot=this.row * this.trecords;
   this.date=JSON.parse(localStorage.getItem('uploaddata')).udate;
   if(JSON.parse(localStorage.getItem('uploaddata')).err==''){
    this.title="Success";
    this.process="Processed Records:" + this.tot;
    }
    else
    {
    this.title="Exception";
    this.process="Exception Record No:"+this.err;
    }
   
  }


}
