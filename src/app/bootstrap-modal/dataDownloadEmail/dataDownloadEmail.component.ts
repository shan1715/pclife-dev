import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../../app.service';
import { AlertComponent } from "../alert/alert.component";
import { FileUploader } from 'ng2-file-upload';
import { Router } from '@angular/router';
import { AlertdataComponent } from "../alertdata/alertdata.component";


export interface PromptModel {
  title: string;
  question1: string;
  question2: string;

}
const URL = 'assets/dataupload/';
@Component({
  selector: 'app-promp',
  templateUrl: './dataDownloadEmail.component.html',
  styleUrls: ['./dataDownloadEmail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DataDownloadEmailComponent extends DialogComponent<PromptModel, string> implements PromptModel, OnInit {


  title: string;
  question1: string;
  question2: string;
  emaillist: any = [];
  userSelected: boolean = false;
  date: any;
  today_date: any;
  fileName: any;
  num: any = [];
  exceldata: any = [];
  dataarray: any = [];
  ws_url:any=[];
  alertContent:any;
  email:any;

  constructor(private router: Router, private changeDetector: ChangeDetectorRef, private appServices: AppServices, dialogService: DialogService, private http: Http) {
    super(dialogService)
    this.email=JSON.parse(localStorage.getItem('accessRights')).email;  

  }



  ngOnInit() {

        this.http.get('assets/data/property.json')
          .subscribe(res => {
            this.ws_url = res.json().web.url;
          });

    this.http.get('assets/data/user.json')
      .subscribe((res) => {

        this.emaillist = res.json();


      }, err => {
        console.log(err);
      })
  }

   selUser() {
    var check = false;
    var chkall=true;
    for (var i = 0; i < document.getElementsByClassName("listUsers").length; i++) {

      if ((<HTMLInputElement>document.getElementsByClassName("listUsers")[i].getElementsByTagName("input")[0]).checked) {
        check = true;


      }else{
        chkall=false;
      }

    }
if(!chkall)
{
  (<HTMLInputElement>document.getElementById('selall')).checked=false;

}else
{
  (<HTMLInputElement>document.getElementById('selall')).checked=true;
}


    if (check) {
      this.userSelected = true;
    } else {
      this.userSelected = false;
    }
  }

  selAllUser(eve) {
    if (eve.target.checked) {
      var check = false;
      for (var i = 0; i < document.getElementsByClassName("listUsers").length; i++) {
        (<HTMLInputElement>document.getElementsByClassName("listUsers")[i].getElementsByTagName("input")[0]).checked = true;
        // 		if((<HTMLInputElement>document.getElementsByClassName("listUsers")[i].getElementsByTagName("input")[0]).checked){
        //       check=true;


        // }
        this.userSelected = true;

      }

    } else if (!eve.target.checked) {
      var check = false;
      for (var i = 0; i < document.getElementsByClassName("listUsers").length; i++) {
        (<HTMLInputElement>document.getElementsByClassName("listUsers")[i].getElementsByTagName("input")[0]).checked = false;
        // 		if((<HTMLInputElement>document.getElementsByClassName("listUsers")[i].getElementsByTagName("input")[0]).checked){
        //       check=true;


        // }
        this.userSelected = false;

      }

    }

  }

  sendMail() {

    var smail = [];
    for (var i = 0; i < document.getElementsByClassName("listUsers").length; i++) {

      if (document.getElementsByClassName("listUsers")[i].getElementsByTagName("input")[0].checked) {

        smail.push({ "eMailTo": document.getElementsByClassName("listUsers")[i].children[2].textContent})

      }
   this.appServices.smail=smail;
    }
    this.result="done";
this.close();

  }
  
 

}
