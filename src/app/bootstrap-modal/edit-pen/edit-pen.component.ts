import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { AppServices } from '../../app.service';
export interface PromptModel {
  title: string;
  fquestion1: string;
  fquestion2: string;
}

@Component({
  selector: 'edit-pen',
  templateUrl: './edit-pen.component.html',
  styleUrls: ['./edit-pen.component.css']
})
export class EditPenComponent extends DialogComponent<PromptModel, string> implements PromptModel {

  //  @ViewChild('selectedFType') selectedFtype: ElementRef;
  // @ViewChild('selectedMType') selectedMtype: ElementRef;

  title: string;
  fquestion1: string = "Item Named";
  fquestion2: string = "Item Type";
  fquestion3: string = "Field Type";
  fquestion4: string = "Masking Format";
  fquestion5: string = "Mandatory";
  fmsgplaceholder1: string = "Please enter item name";
  fmsgplaceholder2: string = "Please enter item type";
  fmsgplaceholder3: string = "Please enter field type ";
  fmsgplaceholder4: string = "Please enter masking format";
  fmessage1: string = '';
  fmessage2: string = '';
  fmessage3: string = '';
  fmessage4: string = '';
  mmsg: string = '';
  dValue: string = '';
  minVal: string = '';
  maxVal: string = '';
  showHide: string = '';
  readOnly: string = '';
  selectIT: boolean = true;
  selectFT: boolean = true;
  selectMT: boolean = true;
  selectMM: boolean = true;
  fType: any = [];
  mType: any = [];
  selectedItype: boolean = true;
  listOfValues: boolean = false;
  tRow: any = [];
  finalOutput: any = [];

  inputType: any = [];
  typeChange: any = [];
  formatChange: any = [];
  mandatoryChange: any = [];
  labelName: any;
  maskT: boolean = false;
  dateMask: boolean = false;
  dateBool: boolean = false;
  numBool: boolean = false;
  textBool: boolean = false;
  mandatoryMsg: boolean = false;
  mandatory: boolean = true;
  fieldtype: boolean = false;
  mappingId: any = [];
  blockNames: any = [];
  fieldNames: any = [];
  dropdownSettings: any = [];
  dropdownFieldSettings: any = [];
  dropdownMappedFieldSettings: any = [];
  selectedItems = [];
  selectedFieldItems = [];
  selectedMappedFieldItems = [];
  mappingField: any;

  constructor(dialogService: DialogService, private appService: AppServices) {
    super(dialogService);
    this.dropdownMappedFieldSettings = {
      singleSelection: false,
      text: "Select Mapped Field",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

    this.dropdownSettings = {
      singleSelection: true,
      text: "Select Block Name",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

    this.dropdownFieldSettings = {
      singleSelection: true,
      text: "Select Field Name",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

    this.mappingId = JSON.parse(localStorage.getItem("editMapping"));
    this.selectedMappedFieldItems = JSON.parse(localStorage.getItem("editMapping"));
    setTimeout(res => {
      if(document.getElementsByClassName("c-list")[0]){
      (<HTMLInputElement>document.getElementsByClassName("c-list")[0]).style.display = "-webkit-box"
      }
    }, 200);
    this.blockNames = JSON.parse(localStorage.getItem("editBlockNameList"));
    this.fieldNames = [];
    this.mappingField = {};

    this.fmessage1 = localStorage.getItem("editName");
    this.mmsg = localStorage.getItem("mandatoryMsg");
    this.dValue = localStorage.getItem("editDefaultValue");
    this.minVal = localStorage.getItem("editMinValue");
    this.maxVal = localStorage.getItem("editMaxValue");  
    var shwhide = localStorage.getItem("editDisplay");
    if (shwhide) {
      this.showHide = 'show';
    } else {
      this.showHide = 'hide';
    }
    this.readOnly = localStorage.getItem("editReadOnly");
    if (localStorage.getItem("editType") == "text") {
      if (localStorage.getItem("mandatory") == "true") {
        this.mandatoryChange.push({ "name": "True", "value": "True" }, { "name": "False", "value": "False" })
        this.mandatoryMsg = true;
      } else if (localStorage.getItem("mandatory") == "false") {
        this.mandatoryChange.push({ "name": "False", "value": "False" }, { "name": "True", "value": "True" })
      }

      this.inputType.push({ "name": "Text", "value": "text" }, { "name": "Drop Down", "value": "select" }, { "name": "Date Picker", "value": "date" })
      this.typeChange.push({ "name": "Text", "value": "text" }, { "name": "Number", "value": "number" }, { "name": "Date", "value": "date" })
      this.maskT = true;
      this.textBool = true;
      this.dateBool = false;
      this.numBool = false;
    } else if (localStorage.getItem("editType") == "select-one") {
      if (localStorage.getItem("mandatory") == "true") {
        this.mandatoryChange.push({ "name": "True", "value": "True" }, { "name": "False", "value": "False" })
        this.mandatoryMsg = true;
      } else if (localStorage.getItem("mandatory") == "false") {
        this.mandatoryChange.push({ "name": "False", "value": "False" }, { "name": "True", "value": "True" })
      }
      this.inputType.push({ "name": "Drop Down", "value": "select" }, { "name": "Text", "value": "text" }, { "name": "Date Picker", "value": "date" })
      var gItem = JSON.parse(localStorage.getItem("editSelectedItem"));
      if (gItem.length > 0) {
        this.listOfValues = true;
        this.tRow = gItem;
      }
      //this.formatChange.push({ "n": "Number (####)", "id": "number" }, { "n": "Number With Decimal (####.##)", "id": "numberwdecimal" })
      this.formatChange.push({ "n": "Number (####)", "id": "number" })
      //this.typeChange.push({"name":"Number","value":"number"},{"name":"Text","value":"text"},{"name":"Date","value":"date"})
    } else if (localStorage.getItem("editType") == "date") {
      if (localStorage.getItem("mandatory") == "true") {
        this.mandatoryChange.push({ "name": "True", "value": "True" }, { "name": "False", "value": "False" })
        this.mandatoryMsg = true;
      } else if (localStorage.getItem("mandatory") == "false") {
        this.mandatoryChange.push({ "name": "False", "value": "False" }, { "name": "True", "value": "True" })
      }
      this.inputType.push({ "name": "Date Picker", "value": "date" }, { "name": "Drop Down", "value": "select" }, { "name": "Text", "value": "text" })
      this.typeChange.push({ "name": "Date", "value": "date" }, { "name": "Number", "value": "number" }, { "name": "Text", "value": "text" })
      //this.formatChange.push({ "n": "DD/MM/YYYY", "id": "d2m2y4" }, { "n": "MM/DD/YYYY", "id": "m2d2y4" }, { "n": "YYYY/MM/DD", "id": "y4m2d2" }, { "n": "YYYY/DD/MM", "id": "y4d2m2" })
      this.formatChange.push({ "n": "mm-dd-yyyy", "id": "m2d2y4" })
      this.dateMask = true;
      this.fieldtype = false;
      this.dateBool = true;
      this.numBool = false;
      this.textBool = false;
    } else if (localStorage.getItem("editType") == "number") {
      this.numBool = true;
      this.dateBool = false;
      this.textBool = false;
      this.maskT = true;
      this.dateMask = true;
      this.formatChange = [];
      this.typeChange = [];
      this.inputType.push({ "name": "Text", "value": "text" }, { "name": "Drop Down", "value": "select" }, { "name": "Date Picker", "value": "date" })
      this.typeChange.push({ "name": "Number", "value": "number" }, { "name": "Text", "value": "text" }, { "name": "Date", "value": "date" })
      //this.formatChange.push({ "n": "Number (####)", "id": "number" }, { "n": "Number With Decimal (####.##)", "id": "numberwdecimal" })
      this.formatChange.push({ "n": "Number (####)", "id": "number" })

      if (localStorage.getItem("mandatory") == "true") {
        this.mandatoryChange.push({ "name": "True", "value": "True" }, { "name": "False", "value": "False" })
        this.mandatoryMsg = true;
      } else if (localStorage.getItem("mandatory") == "false") {
        this.mandatoryChange.push({ "name": "False", "value": "False" }, { "name": "True", "value": "True" })
      }


    }

  }
  editField(ep) {
    this.selectIT = true;

    if (ep == "text") {



    }
  }
  itemType(sp) {


    this.selectIT = true;

    if (sp == "text") {
      this.listOfValues = false;
      this.dateMask = false;
      this.maskT = true;
      this.textBool = true;
      this.dateBool = false;
      this.numBool = false;
      this.typeChange = [];
      this.typeChange.push({ "name": "Text", "value": "text" }, { "name": "Number", "value": "number" }, { "name": "Date", "value": "date" })
      // this.selectedFtype.nativeElement.style.display = "inline-block";
      // this.selectedFtype.nativeElement.previousElementSibling.style.display = "inline-block";
      // this.selectedFtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    } else if (sp == "select") {
      this.fType = [];
      this.fieldtype = false;
      this.selectedItype = false;
      this.maskT = false;
      this.dateMask = false;
      // this.selectedFtype.nativeElement.style.display = "none";
      // this.selectedFtype.nativeElement.previousElementSibling.style.display = "none";
      // this.selectedFtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
      // this.selectedMtype.nativeElement.style.display = "none";
      // this.selectedMtype.nativeElement.previousElementSibling.style.display = "none";
      // this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
      this.listOfValues = true;
      this.tRow = [];
      this.tRow.push({ "optionName": "", "optionValue": "" });
    } else if (sp == "date") {
      this.textBool = false;
      this.dateBool = true;
      this.numBool = false;
      this.dateMask = true;
      this.listOfValues = false;
      this.fieldtype = false;
      this.maskT = false;
      // this.maskT=true;
      this.formatChange = [];
      //this.formatChange.push({ "n": "DD/MM/YYYY", "id": "d2m2y4" }, { "n": "MM/DD/YYYY", "id": "m2d2y4" }, { "n": "YYYY/MM/DD", "id": "y4m2d2" }, { "n": "YYYY/DD/MM", "id": "y4d2m2" })
      this.formatChange.push({ "n": "mm-dd-yyyy", "id": "m2d2y4" })

    }
  }

  fieldType(sp) {
    this.selectFT = true;

    if (sp == "number") {
      this.dateMask = true;
      this.formatChange = [];
      this.textBool = false;
      this.dateBool = false;
      this.numBool = true;
      //this.formatChange.push({ "n": "Number (####)", "id": "number" }, { "n": "Number With Decimal (####.##)", "id": "numberwdecimal" })
      this.formatChange.push({ "n": "Number (####)", "id": "number" })

      // this.selectedMtype.nativeElement.style.display = "inline-block";
      // this.selectedMtype.nativeElement.previousElementSibling.style.display = "inline-block";
      // this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    } else if (sp == "text") {
      this.textBool = true;
      this.dateBool = false;
      this.numBool = false;
      this.dateMask = false;
      // this.selectedMtype.nativeElement.style.display = "none";
      // this.selectedMtype.nativeElement.previousElementSibling.style.display = "none";
      // this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
    } else if (sp == "date") {
      this.textBool = false;
      this.dateBool = true;
      this.numBool = false;
      this.dateMask = true;
      this.formatChange = [];

      //this.formatChange.push({ "n": "DD/MM/YYYY", "id": "d2m2y4" }, { "n": "MM/DD/YYYY", "id": "m2d2y4" }, { "n": "YYYY/MM/DD", "id": "y4m2d2" }, { "n": "YYYY/DD/MM", "id": "y4d2m2" })
      this.formatChange.push({ "n": "mm-dd-yyyy", "id": "m2d2y4" })

      // this.selectedMtype.nativeElement.style.display = "inline-block";
      // this.selectedMtype.nativeElement.previousElementSibling.style.display = "inline-block";
      // this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    }

  }

  MaskType(sp) {
    this.selectMT = true;
  }

  mandatoryType(sp) {
    this.selectMM = true;
    if (sp == "True") {

      this.mandatoryMsg = true;
    } else if (sp == "False") {
      this.mandatoryMsg = false;

    }



  }



  apply() {
    this.finalOutput = [];
    if ((<HTMLInputElement>document.getElementById("it")).value == "text") {
      localStorage.setItem("fmsg1", this.fmessage1);
      localStorage.setItem("mmsg", this.mmsg);

      // localStorage.setItem("fmsg2", it);
      // localStorage.setItem("fmsg3", ft);
      // localStorage.setItem("fmsg4", mt);
      if ((<HTMLInputElement>document.getElementById("lname")).value != "") {
        if ((<HTMLInputElement>document.getElementById("mmsg")) == null) {
          if ((<HTMLInputElement>document.getElementById("showHide")).value == "show") {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": localStorage.getItem("mmsg"), "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "field_type": (<HTMLInputElement>document.getElementById("nt")).value, "mask_type": (<HTMLInputElement>document.getElementById("lname")).value, "display": true, "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })
          } else {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": localStorage.getItem("mmsg"), "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "field_type": (<HTMLInputElement>document.getElementById("nt")).value, "mask_type": (<HTMLInputElement>document.getElementById("lname")).value, "display": false, "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })
          }
        } else {
          if ((<HTMLInputElement>document.getElementById("showHide")).value == "show") {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": (<HTMLInputElement>document.getElementById("mmsg")).value, "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "field_type": (<HTMLInputElement>document.getElementById("nt")).value, "mask_type": (<HTMLInputElement>document.getElementById("lname")).value, "display": true, "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })
          } else {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": (<HTMLInputElement>document.getElementById("mmsg")).value, "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "field_type": (<HTMLInputElement>document.getElementById("nt")).value, "mask_type": (<HTMLInputElement>document.getElementById("lname")).value, "display": false, "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })
          }
        }
      } else {
        if ((<HTMLInputElement>document.getElementById("showHide")).value == "show") {

          this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "field_type": (<HTMLInputElement>document.getElementById("nt")).value, "display": true, "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })
        } else {
          this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "field_type": (<HTMLInputElement>document.getElementById("nt")).value, "display": false, "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })

        }
      }
      localStorage.setItem("output", JSON.stringify(this.finalOutput));
      this.result = this.fmessage1;
      this.result = this.fmessage1;
      this.close();
    } else if ((<HTMLInputElement>document.getElementById("it")).value == "select") {
      localStorage.setItem("fmsg1", this.fmessage1);
      localStorage.setItem("mmsg", this.mmsg);
      // localStorage.setItem("fmsg2", it);
      var store = [];
      var def = false;
      for (var si = 0; si < this.tRow.length; si++) {
        if (this.tRow[si].optionValue == (<HTMLInputElement>document.getElementById("defValue")).value) {
          this.tRow[si]["selected"] = true;
          def = true;
        } else {
          this.tRow[si]["selected"] = false;
        }
      }
      store = this.tRow;
      if ((<HTMLInputElement>document.getElementById("mmsg")) == null) {
        if ((<HTMLInputElement>document.getElementById("showHide")).value == "show") {
          if (def) {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": localStorage.getItem("mmsg"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "optionObj": store, "display": true, "defaultValue": true, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems })
          } else {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": localStorage.getItem("mmsg"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "optionObj": store, "display": true, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems })
          }
        } else {
          if (def) {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": localStorage.getItem("mmsg"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "optionObj": store, "display": false, "defaultValue": true, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems })
          } else {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": localStorage.getItem("mmsg"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "optionObj": store, "display": false, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems })
          }
        }
      } else {
        if ((<HTMLInputElement>document.getElementById("showHide")).value == "show") {
          if (def) {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": (<HTMLInputElement>document.getElementById("mmsg")).value, "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "optionObj": store, "display": true, "defaultValue": true, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems })
          } else {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": (<HTMLInputElement>document.getElementById("mmsg")).value, "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "optionObj": store, "display": true, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems })
          }
        } else {
          if (def) {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": (<HTMLInputElement>document.getElementById("mmsg")).value, "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "optionObj": store, "display": false, "defaultValue": true, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems })
          } else {
            this.finalOutput.push({ "id": localStorage.getItem("editId"), "blockId": localStorage.getItem("editBlockId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": (<HTMLInputElement>document.getElementById("mmsg")).value, "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "optionObj": store, "display": false, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems })
          }
        }
      }
      localStorage.setItem("output", JSON.stringify(this.finalOutput));
      this.result = this.fmessage1;
      this.result = this.fmessage1;
      this.close();
    } else if ((<HTMLInputElement>document.getElementById("it")).value == "date") {
      localStorage.setItem("fmsg1", this.fmessage1);
      localStorage.setItem("mmsg", this.mmsg);
      // localStorage.setItem("fmsg2", it);
      // localStorage.setItem("fmsg4", mt);
      if ((<HTMLInputElement>document.getElementById("mmsg")) == null) {
        if ((<HTMLInputElement>document.getElementById("showHide")).value == "show") {
          this.finalOutput.push({ "id": localStorage.getItem("editId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": localStorage.getItem("mmsg"), "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "mask_type": (<HTMLInputElement>document.getElementById("dt")).value, "display": "true", "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })

        } else {
          this.finalOutput.push({ "id": localStorage.getItem("editId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": localStorage.getItem("mmsg"), "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "mask_type": (<HTMLInputElement>document.getElementById("dt")).value, "display": "false", "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })

        }
      } else {
        if ((<HTMLInputElement>document.getElementById("showHide")).value == "show") {
          this.finalOutput.push({ "id": localStorage.getItem("editId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": (<HTMLInputElement>document.getElementById("mmsg")).value, "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "mask_type": (<HTMLInputElement>document.getElementById("dt")).value, "display": true, "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })

        } else {
          this.finalOutput.push({ "id": localStorage.getItem("editId"), "mandatory": (<HTMLInputElement>document.getElementById("mme")).value, "mandatoryMsg": (<HTMLInputElement>document.getElementById("mmsg")).value, "blockId": localStorage.getItem("editBlockId"), "item_name": this.fmessage1, "item_type": (<HTMLInputElement>document.getElementById("it")).value, "mask_type": (<HTMLInputElement>document.getElementById("dt")).value, "display": false, "defaultValue": this.dValue, "readOnly": (<HTMLInputElement>document.getElementById("readOnly")).value, "mapping": this.mappingField, "mappingId": this.selectedMappedFieldItems, "minVal" :this.minVal, "maxVal": this.maxVal })

        }

      }
      localStorage.setItem("output", JSON.stringify(this.finalOutput));
      this.result = this.fmessage1;
      this.result = this.fmessage1;
      this.close();
    }

  }

  incRow(eve) {
    this.tRow.push({ "optionValue": "", "optionLabel": "" })
  }

  decRow(eve) {
    this.tRow.splice(this.tRow.length - 1, 1);
  }

  getSelLabel(i, eve) {
    this.tRow[i].optionLabel = eve.target.value;
  }

  getSelValue(i, eve) {
    this.tRow[i].optionValue = eve.target.value;
  }


  onItemSelect(item: any) {
    this.fieldNames = [];
    var data = { "accordId": item.id };
    if (localStorage.getItem("editPageName") == "PROD_INFO") {
      this.fieldNames = this.appService.changeAcc(this.appService.tempProdInfo, data, "", "", "fieldList");
    } else if (localStorage.getItem("editPageName") == "AGENCY") {
      this.fieldNames = this.appService.changeAcc(this.appService.agency, data, "","", "fieldList");
    } else if (localStorage.getItem("editPageName") == "OPERATION_ANNUITY") {
      this.fieldNames = this.appService.changeAcc(this.appService.operation_annuity, data, "","", "fieldList");
    } else if (localStorage.getItem("editPageName") == "OPERATION_GROUP") {
      this.fieldNames = this.appService.changeAcc(this.appService.group, data, "","", "fieldList");
    } else if (localStorage.getItem("editPageName") == "POLICYSERVICING") {
      this.fieldNames = this.appService.changeAcc(this.appService.policyServicing, data, "", "","fieldList");
    } else if (localStorage.getItem("editPageName") == "OPERATION_UNDERWRITING") {
      this.fieldNames = this.appService.changeAcc(this.appService.operation_uwritting, data, "", "","fieldList");
    } else if (localStorage.getItem("editPageName") == "OPERATION_UNITLINKED") {
      this.fieldNames = this.appService.changeAcc(this.appService.operation_unitlink, data, "","", "fieldList");
    } else if (localStorage.getItem("editPageName") == "NEW_BUSINESS") {
      this.fieldNames = this.appService.changeAcc(this.appService.operation_nbusiness, data, "","", "fieldList");
    } else if (localStorage.getItem("editPageName") == "PRODUCT_CHARGES") {
      this.fieldNames = this.appService.changeAcc(this.appService.charges, data, "","", "fieldList");
    } else if (localStorage.getItem("editPageName") == "PRODUCT_FINANCE") {
      this.fieldNames = this.appService.changeAcc(this.appService.finance, data, "", "","fieldList");
    } else if (localStorage.getItem("editPageName") == "PRODUCT_RATES") {
      this.fieldNames = this.appService.changeAcc(this.appService.product_rate, data, "", "","fieldList");
    } else if (localStorage.getItem("editPageName") == "PROD_BENEFITS") {
      this.fieldNames = this.appService.changeAcc(this.appService.risk_coverage, data, "","", "fieldList");
    }
  }
  OnItemDeSelect(item: any) {
    this.fieldNames = [];
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }


  onFieldItemSelect(item: any) {
    this.mappingField = { "id": item.id }
  }
  OnFieldItemDeSelect(item: any) {
    this.mappingField = {};
  }
  onFieldSelectAll(items: any) {
    console.log(items);
  }
  onFieldDeSelectAll(items: any) {
    console.log(items);
  }


  onMappedFieldItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedMappedFieldItems);
  }
  OnMappedFieldItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedMappedFieldItems);
  }
  onMappedFieldSelectAll(items: any) {
    console.log(items);
    setTimeout(res => {
      (<HTMLInputElement>document.getElementsByClassName("c-list")[0]).style.display = "-webkit-box"
    }, 200);
  }
  onMappedFieldDeSelectAll(items: any) {
    console.log(items);
    console.log(this.selectedMappedFieldItems);
  }

  minMaxDateValidate(eve){
    var minValue = (<HTMLInputElement>document.getElementById("minVal")).value;
    var maxValue = (<HTMLInputElement>document.getElementById("maxVal")).value;
    var regx=new RegExp(/^\d{4}-\d{2}-\d{2}$/);
    if(regx.test(eve.target.value)){
    if(minValue != '' && maxValue != ''){
      var minValueDate = new Date(minValue);
      var maxValueDate = new Date(maxValue);

      if(minValueDate > maxValueDate){
        (<HTMLInputElement>document.getElementById("maxVal")).value = '';
        (<HTMLInputElement>document.getElementById("minVal")).value = '';
      }
    } 
  }else{
    eve.target.value ='';
  }
  }

  minMaxNumValidate(eve){
    var minValue = (<HTMLInputElement>document.getElementById("minVal")).value;
    var maxValue = (<HTMLInputElement>document.getElementById("maxVal")).value;
    var regexp = /^[0-9]+([,.][0-9]+)?$/g;
    if(regexp.test(eve.target.value)){
    if(minValue != '' && maxValue != ''){
      if(Number(maxValue) < Number(minValue)){
        (<HTMLInputElement>document.getElementById("maxVal")).value = '';
        (<HTMLInputElement>document.getElementById("minVal")).value = '';
      }
    }  
  }else{
    eve.target.value ='';
  }
  }
}