import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';

export interface PromptModel {
  title: string;
  }

@Component({
  selector: 'app-add-row',
  templateUrl: './add-row.component.html',
  styleUrls: ['./add-row.component.css']
})
export class AddRowComponent extends DialogComponent<PromptModel, string> implements PromptModel {
  title: string;
  noOfRowVal = 1;

  constructor(dialogService: DialogService) {
    super(dialogService);
   }

   minNumValidate(eve){
      var val = (<HTMLInputElement>document.getElementById("noOfRowVal")).value;
      var numVal = Number(val);
      var regexp = /^[0-9]+([,.][0-9]+)?$/g;
    if(regexp.test(eve.target.value)){
      if(val != ''){
        if(Number.isInteger(numVal)){
          if(numVal < 1){
            (<HTMLInputElement>document.getElementById("noOfRowVal")).value = '';
          }
        }else{
          (<HTMLInputElement>document.getElementById("noOfRowVal")).value = '';
        }

      }
    }else{
      eve.target.value ='';
    }  
    }

    apply(){
    localStorage.setItem("addRowOutpt", (<HTMLInputElement>document.getElementById("noOfRowVal")).value);
    this.result = "true";
    this.close();
   }

}
