import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../../app.service';
export interface PromptModel {
  title: string;
  question1: string;
  question2: string;

}
@Component({
  selector: 'app-promp',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent extends DialogComponent<PromptModel, string> implements PromptModel {
  title: string;
  question1: string;
  question2: string;
  PageName:string;
  content:string;
  alertMsg: any = [];
  fmessage1: string = '';


  constructor(dialogService: DialogService,appService:AppServices) {
    super(dialogService);

    this.PageName="Notification";
    if(localStorage.getItem("alertContent")!=""){
   this.content=localStorage.getItem("alertContent");
    }
    if(localStorage.getItem("prodAlertMsg")!=""){
      this.alertMsg=JSON.parse(localStorage.getItem("prodAlertMsg"));
    }      
  }

  closeAlert(){
    //this.result=this.fmessage1;
  
    this.close();
  }

  ok(){
    this.result=this.fmessage1;
    this.close();
  }
}
