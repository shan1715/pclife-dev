import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';

export interface PromptModel {
  title: string;
  fquestion1: string;
  fquestion2: string;

}

@Component({
  selector: 'master-field-dialog',
  templateUrl: './master-field-dialog.component.html',
  styleUrls: ['./master-field-dialog.component.css']
})
export class MasterFieldDialogComponent extends DialogComponent<PromptModel, string> implements PromptModel  {

 @ViewChild('selectedFType') selectedFtype: ElementRef;
  @ViewChild('selectedMType') selectedMtype: ElementRef;

  title: string;
  fquestion1: string = "Item Name";
  fquestion2: string = "Item Type";
  fquestion3: string = "Field Type";
  fquestion4: string = "Masking Format";
  fmsgplaceholder1: string = "Please enter item name";
  fmsgplaceholder2: string = "Please enter item type";
  fmsgplaceholder3: string = "Please enter field type ";
  fmsgplaceholder4: string = "Please enter masking format";
  fmessage1: string = '';
  fmessage2: string = '';
  fmessage3: string = '';
  fmessage4: string = '';
  selectIT: boolean = true;
  selectFT: boolean = true;
  selectMT: boolean = true;
  fType: any = [];
  mType: any = [];
  selectedItype: boolean = true;
  listOfValues: boolean = false;
  tRow: any = [];
  masterFinalOutput: any = [];

  constructor(dialogService: DialogService) {
    super(dialogService);


  }

  itemType(sp) {


    this.selectIT = true;

    if (sp == "text") {
      this.listOfValues = false;
      this.fType = ['String', 'Number', 'Date'];
      this.selectedFtype.nativeElement.style.display = "inline-block";
      this.selectedFtype.nativeElement.previousElementSibling.style.display = "inline-block";
      this.selectedFtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    } else if (sp == "select") {
      this.fType = [];
      this.selectedItype = false;
      this.selectedFtype.nativeElement.style.display = "none";
      this.selectedFtype.nativeElement.previousElementSibling.style.display = "none";
      this.selectedFtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
      this.selectedMtype.nativeElement.style.display = "none";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "none";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
      this.listOfValues = true;

    } else if (sp == "date") {
      this.listOfValues = false;
      this.selectedFtype.nativeElement.style.display = "none";
      this.selectedFtype.nativeElement.previousElementSibling.style.display = "none";
      this.selectedFtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
      this.mType = [];
      this.mType.push({ "n": "DD/MM/YYYY", "id": "d2m2y4" }, { "n": "MM/DD/YYYY", "id": "m2d2y4" }, { "n": "YYYY/MM/DD", "id": "y4m2d2" }, { "n": "YYYY/DD/MM", "id": "y4d2m2" })
      this.selectedMtype.nativeElement.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    }
  }

  fieldType(sp) {
    this.selectFT = true;

    if (sp == "Number") {
      this.mType = [];
      this.mType.push({ "n": "Number (####)", "id": "number" }, { "n": "Number With Decimal (####.##)", "id": "numberwdecimal" })
      this.selectedMtype.nativeElement.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    } else if (sp == "String") {
      this.mType = [];
      this.selectedMtype.nativeElement.style.display = "none";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "none";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
    } else if (sp == "Date") {
      this.mType = [];
      this.mType.push({ "n": "DD/MM/YYYY", "id": "d2m2y4" }, { "n": "MM/DD/YYYY", "id": "m2d2y4" }, { "n": "YYYY/MM/DD", "id": "y4m2d2" }, { "n": "YYYY/DD/MM", "id": "y4d2m2" })
      this.selectedMtype.nativeElement.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    }

  }

  maskType(sp) {
    this.selectMT = true;
  }

  apply(it, ft, mt) {

    if (it == "text") {
      localStorage.setItem("fmsg1", this.fmessage1);
      localStorage.setItem("fieldCode", this.fmessage2);
      localStorage.setItem("fmsg2", it);
      localStorage.setItem("fmsg3", ft);
      localStorage.setItem("fmsg4", mt);
      this.masterFinalOutput.push({"field_parentwpercentage":"23","map_field_code":localStorage.getItem("fieldCode"),"item_name": localStorage.getItem("fmsg1"),"item_value":"", "item_type": localStorage.getItem("fmsg2"), "field_type": localStorage.getItem("fmsg3"), "mask_type": localStorage.getItem("fmsg4") })
      localStorage.setItem("masterOutput", JSON.stringify(this.masterFinalOutput));
      this.result = this.fmessage1;
      this.close();
    } else if (it == "select") {
      localStorage.setItem("fmsg1", this.fmessage1);
      localStorage.setItem("fieldCode", this.fmessage2);
      localStorage.setItem("fmsg2", it);
      var store = [];
      for (var i = 0; i < document.getElementsByClassName("tValues").length; i++) {
        store.push({ "optionLabel": (<HTMLInputElement>document.getElementsByClassName("tValues")[i].getElementsByTagName("td")[0].children[0]).value, "optionValue": (<HTMLInputElement>document.getElementsByClassName("tValues")[i].getElementsByTagName("td")[1].children[0]).value })
      }
      this.masterFinalOutput.push({ "field_parentwpercentage":"23","map_field_code":localStorage.getItem("fieldCode"), "item_name": localStorage.getItem("fmsg1"),"item_value":"", "item_type": localStorage.getItem("fmsg2"), "optionObj": store })
      localStorage.setItem("masterOutput", JSON.stringify(this.masterFinalOutput));
      this.result = this.fmessage1;
      this.close();
    } else if (it == "date") {
      localStorage.setItem("fmsg1", this.fmessage1);
      localStorage.setItem("fieldCode", this.fmessage2);
      localStorage.setItem("fmsg2", it);
      localStorage.setItem("fmsg4", mt);
      this.masterFinalOutput.push({ "field_parentwpercentage":"23","map_field_code":localStorage.getItem("fieldCode"), "item_name": localStorage.getItem("fmsg1"),"item_value":"", "item_type": localStorage.getItem("fmsg2"), "mask_type": localStorage.getItem("fmsg4") })
      localStorage.setItem("masterOutput", JSON.stringify(this.masterFinalOutput));
      this.result = this.fmessage1;
      this.close();
    }

    


  }

  incRow(eve) {
    this.tRow.push({ "row": "ok" })
  }

  decRow(eve) {
    this.tRow.splice(this.tRow.length - 1, 1);
  }
}
