import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../../app.service';
import { AlertComponent } from "../alert/alert.component";

export interface PromptModel {
    title: string;
    question1: string;
    question2: string;

}
@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent extends DialogComponent<PromptModel, string> implements PromptModel, OnInit {
    formModel = {

        skills0: [],
        skills1: [],
        skills2: [],
        skills3: [],
        skills4: [],
        skills5: [],
        skills6: [],
        skills7: [],
        skills8: [],
        skills9: [],
        skills10: [],
        skills11: [],
        skills12: [],
        skills13: [],
        skills14: [],
        skills15: [],
        skills16: [],
        skills17: [],
        skills18: [],
        skills19: [],
        skills20: [],
        skills21: [],
        skills22: [],
        skills23: [],
        skills24: [],
        skills25: [],
        skills26: [],
        skills27: []

    };



    tCreate: boolean = false;
    tManage: boolean = false;
    tDesign: boolean = false;
    tApprove: boolean = false;
    pCreate: boolean = false;
    pManage: boolean = false;
    pDesign: boolean = false;
    pApprove: boolean = false;
    title: string;
    question1: string;
    question2: string;
    datas: any = [];
    user: any = [];
    // previlages: any = [];
    // previlageslist: any = [];
    activities: any = [];
    userList: any = [];
    validitylist: any = [];
    squestlist: any = [];
    grpPrevilage: any = [];
    prename: any;
    preid: any;
    grpPrevilageEnable: boolean = false;
    activitiesEnable: boolean = false;
    creategroup: boolean = this.appServices.creategroup;
    editgroup: boolean = this.appServices.editgroup;
    panelHeading: string = "";
    createuser = this.appServices.createuser;
    edituser = this.appServices.edituser;
    fmessage1: string = '';
    optionsModel: number[];
    groupsSelected: boolean = false;
    //template
    createTemplate = [{ "id": 1, "itemName": "Create", "mode": "Create Template", "parent": "Template" }, { "id": 2, "itemName": "Copy", "mode": "Create Template", "parent": "Template" }];
    reviewTemplate = [{ "id": 1, "itemName": "Approval", "mode": "Review Template", "parent": "Template" }, { "id": 2, "itemName": "Deletion", "mode": "Review Template", "parent": "Template" }];
    tbasicDetails = [{ "id": 1, "itemName": "Creation", "mode": "Basic Details", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Basic Details", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Basic Details", "parent": "Template" }];
    tnewBusiness = [{ "id": 1, "itemName": "Creation", "mode": "New Business", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "New Business", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "New Business", "parent": "Template" }];
    tunderWritting = [{ "id": 1, "itemName": "Creation", "mode": "Under Writting", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Under Writting", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Under Writting", "parent": "Template" }];
    tprodBenefits = [{ "id": 1, "itemName": "Creation", "mode": "Prod Benefits", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Prod Benefits", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Prod Benefits", "parent": "Template" }];
    tunitLink = [{ "id": 1, "itemName": "Creation", "mode": "Unit Link", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Unit Link", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Unit Link", "parent": "Template" }];
    tagency = [{ "id": 1, "itemName": "Creation", "mode": "Agency", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Agency", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Agency", "parent": "Template" }];
    tannuity = [{ "id": 1, "itemName": "Creation", "mode": "Annuity", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Annuity", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Annuity", "parent": "Template" }];
    tgroup = [{ "id": 1, "itemName": "Creation", "mode": "Group", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Group", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Group", "parent": "Template" }];
    tpolicyServicing = [{ "id": 1, "itemName": "Creation", "mode": "Policy Servicing", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Policy Servicing", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Policy Servicing", "parent": "Template" }];
    tfinance = [{ "id": 1, "itemName": "Creation", "mode": "Finance", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Finance", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Finance", "parent": "Template" }];
    trates = [{ "id": 1, "itemName": "Creation", "mode": "Rates", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Rates", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Rates", "parent": "Template" }];
    tcharges = [{ "id": 1, "itemName": "Creation", "mode": "Charges", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Charges", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Charges", "parent": "Template" }];
    //product
    createProduct = [{ "id": 1, "itemName": "Create", "mode": "Create Product", "parent": "Product" }, { "id": 1, "itemName": "Copy", "mode": "Create Product", "parent": "Product" }];
    reviewProduct = [{ "id": 1, "itemName": "Approval", "mode": "Review Product", "parent": "Product" }, { "id": 2, "itemName": "Deletion", "mode": "Review Product", "parent": "Product" }];
    pbasicDetails = [{ "id": 1, "itemName": "Creation", "mode": "Basic Details" }, { "id": 2, "itemName": "Modification", "mode": "Basic Details", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Basic Details", "parent": "Product" }];
    pnewBusiness = [{ "id": 1, "itemName": "Creation", "mode": "New Business", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "New Business", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "New Business", "parent": "Product" }];
    punderWritting = [{ "id": 1, "itemName": "Creation", "mode": "Under Writting", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Under Writting", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Under Writting", "parent": "Product" }];
    pprodBenefits = [{ "id": 1, "itemName": "Creation", "mode": "Prod Benefits", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Prod Benefits", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Prod Benefits", "parent": "Product" }];
    punitLink = [{ "id": 1, "itemName": "Creation", "mode": "Unit Link", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "UnitLink", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Unit Link", "parent": "Product" }];
    pagency = [{ "id": 1, "itemName": "Creation", "mode": "Agency", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Agency", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Agency", "parent": "Product" }];
    pannuity = [{ "id": 1, "itemName": "Creation", "mode": "Annuity", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Annuity", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Annuity", "parent": "Product" }];
    pgroup = [{ "id": 1, "itemName": "Creation", "mode": "Group", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Group", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Group", "parent": "Product" }];
    ppolicyServicing = [{ "id": 1, "itemName": "Creation", "mode": "Policy Servicing", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Policy Servicing", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Policy Servicing", "parent": "Product" }];
    pfinance = [{ "id": 1, "itemName": "Creation", "mode": "Finance", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Finance", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Finance", "parent": "Product" }];
    prates = [{ "id": 1, "itemName": "Creation", "mode": "Rates", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Rates", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Rates", "parent": "Product" }];
    pcharges = [{ "id": 1, "itemName": "Creation", "mode": "Charges", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Charges", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Charges", "parent": "Product" }];



    sSettings = {};
    mSettings = {};

    grpSettings = {};

    grpPermission = [];
    selunsel: string = 'Select All';
    grpselunsel: string = 'Select All';
    enableGrpPerm: boolean = false;
    editGrpPerm: boolean = false;
    editPswd: string;
    editStatusFlag: number;
    
    ws_url: any;



    @ViewChild("f") public fields: NgForm;
    @ViewChild("selectedPrevilage") public selctedPrevilage: ElementRef;
    @ViewChild("grpId") grpId: ElementRef;

    ngOnInit() {

        if (this.creategroup) {
            this.editGrpPerm = true;
        } else {
            this.editGrpPerm = false;
        }


        this.sSettings = {
            singleSelection: false,
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: false,
            classes: "myclass custom-class"
        };
        this.mSettings = {
            singleSelection: false,
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: false,
            classes: "myclass custom-class"
        };

        this.grpSettings = {
            singleSelection: false,
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: false,
            classes: "myclass custom-class",
            disabled: true
        };



        if (this.createuser) {
            this.panelHeading = "Create User";
        } else if (this.edituser) {
            this.panelHeading = "Edit User";
        }


        // this.http.get('assets/data/groups.json')
        //     .subscribe(res => {

        //         this.datas = res.json();



        //         // for (var i = 0; i < this.datas.length; i++) {


        //         //     for (var j = 0; j < this.datas[i].previlage.length; j++) {
        //         //         this.previlages.push(this.datas[i].previlage[j])
        //         //     }
        //         // }

        //     })

        // this.http.get('assets/data/user.json')
        //     .subscribe(res => {

        //         this.user = res.json();


        //     })

        // Updated by Pisith 25/11/2019
        this.http.get('http://localhost:4777/api/getGroups')
            .subscribe(res => {
                this.datas = res.json();
            })
        // Updated by Pisith 25/11/2019

        // Updated by Pisith 25/11/2019
        this.http.get('http://localhost:4777/api/getUsers')
            .subscribe(res => {
                this.user = res.json();
            })
        // Updated by Pisith 25/11/2019

        // this.http.get('assets/data/previlagelist.json')
        //     .subscribe(res => {

        //         this.previlageslist = res.json();


        //     })

            this.validitylist.push({ "name": "Enabled", "selected": false }, { "name": "Disabled", "selected": false });
            this.squestlist.push({ "question": "What is your nickname?", "selected": false }, { "question": "What is your favorite color?", "selected": false },{ "question": "What is your birth place", "selected": false });

    }

    onItemSelect(item: any) {
        this.groupsSelected=false;
        var s=0;   


        for (var i = 0; i < Object.keys(this.formModel).length; i++) {
            if (this.formModel[Object.keys(this.formModel)[i]].length > 0) {
                this.groupsSelected = true;
++s;
            }
            if(i==Object.keys(this.formModel).length-1){
                if(s==Object.keys(this.formModel).length){
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked=true;

                }else{
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked=false; 
                }
            }
        }

    }
    OnItemDeSelect(item: any) {
           this.groupsSelected=false;
           var s=0;   

        for (var i = 0; i < Object.keys(this.formModel).length; i++) {
            if (this.formModel[Object.keys(this.formModel)[i]].length > 0) {
                this.groupsSelected = true;
++s;
            }
            if(i==Object.keys(this.formModel).length-1){
                if(s==Object.keys(this.formModel).length){
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked=true;

                }else{
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked=false; 
                }
            }   
        }
    }
    onSelectAll(items: any) {
           this.groupsSelected=false;
           var s=0;   

        for (var i = 0; i < Object.keys(this.formModel).length; i++) {
            if (this.formModel[Object.keys(this.formModel)[i]].length > 0) {
                this.groupsSelected = true;
                ++s;
            }
            if(i==Object.keys(this.formModel).length-1){
                if(s==Object.keys(this.formModel).length){
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked=true;

                }else{
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked=false; 
                }
            }
        }
    }
    onDeSelectAll(items: any) {
           this.groupsSelected=false;
           var s=0;   
           for (var i = 0; i < Object.keys(this.formModel).length; i++) {
            if (this.formModel[Object.keys(this.formModel)[i]].length > 0) {
                this.groupsSelected = true;
                ++s;
            }
            if(i==Object.keys(this.formModel).length-1){
                if(s==Object.keys(this.formModel).length){
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked=true;

                }else{
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked=false; 
                }
            }
        }

    }
    tempCreate() {
        var hide = ["2", "3", "4", "5", "6"];
        this.hidePrevList("1", hide);

        this.tCreate = true;
        this.tManage = false;
        this.tDesign = false;
        this.pCreate = false;
        this.pManage = false;
        this.pDesign = false;


    }
    tempManage() {
        var hide = ["1", "3", "4", "5", "6"];
        this.hidePrevList("2", hide);
        this.tCreate = false;
        this.tManage = true;
        this.tDesign = false;
        this.pCreate = false;
        this.pManage = false;
        this.pDesign = false;
    }
    tempDesign() {
        var hide = ["2", "1", "4", "5", "6"];
        this.hidePrevList("3", hide);
        this.tCreate = false;
        this.tManage = false;
        this.tDesign = true;
        this.pCreate = false;
        this.pManage = false;
        this.pDesign = false;
    }

    prodCreate() {
        var hide = ["1", "2", "3", "5", "6"];
        this.hidePrevList("4", hide);
        this.tCreate = false;
        this.tManage = false;
        this.tDesign = false;
        this.pCreate = true;
        this.pManage = false;
        this.pDesign = false;
    }
    prodManage() {
        var hide = ["1", "2", "3", "4", "6"];
        this.hidePrevList("5", hide);
        this.tCreate = false;
        this.tManage = false;
        this.tDesign = false;
        this.pCreate = false;
        this.pManage = true;
        this.pDesign = false;
    }
    prodDesign() {
        var hide = ["1", "2", "3", "4", "5"];
        this.hidePrevList("6", hide);
        this.tCreate = false;
        this.tManage = false;
        this.tDesign = false;
        this.pCreate = false;
        this.pManage = false;
        this.pDesign = true;
    }


    hidePrevList(showID, hideID) {

        for (var d = 0; d < hideID.length; d++) {
            for (var i = 0; i < document.getElementById(hideID[d]).parentElement.parentElement.getElementsByClassName("sp").length; i++) {
                if (document.getElementById(hideID[d]).parentElement.parentElement.getElementsByClassName("sp")[i].id == hideID[d]) {
                    (<HTMLInputElement>document.getElementById(hideID[d]).parentElement.parentElement.getElementsByClassName("sp")[i]).style.display = "none";
                }
            }
        }

        for (var j = 0; j < document.getElementById(showID).parentElement.parentElement.getElementsByClassName("sp").length; j++) {
            if (document.getElementById(showID).parentElement.parentElement.getElementsByClassName("sp")[j].id == showID) {
                (<HTMLInputElement>document.getElementById(showID).parentElement.parentElement.getElementsByClassName("sp")[j]).style.display = "inherit";
            }
        }
    }



    getDetails(eve) {
        this.editPswd = "";
        this.editStatusFlag = 0;

        for (var i = 0; i < this.user.length; i++) {
            if (eve.target.value == this.user[i].username) {
                this.editPswd = this.user[i].password;
                this.editStatusFlag = this.user[i].statusFlag;
                this.fields.controls["grpId"].setValue(this.user[i].groupid);
                this.formModel = this.user[i].gpa;
                this.fields.controls["firstname"].setValue(this.user[i].firstname);
                this.fields.controls["lastname"].setValue(this.user[i].lastname);
                this.fields.controls["useremail"].setValue(this.user[i].email);
                this.fields.controls["usertext"].setValue(this.user[i].description);
                this.fields.controls["sans"].setValue(this.user[i].securityAns);
                this.fields.controls["expiredate"].setValue(this.user[i].expiredate);
                
                for (var k = 0; k < this.squestlist.length; k++) {
                    if (this.squestlist[k].question == this.user[i].securityQuest) {
                        this.squestlist[k].selected = true;
                        this.fields.controls["squest"].setValue(true);
                        break;
                    } else {
                        this.squestlist[k].selected = false;
                        this.fields.controls["squest"].setValue(true);
                    }
                }

                if (this.user[i].status) {
                    for (var j = 0; j < this.validitylist.length; j++) {
                        if (this.validitylist[j].name == "Enabled") {
                            this.validitylist[j].selected = true;
                            this.fields.controls["validity"].setValue(true);
                            break;
                        } else {
                            this.validitylist[j].selected = false;
                            this.fields.controls["validity"].setValue(true);
                        }
                    }

                } else if (this.user[i].status == "Disabled") {
                    for (var j = 0; j < this.validitylist.length; j++) {
                        if (this.validitylist[j].name == "Disabled") {
                            this.validitylist[j].selected = true;
                            this.fields.controls["validity"].setValue(true);
                            break;
                        } else {
                            this.validitylist[j].selected = false;
                            this.fields.controls["validity"].setValue(true);
                        }
                    }

                }
                   
                this.enableGrpPerm = true;
                setTimeout(() => {
                
                    for (var i = 0; i < this.user.length; i++) {
                        if (eve.target.value == this.user[i].username) {
                                  if(this.user[i].selectedPermissions.length==28){
                        (<HTMLInputElement>document.getElementById("selAllPerm")).checked=true;
                        this.selunsel = "UnSelect All";


                    }
                            for (var k = 0; k < this.user[i].selectedPermissions.length; k++) {


                                for (var h = 0; h < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; h++) {

                                    if (this.user[i].selectedPermissions[k].name == document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[h].parentElement.getElementsByTagName("angular2-multiselect")[0].getAttribute("name")) {
                                        (<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[h].parentElement.children[0]).checked = true;
                                        this.groupsSelected=true;
                                        

                                    }
                                }
                            }
                        }
                    }
                }, 1000);
            

            }
        }
    }



    constructor(private appServices: AppServices, dialogService: DialogService, private http: Http) {
        super(dialogService);

    }


    grpSel(eve) {
        this.grpPrevilage = [];
        this.activities = [];


        this.grpPermission = [];
        for (var i = 0; i < this.datas.length; i++) {
            if (this.datas[i].id == eve.target.value) {
                for (var j = 0; j < Object.keys(this.datas[i].selectedRights).length; j++) {
                    if (Object.keys(this.datas[i].selectedRights)[j].length > 0) {
                        this.formModel = this.datas[i].selectedRights;
                    }
                }
            }
        }
        this.enableGrpPerm = true;
        var enableSelAll = false;
        for (var i = 0; i < Object.keys(this.formModel).length; i++) {
            if (this.formModel[Object.keys(this.formModel)[i]].length != 0) {
                enableSelAll = true;
            }
        }
        if (enableSelAll) {
            if ((<HTMLInputElement>document.getElementById("selInGrp"))) {
                (<HTMLInputElement>document.getElementById("selInGrp")).checked = true;
                this.selunsel = "UnSelect All";
            }

        } else {
            if ((<HTMLInputElement>document.getElementById("selInGrp"))) {
                (<HTMLInputElement>document.getElementById("selInGrp")).checked = false;
                this.selunsel = "Select All";
            }
        }



  
    }

    // grpPrevilageSel(eve,selPre) {

    //     for (var i = 0; i < this.grpPrevilage.length; i++) {
    //         if (eve.target.value == this.grpPrevilage[i].id && eve.target["options"][eve.target["options"].selectedIndex].text == this.grpPrevilage[i].priname) {


    //             this.activities = this.grpPrevilage[i].activity;

    //         }

    //     }
    // }

    actList(eve) {
        this.activitiesEnable = true;
        for (var i = 0; i < eve.target.parentElement.parentElement.getElementsByTagName("input").length; i++) {

            if (eve.target.parentElement.parentElement.getElementsByTagName("input")[i].type == "button") {
                eve.target.parentElement.parentElement.getElementsByTagName("input")[i].style.backgroundColor = "";
            }
        }
        eve.target.style.backgroundColor = "limegreen"

        this.preid = eve.target.previousElementSibling.id;
        this.prename = eve.target.previousElementSibling.textContent.split("(")[0].trim();
        for (var i = 0; i < this.grpPrevilage.length; i++) {
            if (eve.target.previousElementSibling.id == this.grpPrevilage[i].id && eve.target.previousElementSibling.textContent.split("(")[0].trim() == this.grpPrevilage[i].priname) {


                this.activities = this.grpPrevilage[i].activity;

            }

        }
    }

    actSelected(eve) {
        //alert((<HTMLInputElement>document.getElementsByClassName("selectedActivity")[0]).checked);
        var id = this.preid;
        var prename = this.prename;
        var actname = eve.target.nextElementSibling.textContent;
        for (var i = 0; i < this.grpPrevilage.length; i++) {
            if (id == this.grpPrevilage[i].id && prename == this.grpPrevilage[i].priname) {
                var prevSelection = false;
                for (var k = 0; k < this.grpPrevilage[i].activity.length; k++) {

                    if (actname == this.grpPrevilage[i].activity[k].name) {
                        this.grpPrevilage[i].activity[k].selected = eve.target.checked;

                    }




                }
                for (var k = 0; k < this.grpPrevilage[i].activity.length; k++) {

                    if (this.grpPrevilage[i].activity[k].selected) {
                        prevSelection = true;
                    }



                }
                if (prevSelection) {
                    this.grpPrevilage[i].selected = true;
                } else {
                    this.grpPrevilage[i].selected = false;
                }

                //this.activities = this.grpPrevilage[i].activity;

            }

        }
    }

    selActivity(eve, selPre) {
        for (var i = 0; i < this.grpPrevilage.length; i++) {
            if (eve.target.value == this.grpPrevilage[i].id && eve.target["options"][eve.target["options"].selectedIndex].text == this.grpPrevilage[i].priname) {

                for (var k = 0; k < this.grpPrevilage[i].activity.length; k++) {
                    for (var j = 0; j < document.getElementsByClassName("selectedActivity").length; j++) {

                        if ((<HTMLInputElement>document.getElementsByClassName("selectedActivity")[j]).value == this.grpPrevilage[i].activity[k].name) {
                            this.grpPrevilage[i].activity[k].selected = (<HTMLInputElement>document.getElementsByClassName("selectedActivity")[j]).checked;

                        }
                    }



                }
                //this.activities = this.grpPrevilage[i].activity;

            }

        }
    }

    submit(f: NgForm) {


        if (this.edituser) {
            var user = [];
            user.push({
                "username": (<HTMLInputElement>document.getElementById("uname")).value,
                "password": this.editPswd,
                "firstname": f.form.controls["firstname"].value,
                "lastname": f.form.controls["lastname"].value,
                "email": f.form.controls["useremail"].value,
                "description": f.form.controls["usertext"].value,
                "securityQuest": f.form.controls["squest"].value,
                "securityAns": f.form.controls["sans"].value,
                "groupid": f.form.controls["grpId"].value,
                "gpa": this.formModel,
                "status": f.form.controls["validity"].value,
                "expiredate": f.form.controls["expiredate"].value,
                "statusFlag": this.editStatusFlag
            })

            var selectedPermissions = [];

            for (var i = 0; i < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; i++) {
                if ((<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.children[0]).checked) {
                    selectedPermissions.push({ "name": document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.getElementsByTagName("angular2-multiselect")[0].getAttribute("name") })

                }
            }

            user[0]["selectedPermissions"] = selectedPermissions;
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.http.post('/api/updateUser', user[0], { headers: headers })
                .subscribe(res => {
                    this.alertMsg("Updated successfully");
                    this.enableGrpPerm = false;
                    this.close();
                }, err => {
                })

        } else {
            if (this.user.length > 0) {
                var userCreated=false;
                for (var i = 0; i < this.user.length; i++) {
                    if (this.user[i].username == f.form.controls["username"].value) {
                        userCreated=true;
                        this.alertMsg("Username already taken");
                        break;
                    }
                 } 

                 if(!userCreated){
                        var passwordOTP;
                        this.http.get('assets/data/property.json')
                        .subscribe(resp => {
                          this.ws_url = resp.json().web.url;
                        var mail = f.form.controls["useremail"].value;
                        this.http.post(this.ws_url + "ProductInformation/SendEMail/",{ "eMailTo" : mail }, { headers: headers })
                          .subscribe(resCheck => {
                            if((<any>resCheck)._body=="Error"){
                                alert("Password not generated");
                            }else{
                                passwordOTP = (<any>resCheck)._body;

                        var user = []; 
                        user.push({
                            "username": f.form.controls["username"].value,
                            "password": passwordOTP,
                            "firstname": f.form.controls["firstname"].value,
                            "lastname": f.form.controls["lastname"].value,
                            "email": f.form.controls["useremail"].value,
                            "description": f.form.controls["usertext"].value,
                            "securityQuest": f.form.controls["squest"].value,
                            "securityAns": f.form.controls["sans"].value,            
                            "groupid": f.form.controls["grpId"].value,
                            "gpa": this.formModel,
                            "status": f.form.controls["validity"].value,
                            "expiredate": f.form.controls["expiredate"].value,
                            "statusFlag": 1
                        })

                        var selectedPermissions = [];

                        for (var i = 0; i < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; i++) {
                            if ((<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.children[0]).checked) {
                                selectedPermissions.push({ "name": document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.getElementsByTagName("angular2-multiselect")[0].getAttribute("name") })

                            }
                        }

                        user[0]["selectedPermissions"] = selectedPermissions;

                        var headers = new Headers();
                        headers.append('Content-Type', 'application/json');
                        this.http.post('http://localhost:4777/api/createUser', user[0], { headers: headers })
                            .subscribe(res => {
                                this.alertMsg("Created successfully and Password sent to your registered Email id");
                                this.enableGrpPerm = false;
                                this.close();
                            }, err => {
                            })
                            }
                        }, err => {
                            console.log("err " + err);
                          })
                    }); 

                    // var user = [];
                    // user.push({
                    //     "username": f.form.controls["username"].value,
                    //     "password": "nee23a",
                    //     "firstname": f.form.controls["firstname"].value,
                    //     "lastname": f.form.controls["lastname"].value,
                    //     "email": f.form.controls["useremail"].value,
                    //     "description": f.form.controls["usertext"].value,
                    //     "securityQuest": f.form.controls["squest"].value,
                    //     "securityAns": f.form.controls["sans"].value,            
                    //     "groupid": f.form.controls["grpId"].value,
                    //     "gpa": this.formModel,
                    //     "status": f.form.controls["validity"].value,
                    //     "expiredate": f.form.controls["expiredate"].value,
                    //     "statusFlag": 1
                    // })

                    // var selectedPermissions = [];

                    // for (var i = 0; i < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; i++) {
                    //     if ((<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.children[0]).checked) {
                    //         selectedPermissions.push({ "name": document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.getElementsByTagName("angular2-multiselect")[0].getAttribute("name") })

                    //     }
                    // }

                    // user[0]["selectedPermissions"] = selectedPermissions;

                    // var headers = new Headers();
                    // headers.append('Content-Type', 'application/json');
                    // this.http.post('/api/createUser', user[0], { headers: headers })
                    //     .subscribe(res => {
                    //         this.alertMsg("Created successfully and OTP sent to your registered Email id");
                    //         this.enableGrpPerm = false;
                    //         this.close();
                    //     }, err => {
                    //     })

                    
                }
                // Updated by Pisith
                // Generate password for new user
                // we send passwordOTP to user and encrypt password => set to user object => save to db
                // but we encrypt it in node js project (api/createUser)

            } else {
                var passwordOTP;
                this.http.get('assets/data/property.json')
                .subscribe(resp => {
                  this.ws_url = resp.json().web.url;
                var mail = f.form.controls["useremail"].value;
               // this.http.post("http://10.200.104.69:8080/productConfig/ProductInformation/SendEMail/",{ "eMailTo" : mail }, { headers: headers })
                this.http.post(this.ws_url+"ProductInformation/SendEMail/",{ "eMailTo" : mail }, { headers: headers })
                .subscribe(resCheck => {
                    if((<any>resCheck)._body=="Error"){
                        alert("Password not generated");
                    }else{
                        passwordOTP = (<any>resCheck)._body;
                        var user = [];
                        user.push({
                            "username": f.form.controls["username"].value,
                            "password": passwordOTP,
                            "firstname": f.form.controls["firstname"].value,
                            "lastname": f.form.controls["lastname"].value,
                            "email": f.form.controls["useremail"].value,
                            "description": f.form.controls["usertext"].value,
                            "securityQuest": f.form.controls["squest"].value,
                            "securityAns": f.form.controls["sans"].value,    
                            "groupid": f.form.controls["grpId"].value,
                            "gpa": this.formModel,
                            "status": f.form.controls["validity"].value,
                            "expiredate": f.form.controls["expiredate"].value,
                            "statusFlag": 1
                        })
        
                        var selectedPermissions = [];
        
                        for (var i = 0; i < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; i++) {
                            if ((<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.children[0]).checked) {
                                selectedPermissions.push({ "name": document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.getElementsByTagName("angular2-multiselect")[0].getAttribute("name") })
        
                            }
                        }
        
                        user[0]["selectedPermissions"] = selectedPermissions;
        
                        var headers = new Headers();
                        headers.append('Content-Type', 'application/json');
                        this.http.post('/api/createUser', user[0], { headers: headers })
                            .subscribe(res => {
                                this.alertMsg("Created successfully and Password sent to your registered Email id");
                                this.enableGrpPerm = false;
                                this.close();
                            }, err => {
        
                            })
                    }
                })
            });

              

            }

        }
    }


    closeno() {
        this.close();
    }

    createGroup(cg: NgForm) {
        var newGroup = [];
        var groupAvailable = false;
        if (this.creategroup) {
            groupAvailable = true;

            for (var k = 0; k < this.datas.length; k++) {
                if (this.datas[k].title == cg.form.controls["groupname"].value) {

                    this.alertMsg("Groupname already taken");
                    groupAvailable = false;
                    break;
                }
            }
            if (groupAvailable == true) {
                var selectedPre = [];
                var id = this.datas.length + 1;

                newGroup.push({ "userid": 1, "id": id.toString(), "title": cg.form.controls["groupname"].value, "status": cg.form.controls["validity"].value, "selectedRights": [] })

                // for (var j = 0; j < this.previlageslist.length; j++) {

                //     for (var i = 0; document.getElementsByClassName("sp").length; i++) {
                //         if (document.getElementsByClassName("sp")[i] != undefined) {
                //             if ((<HTMLInputElement>document.getElementsByClassName("sp")[i]).checked) {
                //                 if (document.getElementsByClassName("sp")[i].id == this.previlageslist[j].id && (<HTMLInputElement>document.getElementsByClassName("sp")[i]).value == this.previlageslist[j].priname) {
                //                     selectedPre.push(this.previlageslist[j]);
                //                 }


                //             }

                //         } else {
                //             break;
                //         }
                //     }
                // }
                // newGroup[0].previlage = selectedPre;

                var selectedRights = [];

                //working

                // for (var i = 0; i < Object.keys(this.formModel).length; i++) {
                //     if (this.formModel[Object.keys(this.formModel)[i]].length > 0) {
                //         selectedRights.push(this.formModel[Object.keys(this.formModel)[i]])
                //     }
                // }

                newGroup[0].selectedRights = this.formModel;
                this.datas.push(newGroup[0]);

                var headers = new Headers();
                headers.append('Content-Type', 'application/json');
                this.http.post('/api/postGroup', this.datas, { headers: headers })
                    .subscribe(res => {
                        this.alertMsg("Created Sucessfully.");
                        this.result = this.fmessage1;
                        this.editGrpPerm = false;
                        this.close();
                    }, err => {

                    })
            }
        } else if (this.editgroup) {
            for (var k = 0; k < this.datas.length; k++) {
                if (this.datas[k].id == cg.form.controls["grpId"].value) {
                    this.datas[k].status = (<HTMLInputElement>document.getElementById("validity")).value;
                    this.datas[k].selectedRights = this.formModel;

                }
            }
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.http.post('/api/postGroup', this.datas, { headers: headers })
                .subscribe(res => {
                    this.alertMsg("Edited Sucessfully.");
                    this.result = this.fmessage1;
                    this.editGrpPerm = false;
                    this.close();
                }, err => {

                })

        }


    }

    groupList(eve,cg) {
        this.editGrpPerm = true;

        var enableSelAll = true;

        for (var i = 0; i < this.datas.length; i++) {
            if (eve.target.value == this.datas[i].id) {
                this.formModel = this.datas[i].selectedRights;
                 if (this.datas[i].status=="Enabled") {
                    for (var j = 0; j < this.validitylist.length; j++) {
                        if (this.validitylist[j].name == "Enabled") {
                            this.validitylist[j]["selected"] = true;
                            cg.form.controls["validity"].setValue(true);
                        }
                    }

                } else if (this.datas[i].status=="Disabled") {
                    for (var j = 0; j < this.validitylist.length; j++) {
                        if (this.validitylist[j].name == "Disabled") {
                            this.validitylist[j]["selected"] = true;
                            cg.form.controls["validity"].setValue(true);
                        }
                    }

                }
                for (var i = 0; i < Object.keys(this.formModel).length; i++) {
                    if (this.formModel[Object.keys(this.formModel)[i]].length == 0) {
                        enableSelAll = false;
                    }
                }
                setTimeout(res=>{

                if (enableSelAll) {
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked = true;
                    this.grpselunsel = "UnSelect All";
                    this.groupsSelected=true;
                 
                } else {
                    (<HTMLInputElement>document.getElementById("selInGrp")).checked = false;
                    this.grpselunsel = "Select All";
                    this.groupsSelected=false;
             
                }
               
                 },1500)

            }
        }



    }



    // deletePre(eve) {
    //     for (var i = 0; i < this.previlages.length; i++) {
    //         if (eve.target.previousElementSibling.id == this.previlages[i].id && eve.target.previousElementSibling.textContent.split("(")[0].trim() == this.previlages[i].priname) {
    //             this.previlages.splice(i, 1)

    //         }
    //     }
    // }

    editGroup(cg: NgForm) {
        for (var i = 0; i < this.datas.length; i++) {
            if (this.datas[i].id == cg.controls["grpId"].value) {
                this.datas[i].status = cg.controls["validity"].value;
            }
        }
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post('/api/postGroup', this.datas, { headers: headers })
            .subscribe(res => {
                this.result = this.fmessage1;
                this.close();


            }, err => {
            })
    }

    pswdchk(eve) {
        if (eve.target.value != "") {
            if (eve.target.id == "pswd" || eve.target.id == "cpswd") {
                var pswd = (<HTMLInputElement>document.getElementById("pswd")).value;
                var cpswd = (<HTMLInputElement>document.getElementById("cpswd")).value;
                if (pswd != "" && cpswd != "") {
                    if (pswd != cpswd) {
                        this.alertMsg("Password and Confirm password doesn't match");
                    }
                }
            }
        }
    }

    dateChk(eve) {
        var tDate = new Date();
        if (eve.target.value == "") {
            eve.target.value = "";
        } else if (eve.target.value.length > 10) {
            this.alertMsg("Please enter valid Date");
            eve.target.value = "";

        } else {
            var xdate = new Date(eve.target.value);
            if (xdate.setHours(0, 0, 0, 0) < tDate.setHours(0, 0, 0, 0)) {
                this.alertMsg("Invalid Date");
                eve.target.value = "";

            }
        }
    }

    alertMsg(msg) {
        localStorage.setItem("alertContent", msg);
        this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '

        }).subscribe((isConfirmed) => {
        });
    }

    selPerm() {
        if (this.selunsel == "UnSelect All") {
         
            for (var i = 0; i < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; i++) {

                (<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.children[0]).checked = false;



            }
            this.groupsSelected=false;
            this.selunsel = "Select All";
        } else if (this.selunsel == "Select All") {

            for (var i = 0; i < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; i++) {

                (<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.children[0]).checked = true;



            }
              this.groupsSelected=true;
            this.selunsel = "UnSelect All";

        }
    }

    userGroupSelection(){
        this.groupsSelected=false;
        var checkSelection=true;
         for (var i = 0; i < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; i++) {

                if((<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.children[0]).checked){
                     this.groupsSelected=true;
                }else{
                    checkSelection=false;
                }



            }
if(!checkSelection){
            (<HTMLInputElement>document.getElementById("selAllPerm")).checked=false;
            this.selunsel = "Select All";
}else{
    (<HTMLInputElement>document.getElementById("selAllPerm")).checked=true;
    this.selunsel = "UnSelect All";
}

    }

    selGrpAll() {
        if (this.grpselunsel == "Select All") {
            this.formModel = {
                "skills0": [{ "id": 1, "itemName": "Create", "mode": "Create Template", "parent": "Template" }, { "id": 2, "itemName": "Copy", "mode": "Create Template", "parent": "Template" }], "skills1": [{ "id": 1, "itemName": "Approval", "mode": "Review Template", "parent": "Template" }, { "id": 2, "itemName": "Deletion", "mode": "Review Template", "parent": "Template" }], "skills2": [{ "id": 1, "itemName": "Creation", "mode": "Basic Details", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Basic Details", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Basic Details", "parent": "Template" }], "skills3": [{ "id": 1, "itemName": "Creation", "mode": "New Business", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "New Business", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "New Business", "parent": "Template" }], "skills4": [{ "id": 1, "itemName": "Creation", "mode": "Under Writting", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Under Writting", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Under Writting", "parent": "Template" }],
                "skills5": [{ "id": 1, "itemName": "Creation", "mode": "Prod Benefits", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Prod Benefits", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Prod Benefits", "parent": "Template" }],"skills6": [{ "id": 1, "itemName": "Creation", "mode": "Unit Link", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Unit Link", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Unit Link", "parent": "Template" }], "skills7": [{ "id": 1, "itemName": "Creation", "mode": "Agency", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Agency", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Agency", "parent": "Template" }], "skills8": [{ "id": 1, "itemName": "Creation", "mode": "Annuity", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Annuity", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Annuity", "parent": "Template" }], "skills9": [{ "id": 1, "itemName": "Creation", "mode": "Group", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Group", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Group", "parent": "Template" }], "skills10": [{ "id": 1, "itemName": "Creation", "mode": "Policy Servicing", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Policy Servicing", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Policy Servicing", "parent": "Template" }], "skills11": [{ "id": 1, "itemName": "Creation", "mode": "Finance", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Finance", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Finance", "parent": "Template" }], "skills12": [{ "id": 1, "itemName": "Creation", "mode": "Rates", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Rates", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Rates", "parent": "Template" }], "skills13": [{ "id": 1, "itemName": "Creation", "mode": "Charges", "parent": "Template" }, { "id": 2, "itemName": "Modification", "mode": "Charges", "parent": "Template" }, { "id": 3, "itemName": "View", "mode": "Charges", "parent": "Template" }], "skills14": [{ "id": 1, "itemName": "Create", "mode": "Create Product", "parent": "Product" }, { "id": 2, "itemName": "Copy", "mode": "Create Product", "parent": "Product" }], "skills15": [{ "id": 1, "itemName": "Approval", "mode": "Review Product", "parent": "Product" }, { "id": 2, "itemName": "Deletion", "mode": "Review Product", "parent": "Product" }], "skills16": [{ "id": 1, "itemName": "Creation", "mode": "Basic Details" }, { "id": 2, "itemName": "Modification", "mode": "Basic Details", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Basic Details", "parent": "Product" }], "skills17": [{ "id": 1, "itemName": "Creation", "mode": "New Business", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "New Business", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "New Business", "parent": "Product" }], "skills18": [{ "id": 1, "itemName": "Creation", "mode": "Under Writting", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Under Writting", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Under Writting", "parent": "Product" }], "skills19": [{ "id": 1, "itemName": "Creation", "mode": "Prod Benefits", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Prod Benefits", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Prod Benefits", "parent": "Product" }], "skills20": [{ "id": 1, "itemName": "Creation", "mode": "Unit Link", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "UnitLink", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Unit Link", "parent": "Product" }], "skills21": [{ "id": 1, "itemName": "Creation", "mode": "Agency", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Agency", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Agency", "parent": "Product" }], "skills22": [{ "id": 1, "itemName": "Creation", "mode": "Annuity", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Annuity", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Annuity", "parent": "Product" }], "skills23": [{ "id": 1, "itemName": "Creation", "mode": "Group", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Group", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Group", "parent": "Product" }], "skills24": [{ "id": 1, "itemName": "Creation", "mode": "Policy Servicing", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Policy Servicing", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Policy Servicing", "parent": "Product" }], "skills25": [{ "id": 1, "itemName": "Creation", "mode": "Finance", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Finance", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Finance", "parent": "Product" }], "skills26": [{ "id": 1, "itemName": "Creation", "mode": "Rates", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Rates", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Rates", "parent": "Product" }], "skills27": [{ "id": 1, "itemName": "Creation", "mode": "Charges", "parent": "Product" }, { "id": 2, "itemName": "Modification", "mode": "Charges", "parent": "Product" }, { "id": 3, "itemName": "View", "mode": "Charges", "parent": "Product" }]
            };
            this.groupsSelected=true;
            this.grpselunsel = "UnSelect All";


            // for (var i = 0; i < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; i++) {

            //     (<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.children[0]).checked=false;
                 



            // }

        } else if (this.grpselunsel == "UnSelect All") {
            this.formModel = {

                skills0: [],
                skills1: [],
                skills2: [],
                skills3: [],
                skills4: [],
                skills5: [],
                skills6: [],
                skills7: [],
                skills8: [],
                skills9: [],
                skills10: [],
                skills11: [],
                skills12: [],
                skills13: [],
                skills14: [],
                skills15: [],
                skills16: [],
                skills17: [],
                skills18: [],
                skills19: [],
                skills20: [],
                skills21: [],
                skills22: [],
                skills23: [],
                skills24: [],
                skills25: [],
                skills26: [],
                skills27: []
            };
            this.groupsSelected=false;
            this.grpselunsel = "Select All";

            // for (var i = 0; i < document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect").length; i++) {

            //     (<HTMLInputElement>document.getElementById("selAllPerm").parentElement.getElementsByTagName("angular2-multiselect")[i].parentElement.children[0]).checked=true;
                 



            // }
        }
    }

}

