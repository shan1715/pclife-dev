import { Component, OnInit, ViewChild, ElementRef,AfterContentInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../../app.service';
import { AlertComponent } from "../alert/alert.component";
import { Router } from '@angular/router';

export interface PromptModel {
  title: string;
  question1: string;
  question2: string;

}
@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent extends DialogComponent<PromptModel, string> implements PromptModel, OnInit,AfterContentInit {
  title: string;
  question1: string;
  question2: string;
  panelHeading: string = "";
  user: any = [];
  flag: boolean = false;
  changepswd: boolean = false;
  forgetpswd: boolean = false;
  squest: string=null;
  sAnswer: string=null;
  uname: string=null;
  viewDesc:any;
  viewDescContent:any;
  
  @ViewChild("f") public fields: NgForm;
  
  ngOnInit() {
    if(this.appServices.auwDesc!=''){
      this.viewDesc=true;
    this.viewDescContent=this.appServices.auwDesc;
  }
  }
  ngAfterContentInit(){
    this.uname=localStorage.getItem("user");
    
    this.http.get('assets/data/user.json')
    .subscribe(res => {

        this.user = res.json();
        
if(this.appServices.forgetpswd){
  for (var i = 0; i < this.user.length; i++) {
    if (this.uname == this.user[i].username) {
      this.squest = this.user[i].securityQuest;
      this.sAnswer = this.user[i].securityAns;
      this.forgetpswd = this.appServices.forgetpswd;
      break;
    }
  }
    if(this.squest == null || this.sAnswer == null){
      //this.router.navigate(['']);
      localStorage.setItem("alertContent", "Invalid login, Please contact admin");

      this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

      }).subscribe((isConfirmed) => {
        this.close();
      });    }
}else if(this.appServices.changepswd){
  this.changepswd = this.appServices.changepswd;
  
}

    })
  }
  constructor(private appServices: AppServices, private router: Router, dialogService: DialogService, private http: Http) {
    super(dialogService);

}

submit(f: NgForm) {
  this.flag=false;
  var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  var pswd = f.form.controls["pswd"].value;
  var cpswd = f.form.controls["cpswd"].value;
  if(pswd != cpswd){
    this.fields.controls["pswd"].setValue("");
    this.fields.controls["cpswd"].setValue("");
    this.alertMsg("Password and Confirm password doesn't match");
  }else{
    
    var reverseUsername=this.uname.split("").reverse().join("");
    
    for (var i = 0; i < this.user.length; i++) {
      if (this.uname == this.user[i].username) {
        var oldPassword = this.user[i].password;
      }
    }
    // if(pswd.length < 8){
    //   this.flag=true;
    // }else if(pswd == this.uname){
    //   this.flag=true;
    // }else if(pswd == reverseUsername){
    //   this.flag=true;
    // }else if(pswd == oldPassword){
    //   this.flag=true;
    // }

    if(pswd == this.uname){
      this.flag=true;
    }else if(pswd == reverseUsername){
      this.flag=true;
    }else if(pswd == oldPassword){
      this.flag=true;
    }else{
      if(strongRegex.test(pswd)){
        this.flag=false;
      }else{
        this.flag=true;
      }
    }

    if(this.flag){
      this.fields.controls["pswd"].setValue("");
      this.fields.controls["cpswd"].setValue("");
      this.alertMsg("Password doesn't match password rules");
    }else{
      var approve = [];
      approve.push({
        "username": this.uname,
        "password": pswd,
        "statusFlag": 2
      })

      var headers = new Headers();
      headers.append("Content-Type", "application/json");
      this.http.post('/api/readPasswordStatus', JSON.stringify(approve[0]), { headers: headers })
        .subscribe(res => {  
          // Updated by Pisith 27/11/2019
          // /api/readPasswordStatus => update user's password and statusFlag
        }, err => {
          console.log("err " + err);
        })

      this.close();
      this.router.navigate(['']);
      this.alertMsg("Password changed successfully, Login again");
    }

  }
}

submitForgetPswd(f: NgForm){
  this.flag=false;
  var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  var sans = f.form.controls["sans"].value;
  var fpswd = f.form.controls["fpswd"].value;
  var fcpswd = f.form.controls["fcpswd"].value;
  var reverseUsername=this.uname.split("").reverse().join("")
  
  if(sans != this.sAnswer){
    this.fields.controls["sans"].setValue("");
    this.alertMsg("Invalid Security Answer");
  }else if(fpswd != fcpswd){
    this.fields.controls["fpswd"].setValue("");
    this.fields.controls["fcpswd"].setValue("");
    this.alertMsg("Password and Confirm Password doesn't match");
  }else{
    if(fpswd == this.uname){
      this.flag=true;
    }else if(fpswd == reverseUsername){
      this.flag=true;
    }else{
      if(strongRegex.test(fpswd)){
        this.flag=false;
      }else{
        this.flag=true;
      }
    }
    
    // if(fpswd.length < 8){
    //   this.flag=true;
    // }else if(fpswd == this.uname){
    //   this.flag=true;
    // }else if(fpswd == reverseUsername){
    //   this.flag=true;
    // }

    if(this.flag){
      this.fields.controls["fpswd"].setValue("");
      this.fields.controls["fcpswd"].setValue("");
      this.alertMsg("Password doesn't match password rules");
    }else{
      var approve = [];
      approve.push({
        "username": this.uname,
        "password": fpswd,
        "statusFlag": 2
      })

      var headers = new Headers();
      headers.append("Content-Type", "application/json");
      this.http.post('/api/readPasswordStatus', JSON.stringify(approve[0]), { headers: headers })
        .subscribe(res => {  
          
        }, err => {
          console.log("err " + err);
        })

      this.close();
      this.router.navigate(['']);
      this.alertMsg("Password changed successfully, Login again");
    }
  }
}

alertMsg(msg) {
  localStorage.setItem("alertContent", msg);
  this.dialogService.addDialog(AlertComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

  }).subscribe((isConfirmed) => {
  });
}


}
