import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';

export interface PromptModel {
  title: string;
  question1: string;
  question2: string;

}

@Component({
  selector: 'masterdialog',
  templateUrl: './masterdialog.component.html',
  styleUrls: ['./masterdialog.component.css']
})
export class MasterdialogComponent  extends DialogComponent<PromptModel, string> implements PromptModel {

  title: string;
  question1: string;
  question2: string;
  message1: string = '';
  message2: string = '';
  msgplaceholder1: string = "Please enter master name";
  selectPT: boolean = true;


  //dashboard
  question3: string = "Master Name";
  question4: string = "Data Entry Model";
  dataEntry: any = [];
  dboardCall: boolean = false;
  existingTemplate: boolean = false;
  masterDetails:any=[];
  dataEntrySelect: boolean = true;

  constructor(dialogService: DialogService, private http: Http) {
    super(dialogService);

    localStorage.setItem("masterDetails", "");

    // this.http.get("./assets/data/dataEntryModal.json")
    //   .subscribe((res) => {
    //     this.dataEntry = res.json().dataEntryModal;

    //   }, err => {

    //     console.log(err);
    //   })


    this.http.get('http://localhost:4777/api/readFolders')
      .subscribe(folder => {



      })

    if (localStorage.getItem("dboard") == "1") {
      this.dboardCall = true;
    }

  }

  //applydBoard(name, dataEntry) {
  applydBoard(name,code) {

  
    this.masterDetails=[];

      var MasterId=this.MasterIdGenerate();
       if(MasterId.toString().length<=3){
            this.MasterIdGenerate();
        }
        var newday=new Date();  
        var de = newday.toLocaleString();
    // this.masterDetails.push({ "MasterId": MasterId, "MasterName": name, "dataEntryModal": dataEntry, "CreatedBy": localStorage.getItem("user"), "CreatedDate": de })
     this.masterDetails.push({ "MasterId": MasterId, "MasterName": name,"MasterMappingCode":code, "dataEntryModal": "Tabular", "CreatedBy": localStorage.getItem("user"), "CreatedDate": de,"ModifiedDate":"" })
     localStorage.setItem("masterDetails", JSON.stringify(this.masterDetails));

    
    this.result = "Done";
    this.close();


  }


  MasterIdGenerate(){
    var MasterId = Math.floor(Math.random() * 1000) + 1;
    
       return MasterId;
    
   
  }

  copyETemplate() {
    this.existingTemplate = true;
    // (<HTMLInputElement>document.getElementsByClassName("modal-content")[1]).style.display="block";
  }

  closeno(eve) {
    this.existingTemplate = false;
    // (<HTMLInputElement>document.getElementsByClassName("modal-content")[1]).style.display="none";

  }

}
