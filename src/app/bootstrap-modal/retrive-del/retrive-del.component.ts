import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';

export interface PromptModel {
  title: string;
  }

@Component({
  selector: 'app-retrive-del',
  templateUrl: './retrive-del.component.html',
  styleUrls: ['./retrive-del.component.css']
})
export class RetriveDelComponent extends DialogComponent<PromptModel, string> implements PromptModel {
  title: string;
  fieldNames: any = [];
  dropdownSettings: any = [];
  selectedItems = [];

  constructor(dialogService: DialogService) {
    super(dialogService);
    this.dropdownSettings = {
      singleSelection: false,
      text: "Select items to retrive",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

    this.fieldNames = JSON.parse(localStorage.getItem("fieldNameList"));
   }

   apply(){
    localStorage.setItem("fieldNameListOutpt", "");
      localStorage.setItem("fieldNameListOutpt", JSON.stringify(this.selectedItems));
      this.result = "true";
    this.close();    
   }

   onItemSelect(item: any) {
    setTimeout(res => {
      (<HTMLInputElement>document.getElementsByClassName("c-list")[0]).style.display = "-webkit-box"
    }, 200);
  }
  OnItemDeSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    setTimeout(res => {
      (<HTMLInputElement>document.getElementsByClassName("c-list")[0]).style.display = "-webkit-box"
    }, 200);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

}
