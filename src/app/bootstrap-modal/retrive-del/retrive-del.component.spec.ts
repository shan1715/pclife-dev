import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetriveDelComponent } from './retrive-del.component';

describe('RetriveDelComponent', () => {
  let component: RetriveDelComponent;
  let fixture: ComponentFixture<RetriveDelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetriveDelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetriveDelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
