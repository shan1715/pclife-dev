import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { AppServices } from '../../app.service';

export interface PromptModel {
  title: string;
  fquestion1: string;
  fquestion2: string;

}
@Component({
  selector: 'fielddialogue',
  templateUrl: './fielddialogue.component.html',
  styleUrls: ['./fielddialogue.component.css']
})
export class FielddialogueComponent extends DialogComponent<PromptModel, string> implements PromptModel {
  selectMM: boolean;
  mandatoryChange: any=[];

  @ViewChild('selectedFType') selectedFtype: ElementRef;
  @ViewChild('selectedMType') selectedMtype: ElementRef;

  title: string;
  fquestion1: string = "Item Name";
  fquestion2: string = "Item Type";
  fquestion3: string = "Field Type";
  fquestion4: string = "Masking Format";
    fquestion5: string = "Mandatory";

  fmsgplaceholder1: string = "Please enter item name";
  fmsgplaceholder2: string = "Please enter item type";
  fmsgplaceholder3: string = "Please enter field type ";
  fmsgplaceholder4: string = "Please enter masking format";
  fmessage1: string = '';
  fmessage2: string = '';
  fmessage3: string = '';
  fmessage4: string = '';
  selectIT: boolean = true;
  selectFT: boolean = true;
  selectMT: boolean = true;
  mandatory:boolean=true;
   mandatoryMsg:boolean=false;
  fType: any = [];
  mType: any = [];
  selectedItype: boolean = true;
  listOfValues: boolean = false;
  tRow: any = [];
  finalOutput: any = [];
  dateBool:boolean=false;
  numBool:boolean=false;
  textBool:boolean=true;
  blockNames: any = [];
  fieldNames: any = [];
  dropdownSettings: any = [];
  dropdownFieldSettings: any = [];
  selectedItems = [];
  selectedFieldItems = [];
  mappingField: any;

  constructor(dialogService: DialogService, private appService: AppServices) {
    super(dialogService);
    this.dropdownSettings = {
      singleSelection: true,
      text: "Select Block Name",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

    this.dropdownFieldSettings = {
      singleSelection: true,
      text: "Select Field Name",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };

     this.mandatoryChange.push({"name":"False","value":"False"},{"name":"True","value":"True"})
     this.blockNames = JSON.parse(localStorage.getItem("blockNameList"));
    this.fieldNames =[];
    this.mappingField = {};

  }
 mandatoryType(sp){
    this.selectMM =true;
 if (sp == "True") {
   
        this.mandatoryMsg =true;
    }else if(sp == "False"){
 this.mandatoryMsg =false;

    }



  }

  itemType(sp) {


    this.selectIT = true;

    if (sp == "text") {
      this.textBool =true;
      this.dateBool = false;
      this.numBool =false;

      this.listOfValues = false;
      this.fType = ['String', 'Number', 'Date'];
      this.selectedFtype.nativeElement.style.display = "inline-block";
      this.selectedFtype.nativeElement.previousElementSibling.style.display = "inline-block";
      this.selectedFtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    } else if (sp == "select") {
      this.fType = [];
      this.selectedItype = false;
      this.selectedFtype.nativeElement.style.display = "none";
      this.selectedFtype.nativeElement.previousElementSibling.style.display = "none";
      this.selectedFtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
      this.selectedMtype.nativeElement.style.display = "none";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "none";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
      this.listOfValues = true;

    } else if (sp == "date") {
      this.textBool =false;
      this.dateBool = true;
      this.numBool =false;
      this.listOfValues = false;
      this.selectedFtype.nativeElement.style.display = "none";
      this.selectedFtype.nativeElement.previousElementSibling.style.display = "none";
      this.selectedFtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
      this.mType = [];
      //this.mType.push({ "n": "DD/MM/YYYY", "id": "d2m2y4" }, { "n": "MM/DD/YYYY", "id": "m2d2y4" }, { "n": "YYYY/MM/DD", "id": "y4m2d2" }, { "n": "YYYY/DD/MM", "id": "y4d2m2" })
      this.mType.push({ "n": "mm-dd-yyyy", "id": "m2d2y4" })
      this.selectedMtype.nativeElement.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    }
  }

  fieldType(sp) {
    this.selectFT = true;

    if (sp == "Number") {
      this.textBool =false;
      this.dateBool = false;
      this.numBool =true;

      this.mType = [];
      //this.mType.push({ "n": "Number (####)", "id": "number" }, { "n": "Number With Decimal (####.##)", "id": "numberwdecimal" })
      this.mType.push({ "n": "Number (####)", "id": "number" })
      this.selectedMtype.nativeElement.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    } else if (sp == "String") {
      this.textBool =true;
      this.dateBool = false;
      this.numBool =false;

      this.mType = [];
      this.selectedMtype.nativeElement.style.display = "none";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "none";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "none";
    } else if (sp == "Date") {
      this.textBool =false;
      this.dateBool = true;
      this.numBool =false;
      this.mType = [];
      //this.mType.push({ "n": "DD/MM/YYYY", "id": "d2m2y4" }, { "n": "MM/DD/YYYY", "id": "m2d2y4" }, { "n": "YYYY/MM/DD", "id": "y4m2d2" }, { "n": "YYYY/DD/MM", "id": "y4d2m2" })
      this.mType.push({ "n": "mm-dd-yyyy", "id": "m2d2y4" })
      this.selectedMtype.nativeElement.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.style.display = "inline-block";
      this.selectedMtype.nativeElement.previousElementSibling.previousElementSibling.style.display = "inline-block";
    }

  }

  maskType(sp) {
    this.selectMT = true;
  }

  apply(it, ft, mt) {

    if (it == "text") {
      localStorage.setItem("fmsg1", this.fmessage1);
      localStorage.setItem("fmsg2", it);
      localStorage.setItem("fmsg3", ft);
      localStorage.setItem("fmsg4", mt);
      if(ft == "Number" || ft == "Date"){
        this.finalOutput.push({"mandatory":this.mandatoryMsg,"mandatoryMsg":(<HTMLInputElement>document.getElementById("mmsg")).value, "item_name": localStorage.getItem("fmsg1"), "item_type": localStorage.getItem("fmsg2"), "field_type": localStorage.getItem("fmsg3"), "mask_type": localStorage.getItem("fmsg4"),"display":JSON.parse((<HTMLInputElement>document.getElementById("showHide")).value), "defaultValue": (<HTMLInputElement>document.getElementById("dValue")).value, "readOnly":(<HTMLInputElement>document.getElementById("readOnly")).value,"mapping": this.mappingField, "minVal":(<HTMLInputElement>document.getElementById("minVal")).value, "maxVal" :(<HTMLInputElement>document.getElementById("maxVal")).value})
      }else{
      this.finalOutput.push({"mandatory":this.mandatoryMsg,"mandatoryMsg":(<HTMLInputElement>document.getElementById("mmsg")).value, "item_name": localStorage.getItem("fmsg1"), "item_type": localStorage.getItem("fmsg2"), "field_type": localStorage.getItem("fmsg3"), "mask_type": localStorage.getItem("fmsg4"),"display":JSON.parse((<HTMLInputElement>document.getElementById("showHide")).value), "defaultValue": (<HTMLInputElement>document.getElementById("dValue")).value, "readOnly":(<HTMLInputElement>document.getElementById("readOnly")).value,"mapping": this.mappingField})
      }
      
      localStorage.setItem("output", JSON.stringify(this.finalOutput));
      this.result = this.fmessage1;
      this.close();
    } else if (it == "select") {
      localStorage.setItem("fmsg1", this.fmessage1);
      localStorage.setItem("fmsg2", it);
      var store = [];
      var def = false;
      for(var si=0;si<this.tRow.length;si++){
	if(this.tRow[si].optionValue==(<HTMLInputElement>document.getElementById("defValue")).value){
    this.tRow[si]["selected"]=true;
    def = true;
}else{
	this.tRow[si]["selected"]=false;
}
}
store=this.tRow;

      // var store = [];
      //   for (var i = 0; i < document.getElementsByClassName("tValues").length; i++) {
      //     store.push({ "optionLabel": (<HTMLInputElement>document.getElementsByClassName("tValues")[i].getElementsByTagName("td")[0].children[0]).value, "optionValue": (<HTMLInputElement>document.getElementsByClassName("tValues")[i].getElementsByTagName("td")[1].children[0]).value })
      //   }
      if(def){
      this.finalOutput.push({"mandatory":this.mandatoryMsg,"mandatoryMsg":(<HTMLInputElement>document.getElementById("mmsg")).value, "item_name": localStorage.getItem("fmsg1"), "item_type": localStorage.getItem("fmsg2"), "optionObj": store,"display":JSON.parse((<HTMLInputElement>document.getElementById("showHide")).value), "readOnly":(<HTMLInputElement>document.getElementById("readOnly")).value, "defaultValue":true,"mapping": this.mappingField })
      }else{
      this.finalOutput.push({"mandatory":this.mandatoryMsg,"mandatoryMsg":(<HTMLInputElement>document.getElementById("mmsg")).value, "item_name": localStorage.getItem("fmsg1"), "item_type": localStorage.getItem("fmsg2"), "optionObj": store,"display":JSON.parse((<HTMLInputElement>document.getElementById("showHide")).value), "readOnly":(<HTMLInputElement>document.getElementById("readOnly")).value,"mapping": this.mappingField })
      }
      localStorage.setItem("output", JSON.stringify(this.finalOutput));
      this.result = this.fmessage1;
      this.close();
    } else if (it == "date") {
      localStorage.setItem("fmsg1", this.fmessage1);
      localStorage.setItem("fmsg2", it);
      localStorage.setItem("fmsg4", mt);
      this.finalOutput.push({"mandatory":this.mandatoryMsg,"mandatoryMsg":(<HTMLInputElement>document.getElementById("mmsg")).value, "item_name": localStorage.getItem("fmsg1"), "item_type": localStorage.getItem("fmsg2"), "mask_type": localStorage.getItem("fmsg4"),"display":JSON.parse((<HTMLInputElement>document.getElementById("showHide")).value), "defaultValue": (<HTMLInputElement>document.getElementById("dValue")).value, "readOnly":(<HTMLInputElement>document.getElementById("readOnly")).value,"mapping": this.mappingField, "minVal":(<HTMLInputElement>document.getElementById("minVal")).value, "maxVal" :(<HTMLInputElement>document.getElementById("maxVal")).value })
      
      localStorage.setItem("output", JSON.stringify(this.finalOutput));
      this.result = this.fmessage1;
      this.close();
    }


    


  }

  incRow(eve) {
    this.tRow.push({ "optionValue": "","optionLabel": "" })
  }

  decRow(eve) {
    this.tRow.splice(this.tRow.length - 1, 1);
  }

  getSelLabel(i,eve){
    this.tRow[i].optionLabel = eve.target.value;
  }

  getSelValue(i,eve){
    this.tRow[i].optionValue = eve.target.value;
  }

  onItemSelect(item: any) {
    this.fieldNames =[];
    var data ={"accordId":item.id};
    if(localStorage.getItem("pageName") == "PROD_INFO"){
      this.fieldNames = this.appService.changeAcc(this.appService.tempProdInfo,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "AGENCY"){
      this.fieldNames = this.appService.changeAcc(this.appService.agency,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "OPERATION_ANNUITY"){
      this.fieldNames = this.appService.changeAcc(this.appService.operation_annuity,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "OPERATION_GROUP"){
      this.fieldNames = this.appService.changeAcc(this.appService.group,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "POLICYSERVICING"){
      this.fieldNames = this.appService.changeAcc(this.appService.policyServicing,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "OPERATION_UNDERWRITING"){
      this.fieldNames = this.appService.changeAcc(this.appService.operation_uwritting,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "OPERATION_UNITLINKED"){
      this.fieldNames = this.appService.changeAcc(this.appService.operation_unitlink,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "NEW_BUSINESS"){
      this.fieldNames = this.appService.changeAcc(this.appService.operation_nbusiness,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "PRODUCT_CHARGES"){
      this.fieldNames = this.appService.changeAcc(this.appService.charges,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "PRODUCT_FINANCE"){
      this.fieldNames = this.appService.changeAcc(this.appService.finance,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "PRODUCT_RATES"){
      this.fieldNames = this.appService.changeAcc(this.appService.product_rate,data,"","","fieldList");
    }else if(localStorage.getItem("pageName") == "PROD_BENEFITS"){
      this.fieldNames = this.appService.changeAcc(this.appService.risk_coverage,data,"","","fieldList");
    }
  }
  OnItemDeSelect(item: any) {
    this.fieldNames =[];
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }


  onFieldItemSelect(item: any) {
    this.mappingField = {"id":item.id}
  }
  OnFieldItemDeSelect(item: any) {
    this.mappingField = {};
  }
  onFieldSelectAll(items: any) {
    console.log(items);
  }
  onFieldDeSelectAll(items: any) {
    console.log(items);
  }


  minMaxDateValidate(eve){
    var minValue = (<HTMLInputElement>document.getElementById("minVal")).value;
    var maxValue = (<HTMLInputElement>document.getElementById("maxVal")).value;
    var regx=new RegExp(/^\d{4}-\d{2}-\d{2}$/);
    if(regx.test(eve.target.value)){
    if(minValue != '' && maxValue != ''){
      var minValueDate = new Date(minValue);
      var maxValueDate = new Date(maxValue);

      if(minValueDate > maxValueDate){
        (<HTMLInputElement>document.getElementById("maxVal")).value = '';
        (<HTMLInputElement>document.getElementById("minVal")).value = '';
      }
    } 
  }else{
    eve.target.value ='';
  }
  }

  minMaxNumValidate(eve){
    var minValue = (<HTMLInputElement>document.getElementById("minVal")).value;
    var maxValue = (<HTMLInputElement>document.getElementById("maxVal")).value;
    var regexp = /^[0-9]+([,.][0-9]+)?$/g;
    if(regexp.test(eve.target.value)){
    if(minValue != '' && maxValue != ''){
      if(Number(maxValue) < Number(minValue)){
        (<HTMLInputElement>document.getElementById("maxVal")).value = '';
        (<HTMLInputElement>document.getElementById("minVal")).value = '';
      }
    }  
  }else{
    eve.target.value ='';
  }
  }
}

