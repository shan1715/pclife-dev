import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../../app.service';
import { AlertComponent } from "../alert/alert.component";

import { FileUploader } from 'ng2-file-upload';

import { Router } from '@angular/router';
import { DataDownloadEmailComponent } from "../dataDownloadEmail/dataDownloadEmail.component"

import { AlertdataComponent } from "../../bootstrap-modal/alertdata/alertdata.component";

export interface PromptModel {
  title: string;
  question1: string;
  question2: string;

}
const URL = 'assets/dataupload/';
@Component({
  selector: 'app-promp',
  templateUrl: './dataDownload.component.html',
  styleUrls: ['./dataDownload.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DataDownloadComponent extends DialogComponent<PromptModel, string> implements PromptModel, OnInit {
  public uploader: FileUploader = new FileUploader({ url: '/api/upload/' });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  date: any;
  today_date: any;
  fileName: any;
  num: any = [];
  exceldata: any = [];
  dataarray: any = [];
  templateIdGen: any;
  title: string;
  question1: string;
  question2: string;
  message1: string = '';
  product: string = '';
  Module: string = '';
  block: string = '';
  data: string = '';
  table: any[];
  upload: any;
  fname: any;
  alertContent: any;
  email: any;
  ws_url: any = [];
 public showntn:boolean=false;
  constructor(private router: Router, private changeDetector: ChangeDetectorRef, private appServices: AppServices, dialogService: DialogService, private http: Http) {
    super(dialogService)
   this.email=JSON.parse(localStorage.getItem('accessRights')).email;
    this.upload = JSON.parse(localStorage.getItem('upload'));

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      if (item.isUploaded) {

        this.fileName = item.file.name;
        this.readExcelfile(item.file.name);

      } else {
        alert("fail")
      }
    };

  }
  readExcelfile(name) {

    this.date = new Date();

    this.today_date = this.date.getDate().toString() + '/' + (this.date.getMonth() + 1).toString() + '/' + this.date.getFullYear().toString();

    this.exceldata.push({
      "fileName": name,
      "user": localStorage.getItem('user'),
      "udate": this.today_date,
      "pageData":this.appServices.uploadingPageContent,
      "snArr":this.appServices.snArr,
      "selectedNav":this.appServices.selectedNav,
      "data":this.appServices.downloaddata
    })
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append('responseType', 'arraybuffer');
    this.http.post('/api/readexceldata', JSON.stringify(this.exceldata[0]), { headers: headers })
      .subscribe(response => {
     
        console.log(response);
        if (response.json().fName=='success') {
          this.appServices[this.appServices.uploadingPageName]=response.json().data;

          this.fname = name.split('_')[1].split('.')[0];
          this.alertContent = "Upload Ref.No :" + this.fname + " Data is uploaded Sucessfully ,you will be intimated once the data is processed"
          localStorage.setItem("alertContent", this.alertContent);
          
        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.result="done";
          this.close();
        });

        }
        else if (response.json().fName=='failure') {
          this.alertContent = "Excel filename mismatch";
          localStorage.setItem("alertContent", this.alertContent);

          
        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.result="notdone";
          this.close();
        });

        }
        else {
          this.alertContent = "Please enter valid data";
          localStorage.setItem("alertContent", this.alertContent);
          this.dialogService.addDialog(AlertComponent, {
            title: 'Name dialog',
            question1: 'Block Name ',
            question2: 'Block Type '
  
          }).subscribe((isConfirmed) => {
            this.result="notdone";
            this.close();
          });
        }



      }, err => {
        this.alertContent = "Invalid file format"
        localStorage.setItem("alertContent", this.alertContent);
        console.log("err " + err);
        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '

        }).subscribe((isConfirmed) => {
          this.result="notdone";
          this.close();
        });
      })

  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }


  ngOnInit() {
    //if(localStorage.getItem('upload')=='1'){
    this.http.get('assets/data/property.json')
      .subscribe(res => {
        this.ws_url = res.json().web.url;
      });

    var data = this.appServices.downloaddata
    this.product = localStorage.getItem('selectedProduct');
    this.Module = data.accordionName;
    this.block = data.PageName
    console.log(data);
    //}
  }
  UploadData() {

  }
  downloadTemplate() {
    this.date = new Date();
    this.appServices.smail = [];
    this.today_date = this.date.getDate().toString() + '/' + (this.date.getMonth() + 1).toString() + '/' + this.date.getFullYear().toString();
    var tabledata = this.appServices.downloaddata;
    var mainDetails = {};
    mainDetails["ProductName"] = localStorage.getItem('selectedProduct');
    mainDetails["PageName"] = tabledata.PageName;
    mainDetails["blockId"] = tabledata.blockId;
    mainDetails["accordionName"] = tabledata.accordionName;
    mainDetails["data"] = tabledata.tableObj;
    mainDetails["user"] = localStorage.getItem('user');
    mainDetails["date"] = this.today_date;
    if (!this.upload) {
      mainDetails["mode"] = "direct";
      mainDetails["recipients"] = [];
    } else {

    }

    this.dataarray = [];

    for (var c = 0; c < tabledata.tableObj.length; c++) {
      var obj = {};
      for (var i = 0; i < tabledata.tableObj[c].tableRow.length; i++) {
        if (tabledata.tableObj[c].tableRow[i].optionObj) {
          var checkSelected = false;
          for (var j = 0; j < tabledata.tableObj[c].tableRow[i].optionObj.length; j++) {
            if (tabledata.tableObj[c].tableRow[i].optionObj[j].selected) {
              checkSelected = true;
              obj[tabledata.tableObj[c].tableRow[i].theadName] = tabledata.tableObj[c].tableRow[i].optionObj[j].optionValue;
            }
          }
          if (!checkSelected) {
            obj[tabledata.tableObj[c].tableRow[i].theadName] = '';
          }
        }
       else if(tabledata.tableObj[c].tableRow[i].tdObj[0].tagName != "button" && !tabledata.tableObj[c].tableRow[i].id.includes("sequenceNumber")){
        if(tabledata.tableObj[c].tableRow[i].tdObj[0].tagName=='date')
        {
          if(tabledata.tableObj[c].tableRow[i].tdObj[0].tdValue.includes('-')){
          var s = tabledata.tableObj[c].tableRow[i].tdObj[0].tdValue
          if (s) { 
              s = s.replace(/(\d{4})-(\d{1,2})-(\d{1,2})/, function(match,y,m,d) { 
                  return m + '/' + d + '/' + y;   
              });
          }
          obj[tabledata.tableObj[c].tableRow[i].theadName]=s;
        }else
        obj[tabledata.tableObj[c].tableRow[i].theadName]=tabledata.tableObj[c].tableRow[i].tdObj[0].tdValue

      }

        else{
          obj[tabledata.tableObj[c].tableRow[i].theadName]=tabledata.tableObj[c].tableRow[i].tdObj[0].tdValue
        }
        }   
        


        
      }
      this.dataarray.push(obj)
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append('responseType', 'arraybuffer');

    this.http.post('/api/exportexceltemplate', { "mainDetails": mainDetails, "data": this.dataarray }, { headers: headers })
      .subscribe(response => {
        this.fileName = response.json().refId + '.xlsx';
        var a = document.createElement('a');

        a.href = "assets/dataMonitor/" + this.fileName;
        a.download = this.fileName;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        // saveAs(new Blob([(<any>response)._body], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }), "output.xlsx");

      }, err => {
        console.log("err " + err);
      })

  }


  openEmailWizard() {
    (<HTMLInputElement>document.getElementById("emailid")).value ="";
    (<HTMLInputElement>document.getElementById("emailid")).value =JSON.parse(localStorage.getItem('accessRights')).email;
    this.dialogService.addDialog(DataDownloadEmailComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed == "done") {
        var mails = (<HTMLInputElement>document.getElementById("emailid")).value+';';
        for (var i = 0; i < this.appServices.smail.length; i++) {
          mails += this.appServices.smail[i].eMailTo + ";";

        }
        (<HTMLInputElement>document.getElementById("emailid")).value=mails;
      } else {

      }
    })
  }
  // sendEmail() {
  //   var email = JSON.parse(localStorage.getItem('accessRights')).email;
  //   var headers = new Headers();
  //   headers.append('Content-Type', 'application/json');
  //   this.http.post(this.ws_url + '/ProductBenefits/SendAttachment', { "eMailTo": email, "message": "Hai", "filename": "E:/modifiedLifeTables.xlsx" }, { headers: headers })
  //     .subscribe(res => {
  //       console.log(res);

  //     });
  // }

  sendEmails() {

    if((<HTMLInputElement>document.getElementById("emailid")).value==""){

       (<HTMLInputElement>document.getElementById("emailid"));
    }
    else
    {
    this.date=new Date();
    this.today_date = this.date.getDate().toString() + '/' + (this.date.getMonth() + 1).toString() + '/' + this.date.getFullYear().toString();
    var tabledata = this.appServices.downloaddata;
    var smail = [];
    smail = this.appServices.smail;
    var mainDetails = {};
    mainDetails["ProductName"] = localStorage.getItem('selectedProduct');
    mainDetails["PageName"] = tabledata.PageName;
    mainDetails["blockId"] = tabledata.blockId;
    mainDetails["accordionName"] = tabledata.accordionName;
    mainDetails["data"] = tabledata.tableObj;
    mainDetails["user"] = localStorage.getItem('user');
    mainDetails["date"] = this.today_date;
    mainDetails["mode"] = "mail";
    mainDetails["recipients"] = (<HTMLInputElement>document.getElementById("emailid")).value;


    this.dataarray = [];
    var obj = {};
    for (var c = 0; c < tabledata.tableObj.length; c++) {
      for (var i = 0; i < tabledata.tableObj[c].tableRow.length; i++) {
        if (tabledata.tableObj[c].tableRow[i].optionObj) {
          var checkSelected = false;
          for (var j = 0; j < tabledata.tableObj[c].tableRow[i].optionObj.length; j++) {
            if (tabledata.tableObj[c].tableRow[i].optionObj[j].selected) {
              checkSelected = true;
              obj[tabledata.tableObj[c].tableRow[i].theadName] = tabledata.tableObj[c].tableRow[i].optionObj[j].optionValue;
            }
          }
          if (!checkSelected) {
            obj[tabledata.tableObj[c].tableRow[i].theadName] = '';
          }
        } else if (tabledata.tableObj[c].tableRow[i].tdObj[0].tagName != "button" && !tabledata.tableObj[c].tableRow[i].id.includes("sequenceNumber")){
          obj[tabledata.tableObj[c].tableRow[i].theadName] = tabledata.tableObj[c].tableRow[i].tdObj[0].tdValue;
        }
      }


      this.dataarray.push(obj);
    }
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append('responseType', 'arraybuffer');

    this.http.post('/api/exportexceltemplate', { "mainDetails": mainDetails, "data": this.dataarray }, { headers: headers })
      .subscribe(response => {
         this.fileName = "D:/ProjectWorkspaceAngular/ProductConfigureLifeServer/dist/assets/dataMonitor/" + response.json().refId + '.xlsx';
        //this.fileName = "D:/Product Config/server_prod_config_life/dist/assets/dataMonitor/MZWS2_2019_1.xlsx";
        // this.fileName= "D:/Product Config/server_prod_config_life/dist/assets/dataMonitor/"+JSON.parse((<any>response)._body).refId+".xlsx"; 
        var text=(<HTMLInputElement>document.getElementById("emailid")).value;
        smail.push({"eMailTo":JSON.parse(localStorage.getItem('accessRights')).email});
        for (var k = 0; k < this.appServices.smail.length; k++) {
          smail[k]['message'] = "test";
          smail[k]['filename'] = this.fileName;
        }

        console.log(smail)
        this.http.post(this.ws_url + '/ProductBenefits/SendAttachment', smail, { headers: headers })
          .subscribe(res => {
            if ((<any>res)._body == "E-mail Sent Successfully") {
              this.alertContent = "E-mail Sent Successfully";

            } else {
              this.alertContent = "E-mail not sent";
            }
            localStorage.setItem("alertContent", this.alertContent);

            this.dialogService.addDialog(AlertComponent, {
              title: 'Name dialog',
              question1: 'Block Name ',
              question2: 'Block Type '

            }).subscribe((isConfirmed) => {

            });
            this.close();
          }, err => {
            console.log("err " + err);
          })

      }, err => {
        console.log("err " + err);
      })
    }

  }
  showmore()
  {
    this.showntn=true;
    //alert('test')
  }
}
