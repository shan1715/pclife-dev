import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../../app.service';
import { AlertComponent } from "../alert/alert.component";
import { NgxSpinnerService } from "ngx-spinner";
export interface PromptModel {
  title: string;
  question1: string;
  question2: string;

}
@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent extends DialogComponent<PromptModel, string> implements PromptModel,OnInit {
  num: any=[];
  templateIdGen:any;
  title: string;
  question1: string;
  question2: string;
  message1: string = '';
  message2: string = '';
  msgplaceholder1: string = "Please enter block name";
  msgplaceholder2: string = "Please enter block type";
  selectPT: boolean = true;
  commentSaved: boolean = true;
  //dashboard
  question3: string = "Template Name";
  question4: string = "Product Type";
  question5: string = "Copy From Existing Template";
  pType: any = [];
  dboardCall: boolean = false;
  existingTemplate: boolean = false;
  template: any = [];
  templateJson: any = [];
  checkedPages: any = [];
  checkedPageNames:any=[];
  pname: any = [];
  subPageChecked: any = [];
  pTypeSelect: boolean = true;
  folderName: any;
  themes:any=[];
  PId:any;
  PName:any;
  User:any;
  Review:any;
  CType:any;
  CDate:any;
  templateApproval:any;
  templateSaved: boolean = false;
  ReviewList:any;
  PReviewList:any;
  ReviewListLength:any;
  public show:boolean = false;
  pbdModify:boolean = false;
  productApproval:boolean = false;
  public buttonName:any = 'Show Previous Comments';
  bdCreate:any;
  bdModify:any;
  ngOnInit () {  
    
    this.productApproval = this.appService.productApproval;
    this.templateApproval = this.appService.templateApproval;
    this.templateSaved=this.appService.templateSavedStaus;
    this.bdCreate = this.appService.bdCreate;
    this.bdModify = this.appService.bdModify;
    this.pbdModify= this.appService.pbdModify;
    var PReview = [];
    var ProductReview=[];
    this.PId = this.appService.templateId;
    this.PName=  this.appService.templateName ;
  
    this.CType= localStorage.getItem("pageMode");
   
 if( this.appService.templateName!=''){
    PReview.push({
     "PId": this.PId ,
     "PName": this.appService.templateName,
     "ComentType": this.CType,
     "PageName": this.appService.pageName  });
     
     var headers = new Headers();
     headers.append("Content-Type", "application/json");
    this.http.post('/api/getComments', JSON.stringify(PReview[0]), { headers: headers })
    .subscribe(res => {

      var Pcomment = Object.keys(res.json());
       this.ReviewListLength=Pcomment.length;
       if (Pcomment.length > 0) {
         this.ReviewList=res.json();
      }
      else{
        if(document.getElementById("prevcomments")){
          (<HTMLInputElement>document.getElementById("prevcomments")).textContent='No Reviews available...';
        }
    }

    });
  }
if(this.appService.productName!=''){
  // this.spinner.show(); 
    ProductReview.push({
      "ProductId": this.appService.productId,
      "ProductName": this.appService.productName,
      "PageName":  this.appService.pageName  });
      var headers = new Headers();
      headers.append("Content-Type", "application/json");
     this.http.post('/api/getPComments', JSON.stringify(ProductReview[0]), { headers: headers })
     .subscribe(res => {
      this.spinner.hide();
       var Pcomment = Object.keys(res.json());
        this.ReviewListLength=Pcomment.length;
        if (Pcomment.length > 0) {
          this.PReviewList=res.json();
       }
       else{
        if(document.getElementById("pprevcomments")){
        (<HTMLInputElement>document.getElementById("pprevcomments")).textContent='No Reviews available...';
     }
    }
     },err=>{
       this.spinner.hide();
     });
    }


  }

  toggle() {
  
   this.show = !this.show;

    // CHANGE THE NAME OF THE BUTTON.
    if(this.show) {
      this.buttonName = "Hide Previous Comments ";
    }
    else{
      this.buttonName = "Show Previous Comments";
  }
}
  toggleT() {
    this.show = !this.show;
 
     // CHANGE THE NAME OF THE BUTTON.
     if(this.show)  
       this.buttonName = "Hide Previous Comments ";
     else
       this.buttonName = "Show Previous Comments";
   }
  constructor(private spinner: NgxSpinnerService,private appService: AppServices, dialogService: DialogService, private http: Http) {
    super(dialogService);
    

  }

  submit(){
    var PReview = [];
   this.PId = this.appService.templateId;
   this.PName=  this.appService.templateName ;
   this.User=localStorage.getItem("user");
   this.CType= localStorage.getItem("pageMode");
   this.Review=(<HTMLInputElement>document.getElementById("cbox")).value;
   this.CDate =new Date();
if( this.Review!=''){
   PReview.push({
    "PId": this.PId ,
    "PName": this.appService.templateName,
    "User": this.User,
    "ComentType": this.CType,
    "PageName": this.appService.pageName,
    "Review":this.Review,
    "CommentDate":this.CDate
   
  });
  var headers = new Headers();
  headers.append("Content-Type", "application/json");
  this.http.post('/api/reviewComments', JSON.stringify(PReview[0]), { headers: headers })
    .subscribe(res => {
      localStorage.setItem("alertContent", "Submitted");
     
        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '
  
        }).subscribe((isConfirmed) => {
          this.commentSaved = false;
          this.close();
        });
  

    }, err => {
      console.log("err " + err);
      this.close();
    })
  }
  else{
    this.close();
  }
  }

  closeno(){
    this.close();
  }

  
  approval(PR) {
        console.log('zzzzzzzzzzzzz')
        var approve = [];
        approve.push({
          "TemplateId": this.appService.templateId,
          "TemplateName": this.appService.templateName,
          "PageName":  this.appService.pageName,
          "PageReview":  PR,
          "ApproveBy":  localStorage.getItem("user")

        })
    
    console.log(approve);
        var headers = new Headers();
        headers.append("Content-Type", "application/json");
        this.http.post('/api/Approve', JSON.stringify(approve[0]), { headers: headers })
          .subscribe(res => {
            localStorage.setItem("alertContent", PR);
    
            this.dialogService.addDialog(AlertComponent, {
              title: 'Name dialog',
              question1: 'Block Name ',
              question2: 'Block Type '
    
            }).subscribe((isConfirmed) => {
              this.templateSaved = false;
              this.close();
            });
           
          }, err => {
            console.log("err " + err);
            this.close();
          })
    
    
    
      }

      Papproval(PR) { 
        console.log(this.appService.pageName);       
            var approve = [];
            approve.push({
              "ProductId": this.appService.productId,
              "ProductName": this.appService.productName,
              "PageName":  this.appService.pageName,
              "ApproveBy":  localStorage.getItem("user"),
              "PageReview":  PR
            })
           console.log(approve);
            var headers = new Headers();
            headers.append("Content-Type", "application/json");
            var apilink='';
            if(localStorage.getItem('ProductUpdated')=="true")
            {
            apilink="prodpublishApprove";
            localStorage.setItem('ProductUpdated',"false");
            }
            else
            {
            apilink="prodApprove";
            }
            this.http.post('/api/'+apilink, JSON.stringify(approve[0]), { headers: headers })
              .subscribe(res => {
                localStorage.setItem("alertContent", PR);
                 this.dialogService.addDialog(AlertComponent, {
                  title: 'Name dialog',
                  question1: 'Block Name ',
                  question2: 'Block Type '
                  }).subscribe((isConfirmed) => {
                  this.close();
                });
               }, err => {
                console.log("err " + err);
                this.close();
              })
        }
  


        Psubmit(){
          var PReview = [];
      
        
         this.User=localStorage.getItem("user");
       
         this.Review=(<HTMLInputElement>document.getElementById("Pcbox")).value;
         this.CDate =new Date();
      if( this.Review!=''){
         PReview.push({
          "ProductId": this.appService.productId,
          "ProductName": this.appService.productName,
          "PageName":  this.appService.pageName,
          "User": this.User,        
          "Review":this.Review,
          "CommentDate":this.CDate
         
        });
        var headers = new Headers();
        headers.append("Content-Type", "application/json");
        this.http.post('/api/ProductComments', JSON.stringify(PReview[0]), { headers: headers })
          .subscribe(res => {
            localStorage.setItem("alertContent", "Submitted");
           
              this.dialogService.addDialog(AlertComponent, {
                title: 'Name dialog',
                question1: 'Block Name ',
                question2: 'Block Type '
        
              }).subscribe((isConfirmed) => {
                this.commentSaved = false;
                this.close();
              });
         
      
          }, err => {
            console.log("err " + err);
            this.close();
          })
        }
        else{
          this.close();
        }
        }




      }
