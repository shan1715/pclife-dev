import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../../app.service';
import { AlertComponent } from "../alert/alert.component";
import {Router} from '@angular/router';
export interface PromptModel {
  title: string;
  question1: string;
  question2: string;

}
@Component({

  
  selector: 'app-promp',
  templateUrl: './promp.component.html',
  styleUrls: ['./promp.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PrompComponent extends DialogComponent<PromptModel, string> implements PromptModel, OnInit {
  num: any = [];
  templateIdGen: any;
  title: string;
  question1: string;
  question2: string;
  message1: string = '';
  defSelection="--Select--";
  message2: string = '';
  msgplaceholder1: string = "Please enter block name";
  msgplaceholder2: string = "Please enter block type";
  selectPT: boolean = true;


  //dashboard
  question3: string = "Template Name";
  question4: string = "Product Type";
  question5: string = "Copy From Existing Template";
  pType: any = [];
  dboardCall: boolean = false;
  ReviewList: boolean = false;
  PReviewList: boolean = false;
  existingTemplate: boolean = false;
  template: any = [];
  templateFull: any = [];
  templateJson: any = [];
  checkedPages: any = [];
  checkedPageNames: any = [];
  pname: any = [];
  subPageChecked: any = [];
  pTypeSelect: boolean = true;
  folderName: any;
  TemplatePReview: any;
  themes: any = [];
  pageNames: any = [];
  subPageNames: any = [];
  subPageShow: any = [];
  templateCopy: boolean = false;
  testing: any = [];
  copyOldTemplate: any = [];
  pageSelected: boolean = false;
  cpage: boolean = false;
  validProd:boolean=false;
  enableProdAdd:boolean=false;
  TemplatePageReview:any;
  grpSettings = {
    singleSelection: false,
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: false,
    classes: "myclass custom-class"
  };



  @ViewChild('copyTemplate') copyTemplate: ElementRef;
  @ViewChild('newProduct') newProduct:ElementRef;

  formModel = {

    skills0: []

  };


  ngOnInit() {
           this.changeDetector.detectChanges();

    this.templateCopy = this.appServices.templateCopy;

    this.http.get('http://localhost:4777/api/getTmpFolders')
      .subscribe(res => {

        var folder = Object.keys(res.json());
        if (folder.length > 0) {
          for (var i = 0; i < folder.length; i++) {
            var splitWith = (folder[i]).toString().split('-');
            this.num.push(parseInt(splitWith[1]));
          }

        }

      })
    this.http.get('/api/getPublishFolders')
      .subscribe(res => {
        console.log("xxxxzzzzzz")
        console.log(res.json())
        console.log("zzzzzxxxx")
        this.templateFull = Object.keys(res.json());
        if (this.templateFull.length > 0) {
          for (var j = 0; j < this.templateFull.length; j++) {
            var splitWith = (this.templateFull[j]).toString().split('-');
            this.num.push(parseInt(splitWith[1]));
          }
        }
      })
  }


  onItemSelect(item: any) {

  }
  OnItemDeSelect(item: any) {


  }
  onSelectAll(items: any) {
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

  constructor(private router:Router,private changeDetector:ChangeDetectorRef,private appServices: AppServices, dialogService: DialogService, private http: Http) {
    super(dialogService);


    this.copyOldTemplate = [{ "id": 1, "itemName": "Create", "mode": "Create Template", "parent": "Template" }, { "id": 2, "itemName": "Copy", "mode": "Create Template", "parent": "Template" }];


    this.themes.push({ "colors": "Default" }, { "colors": "White" }, { "colors": "Black" }, { "colors": "Others" });
    localStorage.setItem("sPages", "");
    localStorage.setItem("checkedPages", "");
    localStorage.setItem("checkedPageNames", "");

    this.http.get("./assets/data/ProdType.json")
      .subscribe((res) => {
        this.pType = res.json().productType;

      }, err => {

        console.log(err);
      })


    this.http.get('/api/readFolders')
      .subscribe(res => {


        var folder = Object.keys(res.json());
        if (folder.length > 0) {
          for (var j = 0; j < folder.length; j++) {
            // var splitWith = (folder[j]).toString();
            this.template.push({ "FolderName": (folder[j]).toString(), "PageName": [] });
          }

        }

      })



    // this.http.get('/api/getTmpFolders')
    //   .subscribe(res => {

    //     for (var i = 0; i < res.json().length; i++) {
    //       if (res.json()[i].length > 20) {
    //         this.template.push({ "FolderName": res.json()[i].substring(16, 19), "FileName": res.json()[i].substring(20, res.json()[i].length - 5) });
    //       }

    //     }
    //     console.log("whole list");
    //     console.log(res.json());
    //     console.log("folderlist");
    //     console.log(this.template);

    //   }, err => {
    //     console.log("err " + err); 
    //   })

    if (localStorage.getItem("dboard") == "1") {
      this.dboardCall = true;
    }
    if (localStorage.getItem("dboard") == "2") {
      var PReview=[];
      this.ReviewList = true;
      PReview.push({
        "TId":localStorage.getItem("ReviewTID"),
        "TName":localStorage.getItem("ReviewTName") 
         });
        var headers = new Headers();
        console.log(PReview);
        headers.append("Content-Type", "application/json");
       this.http.post('http://localhost:4777/api/getTemplateReview', JSON.stringify(PReview[0]), { headers: headers })
       .subscribe(res => {
         var Pcomment = res.json();
        // console.log(res.json());
          if (Pcomment.length > 0) {
            var resultArray = res.json();
                      
            this.TemplatePageReview=resultArray;
      
         }
        
   
       });


    }
    if (localStorage.getItem("dboard") == "3") {
      var PReview=[];
      this.PReviewList = true;
      PReview.push({
        "TId":localStorage.getItem("ReviewTID")
      
         });
        var headers = new Headers();
        headers.append("Content-Type", "application/json");
        if(localStorage.getItem("publishStatus")=="false"){

       this.http.post('/api/getProductReview', JSON.stringify(PReview[0]), { headers: headers })
       .subscribe(res => {
   
         var Pcomment = Object.keys(res.json());
         console.log(Pcomment);
          if (Pcomment.length > 0) {
            this.TemplatePReview=res.json();
            console.log(this.TemplatePReview);
         }
        
   
       });


      }else if(localStorage.getItem("publishStatus")=="true"){

        this.http.post('/api/getProductReviewPublish', JSON.stringify(PReview[0]), { headers: headers })
        .subscribe(res => {
    
          var Pcomment = Object.keys(res.json());
          console.log(Pcomment);
           if (Pcomment.length > 0) {
             this.TemplatePReview=res.json();
             console.log(this.TemplatePReview);
          }
         
    
        });
 
 
       }
    }

  }

  changeTheme(eve) {
    if (eve.target.value == "Default") {
      document.getElementById('theme').setAttribute('href', 'assets/productcss/custom.css');
      this.appServices.themeSelection = "custom";
    } else if (eve.target.value == "White") {
      document.getElementById('theme').setAttribute('href', 'assets/productcss/custom-theme-2.css');
      this.appServices.themeSelection = "custom-theme-2";
    } else if (eve.target.value == "Black") {
      document.getElementById('theme').setAttribute('href', 'assets/productcss/custom-theme-3.css');
      this.appServices.themeSelection = "custom-theme-3";
    }

  }

  enableProductAdd(){
    this.enableProdAdd=true;
  }

  validProduct(eve){
    if(eve.target.value!=""){
      this.validProd=true;
    }else{
      this.validProd=false;
    }
  }

  addProdType(nProduct){
     var headers = new Headers();
    headers.append('Content-Type', 'application/json');


    this.http.post('http://localhost:4777/api/addProductType', ({ "productType":nProduct.nativeElement.value}), { headers: headers })
      .subscribe(res => {
        if(res.json().output=="pass"){
            this.alertMsg(nProduct.nativeElement.value+" Added");
            this.pType=res.json().data.productType;
            nProduct.nativeElement.value="";
                
        }else{
           this.alertMsg(nProduct.nativeElement.value+" Already available, Enter diffrent type");
               
        }
      })
  
  }

  panelType(sp) {

    this.selectPT = true;
    localStorage.setItem("msg2", sp);
  }

  apply(pt) {
    if (pt == '--Select--') {
      this.selectPT = false;
    } else {
      localStorage.setItem("msg1", this.message1);
      this.result = this.message1;

      this.close();
    }
  }



  // Updated by Pisith
  // Create new Template
  applydBoard(name, pType) {
   
    console.log('Create new template call');
    if (this.copyTemplate != undefined) {
      var copyExTemp = this.copyTemplate.nativeElement.value;
    }

    // for (var i = 0; i < this.pname.length; i++) {
    //     for(var j=0;j<this.template.length;j++){
    //       if(this.template[j].PageName){
    //       if(this.pname[i].PageName==this.template[j].PageName){
    //           this.subPageChecked.push({ "PageName": this.pname[i].PageName, "PageContent": this.template[j].PageContent })
    //         } 
    //     }
    //       }

    // }


    if (this.num.length > 0) {
      var newGenNum = Math.max(...this.num);
      newGenNum++;
      this.templateIdGen = pType + "-" + newGenNum;
    } else {
      this.templateIdGen = pType + "-" + 1;
    } 

    this.checkedPages = [];
    this.checkedPageNames = [];
    // this.subPageChecked = [];
    // console.log(this.pname);
    // console.log(this.pname);
    // for (var i = 0; i < this.pname.length; i++) {
    //   for (var j = 0; j < this.template.length; j++) {
    //     if (this.template[j].FolderName == this.folderName) {

    //       this.subPageChecked = this.template[j];

    //     }
    //   }

    // }



    // localStorage.setItem("sPages", JSON.stringify(this.subPageChecked));
    console.log("Below is subpage");
    console.log(this.subPageChecked);





    if (copyExTemp == "--Select--" || this.copyTemplate == undefined) {
      if (pType != "--Select--") {

        var TemplateId = this.templateIdGen;

        var newday = new Date();
        var de = newday.toLocaleString();
        this.checkedPages.push({ "TemplateId": TemplateId, "TemplateName": name, "ProductType": pType, "CreatedBy": localStorage.getItem("user"), "CreatedDate": de, "ModifiedDate": "", "SelectedTheme": this.appServices.themeSelection, "PageList": [],"Reviews": [] })
        localStorage.setItem("checkedPages", JSON.stringify(this.checkedPages));
      } else {
        this.pTypeSelect = false;
      }
    } else {
      var TemplateId = this.templateIdGen;

      var newday = new Date();
      var de = newday.toLocaleString();
      // var selectedPageLocalStorage = JSON.parse(localStorage.getItem("sPages"));
      this.checkedPageNames.push({ "TemplateId": TemplateId, "TemplateName": name, "ProductType": pType, "CreatedBy": localStorage.getItem("user"), "CreatedDate": de, "ModifiedDate": "", "SelectedTheme": this.appServices.themeSelection, "PageList": [],"Reviews": [] })
      this.checkedPageNames.push({ "CopyExistTemp": copyExTemp, "PagesSelected": this.subPageChecked })
      localStorage.setItem("checkedPageNames", JSON.stringify(this.checkedPageNames));
    }
    //add by darong
    this.http.post("/api/createTmpTemplate",{data:this.checkedPages}).subscribe(res => {
      console.log(res)
    })  
    /////

    console.log("TempSelection");
    console.log(this.checkedPages);

    this.result = "Done";
    this.close();





    //  if (!cbox.target.parentElement.previousElementSibling.getElementsByTagName("input")[i].checked && document.getElementsByClassName("modal-body2")[0].getElementsByTagName("input")[0], document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[1].value!="--Select--" ){
    //     this.checkedPages.push({ "TemplateName": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("input")[0], "ProductType": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[0].value, "CopyExistTemp": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[1].value})
    //   } else if(!cbox.target.parentElement.previousElementSibling.getElementsByTagName("input")[i].checked && document.getElementsByClassName("modal-body2")[0].getElementsByTagName("input")[0], document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[1].value=="--Select--"){
    //     this.checkedPages.push({ "TemplateName": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("input")[0], "ProductType": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[0].value})
    //   }
  }
  // applydBoard(name, pType) {
  //   this.appServices.tempProdInfo= [];
  //   this.appServices.agency= [];
  //   this.appServices.operation_annuity= [];
  //   this.appServices.group= [];
  //   this.appServices.policyServicing= [];
  //   this.appServices.operation_uwritting= [];
  //   this.appServices.operation_unitlink= [];
  //   this.appServices.operation_nbusiness= [];
  //   this.appServices.charges= [];
  //   this.appServices.finance= [];
  //   this.appServices.product_rate= [];
  //   this.appServices.risk_coverage= [];
    
   
  //   if (this.num.length > 0) {
  //     var newGenNum = Math.max(...this.num);
  //     newGenNum++;
  //     this.templateIdGen = pType + "-" + newGenNum;
  //   } else {
  //     this.templateIdGen = pType + "-" + 1;
  //   } 

  //   this.checkedPages = [];
    
  
  //     if (pType != "--Select--") {

  //       var TemplateId = this.templateIdGen;

  //       var newday = new Date();
  //       var de = newday.toLocaleString();
  //       this.checkedPages.push({ "TemplateId": TemplateId, "TemplateName": name, "ProductType": pType, "CreatedBy": localStorage.getItem("user"), "CreatedDate": de, "ModifiedDate": "", "SelectedTheme": this.appServices.themeSelection, "PageList": [],"Reviews": [] })
  //       localStorage.setItem("checkedPages", JSON.stringify(this.checkedPages));
  //     } else {
  //       this.pTypeSelect = false;
  //     }
    
  //   console.log("TempSelection");
  //   console.log(this.checkedPages);
    
  //   this.result = "Done";
  //   this.close();
  //   return this.result;
    





  //   //  if (!cbox.target.parentElement.previousElementSibling.getElementsByTagName("input")[i].checked && document.getElementsByClassName("modal-body2")[0].getElementsByTagName("input")[0], document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[1].value!="--Select--" ){
  //   //     this.checkedPages.push({ "TemplateName": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("input")[0], "ProductType": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[0].value, "CopyExistTemp": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[1].value})
  //   //   } else if(!cbox.target.parentElement.previousElementSibling.getElementsByTagName("input")[i].checked && document.getElementsByClassName("modal-body2")[0].getElementsByTagName("input")[0], document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[1].value=="--Select--"){
  //   //     this.checkedPages.push({ "TemplateName": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("input")[0], "ProductType": document.getElementsByClassName("modal-body2")[0].getElementsByTagName("select")[0].value})
  //   //   }
  // }


  // templateIDGenerate(){
  //   //var templateId = Math.floor(Math.random() * 1000) + 1;
  //   // var templateId=Math.random().toString(36).substr(2, 4);
  //   //    return templateId.toUpperCase();

  //   //var templateId = Math.floor(Math.random() * 1000) + 1;

  //   if(this.num.length > 0){
  //     var newGenNum = Math.max(...this.num);
  //     newGenNum++;
  //     var templateId= "version-"+newGenNum;
  //   }else{
  //     var templateId= "version-1";
  //   }

  //   return templateId.toUpperCase();

  // }




  selectedFolder(eve) {
    this.folderName = eve;
    this.pageNames = [];
    this.subPageNames = [];
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');


    this.http.post('/api/getPublishFiles/publish', ({ "folderId": this.folderName }), { headers: headers })
      .subscribe(res => {
        console.log(res.json());

        for (var i = 0; i < Object.keys(res.json()).length; i++) {

          if (res.json()[Object.keys(res.json())[i]]) {
            if (JSON.parse(res.json()[Object.keys(res.json())[i]])[0].TemplateName) {
              this.pageNames = JSON.parse(res.json()[Object.keys(res.json())[i]])[0].PageList;
            } else {
              this.subPageNames.push({ "PageName": Object.keys(res.json())[i].substring(0, Object.keys(res.json())[i].length - 5), "PageContent": JSON.parse(res.json()[Object.keys(res.json())[i]]) })
            }

          }

        }
      })
    this.existingTemplate = true;

  }


  // copyETemplate(){

  //   this.existingTemplate=true;
  //   console.log(1);
  //   this.testing.push({"d":1},{"d":1},{"d":1});

  // }

  copy(pageName) {

    this.pageSelected = true;


    for (var j = 0; j < this.template.length; j++) {
      if (this.template[j].FolderName == this.folderName) {
        if (this.subPageNames.length == 0) {
          this.template[j].PageName.splice(j, 1);
          // this.templateJson=[];
        } else {
          this.template[j].PageName = [];
              if (pageName == "PROD_INFO") {
              this.copyOldTemplate = [];
             this.formModel = {skills0: []};
             for (var k = 0; k < this.subPageNames.length; k++) {   
              var page = [];
              if (this.subPageNames[k].PageName == "PROD_INFO") {
                this.template[j].PageName.push({ "MainPage": "PROD_INFO", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
               this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "PI") {
                this.template[j].PageName.push({ "SubPage": "PROD_INFO", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
             }
            } else if (pageName == "NEW_BUSINESS") {
               this.copyOldTemplate = [];
               this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "NEW_BUSINESS") {
                this.template[j].PageName.push({ "MainPage": "NEW_BUSINESS", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "NB") {
                this.template[j].PageName.push({ "SubPage": "NEW_BUSINESS", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }else if (pageName == "OPERATION_UNDERWRITING") {
               this.copyOldTemplate = [];
               this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "OPERATION_UNDERWRITING") {
                this.template[j].PageName.push({ "MainPage": "OPERATION_UNDERWRITING", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "UW") {
                this.template[j].PageName.push({ "SubPage": "OPERATION_UNDERWRITING", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }else if (pageName == "OPERATION_UNITLINKED") {
               this.copyOldTemplate = [];
               this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "OPERATION_UNITLINKED") {
                this.template[j].PageName.push({ "MainPage": "OPERATION_UNITLINKED", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "UL") {
                this.template[j].PageName.push({ "SubPage": "OPERATION_UNITLINKED", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }else if (pageName == "OPERATION_ANNUITY") {
               this.copyOldTemplate = [];
               this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "OPERATION_ANNUITY") {
                this.template[j].PageName.push({ "MainPage": "OPERATION_ANNUITY", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "OA") {
                this.template[j].PageName.push({ "SubPage": "OPERATION_ANNUITY", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }else if (pageName == "PRODUCT_RATES") {
               this.copyOldTemplate = [];
               this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "PRODUCT_RATES") {
                this.template[j].PageName.push({ "MainPage": "PRODUCT_RATES", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "PR") {
                this.template[j].PageName.push({ "SubPage": "PRODUCT_RATES", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }else if (pageName == "AGENCY") {
              this.copyOldTemplate = [];
              this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "AGENCY") {
                this.template[j].PageName.push({ "MainPage": "AGENCY", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "AG") {
                this.template[j].PageName.push({ "SubPage": "AGENCY", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }else if (pageName == "OPERATION_GROUP") {
               this.copyOldTemplate = [];
               this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "OPERATION_GROUP") {
                this.template[j].PageName.push({ "MainPage": "OPERATION_GROUP", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "GRP") {
                this.template[j].PageName.push({ "SubPage": "OPERATION_GROUP", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }else if (pageName == "POLICYSERVICING") {
               this.copyOldTemplate = [];
               this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "POLICYSERVICING") {
                this.template[j].PageName.push({ "MainPage": "POLICYSERVICING", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "PS") {
                this.template[j].PageName.push({ "SubPage": "POLICYSERVICING", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }else if (pageName == "PRODUCT_FINANCE") {
               this.copyOldTemplate = [];
               this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "PRODUCT_FINANCE") {
                this.template[j].PageName.push({ "MainPage": "PRODUCT_FINANCE", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "PF") {
                this.template[j].PageName.push({ "SubPage": "PRODUCT_FINANCE", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }else if (pageName == "PRODUCT_CHARGES") {
               this.copyOldTemplate = [];
               this.formModel = {skills0: []};
              for (var k = 0; k < this.subPageNames.length; k++) { 
                 
              var page = [];
              if (this.subPageNames[k].PageName == "PRODUCT_CHARGES") {
                this.template[j].PageName.push({ "MainPage": "PRODUCT_CHARGES", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              } else if (this.subPageNames[k].PageName.split("_")[0] == "PC") {
                this.template[j].PageName.push({ "SubPage": "PRODUCT_CHARGES", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
                 this.copyOldTemplate.push({ "id": k, "itemName": this.subPageNames[k].PageName })

              }
            }
          }


        }

      } else {
        this.template[j].PageName = [];

      }
    }

    this.cpage = true;

    //this.copyOldTemplate=
  }

  // copyETemplate(mainp) {

  //   for (var j = 0; j < this.template.length; j++) {
  //     if (this.template[j].FolderName == this.folderName) {
  //       if (this.subPageNames.length == 0) {
  //         this.template[j].PageName.splice(j, 1);
  //         // this.templateJson=[];
  //       } else {
  //         this.template[j].PageName = [];

  //         for (var k = 0; k < this.subPageNames.length; k++) {
  //           if (mainp == "PROD_INFO") {
  //             var page = [];
  //             if (this.subPageNames[k].PageName == "PROD_INFO") {
  //               this.template[j].PageName.push({ "MainPage": "PROD_INFO", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
  //             } else if (this.subPageNames[k].PageName.split("_")[0] == "PI") {
  //               this.template[j].PageName.push({ "SubPage": "PROD_INFO", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
  //             }
  //           } else if (mainp == "NEW_BUSINESS") {
  //             var page = [];
  //             if (this.subPageNames[k].PageName == "NEW_BUSINESS") {
  //               this.template[j].PageName.push({ "MainPage": "NEW_BUSINESS", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
  //             } else if (this.subPageNames[k].PageName.split("_")[0] == "NB") {
  //               this.template[j].PageName.push({ "SubPage": "NEW_BUSINESS", "PageName": this.subPageNames[k].PageName, "PageContent": this.subPageNames[k].PageContent })
  //             }
  //           }
  //         }


  //       }

  //     }else{
  //        this.template[j].PageName = [];

  //     }
  //   }


  //   this.existingTemplate = true;
  // }

  closeno(eve) {
    this.existingTemplate = false; 
     (<HTMLInputElement>document.getElementById("copyTemplate")).value="";

  }

  copyPage() {

    this.pname = [];

    for (var i = 0; i < this.formModel.skills0.length; i++) {

      this.pname.push({ "PageName": this.formModel.skills0[i].itemName.trim() })


    }



    for (var i = 0; i < this.template.length; i++) {
      if (this.template[i].FolderName == this.folderName) {
        for (var k = 0; k < this.pname.length; k++) {
          for (var j = 0; j < this.template[i].PageName.length; j++) {
            if (this.pname[k].PageName == this.template[i].PageName[j].PageName) {
              this.subPageChecked.push({ "PageName": this.pname[k].PageName, "PageContent": this.template[i].PageName[j].PageContent })

            }


          }
        }
      }
    }
    //this.existingTemplate = false;

    this.alertMsg("Copied Successfuly");

  }

  copyBoard() {

    this.existingTemplate = false;

  }

  tCopyPages(eve) {
    // if (eve.target.parentElement.textContent.trim() == "PROD_INFO") {
    //   for (var i = 0; i < eve.target.parentElement.parentElement.children.length; i++) {
    //     eve.target.parentElement.parentElement.children[i].children[0].checked = true;

    //   }
    // }
    // if (eve.target.parentElement.textContent.trim() == "NEW_BUSINESS") {
    //   for (var i = 0; i < eve.target.parentElement.parentElement.children.length; i++) {
    //     eve.target.parentElement.parentElement.children[i].children[0].checked = true;

    //   }
    // }
  }

  alertMsg(msg) {
    localStorage.setItem("alertContent", msg);
    this.dialogService.addDialog(AlertComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
    });
  }


    searchTemplate(list){
   
    let val = (<HTMLInputElement>document.getElementById("search")).value;
    let filter = val.toUpperCase();
    for(let a=0; a<list.length; a++){
      
      if (list[a].name.toUpperCase().indexOf(filter) > -1 || list[a].name.toUpperCase().indexOf(filter) > -1 ) {
        (<HTMLInputElement>document.getElementById("rowInterface"+a)).style.display = "";
      } else {
        (<HTMLInputElement>document.getElementById("rowInterface"+a)).style.display = "none";
      }
    }
  }

  copyExistingTemplate(name, pType){
     this.appServices.tempProdInfo= [];
    this.appServices.agency= [];
    this.appServices.operation_annuity= [];
    this.appServices.group= [];
    this.appServices.policyServicing= [];
    this.appServices.operation_uwritting= [];
    this.appServices.operation_unitlink= [];
    this.appServices.operation_nbusiness= [];
    this.appServices.charges= [];
    this.appServices.finance= [];
    this.appServices.product_rate= [];
    this.appServices.risk_coverage= [];
    
   
    if (this.num.length > 0) {
      var newGenNum = Math.max(...this.num);
      newGenNum++;
      this.templateIdGen = pType + "-" + newGenNum;
    } else {
      this.templateIdGen = pType + "-" + 1;
    } 

    this.checkedPages = [];
    
  
      if (pType != "--Select--") {

        var TemplateId = this.templateIdGen;

        var newday = new Date();
        var de = newday.toLocaleString();
        this.checkedPages.push({ "TemplateId": TemplateId, "TemplateName": name, "ProductType": pType, "CreatedBy": localStorage.getItem("user"), "CreatedDate": de, "ModifiedDate": "", "SelectedTheme": this.appServices.themeSelection, "PageList": [],"Reviews": [] })
        localStorage.setItem("copyExistPages", JSON.stringify(this.checkedPages));
        this.close();
              this.router.navigate(['/productConfig/templateclone']);

      } else {
        this.pTypeSelect = false;
      }
          
                
  }
  omit_special_char(event)
  {   
     var k;  
     k = event.charCode;  //         k = event.keyCode;  (Both can be used)
     return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57)); 
  }
}
