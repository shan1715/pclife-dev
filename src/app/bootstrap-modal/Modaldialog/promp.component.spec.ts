/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PrompComponent } from './promp.component';

describe('PrompComponent', () => {
  let component: PrompComponent;
  let fixture: ComponentFixture<PrompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
