import { Component, OnInit } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { PrompComponent } from "./Modaldialog/promp.component";


@Component({
  selector: 'app-bootstrap-modal',
  templateUrl: './bootstrap-modal.component.html',
  styleUrls: ['./bootstrap-modal.component.css']
})
export class BootstrapModalComponent implements OnInit {

  constructor(private dialogService:DialogService) { }

  ngOnInit() {
  }
showPrompt() {
    this.dialogService.addDialog(PrompComponent, {
      title:'Name dialog',
      question1:'What is your name?: '});
  }
}
