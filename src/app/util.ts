export default class Utils {
    static getStrValue(val: string) {
        if (val != null || val != undefined)
            return val;
        else
            return "";
    }
    /**
     * @author Viswakarthick. J
     * @param val 
     *  convert Milli Seconds to Date
     */
    static fnConvMLStoDate(val: any) {
        if (val != null || val != undefined) {
            var dt = null;
            try {
                dt = new Date(val);
                dt = this.formatDate(dt);
            } catch (err) {
                console.log("Date Exception:Value:" + val);
            }
            return dt;
        } else
            return null;
    }

    // Updated by Pisith 03/12/2019
    // Format : 'MM-DD-YYYY HH:MM:SS
    /**
     * @author Viswakarthick. J
     * @param date 
     *  Format Date 'YYYY-MM-DD'
     */
    static formatDate(date: Date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear(),
            hour = '' + d.getHours(),
            minute = '' + d.getMinutes(),
            second = '' + d.getSeconds();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hour.length < 2) hour = '0' + hour;
        if (minute.length < 2) minute = '0' + minute;
        if (second.length < 2) second = '0' + second;

        console.log(`formatData called`);
        console.log([[month, day, year].join('-'), [hour, minute, second].join(':')].join(' '));
        return [[month, day, year].join('-'), [hour, minute, second].join(':')].join(' ');
    }

    // /**
    //  * @author Viswakarthick. J
    //  * @param date 
    //  *  Format Date 'YYYY-MM-DD'
    //  */
    // static formatDate(date: Date) {
    //     var d = new Date(date),
    //         month = '' + (d.getMonth() + 1),
    //         day = '' + d.getDate(),
    //         year = d.getFullYear();

    //     if (month.length < 2) month = '0' + month;
    //     if (day.length < 2) day = '0' + day;

    //     return [year, month, day].join('-');
    // }

}