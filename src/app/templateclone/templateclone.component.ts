import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, TemplateRef, ChangeDetectorRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormsModule, NgForm } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../app.service';
import { DragulaService } from 'ng2-dragula';
import { AlertComponent } from "../bootstrap-modal/alert/alert.component";
import { NgxSpinnerService } from 'ngx-spinner';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ElementRef } from "@angular/core";
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-templateclone',
  templateUrl: './templateclone.component.html',
  styleUrls: ['./templateclone.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TemplatecloneComponent implements OnInit, OnDestroy {
  selectedSearchFilter = {
      
    templateId: "", templateName: ""
};
public searchText : string;
  templateFull: any = [];
  template: any = [];
  selectedItems = [];
  dropdownSettings = {};
  productSettings = {};
  publishedFiles = [];
  screen2_values = [];
  templateCodeBool: boolean = false;
  selectedPlanCode: any = [];
  folderDatas: any = [];
  screen3_values = [];
  screen1_bool: boolean = false;
  screen2_bool: boolean = false;
  screen3_bool: boolean = false;
  copyElement: any;
  selectedPages: any = [];
  pType: any = [];
  ProductId: any;
  ProductType: any;
  TemplateId: any;
  planName: any;
  planDesc: any;
  today: any;
  ProductList: any = [];
  selectedProducts: any = [];
  selectedProductsFilter = { ProductId: '' };
  ws_mailClone: any;
  prodTmp: any = [];
  prodPublish: any = [];
  templateName: any = '';
  modalRef: BsModalRef;
  message: string;
  question3: string = "Template Name";
  question4: string = "Product Type";
  question5: string = "Copy From Existing Template";
  enableProdAdd: boolean = false;
  users: any[] = [{ name: 'John' }, { name: 'Jane' }, { name: 'Mario' }];
  userFilter: any = { name: '' };
  validProd: boolean = false;
  num: any = [];
  templateIdGen: any;
  checkedPageNames: any = [];
  selectedTemplates: any = [];
  selectedTemplateId:any=[];
  selectedTemplatesData: any = [];
  pageName: any = [];
   config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false,
    class: "my-modal"
  };


  @ViewChild('s1') public s1: NgForm;
  @ViewChild('newProduct') newProduct:ElementRef;

  constructor(private router:Router,private changedet: ChangeDetectorRef, private modalService: BsModalService, private spinner: NgxSpinnerService, private dialogService: DialogService, private http: Http, private appServices: AppServices, private dragulaService: DragulaService) {

    this.http.get('assets/data/property.json')
      .subscribe(res => {
        this.ws_mailClone = res.json().web.url;
      });

    (<HTMLInputElement>document.getElementsByTagName("app-sidebar")[0]).style.display = "none";


    this.screen1_bool = true;

    this.dragulaService.drag.subscribe((value: any) => {
      console.log("drag");

      this.onDrag(value.slice(1));

    });

    this.dragulaService.drop.subscribe((value: any) => {
      //console.log(`drop: ${value[0]}`);
      this.onDrop(value.slice(1));
      console.log("drop");
    });
    this.dragulaService.over.subscribe((value: any) => {
      // console.log(`over: ${value[0]}`);
      this.onOver(value.slice(1));
      console.log("over");
    });
    this.dragulaService.out.subscribe((value: any) => {
      //console.log(`out: ${value[0]}`);
      this.onOut(value.slice(1));
      console.log("out");
    });
    this.dropdownSettings = {
      singleSelection: true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"

    };

    this.productSettings = {
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"

    };

  }

  ngOnInit() {
    this.screen2_bool=true;
    this.today = new Date().toISOString().substring(0, 10);

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.get('http://localhost:4777/api/getProductTmpFolders')
      .subscribe(resProdTmp => {
        this.prodTmp = resProdTmp.json();
        //var s=Object.keys(resProdTmp.json()).indexOf("plancode123");

        console.log(resProdTmp.json());
      }, err => {
        console.log(err);
      })


    // this.http.get('/api/readFolders')
    //   .subscribe(resTemplate => {
    //     this.templateFull = resTemplate.json();
    //     var folder = Object.keys(resTemplate.json());
    //     if (folder.length > 0) {
    //       for (var j = 0; j < folder.length; j++) {

    //         this.template.push({ "id": j, "itemName": (folder[j]).toString(), "PageName": [], "PageContent": resTemplate.json()[folder[j].toString()] });
    //       }

    //     }
    //     this.changedet.detectChanges();
    //   })

  setTimeout(()=>{
            this.spinner.show();
        },100)
this.http.get('/api/readFolders')
      .subscribe(res => {
      
        this.templateFull = res.json();

    //var template=[];
// for(var i=0;i<Object.keys(res.json()).length;i++){
// 	template.push({"templateId":Object.keys(res.json())[i]})
// }
if(Object.keys(res.json()).length>0){
for(var i=0;i<Object.keys(res.json()).length;i++){
	for(var j=0;j<Object.keys(res.json()[Object.keys(res.json())[i]]).length;j++){
	if(JSON.parse(res.json()[Object.keys(res.json())[i]][Object.keys(res.json()[Object.keys(res.json())[i]])[j]])[0].TemplateName){
		this.template.push({"templateId":JSON.parse(res.json()[Object.keys(res.json())[i]][Object.keys(res.json()[Object.keys(res.json())[i]])[j]])[0].TemplateId,"templateName":JSON.parse(res.json()[Object.keys(res.json())[i]][Object.keys(res.json()[Object.keys(res.json())[i]])[j]])[0].TemplateName})
}
}
if(Object.keys(res.json()).length-1==i){
   setTimeout(()=>{
    // this.searchTemplate(this.template);
         this.spinner.hide();
        },100)
}
}
}else{
   setTimeout(()=>{
    this.spinner.hide();
        },100)
        swal({
              type: 'info',
              title: 'Status',
              text: 'Templates not published'
            }).then(name => {
                          this.router.navigate(['/productConfig/dashboard']);

})
}
      },err=>{
         setTimeout(()=>{
    this.spinner.hide();
        },100)
      
      })

    this.http.get('/api/getProductPublishFolders')
      .subscribe(res => {
        this.prodPublish = res.json();
        this.folderDatas = res.json();
        var folder = Object.keys(res.json());
        if (folder.length > 0) {
          this.publishedFiles = [];
          for (var i = 0; i < folder.length; i++) {

            this.publishedFiles.push(JSON.parse(res.json()[folder[i]][folder[i] + '.json'])[0]);
          }

        }
      })


    this.http.get("./assets/data/ProdType.json")
      .subscribe((res) => {
        this.pType = res.json().productType;

      }, err => {

        console.log(err);
      })

  }


  ngOnDestroy() {
    (<HTMLInputElement>document.getElementsByTagName("app-sidebar")[0]).style.display = "block";

  }

  onItemSelect(item: any) {
   

  }
  OnItemDeSelect(item: any) {
    
  }
  onSelectAll(items: any) {
  

  }
  onDeSelectAll(items: any) {
    
  }


  screen1(s1: NgForm, template) {
    this.screen2_values = [];
    // var startDate = new Date(s1.form.controls["startDate"].value).toDateString();
    // var endDate = new Date(s1.form.controls["endDate"].value).toDateString();
    this.ProductId = this.s1.controls["planCode"].value.toUpperCase();
    this.ProductType = this.s1.controls["productType"].value;

    this.planName = this.s1.controls["planName"].value;
    this.planDesc = this.s1.controls["planDesc"].value;


    if (this.selectedItems.length > 0) {
      this.appServices.productCloneCreateTemplate = false;
      this.TemplateId = this.selectedItems[0].itemName;

      if (this.publishedFiles.length > 0) {
        this.screen2_values = this.publishedFiles;
        // for (var i = 0; i < this.publishedFiles.length; i++) {
        //   this.ProductList.push({ "id": i, "itemName": this.publishedFiles[i].ProductId })
        // }
        this.screen1_bool = false;
        this.screen2_bool = true;
      } else {
        localStorage.setItem("alertContent", "None of the product released");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '
        }).subscribe((isConfirmed) => {
        })
      }
    } else {
      this.templateName = "none";
  //      openModal(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template, this.config);
  // }
      this.openModal(template);
    }

    // if (this.screen2_values.length == 0) {

    // }

  }



  navScreen1() {
    this.screen2_values = [];
  }

  navScreen3() {
    this.selectedTemplateId=[];
for (var h = 0; h < document.getElementsByClassName("templateSelected").length; h++) {
      if ((<HTMLInputElement>document.getElementsByClassName("templateSelected")[h]).checked) {
        this.selectedTemplateId.push({ "templateId": document.getElementsByClassName("templateSelected")[h].nextElementSibling.textContent, "templateName": document.getElementsByClassName("templateSelected")[h].parentElement.nextElementSibling.textContent });
      }
    }
    this.screen1_bool = false;
    this.screen2_bool = false;
    this.screen3_bool = true;
    this.screen3_values = [];
    this.selectedTemplatesData = [];
    var templateOutput = [];
   for (var i = 0; i < this.selectedTemplateId.length; i++) {

      //Template
      for (var j = 0; j < Object.keys(this.templateFull).length; j++) {

        if (this.selectedTemplateId[i].templateId == Object.keys(this.templateFull)[j]) {
          var output = [];
          for (var k = 0; k < Object.keys(this.templateFull[Object.keys(this.templateFull)[j]]).length; k++) {
            if (this.selectedTemplateId[i].templateName + '.json' != (Object.keys(this.templateFull[Object.keys(this.templateFull)[j]])[k])) {
              if (i == 0) {
                var result = { "data": JSON.parse(this.templateFull[Object.keys(this.templateFull)[j]][Object.keys(this.templateFull[Object.keys(this.templateFull)[j]])[k]])[0], "hidden": false };
                output.push(result);
              } else {
                var result = { "data": JSON.parse(this.templateFull[Object.keys(this.templateFull)[j]][Object.keys(this.templateFull[Object.keys(this.templateFull)[j]])[k]])[0], "hidden": true };
                output.push(result)
              }
            }

          }
          this.screen3_values.push(output);
        }
      }



    }

        this.selectedPages = this.screen3_values;


    for (var b = 0; b < this.selectedTemplateId.length; b++) {

      for (var z = 0; z < Object.keys(this.templateFull).length; z++) {

        if (this.selectedTemplateId[b].templateId == Object.keys(this.templateFull)[z]) {
          console.log(this.templateFull[Object.keys(this.templateFull)[z]])
          this.selectedTemplatesData.push(this.templateFull[Object.keys(this.templateFull)[z]])

        }

      }

    }


    //template

  }

  navPrevPlanCode(eve) {
    if (eve == 0) {

    } else {
      var display = eve - 1;
      for (var i = 0; i < this.screen3_values.length; i++) {

        for (var j = 0; j < this.screen3_values[i].length; j++) {
          if (i == display) {
            this.screen3_values[i][j].hidden = false;

          } else {
            this.screen3_values[i][j].hidden = true;
          }
        }
      }
    }
  }
  navNextPlanCode(eve) {
    if (this.screen3_values.length - 1 == eve) {

    } else {
      var display = eve + 1;
      for (var i = 0; i < this.screen3_values.length; i++) {

        for (var j = 0; j < this.screen3_values[i].length; j++) {
          if (i == display) {
            this.screen3_values[i][j].hidden = false;

          } else {
            this.screen3_values[i][j].hidden = true;
          }
        }
      }
    }

  }

  private onDrag(args: any): void {
    let [e, target, parent, moves] = args;
    var copyContent = e.parentElement.parentElement.parentElement;
    this.copyElement = copyContent.cloneNode(true);
  }



  private onDrop(args: any): void {
    let [e, target, parent, moves] = args;



    if (e.parentElement.className == "col-sm-8") {
      // this.dragulaService.find('second-bag').drake.cancel(true);
      // e.className='';
      // this.copyElement.getElementsByClassName("panel-body")[0].innerHTML='';
      // this.copyElement.getElementsByClassName("panel-body")[0].appendChild(e);
      //   target.appendChild(this.copyElement);
    }


    //   for (var i = 0; i < this.copyElement.getElementsByClassName("panel-body")[0].children.length; i++) {
    // //console.log(this.copyElement.getElementsByClassName("panel-body")[0].children[i].getElementsByTagName("h4")[0].textContent.trim())
    //         if (e.getElementsByTagName("h4")[0].textContent.trim() != this.copyElement.getElementsByClassName("panel-body")[0].children[i].getElementsByTagName("h4")[0].textContent.trim()) {
    //           this.copyElement.getElementsByClassName("panel-body")[0].children.removeChild(i);
    //         }
    //       }
    //   target.appendChild(this.copyElement)


  }


  private onOver(args: any): void {

    let [el] = args;

  }

  foutc;

  private onOut(args: any): void {
    let [el] = args;

  }

  selectedPage(eve, pname, blockId,index) {
    if (eve.target.classList.contains("blockName")) {
      var available = false;
      for (var i = 0; i < eve.target.parentElement.parentElement.parentElement.querySelectorAll('.form-control').length; i++) {
        if (eve.target.parentElement.parentElement.parentElement.querySelectorAll('.form-control')[i].checked) {
          available = true;
          break;

        }
      }
    }

    if (available) {
      if (this.pageName.includes(pname.data.PageName)) {

      } else {
        this.pageName.push(pname.data.PageName)
      }
    }
   
    var enableSelect = true;

    for (var i = 0; i < this.selectedPages.length; i++) {
      for (var j = 0; j < this.selectedPages[i].length; j++) {
        if (this.selectedPages[i][j].data.TemplateId == pname.data.TemplateId && this.selectedPages[i][j].data.PageName == pname.data.PageName) {
          for (var k = 0; k < this.selectedPages[i][j].data.PageContent.length; k++) {
            if (this.selectedPages[i][j].data.PageContent[k].blockId == blockId) {
              if (this.selectedPages[i][j].data.PageContent[k].checked) {
                this.selectedPages[i][j].data.PageContent[k]["checked"] = eve.target.checked;

              }
            }
          }
        } else {
          for (var k = 0; k < this.selectedPages[i][j].data.PageContent.length; k++) {
            if (this.selectedPages[i][j].data.PageContent[k].blockId == blockId) {
              if (this.selectedPages[i][j].data.PageContent[k].checked) {
                enableSelect = false;
              }
            }
          }
        }
      }
    }

    if (enableSelect) {
      var enableBlock = false;

      for (var i = 0; i < this.selectedPages.length; i++) {
        for (var j = 0; j < this.selectedPages[i].length; j++) {
          if (this.selectedPages[i][j].data.TemplateId == pname.data.TemplateId && this.selectedPages[i][j].data.PageName == pname.data.PageName) {
            for (var k = 0; k < this.selectedPages[i][j].data.PageContent.length; k++) {
              if (this.selectedPages[i][j].data.PageContent[k].blockId == blockId) {
                this.selectedPages[i][j].data.PageContent[k]["checked"] = eve.target.checked;
              }
              if (this.selectedPages[i][j].data.PageContent[k].checked) {
                enableBlock = true;
              }
            }
            this.selectedPages[i][j].data["enableBlock"] = true;
          }
        }
      }
    } else {
      eve.target.checked = false;
      alert("Product already selected");
    }
    var chkcontent=0;
    for(var k=0;k<pname.data.PageContent.length;k++)
    {
      if(pname.data.PageContent[k].checked)
      {
        chkcontent=chkcontent+1;
      }
    }
    if(chkcontent==0)
    {
      pname.data.enableBlock=false; 
    }
    var selId=pname.data.PageName.split('.')[0]+index;
    if(pname.data.PageContent.length==chkcontent)
    {
   (<HTMLInputElement>document.getElementById(selId)).checked=true;
    }else{
   
      (<HTMLInputElement>document.getElementById(selId)).checked=false;
    }
  }

  selectAllBlocks(pname, eve,index) {
    //selectAll checkbox
    if (eve.target.checked) {
      if (this.pageName.includes(pname.data.PageName)) {

      } else {
        this.pageName.push(pname.data.PageName);

      }
    } else {
      if (this.pageName.includes(pname.data.PageName)) {
        this.pageName.splice(this.pageName.indexOf(pname.data.PageName));
      }
    }
  

    var enableSelection=true;

    for (var ia = 0; ia < this.selectedPages.length; ia++) {
      for (var ja = 0; ja < this.selectedPages[ia].length; ja++) {
        if (this.selectedPages[ia][ja].data.TemplateId == pname.data.TemplateId && this.selectedPages[ia][ja].data.PageName == pname.data.PageName) {

          if (this.selectedPages[ia][ja].data.PageName == pname.data.PageName) {
            if (this.selectedPages[ia][ja].data.enableBlock) {
              enableSelection = true;
              
            }
          }
        } else {
          if (this.selectedPages[ia][ja].data.PageName == pname.data.PageName) {
            if (this.selectedPages[ia][ja].data.enableBlock) {
              enableSelection = false;
            }
          }
        }
      }
      if(ia==this.selectedPages.length-1){

        if (enableSelection) {
          if (eve.target.checked) {
            for (var m = 0; m < eve.target.parentElement.getElementsByClassName("panel-body")[0].children.length; m++) {
              if (eve.target.parentElement.getElementsByClassName("panel-body")[0].children[m].getElementsByTagName("input")[0].type == "checkbox") {
    
                eve.target.parentElement.getElementsByClassName("panel-body")[0].children[m].getElementsByTagName("input")[0].checked = true;
              }
    
            }
            for (var i = 0; i < this.selectedPages.length; i++) {
              for (var j = 0; j < this.selectedPages[i].length; j++) {
                if (this.selectedPages[i][j].data.TemplateId == pname.data.TemplateId && this.selectedPages[i][j].data.PageName == pname.data.PageName) {
    console.log(this.selectedPages[i][j].data.TemplateId)           
       for (var k = 0; k < this.selectedPages[i][j].data.PageContent.length; k++) {
                    this.selectedPages[i][j].data.PageContent[k]["checked"] = true;
    
                  }
                  this.selectedPages[i][j].data["enableBlock"] = true;
                }else if (this.selectedPages[i][j].data.TemplateId != pname.data.TemplateId && this.selectedPages[i][j].data.PageName == pname.data.PageName) {
           
                  for (var k = 0; k < this.selectedPages[i][j].data.PageContent.length; k++) {
                    this.selectedPages[i][j].data.PageContent[k]["checked"] = false;
    
                  }
                  this.selectedPages[i][j].data["enableBlock"] = false;
                }
    
              }
            }
          } else {
            for (var m = 0; m < eve.target.parentElement.getElementsByClassName("panel-body")[0].children.length; m++) {
              if (eve.target.parentElement.getElementsByClassName("panel-body")[0].children[m].getElementsByTagName("input")[0].type == "checkbox") {
    
                eve.target.parentElement.getElementsByClassName("panel-body")[0].children[m].getElementsByTagName("input")[0].checked = false;
              }
    
            }
            for (var i = 0; i < this.selectedPages.length; i++) {
              for (var j = 0; j < this.selectedPages[i].length; j++) {
                if (this.selectedPages[i][j].data.TemplateId == pname.data.TemplateId && this.selectedPages[i][j].data.PageName == pname.data.PageName) {
                  for (var k = 0; k < this.selectedPages[i][j].data.PageContent.length; k++) {
                    this.selectedPages[i][j].data.PageContent[k]["checked"] = false;
    
                  }
                  this.selectedPages[i][j].data["enableBlock"] = false;
                }
    
              }
            }
          }
        } else {
          eve.target.checked = false;
          alert("Already Selected");
        }
    
      }
    }
  // for(var k=0;k<this.pageName.length;k++)
  // {
  //   if(this.pageName[k]==pname.data.pageName)
  //   {
  //     enableSelection = false;

  //   }
  // }
  
  
  }

  copyProduct1(selectedPages) {

  }



  copyTemplate(selectedPages) {


      var nTemplate = JSON.parse(localStorage.getItem("copyExistPages"));
      for (var v = 0; v < Object.keys(this.selectedTemplatesData[0]).length; v++) {
        if (JSON.parse(this.selectedTemplatesData[0][Object.keys(this.selectedTemplatesData[0])[v]])[0].TemplateName) {
          nTemplate[0].PageList = [];
          nTemplate[0].Reviews=[];
        
          // for (var rw = 0; rw < this.pageName.length; rw++) {
          //   nTemplate[0].PageList.push({ "name": this.pageName[rw].substr(0, this.pageName[rw].length - 5), "review": "Approved" })
          // }

          // nTemplate[0].Reviews = JSON.parse(this.selectedTemplatesData[0][Object.keys(this.selectedTemplatesData[0])[v]])[0].Reviews;

        }

      }
      var headers = new Headers();
    headers.append('Content-Type', 'application/json');
      this.http.post('/api/createTmpCloneTemplate', nTemplate, { headers: headers })
        .subscribe(res => {


        localStorage.setItem("alertContent", "Cloning process started, You will receieve an email once done");
        document.getElementsByClassName("cloneBtn")[0].setAttribute("disabled","true");
        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '
        }).subscribe((isConfirmed) => {

           var dataTemplateContent = [];
          var emailData={"eMailTo":  JSON.parse(localStorage.getItem("accessRights")).email, "message": "Template Cloned Successfuly " + nTemplate[0].TemplateId};

          dataTemplateContent.push({ "templateFull": this.templateFull, "selectedPlanCode": this.selectedTemplateId, "selectedPages": this.selectedPages, "nTemplate": nTemplate,"emailData":emailData });

          this.http.post('/api/dataTmpTemplateClone', dataTemplateContent, { headers: headers })
            .subscribe(res => {
              // alert("datatemplatecliond");
            })

        })

         





        }, err => {
          console.log("err " + err);
        })

    

    // if(this.TemplateId==undefined){
    //   this.TemplateId=this.templateIdGen;
    // }

    // var newday = new Date();
    // var de = newday.toLocaleString();
    // var productCreate = [];
    
    // // var pageList=[];
    // // for (var rw = 0; rw < this.pageName.length; rw++) {
    // //         pageList.push({ "name": this.pageName[rw].substr(0, this.pageName[rw].length - 5), "review": "" })
    // //       }

    // productCreate.push({ "ProductId": this.ProductId, "ProductType": this.ProductType, "TemplateId": this.TemplateId, "CreatedBy": localStorage.getItem("user"), "PlanName": this.planName, "PlanDesc": this.planDesc, "Publish": false, "CreatedDate": de, "ModifiedDate": "", "PageList": [], "Reviews": [], "review": false, "DataMonitor": [] })
    // var headers = new Headers();
    // headers.append('Content-Type', 'application/json');
    // this.http.post('/api/createPublishTemplate', productCreate, { headers: headers })
    //   .subscribe(res => {

    //     localStorage.setItem("alertContent", "Cloning process started, You will receieve an email once done");
    //     document.getElementsByClassName("cloneBtn")[0].setAttribute("disabled","true");
    //     this.dialogService.addDialog(AlertComponent, {
    //       title: 'Name dialog',
    //       question1: 'Block Name ',
    //       question2: 'Block Type '
    //     }).subscribe((isConfirmed) => {


    //       var dataContent = [];
    //       var emailData={"eMailTo": JSON.parse(localStorage.getItem("accessRights")).email, "message": "Product Cloned Successfuly " + this.ProductId};
    //       dataContent.push({ "ProductId": this.ProductId, "TemplateContent": this.selectedTemplatesData, "SelectedPages": this.selectedPages,"emailData":emailData});

    //       this.http.post('/api/dataProductClone', dataContent, { headers: headers })
    //         .subscribe(res => {


    //           // this.http.post(this.ws_mailClone + "ProductBenefits/SendCloningEMail/", { "eMailTo": JSON.parse(localStorage.getItem("accessRights")).email, "message": "Product Cloned Successfuly " + this.ProductId }, { headers: headers })
    //           //   .subscribe(resCheck => {
    //           //   }, err => {

    //           //   })
    //         }, err => {
    //           //this.spinner.hide(); 
    //           console.log("err " + err);
    //         })



    //     }, err => {
    //       console.log("err " + err);
    //     })
    //   })









  }



  selectTemplateCode() {
    var templateCodeSelected = false;
    for (var i = 0; i < document.getElementsByClassName("templateSelected").length; i++) {
      if ((<HTMLInputElement>document.getElementsByClassName("templateSelected")[i]).checked) {
        templateCodeSelected = true;
      }
    }
    if (templateCodeSelected) {

      this.templateCodeBool = true;


      //template
      for (var j = 0; j < document.getElementsByClassName("templateSelected").length; j++) {
        if ((<HTMLInputElement>document.getElementsByClassName("templateSelected")[j]).checked) {
          for (var k = 0; k < this.screen2_values.length; k++) {
            if (this.screen2_values[k].templateId == document.getElementsByClassName("templateSelected")[j].nextElementSibling.id) {
              this.selectedTemplates.push(this.screen2_values[k].TemplateId);
            }
          }
        }
      }

    } else {
      this.templateCodeBool = false;
    }

  }

  validProductCode(eve) {
    var productCodes = Object.keys(this.prodTmp).concat(Object.keys(this.prodPublish))
    var valid = productCodes.indexOf(eve.target.value);
    if (valid != -1) {
      this.s1.controls["planCode"].setValue('');
      localStorage.setItem("alertContent", "Product code already taken");

      this.dialogService.addDialog(AlertComponent, {
        title: 'Name dialog',
        question1: 'Block Name ',
        question2: 'Block Type '
      }).subscribe((isConfirmed) => {
      })
    }
  }

  openModal(template) {
    this.modalRef = this.modalService.show(template, this.config);
    this.http.get("./assets/data/ProdType.json")
      .subscribe((res) => {
        this.pType = res.json().productType;

      }, err => {

        console.log(err);
      })

    this.http.get('http://localhost:4777/api/getTmpFolders')
      .subscribe(res => {

        var folder = Object.keys(res.json());
        if (folder.length > 0) {
          for (var i = 0; i < folder.length; i++) {
            var splitWith = (folder[i]).toString().split('-');
            this.num.push(parseInt(splitWith[1]));
          }

        }

      })
    this.http.get('http://localhost:4777/api/getPublishFolders')
      .subscribe(res => {
        var folder = Object.keys(res.json());
        if (folder.length > 0) {
          for (var j = 0; j < folder.length; j++) {
            var splitWith = (folder[j]).toString().split('-');
            this.num.push(parseInt(splitWith[1]));
          }

        }
      })

  }

  confirm(): void {
    this.message = 'Confirmed!';
    this.modalRef.hide();

  }

  decline(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }

  enableProductAdd() {
    this.enableProdAdd = true;
  }
  validProduct(eve) {
    if (eve.target.value != "") {
      this.validProd = true;
    } else {
      this.validProd = false;
    }
  }

  addProdType() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    

    this.http.post('http://localhost:4777/api/addProductType', ({ "productType": (<HTMLInputElement>document.getElementById("newProduct")).value }), { headers: headers })
      .subscribe(res => {
        if (res.json().output == "pass") {
          this.alertMsg((<HTMLInputElement>document.getElementById("newProduct")).value + " Added");
          this.pType = res.json().data.productType;
          (<HTMLInputElement>document.getElementById("newProduct")).value = "";

        } else {
          this.alertMsg((<HTMLInputElement>document.getElementById("newProduct")).value + " Already available, Enter diffrent type");

        }
      })

  }

  alertMsg(msg) {
    localStorage.setItem("alertContent", msg);
    this.dialogService.addDialog(AlertComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
    });
  }


  applydBoard(name, pType) {
    this.appServices.productCloneCreateTemplate = true;
    this.checkedPageNames = [];

    if (this.num.length > 0) {
      var newGenNum = Math.max(...this.num);
      newGenNum++;
      this.templateIdGen = pType + "-" + newGenNum;
    } else {
      this.templateIdGen = pType + "-" + 1;
    }


    setTimeout(() => {
      var newday = new Date();
      var de = newday.toLocaleString();
      // var selectedPageLocalStorage = JSON.parse(localStorage.getItem("sPages"));
      this.checkedPageNames.push({ "TemplateId": this.templateIdGen, "TemplateName": name, "ProductType": pType, "CreatedBy": localStorage.getItem("user"), "CreatedDate": de, "ModifiedDate": "", "SelectedTheme": this.appServices.themeSelection, "PageList": [], "Reviews": [], "review": true })
      localStorage.setItem("checkedPageNames", JSON.stringify(this.checkedPageNames));
      this.modalRef.hide();

      if (this.publishedFiles.length > 0) {
        this.screen2_values = this.publishedFiles;
        // for (var i = 0; i < this.publishedFiles.length; i++) {
        //   this.ProductList.push({ "id": i, "itemName": this.publishedFiles[i].ProductId })
        // }
        this.screen1_bool = false;
        this.screen2_bool = true;
      } else {
        localStorage.setItem("alertContent", "None of the product released");

        this.dialogService.addDialog(AlertComponent, {
          title: 'Name dialog',
          question1: 'Block Name ',
          question2: 'Block Type '
        }).subscribe((isConfirmed) => {
        })
      }
    }, 1000)

  }

    searchTemplate(list){
   
    let val = (<HTMLInputElement>document.getElementById("search")).value.trim();
  // if(val.trim()!=" " && val.trim()!=''){
    let filter = val.toUpperCase();
    for(let a=0; a<list.length; a++){
      
      if (list[a].templateId.toUpperCase().indexOf(filter) > -1 || list[a].templateId.toUpperCase().indexOf(filter) > -1 || list[a].templateName.toUpperCase().indexOf(filter) > -1 || list[a].templateName.toUpperCase().indexOf(filter) > -1 ) {
        (<HTMLInputElement>document.getElementById("rowInterface"+a)).style.display = "";
      } else {
        (<HTMLInputElement>document.getElementById("rowInterface"+a)).style.display = "none";
      }
    }
  }
 //}
}
