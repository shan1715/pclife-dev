import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { AppServices } from '../app.service';
import { DialogService } from "ng2-bootstrap-modal";
import { AlertdataComponent } from "../bootstrap-modal/alertdata/alertdata.component";
import { FileUploader } from 'ng2-file-upload';
import { AlertComponent } from "../bootstrap-modal/alert/alert.component";
@Component({
  selector: 'app-data-monitor',
  templateUrl: './data-monitor.component.html',
  styleUrls: ['./data-monitor.component.css']
})
export class DataMonitorComponent implements OnInit {
  uploaddata:any;
  downloaddata:any;
  fileName:any;
  uDate:any;
  user:any;
  date:any;
  today_date:any;
  exceldata:any=[];
  fname:any;
  alertContent:any;
  public uploader:FileUploader = new FileUploader({url:'/api/upload/'});
  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;
  constructor(private http: Http,private dialogService: DialogService) {
    this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
      if(item.isUploaded){
      
        this.fileName=item.file.name;
        this.readExcelfile(item.file.name);
        console.log(this.fileName);
      }else{
        alert("fail")
      }
     };
   }
   readExcelfile(name)
   {
     
     this.date=new Date();
 
     this.today_date =this.date.getDate().toString()+'/'+(this.date.getMonth()+1).toString()+'/'+this.date.getFullYear().toString();
     
     this.exceldata.push({
     "fileName":name,
      "user":localStorage.getItem('user'),
       "udate":this.today_date
     })
     var headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append('responseType', 'arraybuffer');
        this.http.post('/api/readexceldata', JSON.stringify(this.exceldata[0]), { headers: headers })
         .subscribe(response => {  
          this.fname=name.split('_')[1].split('.')[0];
          this.alertContent="Upload Ref.No :" + this.fname+" Data is uploaded Sucessfully ,you will be intimated once the data is processed"
          localStorage.setItem("alertContent", this.alertContent);
          this.changeStatus(name.split('_')[0]);
    
         }, err => {
           console.log("err " + err);
         })
 
   }


   changeStatus(fname)
   {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('responseType', 'arraybuffer');
    var productName=[];
    productName.push({
      "productName":fname
      })
    this.http.post('/api/readDataUploadFiles', JSON.stringify(productName[0]), { headers: headers })
    .subscribe(response => {
      
    this.uploaddata=response.json();
  
    }, err => {
      console.log(err);
    })
   
   }
  ngOnInit() {
    
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('responseType', 'arraybuffer');
    var productName=[];
    productName.push({
      "productName":localStorage.getItem('downloadProduct')
      })
    this.http.post('/api/readDataUploadFiles', JSON.stringify(productName[0]), { headers: headers })
    .subscribe(response => {
      
    this.uploaddata=response.json();
  
    }, err => {
      console.log(err);
    })
   
    this.http.post('/api/readdataDownloadJsonFiles', JSON.stringify(productName[0]), { headers: headers })
    .subscribe(res => {
      
    this.downloaddata=res.json();
   
    }, err => {
      console.log(err);
    })

  }
  showProgress(data)
  {
    
    localStorage.setItem("uploaddata", JSON.stringify(data));
    this.dialogService.addDialog(AlertdataComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {

      // localStorage.setItem("uploaddata", data);
    });
  }
  showError(data)
  {
    
    localStorage.setItem("uploaddata", JSON.stringify(data));
    this.dialogService.addDialog(AlertdataComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {

      // localStorage.setItem("uploaddata", data);
    });
  }
  dowloadtemplate(pname,name)
  {
     this.fileName=pname+'_'+name+'.xlsx';
      var a = document.createElement('a');
      a.href = "assets/dataUpload/"+this.fileName;
          a.download = this.fileName;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
      
  }

}
