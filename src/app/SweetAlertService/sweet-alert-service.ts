import { Injectable } from '@angular/core';
import swal from 'sweetalert2'
import { Router } from '@angular/router';
@Injectable()
export class SweetAlertService {

  constructor( private router: Router) { }
  successMsgWithTitle(title: string, msg: string) {
    swal({
      title: title,
      text: msg,
      type: 'success',
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#5cb85c'
    });
  }

  successMsg(msg: string) {
    swal({
      title: 'Success',
      text: msg,
      type: 'success',
      confirmButtonColor: '#5cb85c'

    });
  }

  successMsgwithLink(msg: string,linkName:string) {
    swal({
      title: 'Success',
      text: msg,
      type: 'success',
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#5cb85c'
    });
  }

  errorMsgWithAnimation(msg: string) {
    swal({
      title: 'Error',
      text: msg,
      type: 'error',
      allowOutsideClick: false,
      allowEscapeKey: false,
      animation: false,
      customClass: 'animated tada',
      confirmButtonColor: '#ac2925'
    });
  }

  errorMsg(msg: string) {
    swal({
      title: 'Error',
      text: msg,
      type: 'error',
      allowOutsideClick: false,
      allowEscapeKey: false,
      animation: false,
      confirmButtonColor: '#ac2925'
    });
  }

  errorMsgwithLink(msg: string,linkName:string) {
    swal({
      title: 'Error',
      text: msg,
      type: 'error',
      allowOutsideClick: false,
      allowEscapeKey: false,
      animation: false,
      confirmButtonColor: '#ac2925'
    });
  }

  warningMsg(msg: string, successMsg: string, errMsg: string) {
    swal({
      title: 'Warning',
      text: msg,
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-warning',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false,
      allowEscapeKey: false,
      buttonsStyling: false,
    }).then(function () {
      swal({
        title: 'Deleted!',
        text: successMsg,
        type: 'success',
        allowOutsideClick: false,
        allowEscapeKey: false,
        confirmButtonColor: '#5cb85c'
      }
      )
    }, function (dismiss) {
      // dismiss can be 'cancel', 'overlay',
      // 'close', and 'timer'
      if (dismiss === 'cancel') {
        swal({
          title: 'Cancelled',
          text: errMsg,
          type: 'error',
          allowOutsideClick: false,
          allowEscapeKey: false,
          confirmButtonColor: '#5cb85c'
        }
        )
      }
    })
  }

  warningMsgwithLink(msg: string,linkName:string) {
    swal({
      title: 'Warning',
      text: msg,
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-warning',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false,
      allowEscapeKey: false,
      buttonsStyling: false,
    });
  }
}
