import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AppServices } from '../app.service';
import { UserComponent } from '../bootstrap-modal/user/user.component';
import { DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';

@Component({
  selector: 'administrator-group',
  templateUrl: './administrator-group.component.html',
  styleUrls: ['./administrator-group.component.css']
})
export class AdministratorGroupComponent implements OnInit {
  
  datas: any;
  activities: any = []; 
  selectedRights: any = [];
  showT:boolean=false;

      dropdownList = [];
    selectedItems = [];
    dropdownSettings = {}; 
 

  constructor(private http: Http, private appServices: AppServices, private dialogService: DialogService,public router:Router ) {
    
  }



  ngOnInit() {
   this.groupNames();
 
    }

  onItemSelect(item:any){
        
    }
    OnItemDeSelect(item:any){
        
    }
    onSelectAll(items: any){
    }
    onDeSelectAll(items: any){
    }

  groupNames(){
     this.http.get('assets/data/groups.json')
      .subscribe(res => this.datas = res.json());


  }
    showTemp(){
      this.showT=true;
    }

  groupList(eve) {
    this.selectedRights=[];
    for (var i = 0; i < this.datas.length; i++) {
      if(this.datas[i].id==eve.target.value){
for(var j=0;j< Object.keys(this.datas[i].selectedRights).length;j++){
	if(Object.keys(this.datas[i].selectedRights)[j].length>0){
	 for(var k=0;k<this.datas[i].selectedRights[Object.keys(this.datas[i].selectedRights)[j]].length;k++){
		this.selectedRights.push(this.datas[i].selectedRights[Object.keys(this.datas[i].selectedRights)[j]][k])

}

}

}
      }
   }


  }


  // previlageSelect(eve) {
  //   eve.parentElement.getElementsByTagName("button")[0].style = "background-color: limegreen;float:right;"
  //   for (var i = 0; i < this.previlages.length; i++) {
  //     if (eve.value == this.previlages[i].priname) {
  //       this.activities = this.previlages[i].activity;
  //     }
  //   }

  // }

  createUser() {

    this.appServices.createuser=true;
    this.appServices.edituser=false;
    this.appServices.creategroup=false;
    this.appServices.editgroup=false;
    this.dialogService.addDialog(UserComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
   
    });
  }

  editUser(){
    this.appServices.createuser=false;
    this.appServices.edituser=true;
        this.appServices.creategroup=false;
        this.appServices.editgroup=false;

   
    this.dialogService.addDialog(UserComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
    });
  }

  savePrevilage(preId) {
    for (var i = 0; i < this.datas.length; i++) {

      if (preId.value == this.datas[i].id) {

        for (var j = 0; j < this.datas[i].previlage.length; j++) {
          this.datas[i].previlage[j].selected = (<HTMLInputElement>document.getElementsByClassName("selectedItems")[j]).checked;

        }


      }
    }
       var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post('http://localhost:4777/api/postGroup', this.datas, { headers: headers })
      .subscribe(res => {
        setTimeout(() => {
        }, 1500);

      }, err => {
      })


  }



  createGroup(){
    this.appServices.creategroup=true;
        this.appServices.editgroup=false;
        this.appServices.createuser=false;
    this.appServices.edituser=false;

         this.dialogService.addDialog(UserComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
        this.groupNames();
    });
  }

  editGroup(){
    this.appServices.editgroup=true;
        this.appServices.creategroup=false;
this.appServices.createuser=false;
    this.appServices.edituser=false;
         this.dialogService.addDialog(UserComponent, {
      title: 'Name dialog',
      question1: 'Block Name ',
      question2: 'Block Type '

    }).subscribe((isConfirmed) => {
        this.groupNames();

    });
  }

  logout() {



      this.router.navigate(['']);

    

  }

}

