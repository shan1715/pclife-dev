import { Component, OnInit, OnDestroy, ElementRef, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DragulaService } from 'ng2-dragula';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DialogService } from "ng2-bootstrap-modal";
import { PrompComponent } from "../bootstrap-modal/Modaldialog/promp.component";
import { MasterFieldDialogComponent } from "../bootstrap-modal/master-field-dialog/master-field-dialog.component";
import { AppServices } from '../app.service';
import { Observable } from "rxjs";


@Component({
  selector: 'master-content',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.css']
})
export class MasterComponent implements OnInit {

  masterName: any = [];
  showForm: boolean = false;
  showTable: boolean = false;
  display_data: any = [];
  showRow: boolean = false;
  showAddBtn: boolean = false;
  private headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
  private options = new RequestOptions({ headers: this.headers });

  public constructor(private acrivatedRouter: ActivatedRoute,private dialogService: DialogService, private dragulaService: DragulaService, private router: Router, private http: Http, private appService: AppServices) {

  }



  ngOnInit() {

    this.masterName = JSON.parse(localStorage.getItem("seletedMaster"));
    if (this.masterName[0].dataEntryModal == 'Tabular') {
      this.showTable = true;
    } else if (this.masterName[0].dataEntryModal == 'Form') {
      this.showForm = true;
    }
    if (this.masterName[0].fieldContent) {
      if (this.masterName[0].fieldContent.length > 0) {
        this.display_data = this.masterName;
      }

    }

    localStorage.getItem("pageMode");
    if (localStorage.getItem("pageMode") == "1") {
      
      this.showAddBtn = true;
      this.showRow = false;
      //document.getElementById("nRecord").style.display = "none";
    } else if (localStorage.getItem("pageMode") == "2") {
      document.getElementById("sFormat").style.display = "none";
      this.showRow = true;
      this.showAddBtn = false;

      setTimeout(() => {
        this.dragulaService.find("second-bag").drake.destroy();

        // this.disableEditing();

        this.http.get('http://10.200.104.150:8080/productConfig/ProductBenefits/GnluCountryMaster')
          .subscribe(res => {
            console.log(1);
            
            for(var j=0;j<res.json().length;j++){
		 this.display_data.push({"fieldContent":[{"item_name":Object.keys(res.json()[0])[0],"item_type":"text","item_value":res.json()[j].vCountryCode},{"item_name":Object.keys(res.json()[0])[1],"item_type":"text","item_value":res.json()[j].vCountryDesc},{"item_name":Object.keys(res.json()[0])[2],"item_type":"text","item_value":res.json()[2].vStatus}]})
}

setTimeout(() => {
  console.log(1);
   
    for (var i = 1; i < document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody").length; i++) {
        for (var j = 0; j < document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[i].getElementsByTagName("tr").length; j++) {
          (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[i].getElementsByTagName("tr")[j].children[0].children[0]).style.display = "none";
          (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[i].getElementsByTagName("tr")[j].getElementsByClassName("td_value")[0].children[0]).readOnly = true;
        }
        (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[i].children[3]).style.display = "inherit";
        for (var k = 0; k < document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[i].getElementsByClassName("moTable").length; k++) {
          (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[i].getElementsByClassName("moTable")[k]).style.height = "44px";
        }
      }

      document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[0].style.height= "44px";

}, err => {
            console.log("err " + err);
          })


          }, err => {
            console.log("err " + err);
          })

      }, 1000)

    }


  }

  addField(e) {

    localStorage.setItem("id", e.parentElement.parentElement.id);
    localStorage.setItem("className", e.parentElement.className);

    this.dialogService.addDialog(MasterFieldDialogComponent, {
      title: 'Name dialog',
      fquestion1: 'Item Name ',
      fquestion2: 'Item Type '

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {

        this.fieldInsert();

      } else {

      }
    })





  }

  fieldInsert() {

    var s = JSON.parse(localStorage.getItem("masterOutput"));
    if (s[0].field_type == "Date") {
      s[0].item_type = "date";
    }
    if (this.showForm) {
      if (s[0].item_type == "text") {

        this.display_data.push({ "fieldContent": s });
      } else if (s[0].item_type == "select") {
        this.display_data.push({ "fieldContent": s });

      } else if (s[0].item_type == "date") {
        this.display_data.push({ "fieldContent": s });

      }
    } else if (this.showTable) {
      if (s[0].item_type == "text") {
        if (s[0].field_type == "Number") {
          this.display_data.push({ "fieldContent": s });

        } else if (s[0].field_type == "Date") {
          this.display_data.push({ "fieldContent": s });

        } else if (s[0].field_type == "String") {
          this.display_data.push({ "fieldContent": s });

        }

      } else if (s[0].item_type == "select") {
        s[0].item_type = "select-one";
        this.display_data.push({ "fieldContent": s });

      } else if (s[0].item_type == "date") {
        this.display_data.push({ "fieldContent": s });

      }
    }



  }

  saveFormat() {
    if (this.showForm) {

      var df = [];
      df.push({ "MasterId": this.masterName[0].MasterId, "MasterName": this.masterName[0].MasterName, "MasterMappingCode": this.masterName[0].MasterMappingCode, "dataEntryModal": this.masterName[0].dataEntryModal, "CreatedBy": this.masterName[0].CreatedBy, "CreatedDate": this.masterName[0].CreatedDate, "fieldContent": [] })
      for (var i = 0; i < document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set").length; i++) {
        if (document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByClassName("field_1")[0].children[0].tagName != "SELECT") {
          df[0].fieldContent.push({ "field_parentwpercentage": document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].parentElement.style.width.substring(0, document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].parentElement.style.width.length - 1), "item_name": (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].children[0]).value, "item_value": "", "item_type": (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByClassName("field_1")[0].children[0]).type })
        } else {
          if ((<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByClassName("field_1")[0].children[0]).type == "select-one") {
            var d = "select";
          }
          df[0].fieldContent.push({ "field_parentwpercentage": document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].parentElement.style.width.substring(0, document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].parentElement.style.width.length - 1), "item_name": (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].children[0]).value, "item_value": "", "item_type": d, "optionObj": [] })
          for (var j = 1; j < document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByClassName("field_1")[0].children[0].getElementsByTagName("option").length; j++) {
            df[0].fieldContent[i].optionObj.push({ "optionValue": document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByClassName("field_1")[0].children[0].getElementsByTagName("option")[j].value, "optionLabel": document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByClassName("field_1")[0].children[0].getElementsByTagName("option")[j].textContent })
          }
        }



      }
    } else if (this.showTable) {
      console.log(1);
      var df = [];
      df.push({ "MasterId": this.masterName[0].MasterId, "MasterName": this.masterName[0].MasterName, "MasterMappingCode": this.masterName[0].MasterMappingCode, "dataEntryModal": this.masterName[0].dataEntryModal, "CreatedBy": this.masterName[0].CreatedBy, "CreatedDate": this.masterName[0].CreatedDate, "fieldContent": [] })
      for (var i = 0; i < document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1").length; i++) {
        if (document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].children[1].getElementsByClassName("td_value")[0].children[0].tagName != "SELECT") {

          df[0].fieldContent.push({ "item_name": (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].getElementsByTagName("input")[0]).value, "item_value": "", "item_type": (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].children[1].getElementsByClassName("td_value")[0].children[0]).type })

        } else {

          df[0].fieldContent.push({ "item_name": (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].getElementsByTagName("input")[0]).value, "item_value": "", "item_type": (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].children[1].getElementsByClassName("td_value")[0].children[0]).type, "optionObj": [] })
          for (var j = 1; j < document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].children[1].getElementsByClassName("td_value")[0].children[0].getElementsByTagName("option").length; j++) {

            df[0].fieldContent[i].optionObj.push({ "optionValue": document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].children[1].getElementsByClassName("td_value")[0].children[0].getElementsByTagName("option")[j].value, "optionLabel": document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].children[1].getElementsByClassName("td_value")[0].children[0].getElementsByTagName("option")[j].textContent })




          }
        }
      }
    }

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');


    this.http.post('/api/masterCreate', df, { headers: headers })
      .subscribe(res => {


        //this.masterPush.push({ "MasterId": res.json()[0].MasterId, "MasterName": res.json()[0].MasterName, "dataEntryModal": res.json()[0].dataEntryModal, "CreatedBy": res.json()[0].CreatedBy,  "CreatedDate": res.json()[0].CreatedDate });

      }, err => {
        console.log("err " + err);
      })
  }

  editpen(eve) {
    eve.target.previousElementSibling.removeAttribute("readonly");
    eve.target.previousElementSibling.style.backgroundColor = "limegreen";

  }

  disableEdit(eve) {
    eve.target.setAttribute("readonly", true);
    eve.target.style.backgroundColor = "white";
  }

  delfield(eve) {
    var s = [];
    // s.push({
    //   "accordionName": eve.target.parentElement.parentElement.parentElement.parentElement.previousElementSibling.getElementsByTagName("a")[0].firstElementChild.value,
    //   "labelName": eve.target.previousElementSibling.previousElementSibling.value,
    //   "tagName": eve.target.nextElementSibling.firstElementChild.type
    // })
    // console.log(s);
    var node = eve.target.parentNode;
    node.parentNode.removeChild(node);


  }

  deltablefield(eve) {
    // var v = [];

    // v.push({
    //   "accordionName": eve.target.parentElement.offsetParent.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('input')[0].value,
    //   "labelName": eve.target.parentElement.getElementsByTagName("input")[0].value

    // })
    // console.log(v);
    var node = eve.target.parentNode.parentNode;
    node.parentNode.removeChild(node);
  }

  incSize(eve) {

    if (eve.target.parentElement.parentElement.parentElement.style.width == "23%") {
      eve.target.parentElement.parentElement.parentElement.style.width = "55%";
      eve.target.parentElement.parentElement.parentElement.children[0].style.width = "93%";
      eve.target.parentElement.parentElement.getElementsByClassName("field_1")[0].children[0].style.width = "96%";
    } else if (eve.target.parentElement.parentElement.parentElement.style.width == "55%") {
      eve.target.parentElement.parentElement.parentElement.style.width = "89%";
    } else if (eve.target.parentElement.parentElement.parentElement.style.width == "89%") {
      eve.target.parentElement.parentElement.parentElement.style.width = "55%";
    }

  }

  decSize(eve) {


    if (eve.target.parentElement.parentElement.parentElement.style.width == "89%") {
      eve.target.parentElement.parentElement.parentElement.style.width = "55%";
      // eve.target.parentElement.parentElement.parentElement.children[0].style.width="93%";
      // eve.target.parentElement.parentElement.parentElement.getElementsByClassName("field_1")[0].children[0].style.width="96%";
    } else if (eve.target.parentElement.parentElement.parentElement.style.width == "55%") {
      eve.target.parentElement.parentElement.parentElement.style.width = "23%";
    }

  }

  // disableEditing() {

  //   if (this.showForm) {
  //     for (var i = 0; i < document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set").length; i++) {
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].parentElement.style.border = "";
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].parentElement.style.resize = "";
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].parentElement.style.margin = "0% 5%";
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByTagName("img")[0].style.visibility = "hidden";
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByTagName("img")[1].style.visibility = "hidden";
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByTagName("button")[0].style.visibility = "hidden";
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].getElementsByTagName("button")[1].style.visibility = "hidden";
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].removeAttribute("style:hover");
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("field_set")[i].parentElement.style.margin = "0% 0%";
  //     }
  //   } else if (this.showTable) {
  //     for (var i = 0; i < document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1").length; i++) {
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].getElementsByTagName("img")[0].style.visibility = "hidden";
  //       document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].getElementsByTagName("img")[1].style.visibility = "hidden";
  //     }
  //   }
  // }

  addNewRecord(e) {
    this.display_data.push(this.display_data[0]);
    
    setTimeout(() => {
      // for(var i=0;i<document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1").length;i++){
      //      document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].getElementsByTagName("img")[0].style.visibility = "hidden";
      //      document.getElementsByClassName("panel-body")[0].getElementsByClassName("tc1")[i].getElementsByTagName("img")[1].style.visibility = "hidden";
      //    }
      for (var i = 1; i < document.getElementsByClassName("panel-body")[0].getElementsByClassName("disp").length; i++) {
        for (var j = 0; j < document.getElementsByClassName("panel-body")[0].getElementsByClassName("disp")[i].getElementsByClassName("tc1").length; j++) {
          (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("disp")[i].getElementsByClassName("tc1")[j].children[0]).style.display = "none";
        }
        for (var k = 0; k < document.getElementsByClassName("panel-body")[0].getElementsByClassName("disp")[i].getElementsByClassName("moTable").length; k++) {
          (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByClassName("disp")[i].getElementsByClassName("moTable")[k]).style.height = "44px";
        }
      }
    var btnCount = this.display_data.length-1;
    (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[btnCount].children[4]).style.display = "inherit";

    }, 500)

  }


  saveValues(eve) {
   
     var output = {};
    // for (var i = 1; i < document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody").length; i++) {
    //   for (var j = 0; j < document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[i].getElementsByTagName("tr").length; j++) {

    //     output[(<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[i].getElementsByTagName("tr")[j].children[0].children[0].children[0]).value] = (<HTMLInputElement>document.getElementsByClassName("panel-body")[0].getElementsByTagName("tbody")[i].getElementsByTagName("tr")[j].getElementsByClassName("td_value")[0].children[0]).value;

    //   }
    //   console.log(output)
    // }

      for (var j = 0; j < eve.target.parentElement.getElementsByTagName("tr").length; j++) {

        output[(<HTMLInputElement>eve.target.parentElement.getElementsByTagName("tr")[j].children[0].children[0].children[0]).value] = (<HTMLInputElement>eve.target.parentElement.getElementsByTagName("tr")[j].getElementsByClassName("td_value")[0].children[0]).value;

      }
      console.log(output)
      this.http.post('http://10.200.104.150:8080/productConfig/ProductBenefits/GnluCountryMaster', output, this.options)
        .subscribe(res => {
          setTimeout(() => {
            alert("Datas Saved Successfully");
          }, 1500);

        }, err => {
          console.log("err " + err);
        })
    }

    editValues(eve){
           for (var j = 0; j < eve.target.parentElement.getElementsByTagName("tr").length; j++) {
              eve.target.parentElement.getElementsByTagName("tr")[j].getElementsByClassName("td_value")[0].children[0].readOnly = false;
           }
           eve.target.parentElement.children[4].style.display = "inherit";
    }


}

